import { Component, OnInit, AfterViewInit, OnChanges,ViewChild, ViewEncapsulation  } from '@angular/core';
import { DatePipe, CurrencyPipe,DecimalPipe } from '@angular/common';
import { APIService } from '../../../api.service';
import { Helpers} from "../../../helpers";
import { AppSetting } from "../../../settings";
import { SystemService } from "../../../system.service";
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { AppMessage } from "../../../message";
import { jqxGridComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxgrid';
import { jqxWindowComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxwindow'
import { jqxValidatorComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxvalidator';
import { jqxDateTimeInputComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxdatetimeinput';
import { PartnerModule } from '../../../widgets/partner/partner.module';
import { ItemGroupModule } from '../../../widgets/item-group/item-group.module';
import { ItemsModule } from '../../../widgets/items/items.module';
import { jqxDropDownListComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxdropdownlist';
import { NoneItemSetupModule } from '../../../widgets/ptg/general/none-item-setup/none-item-setup.module';
import { VatProductPtgModule } from '../../../widgets/ptg/vat/product/ptg-vat-product.module';
import * as cloneDeep from 'lodash/cloneDeep';

declare var $:any;
declare var Chart:any;
declare var document:any;
declare var toastr:any;


@Component({
  selector: 'app-purchases',
  templateUrl: './purchases.component.html',
})
export class PurchasesComponent implements OnInit, AfterViewInit, OnChanges {
    @ViewChild('grid') grid: jqxGridComponent;
    @ViewChild('lineGrid') lineGrid: jqxGridComponent;
    @ViewChild('customerCbx') customerCbx: PartnerModule;
    @ViewChild('detailWindow') detailWindow: jqxWindowComponent;
    @ViewChild('validator') validator: jqxValidatorComponent;
    @ViewChild('lineValidator') lineValidator: jqxValidatorComponent;
    @ViewChild('itemGroupCbx') itemGroupCbx: ItemGroupModule;
    @ViewChild('itemsCbx') itemsCbx: ItemsModule;
    @ViewChild('vatProdPtgCbx') vatProdPtgCbx: VatProductPtgModule;
    @ViewChild('dueDateTxt') dueDateTxt: jqxDateTimeInputComponent;
    @ViewChild('dateTxt') dateTxt: jqxDateTimeInputComponent;
    @ViewChild('item_typeCbx') item_typeCbx: jqxDropDownListComponent;
    @ViewChild('noneItemSetupCbx') noneItemSetupCbx: NoneItemSetupModule;

    theme = AppSetting.THEME;
    partner_id;
    crmtype;
    setting;
    myActivity = false;
    constructor(
        private api: APIService,
        private router: Router,
        private route: ActivatedRoute,
        private sys: SystemService,
    ) {
        
    }

    render(){
        console.log('render')
    }
    ngOnChanges() {
        console.log('ngOnChanges')
    }

    ngOnInit() {
        let that = this;
        let types = ['invoices','credit-notes','orders']
        console.log('ngOnInit')
        let settings =[{
                api:{
                    create: {
                        type:2,
                        url: AppSetting.PURCHASE_COMMIT_URL
                    },
                    list: {
                        type:2,
                        url: AppSetting.PURCHASES_LIST_URL
                    },
                },
                'title' : 'Purchase Invoices',
                'subtitle' : 'Purchase Invoices'
            },{
                api:{
                    create: {
                        type:3,
                        url: AppSetting.PURCHASE_COMMIT_URL
                    },
                    list: {
                        type:3,
                        url: AppSetting.PURCHASES_LIST_URL
                    },
                },
                'title' : 'Purchase Credit Notes',
                'subtitle' : 'Purchase Credit Notes'
            },{
                api:{
                    create: {
                        type:1,
                        url: AppSetting.PURCHASE_COMMIT_URL,
                    },
                    list: {
                        type:1,
                        url: AppSetting.PURCHASES_LIST_URL
                    },
                },
                'title' : 'Purchase Orders',
                'subtitle' : 'Purchase Order'
            }];
        this.route.data.subscribe(data => {
            that.actived_items = data.items || []
            that.router_params = data;
            that.isMy = data.my;
            that.prefixmy = data.my?'my':'';
            that.is360 = data.is360;
            that.prefix360 = data.is360?'360/':'';
        });
        this.route.params.subscribe(
            params => {
                let urlParams = that.route.snapshot.params;
                var index = types.indexOf(urlParams.type);
                that.setting = settings[index];
                that.partner_id = urlParams.id;
                 that.type = params.type;
                that.crmtype = urlParams.crmtype;
                that.myActivity = urlParams.my;
                that.backToList();
                that.loadDataAdapter()
                that.initGrid();
                that.initAnalytics();
            }
        );
    }
    prefix360
    prefixmy
    is360
    router_params
    type
    isMy
    actived_items = [];
    showDetail(id){
        let that = this;
        that.api.get({
            url: AppSetting.PURCHASE_DETAIL_URL + id
        }).done(function(res){
            console.log(res,'SALE_DETAIL')
            if(res.data){
                // that.item_detail = data.data
                that.setData(res.data)
                that.showCreateForm()
                
            }
        })
    }
    setData(data){
        let that = this;
        data.date = new Date(new DatePipe("en-US").transform(data.date * 1000, 'yyyy-MM-dd'));
        data.due_date = new Date(new DatePipe("en-US").transform(data.due_date * 1000, 'yyyy-MM-dd'));
        that.itemDetail = data;
        that.lines = data.purchase_line;
        that.lines.push(cloneDeep(this.empty_line));
		that.itemDetail.editable = false;
        if(!that.itemDetail.id){
            that.itemDetail.editable = true;
        }
        if(that.itemDetail.type == 1 && that.itemDetail.status==1){
            that.itemDetail.editable = true;
        }
        
    }
    dataAdapter;
    entrySource;
    loadDataAdapter(){
        let that = this;
        let conditions = [
            ['type','=',that.setting.api.list.type],
            ['status',1]
        ]
        if(that.partner_id){
            conditions.push(['business_partner_id','=',that.partner_id])
        }
        if(that.myActivity){
            let user = that.api.getUserInfo();
            conditions.push(['created_by','=',user.id])
        }
        that.entrySource = Helpers.Source({
            datatype: "json",
            type: 'POST',
            url: AppSetting.COMMON_BINDING_URL,
            data: {
                table:'purchase_header',
                conditions: conditions,
                orders:{'id':'desc'}
            },
            datafields: [
                { name: 'id', type: 'int' },
                { name: 'document_no', type: 'int' },
                { name: 'partner_name', type: 'string' },
                { name: 'date', type: 'string' },
                { name: 'due_date', type: 'string' },
                { name: 'discount', type: 'number' },
                { name: 'total_with_tax', type: 'number' },
                { name: 'status', type: 'number' },
                // { name: 'partner_name', type: 'string',map:'partner>name' },
            ],
            addrow: function (rowid, rowdata, position, commit) {
                // synchronize with the server - send insert command
                // call commit with parameter true if the synchronization with the server is successful 
                //and with parameter false if the synchronization failed.
                // you can pass additional argument to the commit callback which represents the new ID if it is generated from a DB.
                commit(true);
            },
            filter: function() {
                // update the grid and send a request to the server.
                that.grid.updatebounddata('filter');
            },
            sort: function() {
                // update the grid and send a request to the server.
                that.grid.updatebounddata('sort');
            },
            id: 'id',
            root: 'data',
            async: false
        })
        that.dataAdapter = new $.jqx.dataAdapter(that.entrySource, {
            beforeLoadComplete: function (records) {
                for (var i = 0; i < records.length; i++) {
                    var row = records[i]
                    if(row){
                        records[i].date = new DatePipe("en-US").transform(row.date * 1000, 'yyyy-MM-dd');
                        records[i].due_date = new DatePipe("en-US").transform(row.due_date * 1000, 'yyyy-MM-dd');
                    }
                }
                return records;
            }
        });
    }
    gridSetting
    initGrid(){
        let that = this;
        let columns = [
                { 
                    text: 'ID', dataField: 'id', width: 80 ,hidden: true
                },{ 
                    text: 'No', dataField: 'document_no', width: 100,
                    renderer: function (text, align, columnsheight) {
                        return '<div class="column-header-two-line">'+Helpers.translate.get('No.')+'</div>';
                    },
                    cellsalign: 'right', 
                },{ 
                    text: 'Partner', dataField: 'partner_name',
                    renderer: function (text, align, columnsheight) {
                        return '<div class="column-header-two-line">'+Helpers.translate.get('Supplier')+'</div>';
                    },
                },{ 
                    text: 'Date', dataField: 'date', width: 100 ,
                    renderer: function (text, align, columnsheight) {
                        return '<div class="column-header-two-line">'+Helpers.translate.get('Date')+'</div>';
                    },
                    // cellsformat: 'yyyy-MM-dd',
                    // filtertype:'range'
                },{  
                    text: 'Due Date', dataField: 'due_date', width: 100 ,
                    renderer: function (text, align, columnsheight) {
                        return '<div class="column-header-two-line">'+Helpers.translate.get('Due Date')+'</div>';
                    },
                    // cellsformat: 'yyyy-MM-dd',
                    // filtertype:'range'
                },{ 
                    text: 'Discount', dataField: 'discount', width: 90, 
                    renderer: function (text, align, columnsheight) {
                        return '<div class="column-header-two-line">'+Helpers.translate.get('Discount')+'</div>';
                    },
                    // aggregates: ["sum"], 
                    cellsalign: 'right', 
                },{ 
                    text: 'Total', dataField: 'total_with_tax', width: 120, 
                    renderer: function (text, align, columnsheight) {
                        return '<div class="column-header-two-line">'+Helpers.translate.get('Total')+'</div>';
                    },
                    // aggregates: ["sum"], 
                    cellsalign: 'right', 
                    cellsformat: 'd'
                },{
                    text: 'Status', dataField: 'status', width: 80, 
                    renderer: function (text, align, columnsheight) {
                        return '<div class="column-header-two-line">'+Helpers.translate.get('Status')+'</div>';
                    },
                    cellsrenderer: (row: number, column: string, value: any) =>{
                        let statuss = ['','Open','Archived','Closed']
                        let status = statuss[value] || '';
                        return [
                        '<div style="padding:5px;line-height: 22px;">',
                        status,
                        '</div>'
                        ].join('')
                    },
                    // aggregates: ["sum"], 
                }
            ]
        let onCellClick = function (event) {
            // event arguments.
            var args = event.args;
            // row's bound index.
            var rowBoundIndex = args.rowindex;
            // row's visible index.
            var rowVisibleIndex = args.visibleindex;
            // right click.
            var rightclick = args.rightclick; 
            // original event.
            var ev = args.originalEvent;
            // column index.
            var columnindex = args.columnindex;
            // column data field.
            var dataField = args.datafield;
            // cell value
            var value = args.value;
            if(args && args.row){
                var dataRow = args.row.bounddata
                console.log(dataRow,'dataRow')
                if(dataRow) that.showDetail(dataRow.id)
            }
        }
        if(that.gridSetting){
            that.grid.setOptions({source:that.dataAdapter});
            return;
        }
        that.gridSetting = {
            columns: columns,
            localization: Helpers.getLocalizatio(Helpers.translate.activeLang),
            source: that.dataAdapter,
            onCellClick: onCellClick,
            rendergridrows: (params: any): any[] => {
                return that.dataAdapter.records;
            },
            rendertoolbar: (toolbar: any): void => {
                var me = this;
                var container = $("<div style='margin: 5px;'></div>");
                toolbar.append(container);
                container.append('<input id="addrowbutton" type="button" value="New Entry"/>');
                $("#addrowbutton").jqxButton({theme: 'office'});
                
                // create new row.
                $("#addrowbutton").on('click', function () {
                    console.log('Clicked Add Entry')
                    that.resetCreateForm();
                    that.showCreateForm();
                });
            }
        }
    
    }
    clickAdd(){
        this.resetCreateForm();
        this.showCreateForm();
    }
    tax_data;
    product_data;
    sync;
    itemGroupSetting
    itemTypeSetting
    initItemGroupCbx(){
        let that = this;
        if(that.itemGroupSetting) return;
        that.itemTypeSetting = {
            source: [
                {id:1,label: Helpers.translate.get('Item')},
                {id:2,label: Helpers.translate.get('Charge (Item)')},
            ],
            width: '100%',
            height: 30,
            promptText: Helpers.translate.get("Please Choose"),
            displayMember: 'label',
            valueMember: 'id',
            onChange: function(event){
                var args = event.args;
                if(args && args.item && args.item.originalItem){
                    var item = args.item.originalItem;
                    if(that.lineDetail){
                        that.lineDetail.item_type = item.id;
                        that.setLineData({
                            id: 0,
                            code:'',
                            item_type:that.lineDetail.item_type,
                            name:'',
                            price:0,
                            discount:0,
                            qty:1,
                            vat:0
                        });
                    }
                }
            },
        };
        that.itemGroupSetting = {
            onOpen: ()=>{
                that.pending = true;
            },
            onClose: ()=>{
                that.pending = false;
                $('.jqx-grid-validation,.jqx-grid-validation-arrow-up').remove()
            },
            onChange: function(event){
                console.log('ONCHANGE')
                let item = that.itemGroupCbx.getSelectedItem();
                if(that.detailWindow.isOpen()){
                    if(item){
                        // that.editingItem.gen_product_ptg = item;
                        // that.editingItem.item_group_id = item.id
                        if(that.itemsCbx){
                            that.itemsCbx.setValue(undefined)
                            that.itemsCbx.filterByGroup(item.id)
                        }
                    }
                    // let windowId = that.validator.host.attr('id')
                    // let windowId = that.validator.host.attr('id')
                    // $('#'+windowId+ ' .cbx_item_group_id').change()

                }else{
                    // if(that.cellEditor) that.cellEditor.val(item.description)
                    // that.currentEditData.item_group_id = item.id
                }
                
            },
        }
    }
    noneItemSetupSetting
    initNoneItemSetupCbx(){
        let that = this;
        if(that.noneItemSetupSetting) return;
        that.noneItemSetupSetting = {
                onOpen: ()=>{
                    that.pending = true;
                },
                onClose: ()=>{
                    that.pending = false;
                    $('.jqx-grid-validation,.jqx-grid-validation-arrow-up').remove()
                },
                onChange: function(args){
                    let item = that.noneItemSetupCbx.getSelectedItem();
                    if(that.detailWindow.isOpen()){
                        if(item){
                            // that.editingItem.pay_acct = item;
                            that.lineDetail.code = item.code
                            that.lineDetail.name = item.description;
                            if(that.vatProdPtgCbx){
                                that.vatProdPtgCbx.setCode(item.def_vat_product_ptg_code,true);
                            }
                        }
                        let windowId = that.validator.host.attr('id')
                        $('#'+windowId+ ' .cbx_line_code').blur()
                    }else{
                        // if(that.cellEditor) that.cellEditor.val(item.acct_code)
                    }

                },
                
            }
    }
    itemsSetting
    initItemCbx(){
        let that = this;
        if(that.itemsSetting) return;
        that.itemsSetting = {
            onOpen: ()=>{
                that.pending = true;
            },
            onClose: ()=>{
                that.pending = false;
                $('.jqx-grid-validation,.jqx-grid-validation-arrow-up').remove()
            },
            onChange: function(event){
                console.log('ONCHANGE')
                let item = that.itemsCbx.getSelectedItem();
                if(that.detailWindow.isOpen()){
                    if(item){
                        console.log(item,'item')
                        that.lineDetail.name = item.description;
                        that.lineDetail.product_code = item.product_code;
                        that.lineDetail.unit_price     = item.purchase_price;
                        that.lineDetail.price = item.purchase_price;
                        that.lineDetail.purchase_tax_group_id = item.purchase_tax_group_id;
                        // that.lineDetail.vat_product_ptg_code = item.purchase_tax_group_id;
                        if(that.vatProdPtgCbx){
                            that.vatProdPtgCbx.setValue(item.purchase_tax_group_id,true);
                        }
                        // that.editingItem.gen_product_ptg = item;
                        // that.editingItem.item_group_id = item.id
                    }
                    let windowId = that.lineValidator.host.attr('id')
                    $('#'+windowId+ ' .cbx_line_product_id').blur()

                }else{
                    // if(that.cellEditor) that.cellEditor.val(item.description)
                    // that.currentEditData.item_group_id = item.id
                }
                
            },
        }
    }
    vatProdPtgSetting
    initVatProdPtgCbx(){
        let that = this;
        if(that.vatProdPtgSetting) return;
        that.vatProdPtgSetting = {
            onOpen: ()=>{
                that.pending = true;
            },
            onClose: ()=>{
                that.pending = false;
                $('.jqx-grid-validation,.jqx-grid-validation-arrow-up').remove()
            },
            onChange: function(event){
                let item = that.vatProdPtgCbx.getSelectedItem();
                if(that.detailWindow.isOpen()){
                    if(item){
                        that.lineDetail.vat_product_ptg_code = item.code;
                        that.lineDetail.tax_group_id = item.id;
                        that.lineDetail.purchase_tax_group_id = item.id;
                        that.lineDetail.tax = item.value;
                    }
                    let windowId = that.lineValidator.host.attr('id')
                    $('#'+windowId+ ' .cbx_vat_product_ptg_code').blur()
                }else{

                }
                
            },
        }
    }
    isCreateAction = false;
    backToList(){
        console.log('Backtolist')
        this.lines = [cloneDeep(this.empty_line)];
        this.isCreateAction = false;
    }
    showAddLineForm(){
        let that = this;
        that.detailWindow.open();
        this.setLineData({
            id: 0,
            code:'',
            item_type:1,
            name:'',
            price:0,
            discount:0,
            qty:1,
            vat:0
        });
        // this.initAddLineFormControl();
    }
    itemDetail:any = {
        id:0,
        date: new Date(),
        due_date: new Date(),
        total_tax:0,
        total_with_tax:0,
        discount:0,
        total_no_discount:0,
        total_with_discount:0,
    }
    resetCreateForm(){
        this.itemDetail = {
            id:0,
            date: new Date(),
            due_date: new Date(),
            item_type:1,
            total_tax:0,
            total_with_tax:0,
            discount:0,
            total_no_discount:0,
            total_with_discount:0,
            editable: true
        }
    }
    showCreateForm(){
        this.isCreateAction = true;
        this.initFormControl();
    }
    empty_line = {
        id:0,
        product_id:'',
        product_code:'',
        code:'',
        item_type:'',
        name:'',
        price:'',
        qty:'',
        discount:'',
        tax_group_id:'',
        tax:'',
        total_no_tax:'',
        total_with_tax:'',
    }
    lines = [cloneDeep(this.empty_line)];
    lineSource;
    lineDataAdapter;
    syncTotal(){
        let that = this;
        
    }
    currentEditData
    beforeEditData
    lineGridSetting
    initLineGrid(){
        let that = this;
        that.lineSource = {
            datatype: "json",
            datafields: [
                { name: 'id', type: 'int' },
                { name: 'product_id', type: 'int' },
                { name: 'product_code', type: 'string' },
                { name: 'item_type', type: 'string' },
                { name: 'code', type: 'string' },
                { name: 'name', type: 'string' },
                { name: 'price', type: 'float' },
                { name: 'qty', type: 'int' },
                { name: 'discount', type: 'int' },
                { name: 'tax_group_id', type: 'int' },
                { name: 'tax', type: 'string'},
                { name: 'total_no_tax', type: 'float' },
                { name: 'total_with_tax', type: 'float' },
            ],
            addrow: function (rowid, rowdata, position, commit) {
                commit(true);
            },
            deleterow: function (rowid, commit) {
                commit(true);
            },
            // updaterow: function (rowid, commit) {
            //     commit(true);
            // },
            localData: that.lines
        }
        that.lineDataAdapter = new $.jqx.dataAdapter(that.lineSource, {
            beforeLoadComplete: function (records) {
                that.itemDetail.total_with_tax = records.reduce(function(sum, item) {
                  return sum + item.total_with_tax;
                }, 0);
                var total_no_tax = records.reduce(function(sum, item) {
                  return sum + item.total_no_tax;
                }, 0);
                that.itemDetail.total_tax = that.itemDetail.total_with_tax - total_no_tax;
                return records;
            }
        });
        if(that.lineGridSetting){
            that.lineGrid.setOptions({
                source: that.lineDataAdapter,
            })
            return;
        }
        let source = Helpers.Source({
            dataType: "json",
            type: 'POST',
            url: AppSetting.ITEMS_BINDING_URL,
            data:{
            },
            dataFields: [
                { name: 'id', type: 'number' },
                { name: 'product_id', type: 'number' },
                { name: 'product_code', type: 'string' },
                { name: 'description', type: 'string' },
                { name: 'uom_id'},
                { name: 'item_uom'},
                { name: 'unit_price'},
                { name: 'sale_price'},
                { name: 'purchase_price'},
                { name: 'unit_cost'},
                { name: 'gen_product_ptg_code'},
                { name: 'vat_product_ptg_code'},
                { name: 'sale_tax_group_id'},
                { name: 'purchase_tax_group_id'},
                { name: 'invt_ptg_code'}
            
            ],
            id: 'id',
            root: 'data',
        });
        // that.gridPartner.setOptions({source:[]});
        let itemDataAdapter = new jqx.dataAdapter(source, {
            beforeLoadComplete: function (records) {
                for (var i = 0; i < records.length; i++) {
                    var row = records[i];
                    row.label = row.product_code + ' ' + row.description
                    row.code = row.product_code
                }
                return records;
            },
            autoBind: true
        });
        let noneitemsetupsource = Helpers.Source({
            dataType: "json",
            type: 'POST',
            url: AppSetting.BINDING_NONE_ITEM_SETUP_URL,
            data:{
                gen_posting_type: 1,
            },
            dataFields: [
                { name: 'id', type: 'number' },
                { name: 'code', type: 'string' },
                { name: 'description', type: 'string' },
            
            ],
            id: 'id',
            root: 'data',
        });
        let noneItemSetupDataAdapter = new jqx.dataAdapter(noneitemsetupsource, {
            beforeLoadComplete: function (records) {
                for (var i = 0; i < records.length; i++) {
                    var row = records[i];
                    row.label = row.code + ' ' + row.description
                    row.code = row.code
                }
                return records;
            },
            autoBind: true
        });
        let taxDataAdapter = that.taxDataAdapter;
        let columns = [
                {
                    text: 'No.', width: 32,dataField:'id' , hidden:true,cellsalign: 'right', 
                    renderer: function (text, align, columnsheight) {
                        return '<div class="column-header-two-line">'+Helpers.translate.get('No.')+'</div>';
                    },
                    cellsrenderer: function (row, column, value,html,setting,data) {
                        
                        return [
                        '<div style="padding: 6px;line-height: 20px;">',
                            row+1,
                        '</div>',
                        ].join('')
                    },
                },{
                    text: 'product_id', dataField: 'product_id', width: 92 , hidden: true
                },{
                    text: 'tax', dataField: 'tax', width: 92 , hidden: true
                },{
                    text: 'total_no_tax', dataField: 'total_no_tax', width: 92 , hidden: true
                },{
                    text: 'Item Type', dataField: 'item_type', width: 92 ,
                    renderer: function (text, align, columnsheight) {
                        return '<div class="column-header-two-line">'+Helpers.translate.get('Item Type')+'</div>';
                    },
                    columntype: 'dropdownlist',
                    cellsrenderer: function (row, column, value,html,setting,data) {
                        let item_types = {
                            '1': Helpers.translate.get('Item'),
                            '2': Helpers.translate.get('Charge (Item)'),
                        }
                        return [
                        '<div style="padding: 6px;line-height: 20px;">',
                            item_types[value],
                        '</div>',
                        ].join('')
                    },
                    createeditor: (row: number, column: any, editor: any): void => {
                        // assign a new data source to the dropdownlist.
                        editor.jqxDropDownList({ 
                            selectedIndex:-1,
                            // autoOpen: true,
                            dropDownWidth : 360,
                            // dropDownHeight : 360,
                            autoDropDownHeight: true, 
                            source: new $.jqx.dataAdapter({
                                datatype: "array",
                                localdata: 
                                [
                                    {value:1,label: Helpers.translate.get('Item')},
                                    {value:2,label: Helpers.translate.get('Charge (Item)')}
                                ],
                                datafields: [
                                    { name: 'value', type: 'string'},
                                    { name: 'label', type: 'string'}
                                ]
                            }),
                            displayMember: 'label', 
                            valueMember: 'value' 
                        }).on('change', function (event) {
                            var args = event.args;
                            if (args && args.item) {
                                var item = args.item.originalItem;
                                console.log(item)
                                // that.lineGrid.setcellvalue(row,'name',item.originalItem.name)
                            }
                        })
                    },
                    initeditor: (row: number, cellvalue: any, editor: any, celltext: any, pressedkey: any): void => {
                        // if(!cellvalue){
                            // console.log('Clear')
                            editor.jqxDropDownList('selectItem',cellvalue)
                        // }
                        
                        console.log('initeditor',cellvalue)
                        // editor.jqxDropDownList("open");
                    },
                    // update the editor's value before saving it.
                    cellvaluechanging: (row: number, column: any, columntype: any, oldvalue: any, newvalue: any): any => {
                        // return the old value, if the new value is empty.
                        console.log(row,'row')
                        if (newvalue == '') return oldvalue;
                    },
                    geteditorvalue: function (row, cellvalue, editor) {
                        // return the editor's value.
                        let item = editor.jqxDropDownList('getSelectedItem'); 
                        if(item && item.originalItem){
                            
                            return item.originalItem.value
                        }
                        return '';
                    }
                },{
                    text: 'Code', dataField: 'code', width: 92 ,
                    renderer: function (text, align, columnsheight) {
                        return '<div class="column-header-two-line">'+Helpers.translate.get('Code')+'</div>';
                    },
                    columntype: 'combobox',
                    createeditor: (row: number, column: any, editor: any): void => {
                        // assign a new data source to the dropdownlist.
                        editor.jqxComboBox({ 
                            selectedIndex:-1,
                            autoComplete:true,
                            searchMode :'containsignorecase',
                            // autoOpen: true,
                            dropDownWidth : 360,
                            dropDownHeight : 360,
                            // autoDropDownHeight: true, 
                            source: itemDataAdapter.records,
                            displayMember: 'label', 
                            valueMember: 'code' 
                        }).on('change', function (event) {
                            var args = event.args;
                            if (args && args.item) {
                                var item = args.item.originalItem;
                                // that.lineGrid.setcellvalue(row,'name',item.originalItem.name)
                            }
                        })
                        editor.find('input').on("keyup", function (e) {
                            e = e || window.event;

                            if (
                                e.keyCode == '38' ||
                                e.keyCode == '40'
                                ) {
                                // up arrow
                                editor.jqxComboBox("open");
                            }
                        });
                    },
                    initeditor: (row: number, cellvalue: any, editor: any, celltext: any, pressedkey: any): void => {
                        if(!cellvalue){
                            console.log('Clear')
                            editor.jqxComboBox('selectItem',null)
                        }
                        let rowData = that.lineGrid.getrowdata(row);
                        if(rowData.item_type==1){
                            editor.jqxComboBox({
                                source: itemDataAdapter.records
                            })
                        }else if(rowData.item_type==2){
                            editor.jqxComboBox({
                                source: noneItemSetupDataAdapter.records
                            })
                        }else{
                            editor.jqxComboBox({
                                source: []
                            })
                        }
                        console.log('initeditor',cellvalue)
                        // editor.jqxComboBox("open");
                    },
                    // update the editor's value before saving it.
                    cellvaluechanging: (row: number, column: any, columntype: any, oldvalue: any, newvalue: any): any => {
                        // return the old value, if the new value is empty.
                        console.log(row,'row')
                        if (newvalue == '') return oldvalue;
                    },
                    geteditorvalue: function (row, cellvalue, editor) {
                        // return the editor's value.
                        let item = editor.jqxComboBox('getSelectedItem'); 
                        if(item && item.originalItem){
                            let rowData = that.lineGrid.getrowdata(row);
                            if(rowData.item_type=='1') {

                                let sale_price = item.originalItem.sale_price
                                let tax_group_id = item.originalItem.sale_tax_group_id
                                let tax = 0;
                                taxDataAdapter.records.map(function(d){
                                    if(d.id == tax_group_id){
                                        tax = d.value
                                    }
                                })
                                that.lineGrid.setcellvalue(row,'product_id',item.originalItem.id)
                                if(item.originalItem.description) that.lineGrid.setcellvalue(row,'name',item.originalItem.description)
                                if(sale_price) that.lineGrid.setcellvalue(row,'price',sale_price)
                                if(tax_group_id) {
                                    that.lineGrid.setcellvalue(row,'tax_group_id',tax_group_id)
                                    that.lineGrid.setcellvalue(row,'tax',tax)
                                }
                            }
                            let qty = that.lineGrid.getcellvalue(row,'qty');
                            if(qty==''){
                                qty = 1;
                                that.lineGrid.setcellvalue(row,'qty',qty)
                            }
                            
                            
                            return item.originalItem.code
                        }
                        return '';
                    }
                },{ 
                    text: 'Description', dataField: 'name', 
                    renderer: function (text, align, columnsheight) {
                        return '<div class="column-header-two-line">'+Helpers.translate.get('Description')+'</div>';
                    },
                },{ 
                    text: 'Quantity', dataField: 'qty', width: 80 ,
                    renderer: function (text, align, columnsheight) {
                        return '<div class="column-header-two-line">'+Helpers.translate.get('Quantity')+'</div>';
                    },
                    cellsalign: 'right', 
                    cellsformat: 'd',
                    columntype: 'numberinput',
                    createeditor: (row: number, cellvalue: any, editor: any, cellText: any, width: any, height: any): void => {
                        console.log(height,'height')
                        editor.jqxNumberInput({ 
                            decimalDigits: 0, 
                            digits: 4,
                            min: 1,
                        });
                    },
                },{ 
                    text: 'Unit Price', dataField: 'price', width: 120 ,
                    renderer: function (text, align, columnsheight) {
                        return '<div class="column-header-two-line">'+Helpers.translate.get('Unit Price')+'</div>';
                    },
                    cellsalign: 'right', 
                    cellsformat: 'f',
                    columntype: 'numberinput',
                    createeditor: (row: number, cellvalue: any, editor: any, cellText: any, width: any, height: any): void => {
                        console.log(height,'height')
                        editor.jqxNumberInput({ 
                            decimalDigits: 2, 
                            digits: 9,
                            min: 0,
                        });
                    },
                },{ 
                    text: 'Discount', dataField: 'discount', width: 80 ,
                    renderer: function (text, align, columnsheight) {
                        return '<div class="column-header-two-line">'+Helpers.translate.get('Discount')+'</div>';
                    },
                    cellsalign: 'right', 
                    cellsformat: 'd',
                    columntype: 'numberinput',
                    createeditor: (row: number, cellvalue: any, editor: any, cellText: any, width: any, height: any): void => {
                        console.log(height,'height')
                        editor.jqxNumberInput({ 
                            decimalDigits: 0, 
                            digits: 3,
                            min: 0,
                            max: 100,
                        });
                    },
                },{ 
                    text: 'Tax', dataField: 'tax_group_id', width: 80 ,
                    renderer: function (text, align, columnsheight) {
                        return '<div class="column-header-two-line">'+Helpers.translate.get('TAX')+'</div>';
                    },
                    columntype: 'dropdownlist',
                    cellsrenderer: function (row, column, value,html,setting,data) {
                        let label = value;
                        taxDataAdapter.records.map((d)=>{
                            if(d.id == value){
                                label=d.code
                            }
                        })
                        console.log(value,taxDataAdapter.records,'TAX')
                        return [
                        '<div style="padding: 6px;line-height: 20px;">',
                            label,
                        '</div>',
                        ].join('')
                    },
                    createeditor: (row: number, column: any, editor: any): void => {
                        editor.jqxDropDownList({ 
                            autoDropDownHeight: true, 
                            source: taxDataAdapter.records,
                            displayMember: 'label', 
                            valueMember: 'value' 
                        }).on('change', function (event) {
                            var args = event.args;
                            if (args && args.item) {
                                var item = args.item.originalItem;
                            }
                        })
                        // editor.find('input').on("keyup", function (e) {
                        //     e = e || window.event;

                        //     if (
                        //         e.keyCode == '38' ||
                        //         e.keyCode == '40'
                        //         ) {
                        //         // up arrow
                        //         editor.jqxDropDownList("open");
                        //     }
                        // });
                    },
                    initeditor: (row: number, cellvalue: any, editor: any, celltext: any, pressedkey: any): void => {
                            editor.jqxDropDownList('val',cellvalue)
                        // if(cellvalue==''){
                        // }else{
                        //     editor.jqxDropDownList('selectItem',null)
                        // }
                        console.log('initeditor',cellvalue)
                        editor.jqxDropDownList("open");
                    },
                    // update the editor's value before saving it.
                    cellvaluechanging: (row: number, column: any, columntype: any, oldvalue: any, newvalue: any): any => {
                        // return the old value, if the new value is empty.
                        console.log(row,'row')
                        if (newvalue == '') return oldvalue;
                    },
                    geteditorvalue: function (row, cellvalue, editor) {
                        // return the editor's value.
                        let item = editor.jqxDropDownList('getSelectedItem'); 
                        if(item && item.originalItem){
                            that.lineGrid.setcellvalue(row,'tax',item.originalItem.value)
                            return item.originalItem.id
                        }
                        return '';
                    }
                },{ 
                    text: 'Total', dataField: 'total_with_tax', width: 120, 
                    renderer: function (text, align, columnsheight) {
                        return '<div class="column-header-two-line">'+Helpers.translate.get('Total')+'</div>';
                    },
                    aggregates: ["sum"], 
                    cellsalign: 'right', 
                    cellsformat: 'd',
                    editable: false,
                },{ 
                    text: '', 
                    datafield: 'Remove', 
                    width: 90,
                    columntype: 'button',
                    editable: false,
                    // hidden:true,
                    // hidden: that.itemDetail.id,
                    cellsrenderer: function () {
                         return "Remove";
                    }, buttonclick: function (row) {
                        if(!that.checkEmptyLine(that.lineGrid.getrowdata(row))){
                            var id = that.lineGrid.getrowid(row);
                            that.lineGrid.deleterow(id);
                        }
                    }
                }
            ];
        
        that.lineGridSetting = {
            columns: columns,
            localization: Helpers.getLocalizatio(Helpers.translate.activeLang),
            source: that.lineDataAdapter,
            onCellClick:(event)=>{
                var args = event.args;
                // row's bound index.
                var rowBoundIndex = args.rowindex;
                // row's visible index.
                var rowVisibleIndex = args.visibleindex;
                // right click.
                var rightclick = args.rightclick; 
                // original event.
                var ev = args.originalEvent;
                // column index.
                var columnindex = args.columnindex;
                // column data field.
                var dataField = args.datafield;
                // cell value
                var value = args.value;
                if(args && args.row && dataField!='Remove'){
                    var dataRow = args.row.bounddata
                    console.log(dataRow,'dataRow')
                    if(dataRow) that.showLineDetail(dataRow)
                }
            },
            onCellBeginEdit(event: any): void {
                let args = event.args;
                if(!that.currentEditData){
                    that.beforeEditData = cloneDeep(args.row);
                    that.currentEditData = cloneDeep(args.row);
                }
                console.log('onCellBeginEdit')
            },
            onCellEndEdit(event: any): void {
                let args = event.args;
                let row = args.row;
                that.currentEditData[args.datafield] = args.value;
                that.update_row_total(args.rowindex)
                if (args.datafield === 'product_code') {
                    // row.name = args.value
                }
                console.log('onCellEndEdit',args,row)
                var rows = that.lineGrid.getrows();
                if(!that.checkEmptyLine(rows[rows.length-1])){
                    var datarow = cloneDeep(that.empty_line);
                    var commit = that.lineGrid.addrow(null, datarow);
                    console.log('ADD  EMPTY ROW')
                    // setTimeout(()=>{
                    //     var cell = that.lineGrid.getselectedcell();
                    //     console.log(cell,'cell')
                    //     if(cell.datafield != args.datafield) {
                    //         that.lineGrid.begincelledit(cell.rowindex, cell.datafield);
                    //     }
                    // },120)
                }
            },
        }
    }
    update_row_total(row){
        let that = this;
        setTimeout(()=>{
            let tax = +that.lineGrid.getcellvalue(row,'tax') || 0;
            let price = +that.lineGrid.getcellvalue(row,'price') || 0;
            let qty = +that.lineGrid.getcellvalue(row,'qty') || 0;
            let discount = +that.lineGrid.getcellvalue(row,'discount') || 0;
            let total_no_tax = price*qty * (1-discount/100)
            let total_with_tax = price*qty * (1-discount/100) * (1+tax/100)
            that.lineGrid.setcellvalue(row,'total_no_tax',total_no_tax.toFixed(2))
            that.lineGrid.setcellvalue(row,'total_with_tax',total_with_tax.toFixed(2))
            let records = that.lineGrid.getrows();
            that.itemDetail.total_with_tax = records.reduce(function(sum, item) {
              return sum + item.total_with_tax;
            }, 0);
            var sum_total_no_tax = records.reduce(function(sum, item) {
              return sum + item.total_no_tax;
            }, 0);
            that.itemDetail.total_tax = that.itemDetail.total_with_tax - sum_total_no_tax;
        },42)
    }
    checkEmptyLine(line){
        if(line.product_id != '') return false;
        if(line.product_code != '') return false;
        if(line.code != '') return false;
        if(line.name != '') return false;
        if(line.price != '') return false;
        if(line.qty != '') return false;
        if(line.discount != '') return false;
        if(line.tax_group_id != '') return false;
        if(line.tax != '') return false;
        // if(line.total_no_tax != '') return false;
        // if(line.total_with_tax != '') return false;
        return true;
    }
    initAddLineFormControl(){
        let that = this;
        that.initItemGroupCbx()
        that.initItemCbx()
        that.initVatProdPtgCbx()
        that.initNoneItemSetupCbx()
        
        
    }

    pending
    customerSetting;
    cellEditor
    initCustomerCbx(){
        let that = this;
        if(that.customerSetting) return;
        that.customerSetting = {
            onOpen: ()=>{
                that.pending = true;
            },
            onClose: ()=>{
                that.pending = false;
                $('.jqx-grid-validation,.jqx-grid-validation-arrow-up').remove()
            },
            onChange: function(event){
                let item = that.customerCbx.getSelectedItem();
                if(item){
                    // that.itemDetail.business_partner_id = item.id;
                    that.itemDetail.partner_name = item.name
                }
                let windowId = that.validator.host.attr('id')
                $('#'+windowId+ ' .cbx_business_partner_id').blur()
                let payment_term = +item.payment_term || 0
                let date = that.dateTxt.getDate()
                let dueDate = new Date(date)
                dueDate.setDate(dueDate.getDate()+payment_term)
                that.itemDetail.due_date = dueDate;
            },
        }
    }
    
    initCbxs(){
        this.initCustomerCbx()
    }
    initDetailWindow(){
        let that = this;
        that.initAddLineFormControl()
        that.initLineValidation();
    }
    initFormControl(){
        console.log('Init From')
        let that = this;
        // that.initDetailWindow()
        that.initCustomerCbx()
        that.initLineGrid()
        that.initValidation()
        
        
    }
    showLineDetail(data){
        this.setLineData(data);
        this.detailWindow.open();
    }
    setLineData(data){
        let that = this;
        that.lineDetail = data
        if(that.itemGroupCbx) that.itemGroupCbx.setValue('')
        if(that.itemsCbx) {
            that.itemsCbx.setValue(data.product_id)
            that.itemsCbx.filterByGroup(undefined)
        }
        if(that.vatProdPtgCbx) {
            that.vatProdPtgCbx.setValue(data.tax_group_id)
            console.log(data.tax_group_id,'data.tax_group_id')
        }
    }
    lineDetail;

    isSubmit = false;
    isLineSubmit = false;
    isContinue = false;
    checkLineValidate(isContinue: any): void {
        let that = this;
        setTimeout(() => {
            that.lineValidator.validate();
        }, 200)
        this.isLineSubmit = true;
        this.isContinue = isContinue;
    }
    onLineValidationError(){
        this.isLineSubmit = false;
    }

    onLineValidationSuccess() : void {
        let that = this;
        if(that.isLineSubmit){
            var formData = cloneDeep(that.lineDetail);
            console.log(formData,'formData')
            let row = formData;
            row.total_no_tax = (row.price) * (1-(row.discount||0) / 100) * (row.qty||0)
            row.total_with_tax = (row.price) * (1-(row.discount||0) / 100) * (row.qty||0) * (1+(row.tax||0)/100)
            if(!row.id){
                row.id = new Date().getTime();
                var rows = that.lineGrid.getrows();
                that.lineGrid.updaterow(rows.length-1, row);
                that.update_row_total(rows.length-1)
                that.lineGrid.addrow(null, cloneDeep(that.empty_line));
            }else{
                var row_index = that.lineGrid.getselectedrowindex()
                // that.lineSource.localData[row_index] = row;
                that.lineGrid.updaterow(row_index, row);
                that.update_row_total(row_index)
            }
            if(that.isContinue){
                this.setLineData({
                    item_type:1,
                    code:'',
                    name:'',
                    price:0,
                    discount:0,
                    qty:1,
                    vat:0
                });
            }else{
                that.detailWindow.close()
            }
        }
    }
    rules
    lineRules
    initLineValidation(){
        let that = this;
        let windowId = that.lineValidator.host.attr('id')
        if(that.lineRules){
            if(that.lineValidator) that.lineValidator.hide();
        }else
        that.lineRules =
        [
            { input: '#' + windowId+ ' .txt_line_name', message: Helpers.translate.get('Name') + Helpers.translate.get(' should be not empty'), action: 'keyup, blur', rule: 'required' },
            { 
            //     input: '#' + windowId+ ' .cbx_line_code', message: 'No. is required!', action: 'blur', rule: function () {
            //         if(that.lineDetail.item_type == 2){

            //             return !!that.noneItemSetupCbx.value;
            //         }else return true;
            //     }
            // },{ 
                input: '#' + windowId+ ' .cbx_line_code', message: Helpers.translate.get('No.') + Helpers.translate.get(' should be not empty'), action: 'blur', rule: function () {
                    // if(that.lineDetail.item_type == 1){
                        return !!that.itemsCbx.value || !!that.noneItemSetupCbx.value;
                    // }else return true;
                }
            },{ 
                input: '#' + windowId+ ' .cbx_vat_product_ptg_code', message: Helpers.translate.get('TAX') + Helpers.translate.get(' should be not empty'), action: 'blur', rule: function () {
                    return !!that.vatProdPtgCbx.value;
                } 
            }

        ];

    }
    initValidation(){
        let that = this;
        let windowId = that.validator.host.attr('id')
        if(that.rules){
            if(that.validator) that.validator.hide();
        }else
        that.rules =
        [
            { input: '#' + windowId+ ' .txt_partner_name', message: Helpers.translate.get('Name') + Helpers.translate.get(' should be not empty'), action: 'keyup, blur', rule: 'required' },
            { 
                input: '#' + windowId+ ' .cbx_business_partner_id', message: Helpers.translate.get('Customer') + Helpers.translate.get(' should be not empty'), action: 'blur', rule: function () {
                    return !!that.customerCbx.value;
                } 
            }

        ];

    }
    currentApi
    checkValidate(): void {
        let that = this;
        that.currentType = that.setting.api.create.type;
        that.currentApi = that.setting.api.create.url;
        that.doValidate()
    }
    doValidate(): void {
        let that = this;
        setTimeout(() => {
            that.validator.validate();
        }, 200)
        this.isSubmit = true;
    }
    onValidationError(){
        this.isSubmit = false;
    }
    checkValidateByConvertToInvoices(): void {
        let that = this;
        Helpers.confirm('<h3>'+Helpers.translate.get('Are you sure you want to convert this Purchase to Purchase Invoice?')+'</h3>',function(){
            that.currentType = 2;
            that.currentApi = AppSetting.PURCHASE_CONVERT_URL;
            that.doValidate()
            // that.api.post({
            //         url: AppSetting.PURCHASE_CONVERT_URL,
            //         params: {
            //             id: that.itemDetail.id,
            //             type: 2
            //         }
            //     }).done(function(res){
            //         if(res.code==1){
            //             that.dataAdapter.dataBind();
            //             that.resetCreateForm();
            //             that.backToList()
            //         }
            //     })
        })
    }
    currentType
    onValidationSuccess() : void {
        let that = this;
        if(that.isSubmit){
            var formData = cloneDeep(that.itemDetail);
            console.log(formData,'formData')
   
            var lines = that.lineGrid.getrows()
                .filter(function(line){
                    return !that.checkEmptyLine(line)
                })
                .map(function(line,i){
                    // return line;
                    return {
                        item_type: line.item_type,
                        product_id: line.product_id,
                        qty: +line.qty,
                        tax: +line.tax,
                        code: line.code,
                        product_code: line.product_code,
                        name: line.name,
                        price: +line.price,
                        price_no_discount: +line.price,
                        discount: +line.discount,
                        price_with_discount: +line.price * (1 - +line.discount/100),
                        tax_group_id: line.tax_group_id || 0,
                        total_no_tax: +line.total_no_tax,
                        total_with_tax: +line.total_with_tax,
                    }
                });
            var date = that.dateTxt.getDate().getTime()/1000;
            var due_date = that.dueDateTxt.getDate().getTime()/1000;
            formData.date = date;
            formData.due_date = due_date;
            let total_with_discount = (+formData.total_with_tax) * (1-(+formData.discount||0)/100)
            var params = {
                // lines: lines,
                type: that.currentType,
                business_partner_id: formData.business_partner_id,
                partner_name: formData.partner_name,
                date: formData.date,
                due_date: formData.due_date,
                total_tax: +formData.total_tax,
                total_with_tax: +formData.total_with_tax,  
                total_no_discount: +formData.total_with_tax,
                discount: +formData.discount,
                total_with_discount: +total_with_discount,
                // external_number: 090810230HQTD,
                comment: formData.comment,
                // delivery_user_id: 7
            }
            console.log(that.lineSource.localData,params,'data');
            
                
                that.api.post({
                    url: that.currentApi,
                    params: {
                        id: that.itemDetail.id,
                        params: params,
                        purchase_line: lines
                    }
                }).done(function(res){
                    if(res.code==1){
                        that.dataAdapter.dataBind();
                        that.resetCreateForm();
                        that.backToList()
                    }
                })
                
        }
    }
    closeLineDetail(){
        this.detailWindow.close();
    }
    summary
    taxDataAdapter
    ngAfterViewInit() {
        let that = this;
        that.sys.Summary()
            .then(
                (res)=>{
                    that.summary = res
                }
            )
        let taxsource = Helpers.Source({
            dataType: "json",
            type: 'POST',
            url: AppSetting.COMMON_BINDING_URL,
            data: {
                table:'tax_groups',
                conditions: ['type',2]
            },
            dataFields: [
                { name: 'id', type: 'number' },
                { name: 'code', type: 'string' },
                { name: 'name', type: 'string' },
                { name: 'value', type: 'number' },
            
            ],
            id: 'id',
            root: 'data',
        });
        // that.gridPartner.setOptions({source:[]});
        that.taxDataAdapter = new jqx.dataAdapter(taxsource, {
            beforeLoadComplete: function (records) {
                for (var i = 0; i < records.length; i++) {
                    var row = records[i];
                    row.label = row.code
                }
                return records;
            },
            autoBind: true
        });
    }
    sale7day = {
        total: null,
        status: false
    }
    sale12month = {
        total: null,
        status: false
    }
    initAnalytics(){
        let that = this;
        let conditions = [
            ['type','=',that.setting.api.list.type]
        ]
        if(that.partner_id){
            conditions.push(['business_partner_id','=',that.partner_id])
        }else{
        }
        that.api.post({
            url: AppSetting.ANALYTICS_PURCHASES_12MONTH_URL,
            params: {
                conditions: conditions
            }
        }).done((res)=>{
            let unit = 1;
            let data = res.map(function(d){
                if(+d.total>1000 && unit<1000){
                    unit = 1000
                }
                if(+d.total>1000000 && unit<1000000){
                    unit = 1000000
                }
                return +d.total;
            })
            let labels = res.map(function(d){
                return d.label;
            })

            var lineData = {
                labels: labels,
                datasets: [
                    {
                        label: "Cash",
                        borderColor: '#2196F3',
                        pointBackgroundColor: "#2196F3",
                        pointBorderColor: "#2196F3",
                        data: data,
                        borderWidth: 4,
                        pointBorderWidth: 2,
                        fill: false,
                        //lineTension: .1
                    }
                ]
            };
            var lineOptions = {
                responsive: true,
                maintainAspectRatio: false,
                legend: {
                    display: false
                },
                tooltips: {
                    mode: 'label',
                    label: 'mylabel',
                    callbacks: {
                        label: function (tooltipItem, data) {
                            var value:any = Number(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]);
                            let unit = '';
                            if(Math.abs(value) >= 1000) {
                                unit = 'K';
                                value/=1000;
                            }
                            if(Math.abs(value) >= 1000) {
                                unit = 'M';
                                value/=1000;
                            }
                            if(Math.abs(value) >= 1000) {
                                unit = 'B';
                                value/=1000;
                            }
                            // value = Math.round(value)
                            let name = data.datasets[tooltipItem.datasetIndex].label
                            if(Math.abs(parseInt(value)) >= 1000){
                                return new DecimalPipe("en-US").transform(value)+ unit;
                            } else {
                                return new DecimalPipe("en-US").transform(value)+ unit;
                            }
                        }, },
                },
                scales: {
                      xAxes: [{
                          gridLines: {
                              display: false,
                              color: 'rgba(255,255,255,.3)',
                          },
                          ticks: {
                              fontColor: '#111',
                              
                          }
                      }],
                      yAxes: [{
                          gridLines: {
                              color: 'rgba(255,255,255,.3)'
                          },
                          ticks: {
                              fontColor: '#111',
                              callback: function(label, index, labels) {
                                let unit = '';
                                  if(label>1000){
                                      unit = 'K'
                                      label/=1000;
                                  }
                                  if(label>1000){
                                      unit = 'M'
                                      label/=1000;
                                  }
                                  if(label>1000){
                                      unit = 'B'
                                      label/=1000;
                                  }
                                return new DecimalPipe("en-US").transform(label) + unit;
                              }
                          }
                      }]
                },
            };
            var ctx = document.getElementById("line_chart2").getContext("2d");
            new Chart(ctx, {type: 'line', data: lineData, options: lineOptions}); 
        })
        
        
    }
}
