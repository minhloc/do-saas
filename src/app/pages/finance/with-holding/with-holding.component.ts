import { Component, OnInit } from '@angular/core';
import { Angular5Csv } from 'angular5-csv/Angular5-csv';

@Component({
  selector: 'app-with-holding',
  templateUrl: './with-holding.component.html',
  styleUrls: ['./with-holding.component.css']
})
export class WithHoldingComponent implements OnInit {
  dataSet = [
   
    {
      wtax_code: 'E158',
      wtax_code_name: 'WI158 - 1% top 10,000 P.Corp supplier of goods',
      wtax: '1',
      document_no: '',
      document_date: '10/22/2018',
      payment_date: '',
      document_currency: '',
      document_amount: '',
      taxable_currency: 'PHP',
      taxable_amount: '3225081',
      wtax_curreny: 'PHP',
      wtax_amount: '32250.81',
      payment_currency: '',
      payment_amount: '',
      nonSbj_currency: 'PHP',
      nonSbj_amount: '0',
      month: 10
    },
    {
      wtax_code: '',
      wtax_code_name: '',
      wtax: '',
      document_no: 'PU 2',
      document_date: '10/27/2018',
      payment_date: '',
      document_currency: 'PHP',
      document_amount: '3330000',
      taxable_currency: 'PHP',
      taxable_amount: '3000000',
      wtax_curreny: 'PHP',
      wtax_amount: '30000',
      payment_currency: 'PHP',
      payment_amount: '3330000',
      nonSbj_currency: 'PHP',
      nonSbj_amount: '0',
      month: 10
    },
    {
      wtax_code: '',
      wtax_code_name: '',
      wtax: '',
      document_no: 'PU 3',
      document_date: '10/27/2018',
      payment_date: '',
      document_currency: 'PHP',
      document_amount: '48842.22',
      taxable_currency: 'PHP',
      taxable_amount: '44002',
      wtax_curreny: 'PHP',
      wtax_amount: '440.02',
      payment_currency: 'PHP',
      payment_amount: '48842.22',
      nonSbj_currency: 'PHP',
      nonSbj_amount: '0',
      month: 10
    },
    {
      wtax_code: '',
      wtax_code_name: '',
      wtax: '',
      document_no: 'PU 4',
      document_date: '10/5/2018',
      payment_date: '',
      document_currency: 'PHP',
      document_amount: '188787.69',
      taxable_currency: 'PHP',
      taxable_amount: '170079',
      wtax_curreny: 'PHP',
      wtax_amount: '1700.79',
      payment_currency: 'PHP',
      payment_amount: '0',
      nonSbj_currency: 'PHP',
      nonSbj_amount: '0',
      month: 10
    },
    {
      wtax_code: '',
      wtax_code_name: '',
      wtax: '',
      document_no: 'PU 5',
      document_date: '11/28/2018',
      payment_date: '',
      document_currency: 'PHP',
      document_amount: '12210',
      taxable_currency: 'PHP',
      taxable_amount: '11000',
      wtax_curreny: 'PHP',
      wtax_amount: '110',
      payment_currency: 'PHP',
      payment_amount: '0',
      nonSbj_currency: 'PHP',
      nonSbj_amount: '0',
      month: 11
    },
  ]

  years: any = []
  selected
  selectedData

  constructor() {
    this.selectedData = this.dataSet;
  }

  ngOnInit() {
    this.years = [
      {
        "month": 1,
        "description": "Jan 2018",
      },
      {
        "month": 2,
        "description": "Fer 2018",
      },
      {
        "month": 3,
        "description": "Mar 2018",
      },
      {
        "month": 4,
        "description": "Apr 2018",
      },
      {
        "month": 5,
        "description": "May 2018",
      },
      {
        "month": 6,
        "description": "Jun 2018",
      },
      {
        "month": 7,
        "description": "July 2018",
      },
      {
        "month": 8,
        "description": "Aug 2018",
      },
      {
        "month": 9,
        "description": "Sep 2018",
      },
      {
        "month": 10,
        "description": "Oct 2018",
      },
      {
        "month": 11,
        "description": "Nov 2018",
      },
      {
        "month": 12,
        "description": "Dec 2018",
      },
    ]
  }

  onSelectYears(element) {
    this.selectedData = this.dataSet.filter(data => data.month == element)
  }
  exportToCSV(){
    var options = { 
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true, 
      showTitle: false,
      useBom: true,
      // noDownload: true,
      headers: ["WTax Code","WTax Code Name","WTax %","Document No.","Document Date","Payment Date","Document Amount (currency)","Document Amount","Taxable Amount (currency)","Taxable Amount","WTax Amount (currency)","WTax Amount","Payment Amount (currency)","Payment Amount","Non-Sbj. Amount (currency)","Payment Amount (currency)",]
    };
    new Angular5Csv(this.selectedData, 'With Holding Tax Report',options);
  }
}
