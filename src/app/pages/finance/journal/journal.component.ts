import { Component, OnInit, AfterViewInit,ViewChild } from '@angular/core';
import { DatePipe, CurrencyPipe } from '@angular/common';
import { APIService } from '../../../api.service';
import {Helpers} from "../../../helpers";
import { AppSetting } from "../../../settings";
import { Router, ActivatedRoute } from '@angular/router';
import { ScriptLoaderService } from '../../../_services/script-loader.service';
import { AppMessage } from "../../../message";
import { AccountModule } from '../../../widgets/account/account.module';
declare var $:any;
declare var toastr: any;
@Component({
  selector: 'app-journal',
  templateUrl: './journal.component.html',
})
export class JournalComponent implements OnInit, AfterViewInit {
    @ViewChild('journal_entry_accountCode') journal_entry_accountCode: AccountModule;
    entryDetail;
    api_type = 1;
    gridElmId = '#treegrid';
    gridElmDetailId = '#jqxGridDetail';
    jqxgrid;
    setting = {
        showunused: false,
        expandAll: false,
        showFilterRow: false
    }
    jqxgridDetail;
    constructor(
        private api: APIService,
        private router: Router,
        private route: ActivatedRoute,
        private scriptLoader: ScriptLoaderService,
    ) { }

    ngOnInit() {
        // var urls = this.route.snapshot.url;
        // switch (urls[urls.length-1].path) {
        //     case "customers":
        //         this.api_type = 1;
        //         break;
        //     case "suppliers":
        //         this.api_type = 2;
        //         break;
        //     default:
        //         this.api_type = 1;
        //         break;
        //   }
      
    }
    accoutDataAdapter;
    entryDataAdapter
    onAddEntry(){
        let that = this;
        var sumCredit = $(that.gridElmDetailId).jqxGrid('getcolumnaggregateddata', 'credit', ['sum']);
        var sumDebit = $(that.gridElmDetailId).jqxGrid('getcolumnaggregateddata', 'debit', ['sum']);
        var posting_date = $('#journal_entry_posting_date').val()
        var comment = $('#journal_entry_comment').val()
        var document_no = $('#journal_entry_document_no').val()
        var entryRecords = that.entryDataAdapter.records
        var params = {
            posting_date: posting_date + ' 00:00:00',
            title: comment,
            document_no: document_no,
            total_credit: sumCredit.sum,
            total_debit: sumDebit.sum,
            items: entryRecords.map(function(item,index){
                return {
                    acct_code: item.acct_code,
                    document_no: document_no,
                    acct_id: item.acct_id,
                    acct_name: item.acct_name,
                    line_memo: item.line_memo,
                    line_number: index+1,
                    credit: item.credit||0,
                    debit: item.debit||0,
                }
            }),
        }
        console.log(params)
        if(params.items.length==0){
            toastr["warning"]("Total Debit and Credit must equal to be posted.")
            return;
        }
        if(sumCredit.sum != sumDebit.sum){
            toastr["warning"]("Total Debit and Credit must equal to be posted.")
            return;
        }
        if(!document_no){
            toastr["warning"]("Document No is required.")
            return;
        }
        this.api.post({
            url: AppSetting.JOURNAL_CREATE_URL,
            params: params
        }).done(function(res){
            that.showListEntry()
            that.dataAdapter.dataBind()
        })
    }
    initAddEntry(){
        let that = this;
        // if(!this.accountCategory){
        //     this.api.get({
        //         url: '/assets/demo/data/accountlv0.json',
        //         params: {
        //         }
        //     }).done(function(data){
        //         data.data.map(function(d){
        //             if(!d.FatherNum){
        //                 return
        //             }
        //         })
        //         console.log(data,'accountlv0')
        //     })
        // }
        if(!that.accoutDataAdapter){
            var source = Helpers.Source({
                datatype: "json",
                type: 'POST',
                url: AppSetting.ACCT_LIST_URL,
                data: {
                    type: 3
                },
                dataFields: [
                    { name: 'id', type: 'number' },
                    { name: 'parent_id', type: 'number' },
                    { name: 'acct_code', type: 'number' },
                    { name: 'acct_name', type: 'string' },
                    { name: 'category_id', type: 'number' },
                    { name: 'balance', type: 'number'},
                ],
                id: 'id',
                root: 'data',
            });
            that.accoutDataAdapter = new $.jqx.dataAdapter(source, {
                beforeLoadComplete: function (records) {
                    for (var i = 0; i < records.length; i++) {
                        records[i].label = records[i].acct_code + ' ' + records[i].acct_name
                    }
                    return records;
                }
            });
        }
        
        $('#btnAddJournalEntry').jqxButton({
            theme: AppSetting.THEME,
            width: 86,
            height: 32,
        }).bind('click', function (){
            var valid = $('#addEntryFrm').jqxValidator('validate');
            if(valid){
                var frmData = $('#addEntryFrm').serializeArray().reduce(function(obj, item) {
                      obj[item.name] = item.value;
                      return obj;
                  }, {});
                var selectItem = that.journal_entry_accountCode.getSelectedItem()
                frmData.acct_id = selectItem.id;
                frmData.acct_code = selectItem.acct_code;
                frmData.acct_name = selectItem.acct_name;
                console.log(frmData,'frmData')
                // var commit = $(that.gridElmDetailId).jqxGrid('addrow', null, frmData);
                that.entrySource.localData.push(frmData);

                that.entryDataAdapter.dataBind();

                $(that.gridElmDetailId).jqxGrid('updatebounddata');
                $('#addEntryFrm').trigger("reset");
                that.journal_entry_accountCode.setValue('')
                $('#addEntryFrm').jqxValidator('hide');
                $('#journal_entry_credit,#journal_entry_debit').prop('disabled',false)
            }
        })
        $('#addEntryFrm').jqxValidator('hide');
        $('#addEntryFrm').jqxValidator({
            hintType: "label",
            rules: [
                    { input: '#journal_entry_lineMemo', message: 'Line memo is required!', action: 'keyup, blur', rule: 'required' },
                    { 
                        input: '#journal_entry_accountCode', message: 'Account is required!', action: 'blur', 
                        rule: function () {
                            var value = that.journal_entry_accountCode.value;
                            return value!="";
                        } 
                    },
                    { input: '#journal_entry_credit', message: 'Credit is required!', action: 'keyup, blur', 
                        rule: function () {
                            var value = $("#journal_entry_credit").val();
                            var result = !isNaN(value);
                            $('#journal_entry_debit').prop('disabled',!result ||+value!=0)
                            
                            return result;
                        } 
                    },
                    { input: '#journal_entry_debit', message: 'Credit is required!', action: 'keyup, blur', 
                        rule: function () {
                            var value = $("#journal_entry_debit").val();
                            var result = !isNaN(value);
                            $('#journal_entry_credit').prop('disabled',!result || +value!=0)
                            if(result && +value!=0){
                                $('#journal_entry_credit').val('')
                            }
                            return result;
                        } 
                    },
                    // { input: '#journal_entry_credit', message: 'Credit is required!', action: 'keyup, blur', rule: 'required' },
                    // { input: '#journal_entry_debit', message: 'Debit is required!', action: 'keyup, blur', rule: 'required' },
            ]
        });
        
        that.initJournalEntryItems([]);
        
        $("#journal_entry_posting_date").jqxDateTimeInput({ 
            theme: AppSetting.THEME,
            width: '180px', height: '30px',
            formatString:'yyyy-MM-dd'
        });
        $('#addEntry').hide();
    }
    initDetailEntry(data){
        let that = this;
        that.initJournalEntryItems(data)
    }
    entrySource;
    initJournalEntryItems(data, editable = true){
        let that = this;
        that.entrySource = {
            datatype: "json",
            datafields: [
                { name: 'id', type: 'int' },
                { name: 'document_no', type: 'int' },
                { name: 'line_number', type: 'int' },
                { name: 'line_memo', type: 'string' },
                { name: 'acct_id', type: 'int' },
                { name: 'acct_code', type: 'string' },
                { name: 'acct_name', type: 'string' },
                { name: 'credit', type: 'number' },
                { name: 'debit', type: 'number' },
                { name: 'amount', type: 'number' },
                { name: 'total_vat', type: 'number' },
                { name: 'document_type', type: 'string' },
                { name: 'source_code', type: 'string' },
                { name: 'source_type', type: 'string' },
                { name: 'source_no', type: 'string' },
            ],
            addrow: function (rowid, rowdata, position, commit) {
                // synchronize with the server - send insert command
                // call commit with parameter true if the synchronization with the server is successful 
                //and with parameter false if the synchronization failed.
                // you can pass additional argument to the commit callback which represents the new ID if it is generated from a DB.
                commit(true);
            },
            localData: data
        }
        that.entryDataAdapter = new $.jqx.dataAdapter(that.entrySource, {
            beforeLoadComplete: function (records) {
            //     var filteredRecords = new Array();
                for (var i = 0; i < records.length; i++) {
                    // records[i].
                }
                return records;
            }
        });
        $(that.gridElmDetailId).jqxGrid({
            localization: Helpers.getLocalizatio(Helpers.translate.activeLang),

            width:  '100%',
            // filterable: false,
            autoheight: true,
            theme: AppSetting.THEME,
            source: that.entryDataAdapter,
            sortable: false,
            showstatusbar: true,
            statusbarheight: 25,
            showaggregates: true,
            ready: function() {
            },
            columns: [
                {
                    text: 'Line No.', dataField: 'line_number', width: 40 ,hidden: editable,
                    cellsalign: 'right',
                    renderer: function (text, align, columnsheight) {
                        return '<div class="column-header-two-line" title="Line No.">' + Helpers.translate.get('Line No.') + '</div>';
                    },
                },{ 
                    text: 'Line Memo', dataField: 'line_memo',minWidth: 200, 
                    renderer: function (text, align, columnsheight) {
                        return '<div class="column-header-two-line" title="Line Memo">' + Helpers.translate.get('Line Memo') + '</div>';
                    },
                },{ 
                    text: 'Account No.', dataField: 'acct_code', width: 72 ,hidden: editable,
                    cellsalign: 'center',
                    renderer: function (text, align, columnsheight) {
                        return '<div class="column-header-two-line" title="Account Code">' + Helpers.translate.get('Account Code') + '</div>';
                    },

                },{ 
                    text: 'Account Name', dataField: 'acct_name', width: 180 ,
                    renderer: function (text, align, columnsheight) {
                        return '<div class="column-header-two-line" title="Account Name">' + Helpers.translate.get('Account Name') + '</div>';
                    },
                },{ 
                    text: 'Debit Amount', dataField: 'debit', width: 120, 
                    renderer: function (text, align, columnsheight) {
                        return '<div class="column-header-two-line" title="Debit Amount">' + Helpers.translate.get('Debit Amount') + '</div>';
                    },
                    aggregates: ["sum"], 
                    cellsalign: 'right', 
                    cellsformat: 'd'
                },{ 
                    text: 'Credit Amount', dataField: 'credit', width: 120, 
                    renderer: function (text, align, columnsheight) {
                        return '<div class="column-header-two-line" title="Credit Amount">' + Helpers.translate.get('Credit Amount') + '</div>';
                    },
                    aggregates: ["sum"], 
                    cellsalign: 'right', 
                    cellsformat: 'd'
                
                },{ 
                    text: 'Amount', dataField: 'amount', width: 100, hidden: editable,
                    renderer: function (text, align, columnsheight) {
                        return '<div class="column-header-two-line" title="Amount">' + Helpers.translate.get('Amount') + '</div>';
                    },
                    cellsalign: 'right', 
                    cellsformat: 'd'
                },{ 
                    text: 'TAX Amount', dataField: 'total_vat', width: 100, hidden: editable,
                    renderer: function (text, align, columnsheight) {
                        return '<div class="column-header-two-line" title="TAX Amount">' + Helpers.translate.get('TAX Amount') + '</div>';
                    },
                    cellsalign: 'right', 
                    cellsformat: 'd'
                },{
                    text: 'Document Type', dataField: 'document_type', width: 100 ,hidden: editable,
                    renderer: function (text, align, columnsheight) {
                        return '<div class="column-header-two-line" title="Document Type">' + Helpers.translate.get('Document Type') + '</div>';
                    },

                },{ 
                    text: 'Source Code', dataField: 'source_code', width: 100 ,hidden: editable,
                    renderer: function (text, align, columnsheight) {
                        return '<div class="column-header-two-line" title="Source Code">' + Helpers.translate.get('Source Code') + '</div>';
                    },
                },{ 
                    text: 'Source Type', dataField: 'source_type', width: 100 ,hidden: editable,
                    renderer: function (text, align, columnsheight) {
                        return '<div class="column-header-two-line" title="Source Type">' + Helpers.translate.get('Source Type') + '</div>';
                    },
                },{ 
                    text: 'Source No.', dataField: 'source_no', width: 100 ,hidden: editable,
                    renderer: function (text, align, columnsheight) {
                        return '<div class="column-header-two-line" title="Source No.">' + Helpers.translate.get('Source No.') + '</div>';
                    },
                },{  
                    text: 'Delete', 
                    renderer: function (text, align, columnsheight) {
                        return '<div class="column-header-two-line" title="Delete">' + Helpers.translate.get('Delete') + '</div>';
                    },
                    datafield: 'Delete', 
                    width: 90,
                    hidden: !editable,
                    columntype: 'button', 
                    cellsrenderer: function () {
                         return "Delete";
                    }, buttonclick: function (row) {
                        console.log(row,'row')
                        that.entrySource.localData.splice(row,1)

                        that.entryDataAdapter.dataBind();
                        $(that.gridElmDetailId).jqxGrid('updatebounddata');
                    }
                }
            ],
        })
    }
    showDetailEntry(id){
        let that = this;
        this.api.get({
            url: AppSetting.JOURNAL_DETAIL_URL + id,
            params: {
                // id: id
            }
        }).done(function(res){
            $('#addEntry').show();
            $('#listEntry,#addEntryFrm>table').hide();
            that.entryDetail = res.data
            that.initJournalEntryItems(res.data.lines,false)
            console.log(res.data,'journalEntryDetail')
        })
    }
    showAddEntry(){
        let that = this;
        $('#listEntry,#addEntryFrm>table').show();
        $('#addEntry').show();
        $('#listEntry').hide();
        $('#journal_entry_document_no').val('')
        $('#journal_entry_comment').val('')
        $('#journal_entry_posting_date').jqxDateTimeInput('setDate', new Date());
        $('#addEntryFrm').trigger("reset");
        that.initJournalEntryItems([],true)
    }   
    showListEntry(){
        this.entryDetail = undefined
        $('#addEntry').hide();
        $('#listEntry').show();
        // this.dataAdapter.dataBind()
    }
    dataAdapter;
    loadJournalDataAdapter(){
        let that = this;
        // prepare the data
        var source = Helpers.Source({
            dataType: "json",
            type: 'POST',
            url: AppSetting.JOURNAL_BINDING_URL,
            data:{
                orders:{
                    id: 'desc'
                }
            },
            dataFields: [
                { name: 'id', type: 'number' },
                { name: 'number', type: 'number' },
                { name: 'document_no', type: 'string' },
                { name: 'title', type: 'string' },
                { name: 'posting_date', type: 'date' },
                { name: 'total_credit', type: 'number'},
                { name: 'total_debit', type: 'number'},
            ],
            filter: function() {
                // update the grid and send a request to the server.
                $(that.gridElmId).jqxGrid('updatebounddata', 'filter');
            },
            sort: function() {
                // update the grid and send a request to the server.
                $(that.gridElmId).jqxGrid('updatebounddata', 'sort');
            },
            id: 'id',
            root: 'data',
            sortcolumn: 'id',
            sortdirection: 'desc'
        });
        that.dataAdapter = new $.jqx.dataAdapter(source,{
            // beforeLoadComplete: function (records) {
            //     for (var i = 0; i < records.length; i++) {
            //         records[i].posting_date = new Date(records[i].posting_date*1000)
            //     }
            //     return records;
            // }
        });
        that.initJournalGrid();
    }
    initJournalGrid(){
        let that = this;
        that.jqxgrid = $(that.gridElmId).jqxGrid({
            localization: Helpers.getLocalizatio(Helpers.translate.activeLang),
            width:  '100%',
            height:  '560',
            filterable: true,
            showfilterrow: true,
            // autoshowfiltericon: false,
            // showfiltermenuitems: true,
            // autoheight: true,
            theme: AppSetting.THEME,
            source: that.dataAdapter,
            sortable: true,
            pageable: true,
            pagesize: 100,
            pagesizeoptions: [100, 500, 1000],
            virtualmode: true,
            rendergridrows: function () {
                    return that.dataAdapter.records;
            },
            ready: function()
            {
            },
            columns: [
                { text: 'ID', dataField: 'id', width: 80 ,hidden: true},
                { 
                    text: Helpers.translate.get('No.'), dataField: 'number', width: 80,
                    cellsalign: 'right', 
                },{ 
                    text: Helpers.translate.get('Document No.'), dataField: 'document_no', width: 120,
                },{ 
                    text: Helpers.translate.get('Posting Date'), dataField: 'posting_date', width: 120 ,
                    cellsformat: 'yyyy-MM-dd',
                    filtertype:'range'
                },{ 
                    text: Helpers.translate.get('Description'), dataField: 'title',  groupable: false,
                },{ 
                    text: Helpers.translate.get('Debit Amount'), dataField: 'total_debit', width: 120, 
                    // aggregates: ["sum"], 
                    cellsalign: 'right', 
                    cellsformat: 'd'
                },{ 
                    text: Helpers.translate.get('Credit Amount'), dataField: 'total_credit', width: 120, 
                    // aggregates: ["sum"], 
                    cellsalign: 'right', 
                    cellsformat: 'd'
                
                },
            ],
            // showtoolbar: true,
            // rendertoolbar: function (toolbar) {
            //     var me = this;
            //     var container = $("<div style='margin: 5px;'></div>");
            //     toolbar.append(container);
            //     container.append('<input id="addrowbutton" type="button" class="btn btn-sm btn-primary" value="Create a new G/L Journal" />');
            //     // $("#addrowbutton").jqxButton({theme: 'office'});
                
            //     // create new row.
            //     $("#addrowbutton").on('click', function () {
            //         that.showAddEntry();
            //         // var datarow = generaterow();
            //         // var commit = $("#grid").jqxGrid('addrow', null, datarow);
            //     });
            // },
        }).on("cellclick", function (event) {
            // event arguments.
            var args = event.args;
            // row's bound index.
            var rowBoundIndex = args.rowindex;
            // row's visible index.
            var rowVisibleIndex = args.visibleindex;
            // right click.
            var rightclick = args.rightclick; 
            // original event.
            var ev = args.originalEvent;
            // column index.
            var columnindex = args.columnindex;
            // column data field.
            var dataField = args.datafield;
            // cell value
            var value = args.value;
            var dataRow = args.row.bounddata
            that.showDetailEntry(dataRow.id);
        });
    }
    ngAfterViewInit() {
        this.initAddEntry()
        let that = this;
        that.loadJournalDataAdapter()
    }

    backToList() {
        this.entryDetail = undefined
    }
    getTransactionType(id){
        let types = ['','','Expenses | Mileage'];
        return types[id] || 'Default'
    }
}
