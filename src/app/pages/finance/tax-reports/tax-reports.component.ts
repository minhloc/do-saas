import { Component, OnInit } from "@angular/core";
import { Angular5Csv } from 'angular5-csv/Angular5-csv';
@Component({
  selector: "app-tax-reports",
  templateUrl: "./tax-reports.component.html",
  styleUrls: ["./tax-reports.component.css"]
})
export class TaxReportsComponent implements OnInit {
  dataSet = [
    {
      reg_no: 1,
      date: "9/10/2017",
      doc_no: "PU 1",
      bp_name: "ACS MANUFACTURING CORPORATION",
      base_amount_c: "PHP",
      base_amount: -100,
      tax_pecent: 0,
      tax_c: "PHP",
      tax: 0,
      non_de: "PHP",
      non: 0,
      total_c: "PHP",
      total: -100,
      month: 9
    },
    {
      reg_no: 2,
      date: "9/27/2017",
      doc_no: "IN 1",
      bp_name: "DDT KONSTRACT INC.",
      base_amount_c: "PHP",
      base_amount: 11000,
      tax_pecent: 12,
      tax_c: "PHP",
      tax: 1320,
      non_de: "PHP",
      non: 0,
      total_c: "PHP",
      total: 12320,
      month: 9
    },
    {
      reg_no: 3,
      date: "10/27/2017",
      doc_no: "IN 2",
      bp_name: "UPLIFT CONSTRUCTION & AUGMENTATION SERVICES INC.",
      base_amount_c: "PHP",
      base_amount: 20000,
      tax_pecent: 12,
      tax_c: "PHP",
      tax: 2400,
      non_de: "PHP",
      non: 0,
      total_c: "PHP",
      total: 22400,
      month: 10
    },
    {
      reg_no: 4,
      date: "10/27/2017",
      doc_no: "IN 3",
      bp_name: "PATK BUILDERS",
      base_amount_c: "PHP",
      base_amount: 5700,
      tax_pecent: 12,
      tax_c: "PHP",
      tax: 684,
      non_de: "PHP",
      non: 0,
      total_c: "PHP",
      total: 6384,
      month: 10
    },
    {
      reg_no: 5,
      date: "10/27/2017",
      doc_no: "IN 4",
      bp_name: "PATK BUILDERS",
      base_amount_c: "PHP",
      base_amount: 2800,
      tax_pecent: 12,
      tax_c: "PHP",
      tax: 336,
      non_de: "PHP",
      non: 0,
      total_c: "PHP",
      total: 3136,
      month: 10
    },
    {
      reg_no: 6,
      date: "10/27/2018",
      doc_no: "CN 1",
      bp_name: "PATK BUILDERS",
      base_amount_c: "PHP",
      base_amount: -300,
      tax_pecent: 12,
      tax_c: "PHP",
      tax: -36,
      non_de: "PHP",
      non: 0,
      total_c: "PHP",
      total: -336,
      month: 10
    },
    {
      reg_no: 7,
      date: "10/27/2018",
      doc_no: "PU 2",
      bp_name: "107 CONSTRUCTION",
      base_amount_c: "PHP",
      base_amount: -3000000,
      tax_pecent: 12,
      tax_c: "PHP",
      tax: -360000,
      non_de: "PHP",
      non: 0,
      total_c: "PHP",
      total: -3360000,
      month: 10
    },
    {
      reg_no: 8,
      date: "10/27/2018",
      doc_no: "PU 3",
      bp_name: "525 HARDWARE TRADING",
      base_amount_c: "PHP",
      base_amount: -44002,
      tax_pecent: 12,
      tax_c: "PHP",
      tax: -5280.24,
      non_de: "PHP",
      non: 0,
      total_c: "PHP",
      total: -49282.24,
      month: 10
    },
    {
      reg_no: 9,
      date: "10/28/2018",
      doc_no: "IN 5",
      bp_name: "DOUBLE DRAGON PROPERTIES CORP.",
      base_amount_c: "PHP",
      base_amount: 17500,
      tax_pecent: 12,
      tax_c: "PHP",
      tax: 2100,
      non_de: "PHP",
      non: 0,
      total_c: "PHP",
      total: 19600,
      month: 10
    },
    {
      reg_no: 10,
      date: "10/28/2018",
      doc_no: "PU 5",
      bp_name: "ACHIEVERS SALES CORP.",
      base_amount_c: "PHP",
      base_amount: -11000,
      tax_pecent: 12,
      tax_c: "PHP",
      tax: -1320,
      non_de: "PHP",
      non: 0,
      total_c: "PHP",
      total: -12320,
      month: 10
    },
    {
      reg_no: 11,
      date: "10/28/2018",
      doc_no: "PU 6",
      bp_name: "ADVANCE COMPUTER FORMS, INC.",
      base_amount_c: "PHP",
      base_amount: -11300,
      tax_pecent: 12,
      tax_c: "PHP",
      tax: -1356,
      non_de: "PHP",
      non: 0,
      total_c: "PHP",
      total: -12656,
      month: 10
    },
    {
      reg_no: 12,
      date: "10/28/2018",
      doc_no: "PU 7",
      bp_name: "ADVANCE DIESEL SYSTEM & TURBO SALES INC",
      base_amount_c: "PHP",
      base_amount: -7800000,
      tax_pecent: 0,
      tax_c: "PHP",
      tax: 0,
      non_de: "PHP",
      non: 0,
      total_c: "PHP",
      total: -7800000,
      month: 10
    },
    {
      reg_no: 13,
      date: "10/29/2018",
      doc_no: "IN 6",
      bp_name: "MEGAWIDE CONSTRUCTION CORP.",
      base_amount_c: "PHP",
      base_amount: 2000,
      tax_pecent: 12,
      tax_c: "PHP",
      tax: 240,
      non_de: "PHP",
      non: 0,
      total_c: "PHP",
      total: 2240,
      month: 10
    },
    {
      reg_no: 14,
      date: "11/29/2018",
      doc_no: "PU 8",
      bp_name: "ALLOY INDUSTRIAL SUPPLY CORPORATION",
      base_amount_c: "PHP",
      base_amount: -265000,
      tax_pecent: 0,
      tax_c: "PHP",
      tax: 0,
      non_de: "PHP",
      non: 0,
      total_c: "PHP",
      total: -265000,
      month: 11
    },
    {
      reg_no: 15,
      date: "11/29/2018",
      doc_no: "PU 9",
      bp_name: "ADVANCE DIESEL SYSTEM & TURBO SALES INC",
      base_amount_c: "PHP",
      base_amount: -6890,
      tax_pecent: 0,
      tax_c: "PHP",
      tax: 0,
      non_de: "PHP",
      non: 0,
      total_c: "PHP",
      total: -6890,
      month: 11
    },
    {
      reg_no: 16,
      date: "11/5/2018",
      doc_no: "PU 4",
      bp_name: "525 HARDWARE TRADING",
      base_amount_c: "PHP",
      base_amount: -170079,
      tax_pecent: 12,
      tax_c: "PHP",
      tax: -20409.48,
      non_de: "PHP",
      non: 0,
      total_c: "PHP",
      total: -190488.48,
      month: 11
    }
  ];
  years: any = []
  selected
  selectedData

  constructor() {
    this.selectedData = this.dataSet;
  }

  ngOnInit() {
    this.years = [
      {
        "month": 1,
        "description": "Jan 2018",
      },
      {
        "month": 2,
        "description": "Fer 2018",
      },
      {
        "month": 3,
        "description": "Mar 2018",
      },
      {
        "month": 4,
        "description": "Apr 2018",
      },
      {
        "month": 5,
        "description": "May 2018",
      },
      {
        "month": 6,
        "description": "Jun 2018",
      },
      {
        "month": 7,
        "description": "July 2018",
      },
      {
        "month": 8,
        "description": "Aug 2018",
      },
      {
        "month": 9,
        "description": "Sep 2018",
      },
      {
        "month": 10,
        "description": "Oct 2018",
      },
      {
        "month": 11,
        "description": "Nov 2018",
      },
      {
        "month": 12,
        "description": "Dec 2018",
      },
    ]
  }

  onSelectYears(element) {
    this.selectedData = this.dataSet.filter(data => data.month == element)
  }
  exportToCSV(){
    var options = { 
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true, 
      showTitle: false,
      useBom: true,
      // noDownload: true,
      headers: ["#", "Reg. No", "Date","Doc. No.","BP Name","Base Amount (currency)","Base Amount","Tax %","Tax (currency)","Tax","Non-Deductible (currency)","Non-Deductible","Total Amount (LC) (currency)","Total Amount (LC)"]
    };
    new Angular5Csv(this.selectedData, 'Tax Report',options);
  }
}
