import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ScriptLoaderService } from '../../_services/script-loader.service';
import { DatePipe, CurrencyPipe,DecimalPipe } from '@angular/common';
import { APIService } from '../../api.service';
import { Helpers} from "../../helpers";
import { AppSetting } from "../../settings";
import { SystemService } from "../../system.service";
declare var Chart:any;
declare var document:any;
@Component({
  selector: 'app-finance',
  templateUrl: './finance.component.html',
})
export class FinanceComponent implements OnInit, AfterViewInit {

    constructor(private api: APIService,private sys: SystemService) { }

    ngOnInit() {}
    system_info
    dashboard_data
    ngAfterViewInit() {
        let that= this;
        that.sys.FinanceInfo()
            .then(
                (res)=>{
                    that.system_info = res
                }
            )
        that.sys.Dashboard()
            .then(
                (res:any)=>{
                    that.dashboard_data = res.data
                    that.profitability_chart(res.data.profits);
                    that.receivable_chart();
                    that.payable_chart();
                    that.cash_flow_chart();
                    that.income_and_expenses_chart();
                    that.cash_end_of_month_chart();
                }
            )
    }



    profitability_chart(data){
        var barData = {
            labels: [Helpers.translate.get("Sales Amount"), Helpers.translate.get("Purchase Amount"), Helpers.translate.get("Expenses Amount")],
            
            datasets: [
                {
                    // label: "Data 1",
                    // backgroundColor:'#DADDE0', //'rgba(220, 220, 220, 0.5)',
                    data: [
                        data[0].sale_amount,
                        data[1].purchase_amount,
                        data[2].expense_amount,
                    ],
                    "fill": false,
                    "backgroundColor": [
                      "rgba(255, 99, 132, 1)",
                      "rgba(255, 205, 86, 1)",
                      "rgba(75, 192, 192, 1)",
                      "rgba(54, 162, 235, 1)",
                      "rgba(153, 102, 255, 1)",
                      "rgba(201, 203, 207, 1)"
                    ],
                    "borderColor": [
                      "rgb(255, 99, 132)",
                      "rgb(255, 205, 86)",
                      "rgb(75, 192, 192)",
                      "rgb(54, 162, 235)",
                      "rgb(153, 102, 255)",
                      "rgb(201, 203, 207)"
                    ],
                    "borderWidth": 0
                }
            ]
        };
        var barOptions = {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                xAxes: [{
                  ticks: {
                    beginAtZero: true,
                    callback: function(value, index, values) {
                      if(parseInt(value) >= 1000){
                        return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                      } else {
                        return value;
                      }
                    }
                  }
                }]
            },
            legend :{
                display:false
            },
            tooltips: {
                mode: 'label',
                label: 'mylabel',
                callbacks: {
                    label: function (tooltipItem, data) {
                        var value:any = Number(data.datasets[0].data[tooltipItem.index]);
                        if(parseInt(value) >= 1000){
                            return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                        } else {
                            return value;
                        }
                    }, },
            }
        };

        var ctx = document.getElementById("profitability_chart").getContext("2d");
        new Chart(ctx, {
            type: 'horizontalBar', 
            data: barData, 
            options:barOptions,
        });
    }
    receivable_chart(){
        // doughnut chart example
        let that = this;
        let p = Math.round(that.dashboard_data.cash_flow.accounts_receivable.percent )
        var doughnutData = {
            labels: [Helpers.translate.get("Received"),Helpers.translate.get("Other") ],
            datasets: [{
                data: [p,100-p],
                backgroundColor: ["#3bceb6","#bdc3c7","#8995c7"]
            }]
        } ;


        var doughnutOptions = {
            responsive: true,
            legend :{
                display:false
            },
        };


        var ctx4 = document.getElementById("receivable_chart").getContext("2d");
        new Chart(ctx4, {type: 'doughnut', data: doughnutData, options:doughnutOptions});
    }
    payable_chart(){
        // doughnut chart example
        let that = this;
        let p = Math.round(that.dashboard_data.cash_flow.accounts_payable.percent)
        var doughnutData = {
            labels: Helpers.translate.get(["Payabled","Other" ]),
            datasets: [{
                data: [p,100-p],
                backgroundColor: ["#3bceb6","#bdc3c7","#8995c7"]
            }]
        } ;


        var doughnutOptions = {
            responsive: true,
            legend :{
                display:false
            },
        };


        var ctx4 = document.getElementById("payable_chart").getContext("2d");
        new Chart(ctx4, {type: 'doughnut', data: doughnutData, options:doughnutOptions});
    }
    cash_flow_chart(){
        let that = this;
        let data = that.dashboard_data.cash_flow.cash_flow_chart;
        var barData = {
            // labels: ["Cash Going In", "Cash Going Out", "Ending Cash Going Hand"],
            
            datasets:[{
              label: Helpers.translate.get('Cash Going In'),
              backgroundColor: 'rgba(37, 187, 42, 0.88)',
              borderColor: 'none',
              data: data.map((row:any)=>{
                  return row.cash_going_in;
              })
            }, {
              label: Helpers.translate.get('Cash Going Out'),
              backgroundColor: 'rgba(233, 30, 99, .8)',
              borderColor: 'none',
              data: data.map((row:any)=>{
                  return row.cash_going_out;
              })
            }, {
              label: Helpers.translate.get('Ending Cash Going Hand'),
              data:data.map((row:any)=>{
                  return row.ending_cash_on_hand;
              }),
              type: 'line',
              fill: 'none',
              backgroundColor: '#111',
              borderColor: '#111',
              borderWidth: 1,
            }],
            labels: Helpers.translate.get(['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']),

        };
        var barOptions = {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                  ticks: {
                    beginAtZero: true,
                    callback: function(value, index, values) {
                        let unit = '';
                        if(Math.abs(value) >= 1000) {
                            unit = 'K';
                            value/=1000;
                        }
                        if(Math.abs(value) >= 1000) {
                            unit = 'M';
                            value/=1000;
                        }
                        if(Math.abs(value) >= 1000) {
                            unit = 'B';
                            value/=1000;
                        }
                      if(Math.abs(parseInt(value)) >= 1000){
                        return  new DecimalPipe("en-US").transform(value) + unit;
                      } else {
                        return new DecimalPipe("en-US").transform(value) + unit;
                      }
                    }
                  }
                }]
            },
            legend :{
                // display:false
            },
            tooltips: {
                mode: 'label',
                label: 'mylabel',
                callbacks: {
                    label: function (tooltipItem, data) {
                        var value:any = Number(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]);
                        let unit = '';
                        if(Math.abs(value) >= 1000) {
                            unit = 'K';
                            value/=1000;
                        }
                        if(Math.abs(value) >= 1000) {
                            unit = 'M';
                            value/=1000;
                        }
                        if(Math.abs(value) >= 1000) {
                            unit = 'B';
                            value/=1000;
                        }
                        // value = Math.round(value)
                        let name = data.datasets[tooltipItem.datasetIndex].label
                        if(Math.abs(parseInt(value)) >= 1000){
                            return name + ':' + new DecimalPipe("en-US").transform(value)+ unit;
                        } else {
                            return name + ':' + new DecimalPipe("en-US").transform(value)+ unit;
                        }
                    }, },
            },
        };

        var ctx = document.getElementById("cash_flow_chart").getContext("2d");
        new Chart(ctx, {
            type: 'bar', 
            data: barData, 
            options:barOptions,
        });
    }
    income_and_expenses_chart(){
        let that = this;
        let data = that.dashboard_data.financial.income_and_expenses;
        var barData = {
            // labels: ["Cash Going In", "Cash Going Out", "Ending Cash Going Hand"],
            
            datasets:[{
              label: Helpers.translate.get('Total Income'),
              backgroundColor: 'rgba(37, 187, 42, 0.88)',
              borderColor: 'none',
              data: data.map((row:any)=>{
                  return row.income;
              })
            }, {
              label: Helpers.translate.get('Total Expenses'),
              backgroundColor: 'rgba(233, 30, 99, .8)',
              borderColor: 'none',
              data: data.map((row:any)=>{
                  return -row.expenses;
              })
            }, {
              label: Helpers.translate.get('Net profit'),
              data:data.map((row:any)=>{
                  return row.net_profit;
              }),
              type: 'line',
              fill: 'none',
              backgroundColor: '#111',
              borderColor: '#111',
              borderWidth: 1,
            }],
            labels: Helpers.translate.get(['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']),

        };
        var barOptions = {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                xAxes: [{
                    stacked: true
                }],
                yAxes: [{
                  stacked: true,
                  ticks: {
                    beginAtZero: true,
                    callback: function(value, index, values) {
                        let unit = '';
                        if(Math.abs(value) >= 1000) {
                            unit = 'K';
                            value/=1000;
                        }
                        if(Math.abs(value) >= 1000) {
                            unit = 'M';
                            value/=1000;
                        }
                        if(Math.abs(value) >= 1000) {
                            unit = 'B';
                            value/=1000;
                        }
                      if(Math.abs(parseInt(value)) >= 1000){
                        return  new DecimalPipe("en-US").transform(value) + unit;
                      } else {
                        return new DecimalPipe("en-US").transform(value) + unit;
                      }
                    }
                  }
                }]
            },
            legend :{
                // display:false
            },
            tooltips: {
                mode: 'label',
                label: 'mylabel',
                callbacks: {
                    label: function (tooltipItem, data) {
                        var value:any = Number(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]);
                        let unit = '';
                        if(Math.abs(value) >= 1000) {
                            unit = 'K';
                            value/=1000;
                        }
                        if(Math.abs(value) >= 1000) {
                            unit = 'M';
                            value/=1000;
                        }
                        if(Math.abs(value) >= 1000) {
                            unit = 'B';
                            value/=1000;
                        }
                        // value = Math.round(value)
                        let name = data.datasets[tooltipItem.datasetIndex].label
                        if(Math.abs(parseInt(value)) >= 1000){
                            return name + ':' + new DecimalPipe("en-US").transform(value)+ unit;
                        } else {
                            return name + ':' + new DecimalPipe("en-US").transform(value)+ unit;
                        }
                    }, },
            },
        };

        var ctx = document.getElementById("income_and_expenses_chart").getContext("2d");
        new Chart(ctx, {
            type: 'bar', 
            data: barData, 
            options:barOptions,
        });
    }
    cash_end_of_month_chart(){
        let that = this;
        let data = that.dashboard_data.financial.cash_at_end_of_month;
        var barData = {
            // labels: ["Cash Going In", "Cash Going Out", "Ending Cash Going Hand"],
            
            datasets:[{
              label: Helpers.translate.get('Total Income'),
              backgroundColor: '#F44336',
              borderColor: '#F44336',
              borderWidth: 1,
              "fill": false,
              data: data.map((row:any)=>{
                  return row.data;
              })
            }],
            labels: Helpers.translate.get(['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']),

        };
        var barOptions = {
            responsive: true,
            // maintainAspectRatio: false,
            scales: {
                xAxes: [{
                    // stacked: true
                    gridLines: {
                        // offsetGridLines: true
                    }
                }],
                yAxes: [{
                  // stacked: true,
                  ticks: {
                    beginAtZero: true,
                    callback: function(value, index, values) {
                        let unit = '';
                        if(Math.abs(value) >= 1000) {
                            unit = 'K';
                            value/=1000;
                        }
                        if(Math.abs(value) >= 1000) {
                            unit = 'M';
                            value/=1000;
                        }
                        if(Math.abs(value) >= 1000) {
                            unit = 'B';
                            value/=1000;
                        }
                      if(Math.abs(parseInt(value)) >= 1000){
                        return  new DecimalPipe("en-US").transform(value) + unit;
                      } else {
                        return new DecimalPipe("en-US").transform(value) + unit;
                      }
                    }
                  }
                }]
            },
            legend :{
                // display:false
            },
            tooltips: {
                mode: 'label',
                label: 'mylabel',
                callbacks: {
                    label: function (tooltipItem, data) {
                        var value:any = Number(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]);
                        let unit = '';
                        if(Math.abs(value) >= 1000) {
                            unit = 'K';
                            value/=1000;
                        }
                        if(Math.abs(value) >= 1000) {
                            unit = 'M';
                            value/=1000;
                        }
                        if(Math.abs(value) >= 1000) {
                            unit = 'B';
                            value/=1000;
                        }
                        // value = Math.round(value)
                        let name = data.datasets[tooltipItem.datasetIndex].label
                        if(Math.abs(parseInt(value)) >= 1000){
                            return name + ':' + new DecimalPipe("en-US").transform(value)+ unit;
                        } else {
                            return name + ':' + new DecimalPipe("en-US").transform(value)+ unit;
                        }
                    }, },
            },
        };

        var ctx = document.getElementById("cash_end_of_month_chart").getContext("2d");
        new Chart(ctx, {
            type: 'line', 
            data: barData, 
            options:barOptions,
        });
    }
}
