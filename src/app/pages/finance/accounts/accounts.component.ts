import { Component, OnInit, AfterViewInit } from '@angular/core';
import { DatePipe, CurrencyPipe } from '@angular/common';
import { APIService } from '../../../api.service';
import {Helpers} from "../../../helpers";
import { AppSetting } from "../../../settings";
import { Router, ActivatedRoute } from '@angular/router';
import { ScriptLoaderService } from '../../../_services/script-loader.service';
import { AppMessage } from "../../../message";

declare var $:any;
declare var moment:any;

@Component({
  selector: 'app-accounts',
  templateUrl: './accounts.component.html',
})
export class AccountsComponent implements OnInit, AfterViewInit {
    item_detail;
    api_type = 1;
    gridElmId = '#treegrid';
    jqxgrid;
    setting = {
        showunused: false,
        expandAll: false,
        showFilterRow: false
    }
    constructor(
        private api: APIService,
        private router: Router,
        private route: ActivatedRoute,
        private scriptLoader: ScriptLoaderService,
    ) { }

    ngOnInit() {
        // var urls = this.route.snapshot.url;
        // switch (urls[urls.length-1].path) {
        //     case "customers":
        //         this.api_type = 1;
        //         break;
        //     case "suppliers":
        //         this.api_type = 2;
        //         break;
        //     default:
        //         this.api_type = 1;
        //         break;
        //   }
      
    }
    
    ngAfterViewInit() {
        
        
    }

    backToList() {
        this.item_detail = undefined
    }

}
