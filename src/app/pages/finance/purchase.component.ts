import { Component, OnInit, AfterViewInit } from '@angular/core';
import { SystemService } from '../../system.service';

@Component({
  selector: 'app-purchase',
  templateUrl: './purchase.component.html',
})
export class PurchaseComponent implements OnInit, AfterViewInit {

    constructor(private sys: SystemService) { }

    ngOnInit() {}
    summary
    ngAfterViewInit() {
        let that= this;
        that.sys.Summary()
            .then(
                (res)=>{
                    that.summary = res
                }
            )
    }


}
