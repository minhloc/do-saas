import { Component, OnInit, AfterViewInit, OnChanges,ViewChild, ViewEncapsulation  } from '@angular/core';
import { DatePipe, CurrencyPipe } from '@angular/common';
import { APIService } from '../../../api.service';
import { SystemService } from '../../../system.service';
import { Helpers} from "../../../helpers";
import { AppSetting } from "../../../settings";
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { AppMessage } from "../../../message";
import { FormsModule } from '@angular/forms';
import * as cloneDeep from 'lodash/cloneDeep';

import { jqxGridComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxgrid';
import { jqxComboBoxComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxcombobox';
import { jqxDropDownListComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxdropdownlist';
import { jqxDateTimeInputComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxdatetimeinput';
import { jqxValidatorComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxvalidator';
import { jqxFileUploadComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxfileupload';

import { jqxWindowComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxwindow';
import { jqxTabsComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxtabs';

import { BankPtgModule } from '../../../widgets/ptg/bank/ptg-bank.module';

declare var $:any;
declare var toastr:any;

@Component({
  selector: 'app-bank',
  templateUrl: './bank.component.html',
})
export class BankComponent implements OnInit, AfterViewInit {
    constructor(
        private api: APIService,
        private router: Router,
        private route: ActivatedRoute,
        private sys: SystemService
    ) { }
    @ViewChild('grid') grid: jqxGridComponent;
    @ViewChild('window') window: jqxWindowComponent;
    @ViewChild('validator') validator: jqxValidatorComponent;
    @ViewChild('bankPtgCbx') bankPtgCbx: BankPtgModule;

    theme = AppSetting.THEME;
    ngOnInit() {
        let that = this;
        this.route.params.subscribe(
            params => {
                that.loadDataAdapter();
                that.initGrid();
            }
        );
    }
    source;
    dataAdapter;
    gridSetting;
    loadDataAdapter(){
        let that = this;

        
        that.source = Helpers.Source({
            dataType: "json",
            type: 'POST',
            url: AppSetting.COMMON_BINDING_URL,
            data: {
                table:'banks',
                orders: {'id':'desc'}
            },
            dataFields: [
                { name: 'id', type: 'number' },
                { name: 'code', type: 'string' },
                { name: 'description', type: 'string' },
                { name: 'branch', type: 'string' },
                { name: 'account', type: 'string' },
            
            ],
            filter: function() {
                // update the grid and send a request to the server.
                that.grid.updatebounddata('filter');
            },
            sort: function() {
                // update the grid and send a request to the server.
                that.grid.updatebounddata('sort');
            },
            id: 'id',
            root: 'data',
        });
        // that.gridPartner.setOptions({source:[]});
        that.dataAdapter = new jqx.dataAdapter(that.source, {
            beforeLoadComplete: function (records) {
                for (var i = 0; i < records.length; i++) {
                    var row = records[i];
                }
                return records;
            }
        });
    }
    initGrid(){
        let that = this;
        
        let columns = [
                { 
                    text: 'ID', dataField: 'id', width: 80 , hidden: true
                },{
                    text: Helpers.translate.get('Code'), dataField: 'code', width: 100 ,
                },{ 
                    text: Helpers.translate.get('Branch'), dataField: 'branch', width: 160 ,
                },{ 
                    text: Helpers.translate.get('Account'), dataField: 'account', width: 120 ,
                },{ 
                    text: Helpers.translate.get('Description'), dataField: 'description',
                }
            ]
        let onCellClick = function (event) {
            // event arguments.
            var args = event.args;
            // row's bound index.
            var rowBoundIndex = args.rowindex;
            // row's visible index.
            var rowVisibleIndex = args.visibleindex;
            // right click.
            var rightclick = args.rightclick; 
            // original event.
            var ev = args.originalEvent;
            // column index.
            var columnindex = args.columnindex;
            // column data field.
            var dataField = args.datafield;
            // cell value
            var value = args.value;
            if(args && args.row){
                var dataRow = args.row.bounddata
                console.log(dataRow,'dataRow')
                if(dataRow) {
                    that.showDetailForm(dataRow.id)
                }
            }
        }
        if(that.gridSetting){
            that.grid.setOptions({source:that.dataAdapter});
            return;
        }
        Helpers.translate.do(()=>{
            if(that.grid){
                that.grid.setcolumnproperty('code','text', Helpers.translate.get('Code'))
                that.grid.setcolumnproperty('branch','text', Helpers.translate.get('Branch'))
                that.grid.setcolumnproperty('account','text', Helpers.translate.get('Account'))
                that.grid.setcolumnproperty('description','text', Helpers.translate.get('Description'))
                that.grid.setOptions({'localization': Helpers.getLocalizatio(Helpers.translate.activeLang)})
            }
        })
        that.gridSetting = {
            columns: columns,
            localization: Helpers.getLocalizatio(Helpers.translate.activeLang),
            source: that.dataAdapter,
            onCellClick: onCellClick,
            rendergridrows: (params: any): any[] => {
                return that.dataAdapter.records;
            },
        }
    }
    sales_info
    ngAfterViewInit() {
        let that= this;
        
        // that.sys.SaleInfo()
        //     .then(
        //         (res)=>{
        //             that.sales_info = res
        //         }
        //     )
    }
    rules
    initValidation(){
        let that = this;
        let windowId = that.validator.host.attr('id')
        if(that.rules){
            this.validator.hide();
        }else
        this.rules = [
            { input: '#' + windowId+ ' .txt_code', message: 'This is required!', action: 'keyup, blur', rule: 'required' },
            { input: '#' + windowId+ ' .txt_description', message: 'This is required!', action: 'keyup, blur', rule: 'required' },
            { input: '#' + windowId+ ' .txt_branch', message: 'This is required!', action: 'keyup, blur', rule: 'required' },
            { input: '#' + windowId+ ' .txt_account', message: 'This is required!', action: 'keyup, blur', rule: 'required' },
            { input: '#' + windowId+ ' .txt_swift_code', message: 'This is required!', action: 'keyup, blur', rule: 'required' },
            { input: '#' + windowId+ ' .txt_iban', message: 'This is required!', action: 'keyup, blur', rule: 'required' },

        ];
    }
    editingItem
    showDetailForm(id){
        let that= this;
        // that.initValidation();
        this.window.open();
        if(id){
            that.api.get({
                url: AppSetting.BANK_DETAIL_URL + id
            }).done(function(res){
                if(res.data){
                    that.setData(res.data)
                }
            })
        }else{
            that.setData({})
        }
    }
    setData(data){
        let that = this
        that.editingItem = data
        
        if(data.bank_ptg_code && that.bankPtgCbx){
            that.bankPtgCbx.setValue(data.bank_ptg_code)
        } else if(that.bankPtgCbx) that.bankPtgCbx.setValue(null);
    }
    initDetailWindow(){
        let that = this;
        that.initCbxs()
        that.initValidation();
    }
    bankSetting
    initBankCbx(){
        let that = this;
        if(that.bankSetting) return;
        that.bankSetting = {
            onOpen: ()=>{
            },
            onClose: ()=>{
                $('.jqx-grid-validation,.jqx-grid-validation-arrow-up').remove()
            },
            onChange: function(event){
                let item = that.bankPtgCbx.getSelectedItem();
                // let windowId = that.validator.host.attr('id')
                // $('#'+windowId+ ' .cbx_bank_ptg_code').blur()

                
            },
        }
    }
    initCbxs(){
        this.initBankCbx()
    }
    checkValidate(): void {
        setTimeout(() => {
            this.validator.validate();
        }, 200)
    }
    onValidationSuccess() : void {
        let that = this;
        

        // Helpers.confirm('<h3>Are you sure you want to approve selected expense(s)?</h3>',function(){
            let url = AppSetting.BANK_CREATE_URL;
            let method = 'post';
            if(that.editingItem.id){
                url = AppSetting.BANK_UPDATE_URL + that.editingItem.id;
                method = 'put';
            }
            var formData = cloneDeep(this.editingItem);
            that.api[method]({
                url: url,
                params: this.editingItem,
            }).done(function(res){
                that.dataAdapter.dataBind();
                that.window.close();
            })
        // })
    }
}    
