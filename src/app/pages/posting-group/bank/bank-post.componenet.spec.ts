import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BankPostComponent } from './bank-post.component';

describe('BankPostComponent', () => {
  let component: BankPostComponent;
  let fixture: ComponentFixture<BankPostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BankPostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BankPostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
