import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VenderPostComponent } from './vender-post.component';

describe('VenderPostComponent', () => {
  let component: VenderPostComponent;
  let fixture: ComponentFixture<VenderPostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VenderPostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VenderPostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
