import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NoneItemSetupComponent } from './none-item-setup.component';

describe('NoneItemSetupComponent', () => {
  let component: NoneItemSetupComponent;
  let fixture: ComponentFixture<NoneItemSetupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NoneItemSetupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NoneItemSetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
