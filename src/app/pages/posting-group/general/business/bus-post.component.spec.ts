import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusPostComponent } from './bus-post.component';

describe('BusPostComponent', () => {
  let component: BusPostComponent;
  let fixture: ComponentFixture<BusPostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusPostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusPostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
