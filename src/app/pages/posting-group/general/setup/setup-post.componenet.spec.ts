import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SetupPostComponent } from './setup-post.component';

describe('SetupPostComponent', () => {
  let component: SetupPostComponent;
  let fixture: ComponentFixture<SetupPostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SetupPostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SetupPostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
