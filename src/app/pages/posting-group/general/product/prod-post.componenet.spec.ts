import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProdPostComponent } from './prod-post.component';

describe('ProdPostComponent', () => {
  let component: ProdPostComponent;
  let fixture: ComponentFixture<ProdPostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProdPostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProdPostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
