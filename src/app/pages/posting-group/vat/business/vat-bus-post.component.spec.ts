import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VatBusPostComponent } from './vat-bus-post.component';

describe('VatBusPostComponent', () => {
  let component: VatBusPostComponent;
  let fixture: ComponentFixture<VatBusPostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VatBusPostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VatBusPostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
