import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VatProdPostComponent } from './vat-prod-post.component';

describe('VatProdPostComponent', () => {
  let component: VatProdPostComponent;
  let fixture: ComponentFixture<VatProdPostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VatProdPostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VatProdPostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
