import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VatSetupPostComponent } from './vat-setup-post.component';

describe('VatSetupPostComponent', () => {
  let component: VatSetupPostComponent;
  let fixture: ComponentFixture<VatSetupPostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VatSetupPostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VatSetupPostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
