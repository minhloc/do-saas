import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustPostComponent } from './cust-post.component';

describe('CustPostComponent', () => {
  let component: CustPostComponent;
  let fixture: ComponentFixture<CustPostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustPostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustPostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
