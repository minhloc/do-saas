import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InventoryPostComponent } from './inventory-post.component';

describe('InventoryPostComponent', () => {
  let component: InventoryPostComponent;
  let fixture: ComponentFixture<InventoryPostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InventoryPostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InventoryPostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
