import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvtSetupPostComponent } from './invt-setup-post.component';

describe('InvtSetupPostComponent', () => {
  let component: InvtSetupPostComponent;
  let fixture: ComponentFixture<InvtSetupPostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvtSetupPostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvtSetupPostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
