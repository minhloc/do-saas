import { Component, OnInit, Input,forwardRef,AfterViewInit,OnChanges, ViewChild, ViewEncapsulation, EventEmitter, Output } from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from "@angular/forms";
import { DatePipe, CurrencyPipe } from '@angular/common';
import { ScriptLoaderService } from '../../../_services/script-loader.service';
import { APIService } from '../../../api.service';
import { Helpers} from "../../../helpers";
import { AppSetting } from "../../../settings";
import * as cloneDeep from 'lodash/cloneDeep';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { jqxWindowComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxwindow';
import { jqxGridComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxgrid';
import { jqxValidatorComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxvalidator';
import { jqxDropDownListComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxdropdownlist';
import { jqxComboBoxComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxcombobox';
import { jqxDateTimeInputComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxdatetimeinput';

import { ItemGroupModule } from '../../../widgets/item-group/item-group.module';
import { ItemsModule } from '../../../widgets/items/items.module';
import { LocationModule } from '../../../widgets/location/location.module';
// import { GeneralProductPtgModule } from '../../widgets/ptg/general/product/ptg-gen-product.module';
// import { VatProductPtgModule } from '../../widgets/ptg/vat/product/ptg-vat-product.module';
// import { InventoryPtgModule } from '../../widgets/ptg/inventory/ptg-inventory.module';
// import { PartnerModule } from '../../widgets/partner/partner.module';
// import { UomModule } from '../../widgets/uom/uom.module';

declare var $:any;
declare var toastr:any;

@Component({
  selector: 'app-item-journals',
  templateUrl: './item-journals.component.html',
})
export class ItemJournalsComponent implements OnInit, AfterViewInit {
    constructor(
        private api: APIService,
        private router: Router,
        private route: ActivatedRoute,
        ) { 
    }
    @ViewChild('grid') grid: jqxGridComponent;
    @ViewChild('lineGrid') lineGrid: jqxGridComponent;
    @ViewChild('validator') validator: jqxValidatorComponent;
    @ViewChild('detailWindow') detailWindow: jqxWindowComponent;
    @ViewChild('lineValidator') lineValidator: jqxValidatorComponent;
    @ViewChild('itemGroupCbx') itemGroupCbx: ItemGroupModule;
    @ViewChild('itemsCbx') itemsCbx: ItemsModule;
    @ViewChild('postingDateTxt') postingDateTxt: jqxDateTimeInputComponent;
    @ViewChild('locationCbx') locationCbx: LocationModule;
    theme = AppSetting.THEME
    ngOnInit() {
        let that = this;
        this.route.params.subscribe(
            params => {
                that.loadAdapter();
                that.initGrid();
            }
        );
    }
    ngAfterViewInit() {
        let that= this;
        // that.initGrid();
    }
    source;
    dataAdapter;
    gridSetting;
    loadAdapter(){
        let that = this;
        that.source = Helpers.Source({
            dataType: "json",
            type: 'POST',
            url: AppSetting.COMMON_BINDING_URL,
            data:{
                table:'item_journal_header'
            },
            dataFields: [
                { name: 'id', type: 'number' },
                { name: 'trans_id', type: 'number'},
                { name: 'title', type: 'string' },
                { name: 'posting_date', type: 'date' },
            
            ],
            filter: function() {
                // update the grid and send a request to the server.
                that.grid.updatebounddata('filter');
            },
            sort: function() {
                // update the grid and send a request to the server.
                that.grid.updatebounddata('sort');
            },
            id: 'id',
            root: 'data',
        });
        // that.gridPartner.setOptions({source:[]});
        that.dataAdapter = new jqx.dataAdapter(that.source, {
            beforeLoadComplete: function (records) {
                for (var i = 0; i < records.length; i++) {
                    var row = records[i];
                }
                return records;
            }
        });
    }
    initGrid(){
        let that = this;
        // that.loadAdapter()
        let columns = [
                { text: '#', dataField: 'id', width: 120 ,hidden: true},
                { text: 'Trans ID', dataField: 'trans_id', width: 100},
                { text: 'Title', dataField: 'title'},
                { text: 'Posting date', dataField: 'posting_date', width: 100},
                
                
            ]
        
        if(that.gridSetting){
            return;
        }
        that.gridSetting = {
            columns: columns,
            source: that.dataAdapter,
            ready: function() {
                that.initCbxs()
            },
            rendergridrows: (params: any): any[] => {
                return that.dataAdapter.records;
            },
            rendered(event: any): void {
            },
            onCellClick: (event:any)=>{
                // event arguments.
                var args = event.args;
                // row's bound index.
                var rowBoundIndex = args.rowindex;
                // row's visible index.
                var rowVisibleIndex = args.visibleindex;
                // right click.
                var rightclick = args.rightclick; 
                // original event.
                var ev = args.originalEvent;
                // column index.
                var columnindex = args.columnindex;
                // column data field.
                var dataField = args.datafield;
                // cell value
                var value = args.value;
                console.log(args.row,'args.row')
                if(args && args.row){
                    var dataRow = args.row.bounddata
                    if(dataRow) that.showDetail(dataRow.id)
                }
            },
        }
    }
    initCbxs(){

    }
    editingItem;
    clickAddNew(){
        this.setData({
            title: '',
            posting_date: new Date(),
            lines:[]
        })
        this.initLineGrid()
        this.initDetailForm()
        
    }
    initDetailForm(){
        this.initValidation()
    }
    showDetail(id){
        let that = this;
        that.api.post({
            url: AppSetting.GET_ITEM_JOURNAL_URL,
            params:{
                id:id
            }
        }).done(function(res){
            if(res.data){
                that.setData(res.data)
                that.initLineGrid()
                that.initDetailForm()
            }
        })
    }
    setData(data){
        let that = this;
        // data.posting_date = new Date(new DatePipe("en-US").transform(data.posting_date, 'yyyy-MM-dd'));
        that.itemDetail = data;
        that.line_data = data.lines;
        this.isCreateAction = true;
    }
    isCreateAction
    lineGridSetting
    lineDataAdapter
    lineSource
    line_data;
    initLineGrid(){
        
        let that = this;
        that.lineSource = {
            datatype: "json",
            datafields: [
                { name: 'id', type: 'number' },
                { name: 'type', type: 'string' },
                { name: 'document_no', type: 'string' },
                { name: 'location_code', type: 'string' },
                { name: 'product_id', type: 'number' },
                { name: 'product_code', type: 'string' },
                { name: 'product_name', type: 'string' },
                { name: 'line_memo', type: 'string' },
                { name: 'quantity', type: 'number' },
                { name: 'discount', type: 'number' },
                { name: 'price', type: 'number' },
                { name: 'total', type: 'number' },
            ],
            addrow: function (rowid, rowdata, position, commit) {
                // synchronize with the server - send insert command
                // call commit with parameter true if the synchronization with the server is successful 
                //and with parameter false if the synchronization failed.
                // you can pass additional argument to the commit callback which represents the new ID if it is generated from a DB.
                commit(true);
            },
            localData: that.line_data
        }
        let columns = [
            { text: 'Doc. No', dataField: 'id', width: 80,hidden: true},
            { 
                text: 'Type', dataField: 'type', width: 80,
                cellsrenderer: function (row, column, value,html,setting,data) {
                    let values = {
                        '1':'Positive Adjmt.',
                        '-1':'Negative Adjmt.'
                    }
                    return '<div style="padding:5px;line-height: 22px;">' + values[value] + '</div>'
                }
            },
            { text: 'Doc. No', dataField: 'document_no', width: 80},
            { text: 'Location Code', dataField: 'location_code', width: 100},
            { text: 'Product Code', dataField: 'product_code', width: 100},
            { text: 'Product Name', dataField: 'product_name',width:180},
            { text: 'Line Memo', dataField: 'line_memo'},
            { text: 'Quantity', dataField: 'quantity', width: 60,cellsalign: 'right',},
            { 
                text: 'Unit Cost', dataField: 'price', width: 100,
                cellsalign: 'right', 
                cellsformat: 'd'
            },
            { text: 'Discount', dataField: 'discount', width: 60,cellsalign: 'right',},
            { 
                text: 'Total', dataField: 'total', width: 100,
                aggregates: ["sum"], 
                cellsalign: 'right', 
                cellsformat: 'd'
            },
        ];
        that.lineDataAdapter = new jqx.dataAdapter(that.lineSource,{
            beforeLoadComplete: function (records) {
                that.itemDetail.total = records.reduce(function(sum, item) {
                  return sum + item.total;
                }, 0);
                return records;
            }
        });
        if(that.lineGridSetting){
            that.lineGrid.setOptions({
                source: that.lineDataAdapter,
            })
            return;
        }
        that.lineGridSetting = {
            columns: columns,
            source:  that.lineDataAdapter,
            ready: function() {

            },
            onCellClick:(event)=>{
                var args = event.args;
                // row's bound index.
                var rowBoundIndex = args.rowindex;
                // row's visible index.
                var rowVisibleIndex = args.visibleindex;
                // right click.
                var rightclick = args.rightclick; 
                // original event.
                var ev = args.originalEvent;
                // column index.
                var columnindex = args.columnindex;
                // column data field.
                var dataField = args.datafield;
                // cell value
                var value = args.value;
                if(args && args.row && dataField!='Delete'){
                    var dataRow = args.row.bounddata
                    console.log(dataRow,'dataRow')
                    if(dataRow) that.showLineDetail(dataRow)
                }
            }
        }
    }
    rules
    initValidation(){
        let that = this;
        let windowId = that.validator.host.attr('id')
        if(that.rules){
            if(that.validator) that.validator.hide();
        }else
        that.rules =
        [
            { input: '#' + windowId+ ' .txt_title', message: 'Memo is required!', action: 'keyup, blur', rule: 'required' },

        ];

    }
    itemDetail
    checkValidate(): void {
        let that = this;
        setTimeout(() => {
            that.validator.validate();
        }, 200)
        this.isSubmit = true;
    }
    onValidationSuccess(){
        let that = this;
        if(that.isSubmit){
            var formData = cloneDeep(that.itemDetail);
            console.log(formData,'formData',that.postingDateTxt)
   
            var lines = that.lineSource.localData;
            var date = that.postingDateTxt.getDate();
            formData.date = date;
            that.api.post({
                url: AppSetting.ITEM_JOURNAL_CREATE_URL,
                params: {
                    params: formData,
                    lines: lines
                }
            }).done(function(res){
                if(res.code==1){
                    that.dataAdapter.dataBind();
                    // that.resetCreateForm();
                    that.backToList()
                }
            })
                
        }
    }
    onValidationError(){
        this.isSubmit = false;
    }
    emptyLineRow = {
        id: 0,
        type:0,
        location_code:'',
        product_name:'',
        discount:0,
        quantity:1,
        total:0,
        price:0
    }
    showAddLineForm(){
        let that = this;
        that.detailWindow.open();
        this.setLineData(cloneDeep(that.emptyLineRow));
    }
    showLineDetail(data){
        this.setLineData(data);
        this.detailWindow.open();
    }
    setLineData(data){
        let that = this;
        that.lineDetail = data

        if(that.itemGroupCbx) that.itemGroupCbx.setValue('')
        if(that.itemsCbx) {
            that.itemsCbx.setValue(data.product_id)
            that.itemsCbx.filterByGroup(undefined)
        }
        if(that.locationCbx) {
            that.locationCbx.setValue(data.location_code)
        }
    }
    lineDetail;
    initDetailWindow(){
        let that = this;
        that.initAddLineFormControl()
        that.initLineValidation();
    }
    isSubmit = false;
    isLineSubmit = false;
    isContinue = false;
    checkLineValidate(isContinue: any): void {
        let that = this;
        setTimeout(() => {
            that.lineValidator.validate();
        }, 200)
        this.isLineSubmit = true;
        this.isContinue = isContinue;
    }
    onLineValidationError(){
        this.isLineSubmit = false;
    }
    onLineValidationSuccess(){
        let that = this;
        if(that.isLineSubmit){
            var formData = cloneDeep(that.lineDetail);
            let row = formData;
            row.total = (row.price) * (1-(row.discount||0) / 100) * (row.quantity||0)
            if(!row.id){
                row.id = new Date().getTime();
                that.line_data.push(row);
                that.lineDataAdapter.dataBind();
            }else{
                var row_index = that.lineGrid.getselectedrowindex()
                that.lineSource.localData[row_index] = row;
                that.lineGrid.updaterow(row_index, row);
            }
            if(that.isContinue){
                this.setLineData(cloneDeep(that.emptyLineRow));
            }else{
                that.detailWindow.close()
            }
        }
    }
    
    pending
    itemGroupSetting
    initItemGroupCbx(){
        let that = this;
        if(that.itemGroupSetting) return;
        that.itemGroupSetting = {
            onOpen: ()=>{
                that.pending = true;
            },
            onClose: ()=>{
                that.pending = false;
                $('.jqx-grid-validation,.jqx-grid-validation-arrow-up').remove()
            },
            onChange: function(event){
                console.log('ONCHANGE')
                let item = that.itemGroupCbx.getSelectedItem();
                if(that.detailWindow.isOpen()){
                    if(item){
                        // that.editingItem.gen_product_ptg = item;
                        // that.editingItem.item_group_id = item.id
                        if(that.itemsCbx){
                            that.itemsCbx.setValue(undefined)
                            that.itemsCbx.filterByGroup(item.id)
                        }
                    }
                    // let windowId = that.validator.host.attr('id')
                    // let windowId = that.validator.host.attr('id')
                    // $('#'+windowId+ ' .cbx_item_group_id').change()

                }else{
                    // if(that.cellEditor) that.cellEditor.val(item.description)
                    // that.currentEditData.item_group_id = item.id
                }
                
            },
        }
    }
    itemsSetting
    initItemCbx(){
        let that = this;
        if(that.itemsSetting) return;
        that.itemsSetting = {
            onOpen: ()=>{
                that.pending = true;
            },
            onClose: ()=>{
                that.pending = false;
                $('.jqx-grid-validation,.jqx-grid-validation-arrow-up').remove()
            },
            onChange: function(event){
                console.log('ONCHANGE')
                let item = that.itemsCbx.getSelectedItem();
                if(that.detailWindow.isOpen()){
                    if(item){
                        console.log(item,'item')
                        that.lineDetail.product_name = item.description;
                        that.lineDetail.product_code = item.product_code;
                        that.lineDetail.price     = item.unit_cost;
                        
                        // that.editingItem.gen_product_ptg = item;
                        // that.editingItem.item_group_id = item.id
                    }
                    let windowId = that.lineValidator.host.attr('id')
                    $('#'+windowId+ ' .cbx_line_product_id').blur()

                }else{
                    // if(that.cellEditor) that.cellEditor.val(item.description)
                    // that.currentEditData.item_group_id = item.id
                }
                
            },
        }
    }
    locationSetting
    initLocationCbx(){
        let that = this;
        that.locationSetting = {
                onOpen: ()=>{
                    that.pending = true;
                },
                onClose: ()=>{
                    that.pending = false;
                    $('.jqx-grid-validation,.jqx-grid-validation-arrow-up').remove()
                },
                onChange: function(args){
                    let item = args.getSelectedItem();
                    if(item){
                        // that.editingItem.sales_acct = item;
                        that.lineDetail.location_code = item.code
                    }
                    let windowId = that.lineValidator.host.attr('id')
                    $('#'+windowId+ ' .cbx_location_code').blur()
                },
            }
    }
    initAddLineFormControl(){
        let that = this;
        that.initItemGroupCbx()
        that.initItemCbx()
        that.initLocationCbx()
        
        
    }
    lineRules
    initLineValidation(){
        let that = this;
        let windowId = that.lineValidator.host.attr('id')
        if(that.lineRules){
            if(that.lineValidator) that.lineValidator.hide();
        }else
        that.lineRules =
        [
            { input: '#' + windowId+ ' .txt_document_no', message: 'Document No is required!', action: 'keyup, blur', rule: 'required' },
            { input: '#' + windowId+ ' .txt_line_memo', message: 'Line Memo is required!', action: 'keyup, blur', rule: 'required' },
            { input: '#' + windowId+ ' .txt_line_name', message: 'Product Name is required!', action: 'keyup, blur', rule: 'required' },
            { 
                input: '#' + windowId+ ' .cbx_line_product_id', message: 'Items is required!', action: 'blur', rule: function () {
                    return !!that.itemsCbx.value;
                } 
            },{ 
                input: '#' + windowId+ ' .rd_type', message: 'Type is required!', action: 'blur', rule: 'required' 
            }

        ];

    }
    closeLineDetail(){
        this.detailWindow.close();
    }
    backToList(){
        console.log('Backtolist')
        this.line_data = [];
        this.isCreateAction = false;
    }
}
