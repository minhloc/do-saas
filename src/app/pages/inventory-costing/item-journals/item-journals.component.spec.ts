import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemJournalsComponent } from './item-journals.component';

describe('ItemJournalsComponent', () => {
  let component: ItemJournalsComponent;
  let fixture: ComponentFixture<ItemJournalsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemJournalsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemJournalsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
