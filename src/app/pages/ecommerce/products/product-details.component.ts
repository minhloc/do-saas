import { Component, OnInit, AfterViewInit, Output, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { APIService } from '../../../api.service';

import {AppComponent} from '../../../app.component';
import {Helpers} from "../../../helpers";
import { AppSetting } from "../../../settings";
import { ActivatedRoute, Router } from '@angular/router';

declare var $:any;

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html', 
  providers: [AppComponent]
})
export class ProductDetailsComponent implements OnInit, AfterViewInit {
  private id: any;
  productData = {};
  constructor(
    private http: HttpClient,
    private api: APIService,
    private appComponent: AppComponent,
    private router: Router,
    private route: ActivatedRoute,
    ) { }
	items = [{name:'aaa'}];
    customMessage = '';
    @Output() observableEvent: EventEmitter<any> = new EventEmitter<any>();
    ngOnInit() {
        this.id = this.route.snapshot.params.id
    }

    ngAfterViewInit() {
        let that = this;
        this.api.get({
            url: AppSetting.PRODUCT_DETAIL_URL + this.id
        }).done(function(data){
          console.log(data,'data')
            that.productData = data
        })
    
        
    }
}
