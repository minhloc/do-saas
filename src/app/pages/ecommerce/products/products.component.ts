import { Component, OnInit, AfterViewInit, Output, EventEmitter } from '@angular/core';
import { APIService } from '../../../api.service';
import { AppMessage } from "../../../message";
import {AppComponent} from '../../../app.component';
import {Helpers} from "../../../helpers";
import { AppSetting } from "../../../settings";
import { Router } from '@angular/router';
import { ScriptLoaderService } from '../../../_services/script-loader.service';

declare var $:any;

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html', 
  providers: [AppComponent]
})
export class ProductsComponent implements OnInit, AfterViewInit {

  constructor(
    private api: APIService,
    private appComponent: AppComponent,
    private router: Router,
    private scriptLoader: ScriptLoaderService,
    ) { }
	items = [{name:'aaa'}];
    item_detail;
    uom_list = [];
    tax_list = [];
    group_list = [];
    suppier_list = [];
    customMessage = '';
    @Output() observableEvent: EventEmitter<any> = new EventEmitter<any>();
    ngOnInit() {
    }

    ngAfterViewInit() {
        let that = this;
        
    
        
        // var draw = 0;
        var columns = [
                { "data": "id" },
                { "data": "product_code" },
                { 
                    "data": "description" ,
                    "render": function ( value, type, dataRow ) {
                        if(dataRow.images && dataRow.images.length>0){
                            var image = '<img class="mr-3" onerror="$(this).remove()" src="' + dataRow.images[0].url + '" alt="image" height="28" />'
                            return image + value
                        }
                        return value;
                    },
                },
                { "data": "purchase_price" },
                {"data":null,className:"action-c", "defaultContent":'<span class="text-light mr-3 font-16"><i class="ti-pencil"></i></span>'}
            ]
        $('#products-table2').DataTable({
            "ajax": {
                "url": AppSetting.PRODUCT_LIST_URL,
                "contentType": "application/json",
                "beforeSend": function(xhr){
                    xhr.setRequestHeader("Authorization", "bearer " + localStorage.getItem('token'));
                 },
                "type": "POST",
                // "dataSrc": 'data',
                "dataSrc": function ( data ) {
                    console.log(data,'data');
                    data.recordsFiltered = data.meta.total;
                    data.recordsTotal = data.meta.total;
                    // data.draw = ++draw
                    delete(data.links)
                    delete(data.message)
                    delete(data.meta)
                    delete(data.status)
                    // data.draw = data.meta.current_page
                    return data.data;
                },
                "data": function ( d ) {
                    console.log(d,'d')
                    d.page = d.start/d.length + 1
                    d.pageSize = d.length
                    d.order_name = columns[d.order[0].column].data
                    d.order_type = d.order[0].dir
                    return JSON.stringify( d );
                },
                "error": function(xhr, textStatus, errorThrown) {
                    Helpers.alert(AppMessage.E001);
                },
            },
            "processing": true,
            "serverSide": true,
            // data: dataSet,
            "columns": columns,
            pageLength: 10,
            fixedHeader: true,
            responsive: true,
            "sDom": 'rtip',
        });
        $('#products-table2 tbody').on( 'click', '.action-c span', function () {
            var data = table.row( $(this).parents('tr') ).data();
            console.log(data);
            //that.router.navigateByUrl('/product/'+data.id,data);
            that.item_detail = data;
            that.scriptLoader.load('./assets/js/scripts/form-plugins.js');
            //$('.selectpicker').select2();
        } );
        var table = $('#products-table2').DataTable();
        $('#key-search').on('keyup', function() {
            table.search(this.value).draw();
        });
        $('#type-filter').on('change', function() {
            table.column(2).search($(this).val()).draw(); 
        });
        this.api.send({
            url: AppSetting.UOM_LIST_URL
        }).done(function(data){
            if(data.data)
            that.uom_list = data.data.map(function(uom){
                uom.content = '<div>' + uom.code + '</div><div>' + uom.description + '</div>'
                uom.data = uom.description
                return uom;
            });
            console.log(data,'UOM_LIST_URL')
        })
        this.api.send({
            url: AppSetting.GROUP_LIST_URL
        }).done(function(data){
            that.group_list = data.data;
            console.log(data,'GROUP_LIST_URL')
        })
        this.api.send({
            url: AppSetting.TAX_LIST_URL
        }).done(function(data){
            that.tax_list = data.data;
            console.log(data,'TAX_LIST_URL')
        })
        this.api.send({
            url: AppSetting.PARTNER_LIST_URL,
            params: {
                type: 2
            }
        }).done(function(data){
            that.suppier_list = data.data;
            console.log(data,'PARTNER_LIST_URL')
        })
    }
	uomFormat(value) {
		return '<span style="color:red">' + value + '</span>';
	}
    backToList() {
        this.item_detail = undefined
    }
}
