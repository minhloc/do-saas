import { Component, OnInit, AfterViewInit } from '@angular/core';
import { DatePipe, CurrencyPipe } from '@angular/common';
import { APIService } from '../../../api.service';
import {Helpers} from "../../../helpers";
import { AppSetting } from "../../../settings";
import { Router, ActivatedRoute } from '@angular/router';
import { ScriptLoaderService } from '../../../_services/script-loader.service';
import { AppMessage } from "../../../message";

declare var $:any;

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
})
export class CustomersComponent implements OnInit, AfterViewInit {
    item_detail;
    api_type = 1;
    constructor(
        private api: APIService,
        private router: Router,
        private route: ActivatedRoute,
        private scriptLoader: ScriptLoaderService,
    ) { }

    ngOnInit() {
        var urls = this.route.snapshot.url;
        switch (urls[urls.length-1].path) {
            case "customers":
                this.api_type = 1;
                break;
            case "suppliers":
                this.api_type = 2;
                break;
            default:
                this.api_type = 1;
                break;
          }
      
    }

  ngAfterViewInit() {
    let that = this;
    var columns = [
                { "data": "id" },
                { "data": "name" },
                { "data": "email" },
                { "data": "phone" },
                { "data": "balance" },
                {"data":null,className:"action-c", "defaultContent":'<span class="text-light mr-3 font-16"><i class="ti-pencil"></i></span>'}
            ]
        $('#customers-table').DataTable({
            "ajax": {
                "url": AppSetting.PARTNER_LIST_URL,
                "contentType": "application/json",
                "beforeSend": function(xhr){
                    xhr.setRequestHeader("Authorization", "bearer " + localStorage.getItem('token'));
                 },
                "type": "POST",
                // "dataSrc": 'data',
                "dataSrc": function ( data ) {
                    console.log(data,'data');
                    data.recordsFiltered = data.total;
                    data.recordsTotal = data.total;
                    // data.draw = ++draw
                    // delete(data.links)
                    // delete(data.message)
                    // delete(data.meta)
                    // delete(data.status)
                    // data.draw = data.meta.current_page
                    data.data.map(function(row){
                        row.balance = new CurrencyPipe("en-US").transform(row.balance)
                    })
                    return data.data;
                },
                "data": function ( d ) {
                    console.log(d,'d')
                    d.type = that.api_type;
                    d.page = d.start/d.length + 1
                    d.pageSize = d.length
                    d.order_name = columns[d.order[0].column].data
                    d.order_type = d.order[0].dir
                    return JSON.stringify( d );
                },
                "error": function(xhr, textStatus, errorThrown) {
                    Helpers.alert(AppMessage.E001);
                },
            },
            "processing": true,
            "serverSide": true,
            // data: dataSet,
            "columns": columns,
            pageLength: 20,
            fixedHeader: true,
            responsive: true,
            "sDom": 'rtip',
        });
        $('#customers-table tbody').on( 'click', '.action-c span', function () {
            var dataRow = table.row( $(this).parents('tr') ).data();
            console.log(dataRow);
            that.api.get({
                url: AppSetting.PARTNER_DETAIL_URL + dataRow.id
            }).done(function(data){
                console.log(data,'CUSTOMER_DETAIL')
                that.item_detail = data
                that.scriptLoader.load('./assets/js/scripts/form-plugins.js');
            })
        } );

        var table = $('#customers-table').DataTable();
        $('#key-search').on('keyup', function() {
            table.search(this.value).draw();
        });
        $('#type-filter').on('change', function() {
            table.column(2).search($(this).val()).draw();
        });
    }

    backToList() {
        this.item_detail = undefined
    }

}
