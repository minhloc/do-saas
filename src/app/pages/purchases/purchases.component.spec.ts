import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchasesInfoComponent } from './purchases.component';

describe('PurchasesInfoComponent', () => {
  let component: PurchasesInfoComponent;
  let fixture: ComponentFixture<PurchasesInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PurchasesInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchasesInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
