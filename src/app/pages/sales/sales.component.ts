import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ScriptLoaderService } from '../../_services/script-loader.service';
import { APIService } from '../../api.service';
import { Helpers} from "../../helpers";
import { AppSetting } from "../../settings";
import { SystemService } from "../../system.service";
@Component({
  selector: 'app-sales-info',
  templateUrl: './sales.component.html',
})
export class SalesInfoComponent implements OnInit, AfterViewInit {

    constructor(private api: APIService,private sys: SystemService) { }

    ngOnInit() {}
    summary
    ngAfterViewInit() {
        let that= this;
        that.sys.Summary()
            .then(
                (res)=>{
                    that.summary = res
                }
            )
    }

}
