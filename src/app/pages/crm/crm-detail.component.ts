import { Component, OnInit, AfterViewInit, OnChanges,ViewChild, ViewEncapsulation  } from '@angular/core';
import { DatePipe, CurrencyPipe,Location } from '@angular/common';
import { APIService } from '../../api.service';
import { Helpers} from "../../helpers";
import { AppSetting } from "../../settings";
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { SystemService } from "../../system.service";
import { ScriptLoaderService } from '../../_services/script-loader.service';
import { AppMessage } from "../../message";
import { FormsModule } from '@angular/forms';
import * as cloneDeep from 'lodash/cloneDeep';

import { jqxGridComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxgrid';
import { jqxComboBoxComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxcombobox';
import { jqxDropDownListComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxdropdownlist';
import { jqxDateTimeInputComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxdatetimeinput';
import { jqxValidatorComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxvalidator';
import { jqxFileUploadComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxfileupload';

import { GeneralBussinessPtgModule } from '../../widgets/ptg/general/business/ptg-gen-business.module';
import { VatBussinessPtgModule } from '../../widgets/ptg/vat/business/ptg-vat-business.module';
import { CustomerPtgModule } from '../../widgets/ptg/customer/ptg-customer.module';
import { VenderPtgModule } from '../../widgets/ptg/vender/ptg-vender.module';

declare var document:any;
declare var $:any;
declare var toastr:any;
declare var window:any;

@Component({
  selector: 'app-crm',
  templateUrl: './crm-detail.component.html',
  encapsulation: ViewEncapsulation.None
})
export class CrmDetailComponent implements OnInit, AfterViewInit, OnChanges {
    @ViewChild('taxCbx') taxCbx: jqxDropDownListComponent;
    @ViewChild('countryCbx') countryCbx: jqxComboBoxComponent;
    @ViewChild('myValidator') myValidator: jqxValidatorComponent;
    @ViewChild('fileUpload') fileUpload: jqxFileUploadComponent;

    @ViewChild('genBusPtgCbx') genBusPtgCbx: GeneralBussinessPtgModule;
    @ViewChild('vatBusPtgCbx') vatBusPtgCbx: VatBussinessPtgModule;
    @ViewChild('custPtgCbx') custPtgCbx: CustomerPtgModule;
    @ViewChild('vendPtgCbx') vendPtgCbx: VenderPtgModule;

    theme = AppSetting.THEME;
    id;
    setting;
    action = 'list';
    constructor(
        private sys: SystemService,
        private api: APIService,
        private router: Router,
        private route: ActivatedRoute,
        private scriptLoader: ScriptLoaderService,
        private location: Location
    ) {
        
    }

    render(){
        console.log('render')
    }
    ngOnChanges() {
        console.log('ngOnChanges')
    }
    ngOnInit() {
        let that = this;
        let user = that.api.getUserInfo();
        console.log('ngOnInit')
        let settings ={
            'customers':{
                page:'customers',
                title:'Customers',
                type: 1
            },
            'suppliers':{
                page:'suppliers',
                title:'Suppliers',
                type: 2
            }
        };
        this.route.data.subscribe(data => {
            console.log('INIT-PAGE')
            let urls = that.route.snapshot.url;
            that.setting = settings[data.partner_type || 'customers'];
            that.router_params = data;
        });
        this.route.params.subscribe(
            params => {
                that.id = params.id;
                that.showDetail(params.id)
                
            }
        );
        
    }

    router_params
    
    showAddItem(){
        let that = this;
        // that.setData({})
        this.editingItem = cloneDeep({
            type: that.setting.type,
            contacts: [],
            addresses: [],
        });
        
        this.editable = true;
        this.action = "detail";
        // setTimeout(()=>{
            that.initForm()
            
        // },42)
    }
    showDetail(id){
        let that = this;
        if(id){
            that.api.get({
                url: AppSetting.PARTNER_DETAIL_URL + id
            }).done(function(res){
                if(res.data){
                    that.setData(res.data)
                    
                }
            })
        }else{
            // that.showAddItem();
            that.setData({
                type: that.setting.type,
                contacts: [],
                addresses: [],
                images: []
            })
            setTimeout(()=>{
                that.setEditable(true)
            },500)
        }
    }
    showFullDetail(){
        this.action = "detail";
    }

    itemDetail
    setData(data){
        let that = this;
        data.newImages = []
        that.itemDetail = data;

        if(data.gen_business_ptg_code && that.genBusPtgCbx){
            that.genBusPtgCbx.setValue(data.gen_business_ptg_code)
        } else if(that.genBusPtgCbx) that.genBusPtgCbx.setValue(null);
        if(data.vat_business_ptg_code && that.vatBusPtgCbx){
            that.vatBusPtgCbx.setValue(data.vat_business_ptg_code)
        } else if(that.vatBusPtgCbx) that.vatBusPtgCbx.setValue(null);
        if(data.partner_ptg_code && that.custPtgCbx){
            that.custPtgCbx.setValue(data.partner_ptg_code)
        } else if(that.custPtgCbx) that.custPtgCbx.setValue(null);
        if(data.partner_ptg_code && that.vendPtgCbx){
            that.vendPtgCbx.setValue(data.partner_ptg_code)
        } else if(that.vendPtgCbx) that.vendPtgCbx.setValue(null);
    }
    showCreateForm(type){
        // if(type == 2){
        //     alert ('function updating...');
        //     return;
        // }
        this.itemDetail = undefined
        this.resetCreateForm();
    }
    
    resetCreateForm(){
        $('#addEntryFrm').trigger("reset");
        $('#cbx_cost').jqxDropDownList('selectItem', null );
        $('#cbx_country').jqxComboBox('selectItem', null );
        $('#txt_date ').jqxDateTimeInput('setDate', new Date());
        $('#jqxFileUpload').jqxFileUpload('cancelAll');
    }
    
    editable = false;
    editingItem;
    onCancel(){
        if(this.editingItem.id)
            this.setEditable(false);
        else{
            // this.router.navigateByUrl('/' + this.router_params.type + '/' + this.router_params.partner_type);
            this.location.back()
        }
    }
    goback(){
        this.location.back()
    }
    setEditable(editable){
        if(editable){
            this.editingItem = cloneDeep(this.itemDetail);
            this.initForm()
        }else{
            this.editingItem = undefined
        }
        this.editable = editable;
    }
    deleteContact(i){
        this.editingItem.contacts.splice(i,1)
        console.log(i,'i')
    }
    addContact(){
        this.editingItem.contacts.push({})
    }
    deleteAddress(i){
        this.editingItem.addresses.splice(i,1)
        console.log(i,'i')
    }
    addAddress(){
        this.editingItem.addresses.push({})
    }
    partner_counts
    ngAfterViewInit() {
        let that = this;
        console.log('ngAfterViewInit')
        that.sys.PartnerInfo(that.id)
            .then(
                (res)=>{
                    that.partner_counts = res
                }
            )
    }
    tax_data
    taxSetting;
    initTaxCbx(){
        let that = this;
        if(that.taxSetting) return;
        that.api.send({
            url: AppSetting.TAX_LIST_URL,
            params: {
                type: 1
            }
        }).done(function(data){
            that.tax_data = data.data.map(function(d,i){
                d.label = d.name
                return d;
            });
            that.taxSetting = {
                data: that.tax_data,
                source: that.tax_data,//new jqx.dataAdapter(that.tax_data),
                width: '100%',
                height: 30,
                promptText: "Select tax...",
                displayMember: 'label',
                valueMember: 'id',
                selectedIndex: function(index){
                    for (var i = that.tax_data.length - 1; i >= 0; i--) {
                        if(that.editingItem)
                        if(that.tax_data[i].id==that.editingItem.default_tax_group){
                            return i;
                        }
                    }
                    return -1;
                },
                onChange: function(event){
                    var args = event.args;
                    if(args && args.item && args.item.originalItem){
                        var item = args.item.originalItem;
                        that.editingItem.default_tax_group = item.id;
                        that.editingItem.tax_group = item;
                    }
                    // that.editingItem.default_tax_group = 
                }
            }
        })
    }
    countrySetting;
    initCountryCbx(){
        let that = this;
        if(that.countrySetting) return;
        that.api.send({
            url: AppSetting.COUNTRY_LIST_URL,
            params: {
                pageSize: 100
            }
        }).done(function(res){
            var selectedIndex = -1;
            var data = res.data.map(function(d,i){
                d.label = d.description
                if(d.id == that.editingItem.county){
                    selectedIndex = i;
                }
                return d;
            });
            that.countrySetting = {
                data: data,
                source: data,//new jqx.dataAdapter(data),
                width: '100%',
                height: 30,
                promptText: "Select item...",
                displayMember: 'label',
                valueMember: 'id',
                selectedIndex: function(index){
                    for (var i = data.length - 1; i >= 0; i--) {
                        if(that.editingItem)
                        if(data[i].id==that.editingItem.addresses[index].country_id){
                            return i;
                        }
                    }
                    return -1;
                },
                onChange: function(event,i){
                    var args = event.args;
                    if(args && args.item && args.item.originalItem){
                        var item = args.item.originalItem;
                        that.editingItem.addresses[i].country_id = item.id;
                        that.editingItem.addresses[i].country = item;
                    }
                    // that.editingItem.default_tax_group = 
                }
            }
        })
    }
    rules = [];
    initValidation(){

        let that = this;
        if(that.rules){
            if(that.myValidator) that.myValidator.hide();
        }else if(that.myValidator){
            let windowId = that.myValidator.host.attr('id')
            this.rules =
            [
                { input: '#' + windowId+ ' .txt_name', message: 'Name is required!', action: 'keyup, blur', rule: 'required' },
                { input: '#' + windowId+ ' .txt_address', message: 'Address is required!', action: 'keyup, blur', rule: 'required' },
                { input: '#' + windowId+ ' .txt_phone', message: 'Phone is required!', action: 'keyup, blur', rule: 'required' },
                { input: '#' + windowId+ ' .txt_email', message: 'Email is required!', action: 'keyup, blur', rule: 'required' },
                { input: '#' + windowId+ ' .txt_email', message: 'Invalid e-mail!', action: 'keyup', rule: 'email' },
                { input: '#' + windowId+ ' .txt_website', message: 'Website is required!', action: 'keyup, blur', rule: 'required' },
                { input: '#' + windowId+ ' .cbx_gen_product_ptg_code', message: 'Inventory Posting Group is required!', action: 'change', rule: function () {
                        return !!that.genBusPtgCbx.value;
                    } 
                },{ input: '#' + windowId+ ' .cbx_vat_product_ptg_code', message: 'Inventory Posting Group is required!', action: 'change', rule: function () {
                        return !!that.vatBusPtgCbx.value;
                    } 
                },{ input: '#' + windowId+ ' .cbx_partner_ptg_code', message: 'Partner Posting Group is required!', action: 'change', rule: function () {
                        if(that.editingItem.type==1)
                            return !!that.custPtgCbx.value;
                        else return !!that.vendPtgCbx.value;
                    } 
                },
            ];
            that.myValidator.setOptions({rules:that.rules})
        }
    }
    pending
    genBusPtgSetting
    initGenBusPtgCbx(){
        let that = this;
        if(that.genBusPtgSetting) return;
        that.genBusPtgSetting = {
            onOpen: ()=>{
                that.pending = true;
            },
            onClose: ()=>{
                that.pending = false;
                $('.jqx-grid-validation,.jqx-grid-validation-arrow-up').remove()
            },
            onChange: function(event){
                let item = that.genBusPtgCbx.getSelectedItem();
                    if(item){
                        // that.editingItem.gen_business_ptg = item;
                        // that.editingItem.gen_business_ptg_code = item.code
                    }
                    let windowId = that.myValidator.host.attr('id')
                    $('#'+windowId+ ' .cbx_gen_business_ptg_code').change()

                
            },
        }
    }
    vatBusPtgSetting
    initVatBusPtgCbx(){
        let that = this;
        if(that.vatBusPtgSetting) return;
        that.vatBusPtgSetting = {
            onOpen: ()=>{
                that.pending = true;
            },
            onClose: ()=>{
                that.pending = false;
                $('.jqx-grid-validation,.jqx-grid-validation-arrow-up').remove()
            },
            onChange: function(event){
                let item = that.vatBusPtgCbx.getSelectedItem();
                    if(item){
                        // that.editingItem.gen_business_ptg = item;
                        // that.editingItem.vat_business_ptg_code = item.code
                    }
                    let windowId = that.myValidator.host.attr('id')
                    $('#'+windowId+ ' .cbx_vat_business_ptg_code').change()

                
                
            },
        }
    }
    custPtgSetting
    initCustPtgCbx(){
        let that = this;
        if(that.custPtgSetting) return;
        that.custPtgSetting = {
            onOpen: ()=>{
                that.pending = true;
            },
            onClose: ()=>{
                that.pending = false;
                $('.jqx-grid-validation,.jqx-grid-validation-arrow-up').remove()
            },
            onChange: function(event){
                let item = that.custPtgCbx.getSelectedItem();
                    if(item){
                        // that.editingItem.gen_product_ptg = item;
                        // that.editingItem.vat_product_ptg_code = item.code
                    }
                    let windowId = that.myValidator.host.attr('id')
                    $('#'+windowId+ ' .cbx_partner_ptg_code').change()

                
            },
        }
    }
    vendPtgSetting
    initVendPtgCbx(){
        let that = this;
        if(that.vendPtgSetting) return;
        that.vendPtgSetting = {
            onOpen: ()=>{
                that.pending = true;
            },
            onClose: ()=>{
                that.pending = false;
                $('.jqx-grid-validation,.jqx-grid-validation-arrow-up').remove()
            },
            onChange: function(event){
                let item = that.vendPtgCbx.getSelectedItem();
                    if(item){
                        // that.editingItem.gen_product_ptg = item;
                        // that.editingItem.vat_product_ptg_code = item.code
                    }
                    let windowId = that.myValidator.host.attr('id')
                    $('#'+windowId+ ' .cbx_partner_ptg_code').change()

                
            },
        }
    }
    initForm(){
        let that = this;
        that.initGenBusPtgCbx()
        that.initVatBusPtgCbx()
        that.initCustPtgCbx()
        that.initVendPtgCbx()

        that.initTaxCbx()
        that.initCountryCbx()
        that.initValidation()
    }
    onSelectPhoto(event){
        let that = this;
        var args = event.args;
        var fileName = args.file;
        var fileSize = args.size;
        console.log(args,that.fileUpload)
        var files = $(that.fileUpload.elementRef.nativeElement).find('input[type="file"]').get().map(function(f){
            return f.files[0]
        }).filter(function(f){
            return f;
        });

        // var images = files.map(function(file,i){
        Helpers.getBase64(files[files.length-1]).then(
          data => {
                that.fileUpload.cancelAll();
                that.editingItem.newImages = [{
                    filename: fileName,
                    photo:data
                }]
        //       // console.log(data)
        //        $('#jqxFileUpload .jqx-file-upload-file-row:eq('+i+') img').remove();
        //        $('#jqxFileUpload .jqx-file-upload-file-row:eq('+i+')').prepend($('<img/>').attr('src',data))
          }
        )
    }
    onAddItem() {
        let that = this;
        // if(this.myValidator.validate()){

        // }
        // Helpers.confirm('<h3>Are you sure you want to approve selected expense(s)?</h3>',function(){
            let url = AppSetting.PARTNER_CREATE_URL;
            let method = 'post';
            if(that.editingItem.id){
                url = AppSetting.PARTNER_UPDATE_URL + that.editingItem.id;
                method = 'put';
            }
            var formData = cloneDeep(this.editingItem);
            console.log(formData)
            var images = formData.addImages
            // delete formData.images;
            formData.images = formData.newImages
            delete formData.addImages;
            that.api[method]({
                url: url,
                params: formData,
                
            }).done(function(res){
                if(that.editingItem.id){
                    that.itemDetail = that.editingItem;
                    that.setEditable(false)
                }else{
                    // that.router.navigateByUrl('/' + that.router_params.type + '/' + that.router_params.partner_type + '/'+res.data.id);
                }
                window.history.back(1)
                that.editingItem = undefined
            })
        // })
    }
    getAvatar(){
        if(this.editingItem && this.editingItem.newImages && this.editingItem.newImages[0]){
            return this.editingItem.newImages[0].photo;
        }
        if(this.editingItem && this.editingItem.images && this.editingItem.images[0]){
            return this.editingItem.images[0];
        }
        return '';
    }
}
