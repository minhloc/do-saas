import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrmDetailComponent } from './crm-detail.component';

describe('CrmDetailComponent', () => {
  let component: CrmDetailComponent;
  let fixture: ComponentFixture<CrmDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrmDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrmDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
