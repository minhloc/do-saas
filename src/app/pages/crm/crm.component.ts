import { Component, OnInit, AfterViewInit, OnChanges,ViewChild, ViewEncapsulation  } from '@angular/core';
import { DatePipe, CurrencyPipe,DecimalPipe } from '@angular/common';
import { APIService } from '../../api.service';
import { Helpers} from "../../helpers";
import { AppSetting } from "../../settings";
import { SystemService } from "../../system.service";
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { ScriptLoaderService } from '../../_services/script-loader.service';
import { AppMessage } from "../../message";
import { FormsModule } from '@angular/forms';
import * as cloneDeep from 'lodash/cloneDeep';

import { jqxGridComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxgrid';
import { jqxComboBoxComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxcombobox';
import { jqxDropDownListComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxdropdownlist';
import { jqxDateTimeInputComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxdatetimeinput';
import { jqxValidatorComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxvalidator';

declare var $:any;
declare var toastr:any;

@Component({
  selector: 'app-crm',
  templateUrl: './crm.component.html',
})
export class CrmComponent implements OnInit, AfterViewInit, OnChanges {
    @ViewChild('gridPartner') gridPartner: jqxGridComponent;
    theme = AppSetting.THEME;
    setting;
    action = 'list';
    constructor(
        private sys: SystemService,
        private api: APIService,
        private router: Router,
        private route: ActivatedRoute,
        private scriptLoader: ScriptLoaderService,
    ) {
        
    }

    render(){
        console.log('render')
    }
    ngOnChanges() {
        console.log('ngOnChanges')
    }
    ngOnInit() {


        let that = this;
        let user = that.api.getUserInfo();
        console.log('ngOnInit')
        let settings ={
            'customers':{
                page:'customers',
                title:'Customers',
                type: 1,
                action:{
                    create: false,
                    edit: true,
                    approve: false
                },
            },
            'suppliers':{
                page:'suppliers',
                title:'Suppliers',
                type: 2,
                action:{
                    create: false,
                    edit: false,
                    approve: true
                },
            }
        };
        this.route.data.subscribe(data => {
            console.log('INIT-PAGE',data)
            let urls = that.route.snapshot.url;
            that.isMy = data.my;
            that.setting = settings[data.partner_type || 'customers'];
            that.myActivity = data.my || false;
            that.isfavourites = data.favourites || false;
            that.router_params = data;
            that.prefixmy = data.my?'my':'';
            that.is360 = data.is360;
            that.prefix360 = data.is360?'360/':'';
            that.loadDataAdapter()
            that.initGrid();
        });
        this.route.params.subscribe(
            params => {
            }
        );

        
    }
    isMy
    prefix360
    prefixmy
    is360
    router_params
    isfavourites
    myActivity
    dataAdapter;
    entrySource;
    gridElmId = '#myGrid';
    loadDataAdapter(){
        let that = this;
        let conditions = [['type','=', that.setting.type]];
        if(that.isfavourites){
            conditions.push(['is_favorites','=', 1]);
        }
        if(that.myActivity){
            let user = that.api.getUserInfo();
            if(user) conditions.push(['created_by','=',user.id])
        }
        that.entrySource = Helpers.Source({
            datatype: "json",
            type: 'POST',
            // url: AppSetting.PARTNER_LIST_URL,
            // data: {
            //     type: that.setting.type,
            //     pageSize: 1000,
            //     "order_name" : "name",
            //     "order_type" : "asc",
            // },
            url: AppSetting.COMMON_BINDING_URL,
            data: {
                table:'business_partner',
                conditions: conditions,
                orders:{'id':'desc'}
            },
            datafields: [
                { name: 'id', type: 'int' },
                { name: 'No', type: 'int' },
                { name: 'name', type: 'string' },
                { name: 'label', type: 'string' },
                { name: 'phone', type: 'string' },
                { name: 'email', type: 'string' },
                { name: 'balance', type: 'number' },
                { name: 'status', type: 'number' },
                { name: 'is_favorites', type: 'bool' },
            ],
            addrow: function (rowid, rowdata, position, commit) {
                // synchronize with the server - send insert command
                // call commit with parameter true if the synchronization with the server is successful 
                //and with parameter false if the synchronization failed.
                // you can pass additional argument to the commit callback which represents the new ID if it is generated from a DB.
                commit(true);
            },
            filter: function() {
                // update the grid and send a request to the server.
                that.gridPartner.updatebounddata('filter');
            },
            sort: function() {
                // update the grid and send a request to the server.
                that.gridPartner.updatebounddata('sort');
            },
            id: 'id',
            root: 'data',
            // async: false
            // localData: data
        })
        // that.gridPartner.setOptions({source:[]});
        that.dataAdapter = new jqx.dataAdapter(that.entrySource, {
            beforeLoadComplete: function (records) {
                let index = 0;
                index = that.gridPartner.getselectedrowindex();
                if(index==-1 || records.length-1 < index) index = 0;
                if(records[index]) that.showDetail(records[index].id)
                for (var i = 0; i < records.length; i++) {
                    var row = records[i];
                    
                    // records[i].date = new DatePipe("en-US").transform(row.date * 1000, 'yyyy-MM-dd');
                }
                return records;
            }
        });
    }
    gridSetting;
    columns
    isMybusiness
    onFilterChange(){
        this.loadDataAdapter()
        this.initGrid();
    }
    initGrid(){
        let that = this;
        
        let columns = [
                { 
                    text: 'ID', dataField: 'id', width: 80 ,hidden: true
                },{  
                
                    text: Helpers.translate.get('Name'), dataField: 'name',editable: false,
                    cellsrenderer: function (row, column, value,html,setting,data) {
                        return [
                        '<div style="padding: 5px">',
                            data.No||'',
                        '<br/>',
                        '<b>',
                            $('<div/>').text(data.name).prop('innerHTML'),
                        '</b>',
                        '</div>',
                        ].join('')
                    }
                },{  
                    text: '', dataField: 'is_favorites' ,width: 80, columntype: 'checkbox',
                    filterable: false,
                    cellclassname: function (row, columnfield, value) {
                        return 'is_favorites'
                    }
                // },{ 
                //     text: 'phone', dataField: 'phone' ,width: 120, hidden: true
                // },{ 
                //     text: 'email', dataField: 'email', width: 180, hidden: true
                // },{ 
                //     text: 'balance', dataField: 'balance', width: 120, hidden: true,
                //     cellsalign: 'right', 
                //     cellsformat: 'd'
                }
            ]
        let onCellClick = function (event) {
            // event arguments.
            var args = event.args;
            // row's bound index.
            var rowBoundIndex = args.rowindex;
            // row's visible index.
            var rowVisibleIndex = args.visibleindex;
            // right click.
            var rightclick = args.rightclick; 
            // original event.
            var ev = args.originalEvent;
            // column index.
            var columnindex = args.columnindex;
            // column data field.
            var dataField = args.datafield;
            // cell value
            var value = args.value;
            if(args && args.row){
                var dataRow = args.row.bounddata
                console.log(dataRow,'dataRow')
                if(dataRow) {
                    that.showDetail(dataRow.id)
                }
            }
        }
        if(that.gridSetting){
            // setTimeout(() => {
                that.gridPartner.setOptions({source:that.dataAdapter});
            // }, 200)
            // that.gridPartner.setOptions({source:that.dataAdapter});
            console.log('HERE')
            return;
        }
        Helpers.translate.do(()=>{
            if(that.gridPartner){
                that.gridPartner.setcolumnproperty('id','text', '#')
                that.gridPartner.setcolumnproperty('name','text', Helpers.translate.get('Name'))
                that.gridPartner.setOptions({'localization': Helpers.getLocalizatio(Helpers.translate.activeLang)})
            }
        })
        that.gridSetting = {
            columns: columns,
            source: that.dataAdapter,
            onCellClick: onCellClick,
            rendergridrows: (params: any): any[] => {
                return that.dataAdapter.records;
            },
            onCellBeginEdit(event: any): void {
                let args = event.args;
            },
            onCellEndEdit(event: any): void {
                let args = event.args;
                let row = args.row;
                if (args.datafield === 'is_favorites') {
                    console.log(args.value,'args.value')
                    that.api.put({
                        url: AppSetting.PARTNER_UPDATE_URL+row.id,
                        params: {
                            is_favorites: + args.value
                        },
                        onDone:function(res){
                            
                        },
                        onFail:function(){
                            toastr.warning('Fail to add favorites')
                        },
                    })
                }
            },
        }
        // if($(that.gridElmId).hasClass('jqx-widget')){
        //     $(that.gridElmId).jqxGrid({
        //         source: that.dataAdapter,
        //     });
        //     return;
        // }
    }
    partner_counts
    showDetail(id){
        let that = this;
        that.api.get({
            url: AppSetting.PARTNER_DETAIL_URL + id
        }).done(function(res){
            console.log(res,'RES')
            if(res.data){
                that.setData(res.data)
                that.loadReciept()
            }
        })
        that.sys.PartnerInfo(id)
            .then(
                (res)=>{
                    that.partner_counts = res
                }
            )
    }
    recie_pay_data;
    loadReciept(){
        let that = this
        that.api.post({
            url: AppSetting.RE_PAY_BYPARTNER_URL + that.itemDetail.id,
            params: {
                type: that.itemDetail.type
            }
        }).done(function(data){
            if(data.data){
                data.data.map(function(row){
                    row.date = new DatePipe("en-US").transform(row.date * 1000, 'dd/MM/yyyy');
                    row.due_date = new DatePipe("en-US").transform(row.due_date * 1000, 'dd/MM/yyyy');
                    row.total_with_tax = new DecimalPipe("en-US").transform(row.total_with_tax)
                })
                that.recie_pay_data = data.data;
            }
        })
    }
    itemDetail
    setData(data){
        let that = this;
        if(data.type == 1){
            data.link = 'customers';
        }else if(data.type == 2){
            data.link = 'suppliers';
        }
        that.itemDetail = data;
    }
    summary
    ngAfterViewInit() {
        let that = this;
        that.sys.Summary()
            .then(
                (res)=>{
                    that.summary = res
                }
            )
        
    }
}
