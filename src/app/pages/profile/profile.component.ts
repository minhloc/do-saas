import { Component, OnInit, AfterViewInit,ViewChild } from '@angular/core';
import { APIService } from '../../api.service';
import { Helpers } from "../../helpers";
import { AppSetting } from "../../settings";
import { SystemService } from "../../system.service"
import { TranslateService } from './../../translate.service';
import { jqxFileUploadComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxfileupload';
declare var document:any;
declare var $:any;
declare var toastr:any;
declare var window:any;
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
})
export class ProfileComponent implements OnInit, AfterViewInit {
    @ViewChild('fileUpload') fileUpload: jqxFileUploadComponent;

  constructor(
        private sys: SystemService,
      private api: APIService,
      private translate: TranslateService
      ) {

  }
    summary
    theme = AppSetting.THEME

  ngOnInit() {
      this.user = this.api.getUserInfo();
      this.sys.Summary()
            .then(
                (res)=>{
                    this.summary = res

                }
            )
      this.profile={
          name:this.user.username,
          old_password:'',
          password:'',
          confirm_password:'',
      }
  }
  user
  ngAfterViewInit() { }
  getAvatar(){
    if(this.user && this.user.new_avatar){
        return this.user.new_avatar.photo ||  '';
    }
    if(this.user){
        return this.user.avatar1 || '';
    }
    return '';
  }
  onSelectPhoto(event){
      let that = this;
        var args = event.args;
        var fileName = args.file;
        var fileSize = args.size;
        console.log(args,that.fileUpload)
        var files = $(that.fileUpload.elementRef.nativeElement).find('input[type="file"]').get().map(function(f){
            return f.files[0]
        }).filter(function(f){
            return f;
        });

        // var images = files.map(function(file,i){
        Helpers.getBase64(files[files.length-1]).then(
          data => {
                that.fileUpload.cancelAll();
                that.user.new_avatar = {
                    filename: 'avatar',
                    photo:data
                }
        //       // console.log(data)
        //        $('#jqxFileUpload .jqx-file-upload-file-row:eq('+i+') img').remove();
        //        $('#jqxFileUpload .jqx-file-upload-file-row:eq('+i+')').prepend($('<img/>').attr('src',data))
          }
        )
  }
  profile: any;
  update_avatar(){
        let that = this;
        that.api.post({
            url: AppSetting.UPDATE_PROFILE_URL,
            params: {
                avatar: [that.user.new_avatar]
            }
        }).done(function(res){
            if(res.data){
                that.user.avatar1 = res.data.avatar1
                localStorage.setItem('user', JSON.stringify(that.user));
                toastr.success(Helpers.translate.get('Change avatar success'))

            }
        })
  }
  change_password(){
        let that = this;
        that.api.put({
            url: AppSetting.CHANGE_PASSWORD_URL,
            params: that.profile
        }).done(function(res){
            // if(res.data){
                that.profile.old_password = '';
                that.profile.password = '';
                that.profile.confirm_password = '';
            // }
            toastr.success(Helpers.translate.get('Change password success'))
        })
  }
  update_profile(){
        let that = this;
        that.api.post({
            url: AppSetting.UPDATE_PROFILE_URL,
            params: {
                name: that.profile.name
            }
        }).done(function(res){
            if(res.data){
                that.user.username = that.profile.name
                localStorage.setItem('user', JSON.stringify(that.user));
                toastr.success(Helpers.translate.get('Update profile success'))

            }
        })
  }
}
