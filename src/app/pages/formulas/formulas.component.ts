import { Component, OnInit, AfterViewInit, OnChanges,ViewChild, ViewEncapsulation  } from '@angular/core';
import { DatePipe, CurrencyPipe } from '@angular/common';
import { APIService } from '../../api.service';
import { Helpers} from "../../helpers";
import { AppSetting } from "../../settings";
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { AppMessage } from "../../message";
import { FormsModule } from '@angular/forms';
import * as cloneDeep from 'lodash/cloneDeep';


declare var $:any;
declare var document:any;
declare var toastr:any;

@Component({
  selector: 'app-formulas',
  templateUrl: './formulas.component.html',
})
export class FormulasComponent implements OnInit, AfterViewInit {
    constructor(
        private api: APIService,
        private router: Router,
        private route: ActivatedRoute
    ) { }

    theme = AppSetting.THEME;
    ngOnInit() {
        let that = this;
        this.route.params.subscribe(
            params => {
            }
        );
    }
    
    ngAfterViewInit() {

    }
}
