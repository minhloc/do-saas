import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Activity360Component } from './360.component';

describe('Activity360Component', () => {
  let component: Activity360Component;
  let fixture: ComponentFixture<Activity360Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Activity360Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Activity360Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
