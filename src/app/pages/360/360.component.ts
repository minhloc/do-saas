import { Component, OnInit, AfterViewInit, OnChanges,ViewChild, ViewEncapsulation  } from '@angular/core';
import { DatePipe, CurrencyPipe } from '@angular/common';
import { APIService } from '../../api.service';
import { Helpers} from "../../helpers";
import { AppSetting } from "../../settings";
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { AppMessage } from "../../message";
import { TranslateService } from './../../translate.service';
declare var $:any;
declare var toastr:any;
import { SystemService } from "../../system.service"

@Component({
  selector: 'app-360-activity',
  templateUrl: './360.component.html',
})
export class Activity360Component implements OnInit, AfterViewInit, OnChanges {
    
    setting;
    theme = AppSetting.THEME
    constructor(
        private api: APIService,
        private router: Router,
        private route: ActivatedRoute,
        private sys: SystemService,
        private translate: TranslateService,
    ) {
        console.log(translate.data,'data,translate.data');
    }
    render(){
        console.log('render')
    }
    testOnChange(event){
        console.log(event)
    }
    ngOnChanges() {
        console.log('ngOnChanges')
    }
    ngOnInit() {
        let that = this;
        let user = that.api.getUserInfo();
        let settings ={
            'my-activity':{
                page:'my',
                title:'My Activity',
            },
            'all-activity':{
                page:'',
                title:'All Activity',
            }
        };
        this.route.data.subscribe(data => {
            that.router_params = data;
            that.isMy = data.my;
            that.prefix_360 = data.is360?'360/':''
        });
        this.route.params.subscribe(
            params => {
                let urls = that.route.snapshot.url;
                let uparams = that.route.snapshot.params;
                that.setting = settings[uparams.type];
                that.isMy = that.setting.page == 'my';
                that.loadInfo()
            }
        );
        
    }
    router_params
    prefix_360
    summary
    isMy = false
    loadInfo(){
        let that = this;
        that.sys.Summary()
            .then(
                (res)=>{
                    that.summary = res

                }
            )
    }
    ngAfterViewInit(){
         let that = this;
        
    }
    onActivityChange(){

    }
}
