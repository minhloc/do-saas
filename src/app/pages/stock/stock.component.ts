import { Component, OnInit, AfterViewInit, OnChanges,ViewChild, ViewEncapsulation  } from '@angular/core';
import { DatePipe, CurrencyPipe } from '@angular/common';
import { APIService } from '../../api.service';
import { Helpers} from "../../helpers";
import { AppSetting } from "../../settings";
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { ScriptLoaderService } from '../../_services/script-loader.service';
import { AppMessage } from "../../message";
import { FormsModule } from '@angular/forms';
import * as cloneDeep from 'lodash/cloneDeep';

import { jqxGridComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxgrid';
import { jqxComboBoxComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxcombobox';
import { jqxDropDownListComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxdropdownlist';
import { jqxDateTimeInputComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxdatetimeinput';
import { jqxValidatorComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxvalidator';
import { jqxFileUploadComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxfileupload';


import { jqxWindowComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxwindow';
import { jqxTabsComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxtabs';

import { ItemGroupModule } from '../../widgets/item-group/item-group.module';
import { GeneralProductPtgModule } from '../../widgets/ptg/general/product/ptg-gen-product.module';
import { VatProductPtgModule } from '../../widgets/ptg/vat/product/ptg-vat-product.module';
import { InventoryPtgModule } from '../../widgets/ptg/inventory/ptg-inventory.module';

declare var $:any;
declare var document:any;
declare var toastr:any;

@Component({
  selector: 'app-stock',
  templateUrl: './stock.component.html',
})
export class StockComponent implements OnInit, AfterViewInit {
    constructor(
        private api: APIService,
        private router: Router,
        private route: ActivatedRoute,
        private scriptLoader: ScriptLoaderService,
    ) { }
    @ViewChild('grid') grid: jqxGridComponent;
    @ViewChild('groupGrid') groupGrid: jqxGridComponent;
    @ViewChild('supplierCbx') supplierCbx: jqxComboBoxComponent;
    @ViewChild('saleTaxCbx') saleTaxCbx: jqxDropDownListComponent;
    @ViewChild('purchaseTaxCbx') purchaseTaxCbx: jqxDropDownListComponent;
    @ViewChild('baseUOMCbx') baseUOMCbx: jqxDropDownListComponent;
    @ViewChild('itemUOMCbx') itemUOMCbx: jqxDropDownListComponent;
    @ViewChild('fileUpload') fileUpload: jqxFileUploadComponent;
    @ViewChild('validator') validator: jqxValidatorComponent;


    @ViewChild('itemGroupCbx') itemGroupCbx: ItemGroupModule;
    @ViewChild('genProdPtgCbx') genProdPtgCbx: GeneralProductPtgModule;
    @ViewChild('vatProdPtgCbx') vatProdPtgCbx: VatProductPtgModule;
    @ViewChild('invtPtgCbx') invtPtgCbx: InventoryPtgModule;

    theme = AppSetting.THEME;
    ngOnInit() {
        let that = this;
        this.route.params.subscribe(
            params => {
                // that.loadDataAdapter();
                // that.initGrid();
            }
        );
    }
    source;
    dataAdapter;
    gridSetting;
    group_data;
    groupSource: any = Helpers.Source({
        dataType: "json",
        type: 'POST',
        datafields: [
            { name: 'id', type: 'number' },
            { name: 'code', type: 'string' },
            { name: 'description', type: 'string' }
        ],
        data: {
            pageSize: 1000,
        },
        root: 'data',
        id: 'id',
        url: AppSetting.GROUP_LIST_URL,
        async: false,
        addrow: function (rowid, rowdata, position, commit) {
            // synchronize with the server - send insert command
            // call commit with parameter true if the synchronization with the server is successful 
            //and with parameter false if the synchronization failed.
            // you can pass additional argument to the commit callback which represents the new ID if it is generated from a DB.
            commit(true);
        },
    })
    groupAdapter: any = new jqx.dataAdapter(this.groupSource, {
        // autoBind: true,
        // beforeLoadComplete: (records: any[]): any[] => {
        //     // let data = new Array();
        //     // update the loaded records. Dynamically add EmployeeName and EmployeeID fields. 
        //     for (let i = 0; i < records.length; i++) {
                
        //     }
        //     return records;
        // }
    });
    loadDataAdapter(){
        let that = this;

        that.api.send({
            url: AppSetting.GROUP_LIST_URL
        }).done(function(res){
            that.group_data = res.data;
        })
        that.source = Helpers.Source({
            dataType: "json",
            type: 'POST',
            url: AppSetting.COMMON_BINDING_URL,
            data: {
                table:'products'
            },
            dataFields: [
                { name: 'id', type: 'number' },
                { name: 'product_code', type: 'string' },
                { name: 'description', type: 'string' },
                // { name: 'posting_date', type: 'date' },
                // { name: 'total_credit', type: 'number'},
                { name: 'current_stock', type: 'number'},
                { name: 'purchase_price', type: 'number'},
                { name: 'sale_price', type: 'number'},
                { name: 'status', type: 'bool'},
                // { name: 'item_group_id', type: 'number'},
                { 
                    name: 'item_group_id', 
                    //value: 'item_group_id', 
                    values: { 
                        source: that.groupAdapter.records, value: 'id', name: 'description' 
                    } 
                },
            
            ],
            filter: function() {
                // update the grid and send a request to the server.
                that.grid.updatebounddata('filter');
            },
            sort: function() {
                // update the grid and send a request to the server.
                that.grid.updatebounddata('sort');
            },
            id: 'id',
            root: 'data',
            sortcolumn: 'id',
            sortdirection: 'desc'
        });
        // that.gridPartner.setOptions({source:[]});
        that.dataAdapter = new jqx.dataAdapter(that.source, {
            beforeLoadComplete: function (records) {
                for (var i = 0; i < records.length; i++) {
                    var row = records[i];
                }
                return records;
            }
        });
    }

    initGrid(){
        let that = this;
        
        let columns = [
                { 
                    text: 'ID', dataField: 'id', width: 80 , hidden: true
                },{ 
                    text: 'Group', datafield: 'item_group_id', //displayfield: 'group', 
                    // columntype: 'dropdownlist', 
                    filtertype: 'list',
                    createfilterwidget: function (column, columnElement, widget) {
                        widget.jqxDropDownList({ 
                            source: that.groupAdapter,
                            displayMember: 'description',
                            valueMember: 'id',
                            
                        });
                    },
                    // cellsrenderer: function (row, datafield, value) {
                    //     console.log(arguments)
                    //     return value;
                    // },
                    width: 150 
                },{ 
                    text: 'Code', dataField: 'product_code', width: 100 ,
                },{ 
                    text: 'Description', dataField: 'description',
                },{ 
                    text: 'Current Stock', dataField: 'current_stock', width: 100 ,//hidden: true
                    cellsalign: 'right',
                },{  
                    text: 'Purchase Price', dataField: 'purchase_price', width: 100 ,//hidden: true
                    cellsalign: 'right', 
                    cellsformat: 'd'
                },{ 
                    text: 'Sale Price', dataField: 'sale_price', width: 100 ,//hidden: true
                    cellsalign: 'right', 
                    cellsformat: 'd'
                },{ 
                    text: 'Status', dataField: 'status', width: 80 , columntype: 'checkbox',
                    filtertype: 'bool'
                }
            ]
        let onCellClick = function (event) {
            // event arguments.
            var args = event.args;
            // row's bound index.
            var rowBoundIndex = args.rowindex;
            // row's visible index.
            var rowVisibleIndex = args.visibleindex;
            // right click.
            var rightclick = args.rightclick; 
            // original event.
            var ev = args.originalEvent;
            // column index.
            var columnindex = args.columnindex;
            // column data field.
            var dataField = args.datafield;
            // cell value
            var value = args.value;
            if(args && args.row){
                var dataRow = args.row.bounddata
                console.log(dataRow,'dataRow')
                if(dataRow) {
                    that.showDetailForm(dataRow.id)
                }
            }
        }
        if(that.gridSetting){
            that.grid.setOptions({source:that.dataAdapter});
            return;
        }
        that.gridSetting = {
            ready: function() {
                that.initCbxs();
            },
            columns: columns,
            source: that.dataAdapter,
            onCellClick: onCellClick,
            rendergridrows: (params: any): any[] => {
                return that.dataAdapter.records;
            },
        }
        // if($(that.gridElmId).hasClass('jqx-widget')){
        //     $(that.gridElmId).jqxGrid({
        //         source: that.dataAdapter,
        //     });
        //     return;
        // }
    }
    editingItem:any = {};
    itemGroupSetting
    initGroupCbx(){
        let that = this;
        if(that.itemGroupSetting) return;
        that.itemGroupSetting = {

            onChange: (event)=>{
                // let item = that.itemGroupCbx.getSelectedItem();
                // if(item && that.editingItem){
                //     // that.editingItem.gen_business_ptg = item;
                //     // that.editingItem.item_group_id = item.id
                // }
                let windowId = that.validator.host.attr('id')
                $('#'+windowId+ ' .cbx_group').change()

                
            },
        }
    }
    genProdPtgSetting
    initGenProdPtgCbx(){
        let that = this;
        if(that.genProdPtgSetting) return;
        that.genProdPtgSetting = {
            onChange: (event)=>{
                // let item = that.itemGroupCbx.getSelectedItem();
                // if(item && that.editingItem){
                //     // that.editingItem.gen_business_ptg = item;
                //     // that.editingItem.item_group_id = item.id
                // }
                let windowId = that.validator.host.attr('id')
                $('#'+windowId+ ' .cbx_gen_product_ptg_code').change()

                
            },
        }
    }
    vatProdPtgSetting
    initVatProdPtgCbx(){
        let that = this;
        if(that.vatProdPtgSetting) return;
        that.vatProdPtgSetting = {
            onChange: (event)=>{
                // let item = that.itemGroupCbx.getSelectedItem();
                // if(item && that.editingItem){
                //     // that.editingItem.gen_business_ptg = item;
                //     // that.editingItem.item_group_id = item.id
                // }
                let windowId = that.validator.host.attr('id')
                $('#'+windowId+ ' .cbx_vat_product_ptg_code').change()

                
            },
        }
    }
    invtPtgSetting
    initInvtPtgCbx(){
        let that = this;
        if(that.invtPtgSetting) return;
        that.invtPtgSetting = {
            onChange: (event)=>{
                // let item = that.itemGroupCbx.getSelectedItem();
                // if(item && that.editingItem){
                //     // that.editingItem.gen_business_ptg = item;
                //     // that.editingItem.item_group_id = item.id
                // }
                let windowId = that.validator.host.attr('id')
                $('#'+windowId+ ' .cbx_inventory_ptg_code').change()

                
            },
        }
    }
    initCbxs(){
        this.initGroupCbx();
        this.initGenProdPtgCbx();
        this.initVatProdPtgCbx();
        this.initInvtPtgCbx();
    }
    uomSetting
    initUOMCbx(){
        let that = this;
        if(!that.uomSetting){

            that.api.send({
                url: AppSetting.UOM_LIST_URL,
            }).done(function(res){
                // var selectedIndex = -1;
                var data = res.data.map(function(d,i){
                    d.label = d.description
                    // if(d.id == that.editingItem.county){
                        // selectedIndex = i;
                    // }
                    return d;
                });
                that.uomSetting = {
                    data: data,
                    source: data,//new jqx.dataAdapter(data),
                    width: '100%',
                    height: 30,
                    promptText: "Select item...",
                    displayMember: 'label',
                    valueMember: 'id',
                    onBaseChange: function(event){
                        var args = event.args;
                        if(args && args.item && args.item.originalItem){
                            var item = args.item.originalItem;
                            that.editingItem.uom_id = item.id;
                            that.editingItem.get_u_o_m = item;
                        }
                        // that.editingItem.default_tax_group = 
                    },
                    onItemChange: function(event){
                        var args = event.args;
                        if(args && args.item && args.item.originalItem){
                            var item = args.item.originalItem;
                            that.editingItem.item_uom_id = item.id;
                            that.editingItem.item_uom = item;
                        }
                        // that.editingItem.default_tax_group = 
                    }
                }
            })
        }
    }
    tax_data
    taxSetting;
    initTaxCbx(){
        let that = this;
        if(that.taxSetting) return;
        that.api.send({
            url: AppSetting.TAX_LIST_URL,
            params: {
                type: 1
            }
        }).done(function(data){
            that.tax_data = data.data.map(function(d,i){
                d.label = d.name + '(' + d.value + '%)';
                return d;
            });
            that.taxSetting = {
                data: that.tax_data,
                source: that.tax_data,//new jqx.dataAdapter(that.tax_data),
                width: '100%',
                height: 30,
                promptText: "Select tax...",
                displayMember: 'label',
                valueMember: 'id',
                onChange: function(event,type){
                    var args = event.args;
                    if(args && args.item && args.item.originalItem){
                        var item = args.item.originalItem;
                        that.editingItem[type+'_id'] = item.id;
                        that.editingItem[type] = item;
                    }
                    // that.editingItem.default_tax_group = 
                }
            }
        })
    }
    supplierSetting;
    initSupplierCbx(){
        let that = this;
        if(that.supplierSetting) return;
        that.api.send({
            url: AppSetting.PARTNER_LIST_URL,
            params: {
                pageSize: 1000,
                type: 2
            }
        }).done(function(res){
            var data = res.data.map(function(d,i){
                d.label = d.No + ' ' + d.name;
                return d;
            });
            that.supplierSetting = {
                source: data,//new jqx.dataAdapter(that.tax_data),
                width: '100%',
                height: 30,
                promptText: "Select item...",
                displayMember: 'label',
                valueMember: 'id',
                onChange: function(event){
                    var args = event.args;
                    if(args && args.item && args.item.originalItem){
                        var item = args.item.originalItem;
                        that.editingItem.supplier_id = item.id;
                        that.editingItem.supplier = item;
                    }

                    // that.editingItem.default_tax_group = 
                }
            }
        })
    }
    rules
    initValidation(){
        let that = this;
        let frmId = that.validator.host.attr('id')
        if(that.rules){
            this.validator.hide();
        }else
        this.rules =
        [
            { input: '#' + frmId+ ' .txt_description', message: 'This is required!', action: 'keyup, blur', rule: 'required' },
            { input: '#' + frmId+ ' .txt_product_code', message: 'This is required!', action: 'keyup, blur', rule: 'required' },
            { input: '#' + frmId+ ' .txt_sale_price_exclude_tax', message: 'This is required!', action: 'keyup, blur', rule: 'required' },
            { input: '#' + frmId+ ' .txt_purchase_price_exclude_tax', message: 'This is required!', action: 'keyup, blur', rule: 'required' },
            { input: '#' + frmId+ ' .txt_case', message: 'This is required!', action: 'keyup,blur', rule: 'required' },
            { 
                input: '#' + frmId+ ' .cbx_group', message: 'This is required!', action: 'change, select', 
                rule: function () {
                    return !!that.itemGroupCbx.value;
                } 
            },{ 
                input: '#' + frmId+ ' .cbx_gen_product_ptg_code', message: 'This is required!', action: 'change, select', 
                rule: function () {
                    return !!that.genProdPtgCbx.value;
                } 
            },{ 
                input: '#' + frmId+ ' .cbx_vat_product_ptg_code', message: 'This is required!', action: 'change, select', 
                rule: function () {
                    return !!that.vatProdPtgCbx.value;
                } 
            },{ 
                input: '#' + frmId+ ' .cbx_invt_ptg_code', message: 'This is required!', action: 'change, select', 
                rule: function () {
                    return !!that.invtPtgCbx.value;
                } 
            }

        ];
    }
    initForm(){
        let that = this;
        that.initCbxs()
        that.initUOMCbx()
        that.initTaxCbx()
        that.initSupplierCbx()
        that.initValidation()
    }

    setData(data){
        let that = this;
        that.editingItem = data;
        if(data.item_group && that.itemGroupCbx){
            data.item_group_id=data.item_group.id
            that.itemGroupCbx.setValue(data.item_group_id)
        } else if(that.itemGroupCbx) that.itemGroupCbx.setValue(null);

        if(data.gen_product_ptg_code && that.genProdPtgCbx){
            that.genProdPtgCbx.setValue(data.gen_product_ptg_code)
        } else if(that.genProdPtgCbx) that.genProdPtgCbx.setValue(null);

        if(data.vat_product_ptg_code && that.vatProdPtgCbx){
            that.vatProdPtgCbx.setValue(data.vat_product_ptg_code)
        } else if(that.vatProdPtgCbx) that.vatProdPtgCbx.setValue(null);

        if(data.invt_ptg_code && that.invtPtgCbx){
            that.invtPtgCbx.setValue(data.invt_ptg_code)
        } else if(that.invtPtgCbx) that.invtPtgCbx.setValue(null);

        if(data.get_u_o_m && that.baseUOMCbx){
            data.uom_id=data.get_u_o_m.id
            that.baseUOMCbx.val(data.uom_id)
        } else if(that.baseUOMCbx) that.baseUOMCbx.selectItem(null);
        if(data.item_uom && that.itemUOMCbx){
            data.item_uom_id=data.item_uom.id
            that.itemUOMCbx.val(data.item_uom_id)
        } else if(that.itemUOMCbx) that.itemUOMCbx.selectItem(null);
        if(data.sale_tax && that.saleTaxCbx){
            data.sale_tax_id=data.sale_tax.id
            that.saleTaxCbx.val(data.sale_tax_id)
        } else if(that.saleTaxCbx) that.saleTaxCbx.selectItem(null);
        if(data.purchase_tax && that.purchaseTaxCbx){
            data.purchase_tax_id=data.purchase_tax.id
            that.purchaseTaxCbx.val(data.purchase_tax_id)
        } else if(that.purchaseTaxCbx) that.purchaseTaxCbx.selectItem(null);
        if(data.supplier && that.supplierCbx){
            // data.supplier_id=data.supplier.id
            that.supplierCbx.val(data.supplier_id)
        } else if(that.supplierCbx) that.supplierCbx.selectItem(null);


    }
    showDetailForm(id = 0){
        let that = this;
        this.editable = true;
        that.initForm()

        if(id){
            that.api.get({
                url: AppSetting.PRODUCT_DETAIL_URL + id
            }).done(function(res){
                if(res.data){
                    that.setData(res.data)
                }
            })
        }else{
            that.setData({
                id:0,
                current_stock: 0,
                unit_price:0
            })
        }
    }
    onSetDefaultPhoto(udid){
        this.editingItem.default_image = udid
    }
    onSelectPhoto(event){
        let that = this;
        var args = event.args;
        var fileName = args.file;
        var fileSize = args.size;
        console.log(args,that.fileUpload)
        var files = $(that.fileUpload.elementRef.nativeElement).find('input[type="file"]').get().map(function(f){
            return f.files[0]
        }).filter(function(f){
            return f;
        });

        // var images = files.map(function(file,i){
        Helpers.getBase64(files[files.length-1]).then(
          data => {
                that.fileUpload.cancelAll();
                if(!that.editingItem.newImages) that.editingItem.newImages = []
                let key = new Date().getTime()
                if(!that.editingItem.default_image) that.editingItem.default_image = key
                that.editingItem.newImages.push({
                    filename: fileName,
                    photo:data,
                    udid: key
                })
        //       // console.log(data)
        //        $('#jqxFileUpload .jqx-file-upload-file-row:eq('+i+') img').remove();
        //        $('#jqxFileUpload .jqx-file-upload-file-row:eq('+i+')').prepend($('<img/>').attr('src',data))
          }
        )
    }
    editable = false;
    onCancel(){
        this.editable = false;
    }
    checkValidate(): void {
        setTimeout(() => {
            this.validator.validate();
        }, 200)
    }
    onValidationSuccess() : void {
        let that = this;
        

        // Helpers.confirm('<h3>Are you sure you want to approve selected expense(s)?</h3>',function(){
            let url = AppSetting.PRODUCT_CREATE_URL;
            let method = 'post';
            if(that.editingItem.id){
                url = AppSetting.PRODUCT_UPDATE_URL + that.editingItem.id;
                // method = 'put';
            }
            var formData = cloneDeep(this.editingItem);
            formData.sale_price = formData.sale_price_exclude_tax
            formData.sale_price_include_tax = formData.sale_price_exclude_tax * (1 + formData.sale_tax.value / 100)
            formData.purchase_price = formData.purchase_price_exclude_tax
            formData.purchase_price_include_tax = formData.purchase_price_exclude_tax * (1 + formData.purchase_tax.value / 100)
            formData.sale_tax_group_id = formData.sale_tax_id
            formData.purchase_tax_group_id = formData.purchase_tax_id
            formData.item_uom = formData.item_uom_id

            console.log(formData)
            var images = formData.addImages
            delete formData.images;
            formData.images = formData.newImages
            // delete formData.addImages;
            that.api[method]({
                url: url,
                params: formData,
                
            }).done(function(res){
                that.dataAdapter.dataBind()
                that.onCancel()
            })
        // })
    }
    ngAfterViewInit() {

    }
    showGroupPopup(){
        this.itemGroupCbx.open()
    }
}
