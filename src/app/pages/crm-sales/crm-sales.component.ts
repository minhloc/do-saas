import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ScriptLoaderService } from '../../_services/script-loader.service';
import { DatePipe, CurrencyPipe,DecimalPipe } from '@angular/common';
import { APIService } from '../../api.service';
import { Helpers} from "../../helpers";
import { AppSetting } from "../../settings";
import { SystemService } from "../../system.service";
declare var $:any;
declare var Chart:any;
declare var document:any;
@Component({
  selector: 'app-crm-sales',
  templateUrl: './crm-sales.component.html',
})
export class CRMSalesComponent implements OnInit, AfterViewInit {

    constructor(private api: APIService,private sys: SystemService) { }
    chartColors = {"red":"rgb(255, 99, 132)","orange":"rgb(255, 159, 64)","yellow":"rgb(255, 205, 86)","green":"rgb(75, 192, 192)","blue":"rgb(54, 162, 235)","purple":"rgb(153, 102, 255)","grey":"rgb(201, 203, 207)"};
    ngOnInit() {}
    system_info
    dashboard_datav1
    ngAfterViewInit() {
        let that= this;
        that.sys.FinanceInfo()
            .then(
                (res)=>{
                    that.system_info = res
                }
            )
        that.sys.DashboardV1()
            .then(
                (res:any)=>{
                    that.dashboard_datav1 = res.data
                    setTimeout(()=>{
                      $('.easypie').each(function(){
                          $(this).easyPieChart({
                            trackColor: $(this).attr('data-trackColor') || '#f2f2f2',
                            scaleColor: false,
                          });
                      }); 
                    },300)
                    that.pieline_and_active_opp()
                    that.number_of_opp_by_week()
                    that.pieline_by_opp_size()
                    that.total_sale_qty_product()
                    that.top_products()
                    that.won_lost_trend()
                    that.pieline_by_week_product()
                }
            )
    }
    //Pieline and Active Opp
    //opporttunity_of_salesperson
    pieline_and_active_opp(){
        let that = this;
        let data = that.dashboard_datav1.opporttunity_of_salesperson;
        let barData = {
            labels: data.map((row)=>{
                return row.name
            }),
            
            datasets: [
                {
                    // label: "Data 1",
                    // backgroundColor:'#DADDE0', //'rgba(220, 220, 220, 0.5)',
                    data: data.map((row)=>{
                        return row.value
                    }),
                    "fill": false,
                    "backgroundColor": [
                      "#009688",
                      "rgba(255, 205, 86, 1)",
                      "rgba(75, 192, 192, 1)",
                      "rgba(54, 162, 235, 1)",
                      "rgba(153, 102, 255, 1)",
                      "rgba(201, 203, 207, 1)"
                    ],
                    "borderColor": [
                      "#009688",
                      "rgb(255, 205, 86)",
                      "rgb(75, 192, 192)",
                      "rgb(54, 162, 235)",
                      "rgb(153, 102, 255)",
                      "rgb(201, 203, 207)"
                    ],
                    "borderWidth": 0
                }
            ]
        };
        var barOptions = {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                xAxes: [{
                  ticks: {
                    beginAtZero: true,
                    
                  }
                }],
                yAxes: [{
                    ticks: {
                        fixedStepSize: 1
                    }
                }],
            },
            legend :{
                display:false
            },
            tooltips: {
                mode: 'label',
                label: 'mylabel',
            }
        };

        var ctx = document.getElementById("pieline_and_active_opp").getContext("2d");
        new Chart(ctx, {
            type: 'bar', 
            data: barData, 
            options:barOptions,
        });
    }
    //
    //opportunity_by_week
    number_of_opp_by_week(){
        let that = this;
        let data = that.dashboard_datav1.opportunity_by_week.sort((a,b)=>{
            return a.opportunity_week - b.opportunity_week
        });
        let barData = {
            labels: data.map((row)=>{
                return row.opportunity_week
            }),
            
            datasets: [
                {
                    // label: "Data 1",
                    backgroundColor:'#4CAF50', //'rgba(220, 220, 220, 0.5)',
                    data: data.map((row)=>{
                        return row.value
                    }),
                    "fill": false,
                    // "backgroundColor": [
                    //   "#4CAF50",
                    //   "#4CAF50",
                    //   "#4CAF50",
                    //   "#4CAF50",
                    //   "#4CAF50",
                    //   "#4CAF50",
                    // ],
                    // "borderColor": [
                    //   "#4CAF50",
                    //   "rgb(255, 205, 86)",
                    //   "rgb(75, 192, 192)",
                    //   "rgb(54, 162, 235)",
                    //   "rgb(153, 102, 255)",
                    //   "rgb(201, 203, 207)"
                    // ],
                    "borderWidth": 0
                }
            ]
        };
        var barOptions = {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                xAxes: [{
                  ticks: {
                    beginAtZero: true,
                    
                  }
                }],
                yAxes: [{
                    ticks: {
                        fixedStepSize: 1
                    }
                }],
            },
            legend :{
                display:false
            },
            tooltips: {
                mode: 'label',
                label: 'mylabel',
            }
        };

        var ctx = document.getElementById("number_of_opp_by_week").getContext("2d");
        new Chart(ctx, {
            type: 'bar', 
            data: barData, 
            options:barOptions,
        });
    }
    //opportunity_by_size
    pieline_by_opp_size(){
        let that = this;
        let data = that.dashboard_datav1.opportunity_by_size.sort((a,b)=>{
            return a.opportunity_week - b.opportunity_week
        });
        let sizes = {'1':'Extra Big', '2': 'Large','3': 'Medium','4': 'Small'}
        let barData = {
            datasets: [{
                data: data.map((row)=>{
                    return row.value
                }),
                backgroundColor: [
                    that.chartColors.red,
                    that.chartColors.orange,
                    that.chartColors.yellow,
                    that.chartColors.green,
                    that.chartColors.blue,
                ],
                label: 'Dataset 1'
            }],
            labels: data.map((row)=>{
                return sizes[row.size]
            }),
        };
        var barOptions = {
        };

        var ctx = document.getElementById("pieline_by_opp_size").getContext("2d");
        new Chart(ctx, {
            type: 'pie', 
            data: barData, 
            options:barOptions,
        });
    }
    //opportunity_by_month
    won_lost_trend(){
        let that = this;
        let data = that.dashboard_datav1.opportunity_by_month.sort((a,b)=>{
            return a.month - b.month
        });
        let barData = {
            labels: data.map((row)=>{
                return row.month + '/'+row.year
            }),
            
            datasets: [
                {
                    label: "Won",
                    backgroundColor: that.chartColors.green,
                    borderColor: that.chartColors.green,
                    data: data.map((row)=>{
                        return +row.win
                    }),
                    fill: 'none',
                },{
                    label: "Lost",
                    backgroundColor: that.chartColors.red,
                    borderColor: that.chartColors.red,
                    data: data.map((row)=>{
                        return +row.lost
                    }),
                    fill: 'none',
                }
            ]
        };
        var barOptions = {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                xAxes: [{
                  ticks: {
                    beginAtZero: true,
                    callback: function(value, index, values) {
                      if(parseInt(value) >= 1000){
                        return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                      } else {
                        return value;
                      }
                    }
                  }
                }]
            },
            legend :{
                // display:false
            },
            tooltips: {
                mode: 'label',
                label: 'mylabel',
                callbacks: {
                    label: function (tooltipItem, data) {
                        let name = data.datasets[tooltipItem.datasetIndex].label
                        var value:any = Number(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]);
                        if(parseInt(value) >= 1000){
                            return name+':'+value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                        } else {
                            return name+':'+value;
                        }
                    }, },
            }
        };

        var ctx = document.getElementById("won_lost_trend").getContext("2d");
        var ctx = document.getElementById("won_lost_trend").getContext("2d");
        new Chart(ctx, {
            type: 'line', 
            data: barData, 
            options:barOptions,
        });
    }
    //total_sale_qty_product
    total_sale_qty_product(){
        let that = this;
        let data = that.dashboard_datav1.total_sale_qty_product
        let barData = {
            labels: data.map((row)=>{
                return row.name
            }),
            
            datasets: [
                {
                    // label: "Data 1",
                    backgroundColor:'#4CAF50', //'rgba(220, 220, 220, 0.5)',
                    data: data.map((row)=>{
                        return +row.quantity
                    }),
                    "fill": false,
                    // "backgroundColor": [
                    //   "#4CAF50",
                    //   "#4CAF50",
                    //   "#4CAF50",
                    //   "#4CAF50",
                    //   "#4CAF50",
                    //   "#4CAF50",
                    // ],
                    // "borderColor": [
                    //   "#4CAF50",
                    //   "rgb(255, 205, 86)",
                    //   "rgb(75, 192, 192)",
                    //   "rgb(54, 162, 235)",
                    //   "rgb(153, 102, 255)",
                    //   "rgb(201, 203, 207)"
                    // ],
                    "borderWidth": 0
                }
            ]
        };
        var barOptions = {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                xAxes: [{
                  ticks: {
                    beginAtZero: true,
                    
                  }
                }],
                yAxes: [{
                    ticks: {
                        fixedStepSize: 1
                    }
                }],
            },
            legend :{
                display:false
            },
            tooltips: {
                mode: 'label',
                label: 'mylabel',
            }
        };

        var ctx = document.getElementById("total_sale_qty_product").getContext("2d");
        new Chart(ctx, {
            type: 'horizontalBar', 
            data: barData, 
            options:barOptions,
        });
    }

    //product_by_week
    pieline_by_week_product_legend
    pieline_by_week_product(){
        let that = this;
        let series = {}
        let data = that.dashboard_datav1.product_by_week.sort((a,b)=>{
            return a.week - b.week
        });
        data.map((row)=>{
            row.products.map((p)=>{
                if(!series[p.product_id]) series[p.product_id] = {}
                series[p.product_id].title = p.description
            })
        })
        let series_data = [];
        let keys = Object.keys(series)
        keys.map((k)=>{
            series[k].data = new Array(data.length).fill(0);
            data.map((row,i)=>{
                row.products.map((p)=>{
                    if(p.product_id == k){
                        series[k].data[i] = +p.total
                    }
                })
            })
            
        });
        console.log(series,'series')
        let barData = {
            labels: data.map((row)=>{
                return row.week
            }),
            
            datasets: keys.map((k)=>{
                return {
                    label:series[k].title,
                    data: series[k].data,
                    backgroundColor:'#'+(Math.random()*0xFFFFFF<<0).toString(16)
                }
            })
            // [
            //     {
            //         label: "Data 1",
            //         // backgroundColor:'#4CAF50', //'rgba(220, 220, 220, 0.5)',
            //         data: data.map((row)=>{
            //             if(row.products[0]) return +row.products[0].total
            //                return 0
            //         }),
            //         "fill": false,
            //         "backgroundColor": [
            //           "rgba(255, 99, 132, 1)",
            //           "rgba(255, 205, 86, 1)",
            //           "rgba(75, 192, 192, 1)",
            //           "rgba(54, 162, 235, 1)",
            //           "rgba(153, 102, 255, 1)",
            //           "rgba(201, 203, 207, 1)"
            //         ],
            //         "borderColor": [
            //           "rgb(255, 99, 132)",
            //           "rgb(255, 205, 86)",
            //           "rgb(75, 192, 192)",
            //           "rgb(54, 162, 235)",
            //           "rgb(153, 102, 255)",
            //           "rgb(201, 203, 207)"
            //         ],
            //         "borderWidth": 0
            //     }
            // ]
        };
        var barOptions = {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    stacked: true
                }],
                xAxes: [{
                   stacked: true,
                  ticks: {
                    beginAtZero: true,
                    callback: function(value, index, values) {
                      if(parseInt(value) >= 1000){
                        return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                      } else {
                        return value;
                      }
                    }
                  }
                }]
            },
            legend :{
                display:false
            },
            tooltips: {
                mode: 'label',
                label: 'mylabel',
                callbacks: {
                    label: function (tooltipItem, data) {
                        var value:any = Number(data.datasets[0].data[tooltipItem.index]);
                        let name = data.datasets[tooltipItem.datasetIndex].label
                        if(Math.abs(parseInt(value)) >= 1000){
                            return name + ':' + new DecimalPipe("en-US").transform(value);
                        } else {
                            return name + ':' + new DecimalPipe("en-US").transform(value);
                        }
                    }, 
                },
            }
        };

        var ctx = document.getElementById("pieline_by_week_product").getContext("2d");
        var myChart = new Chart(ctx, {
            type: 'bar', 
            data: barData, 
            options:barOptions,
        });
        setTimeout(()=>{
            if($('#pieline_by_week_product_legend').html()=="")
            $('#pieline_by_week_product_legend').html( myChart.generateLegend());
        },300)
    }
    //top_products
    top_products(){
        let that = this;
        let data = that.dashboard_datav1.top_products
        let barData = {
            labels: data.map((row)=>{
                return row.name
            }),
            
            datasets: [
                {
                    label: "",
                    // backgroundColor:'#4CAF50', //'rgba(220, 220, 220, 0.5)',
                    data: data.map((row)=>{
                        return +row.value
                    }),
                    "fill": false,
                    "backgroundColor": [
                      "rgba(255, 99, 132, 1)",
                      "rgba(255, 205, 86, 1)",
                      "rgba(75, 192, 192, 1)",
                      "rgba(54, 162, 235, 1)",
                      "rgba(153, 102, 255, 1)",
                      "rgba(201, 203, 207, 1)"
                    ],
                    "borderColor": [
                      "rgb(255, 99, 132)",
                      "rgb(255, 205, 86)",
                      "rgb(75, 192, 192)",
                      "rgb(54, 162, 235)",
                      "rgb(153, 102, 255)",
                      "rgb(201, 203, 207)"
                    ],
                    "borderWidth": 0
                }
            ]
        };
        var barOptions = {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                  ticks: {
                    beginAtZero: true,
                    callback: function(value, index, values) {
                        value=+value||0
                      let unit = '';
                        if(Math.abs(value) >= 1000) {
                            unit = 'K';
                            value/=1000;
                        }
                        if(Math.abs(value) >= 1000) {
                            unit = 'M';
                            value/=1000;
                        }
                        if(Math.abs(value) >= 1000) {
                            unit = 'B';
                            value/=1000;
                        }
                      if(Math.abs(parseInt(value)) >= 1000){
                        return  new DecimalPipe("en-US").transform(value) + unit;
                      } else {
                        return new DecimalPipe("en-US").transform(value) + unit;
                      }
                    }
                  }
                }]
            },
            legend :{
                display:false
            },
            tooltips: {
                mode: 'label',
                label: 'mylabel',
                callbacks: {
                    label: function (tooltipItem, data) {
                         var value:any = Number(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]);
                        let unit = '';
                        if(Math.abs(value) >= 1000) {
                            unit = 'K';
                            value/=1000;
                        }
                        if(Math.abs(value) >= 1000) {
                            unit = 'M';
                            value/=1000;
                        }
                        if(Math.abs(value) >= 1000) {
                            unit = 'B';
                            value/=1000;
                        }
                        // value = Math.round(value)
                        let name = data.datasets[tooltipItem.datasetIndex].label
                        if(Math.abs(parseInt(value)) >= 1000){
                            return new DecimalPipe("en-US").transform(value)+ unit;
                        } else {
                            return new DecimalPipe("en-US").transform(value)+ unit;
                        }
                    }, 
                },
            }
        };

        var ctx = document.getElementById("top_products").getContext("2d");
        new Chart(ctx, {
            type: 'bar', 
            data: barData, 
            options:barOptions,
        });
    }

}
