import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CRMSalesComponent } from './crm-sales.component';

describe('CRMSalesComponent', () => {
  let component: CRMSalesComponent;
  let fixture: ComponentFixture<CRMSalesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CRMSalesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CRMSalesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
