import { Component, OnInit, AfterViewInit, OnChanges,ViewChild, ViewEncapsulation  } from '@angular/core';
import { DatePipe, CurrencyPipe } from '@angular/common';
import { APIService } from '../../api.service';
import { Helpers} from "../../helpers";
import { AppSetting } from "../../settings";
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { ScriptLoaderService } from '../../_services/script-loader.service';
import { AppMessage } from "../../message";

import { jqxGridComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxgrid';
import { jqxComboBoxComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxcombobox';
import { jqxDropDownListComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxdropdownlist';
import { jqxDateTimeInputComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxdatetimeinput';
import { jqxValidatorComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxvalidator';
import { jqxFileUploadComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxfileupload';

import { jqxWindowComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxwindow';
import { jqxTabsComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxtabs';

import { ActivityModule } from '../../widgets/activity/activity.module';


declare var $:any;
declare var toastr:any;

@Component({
  selector: 'app-expenses',
  templateUrl: './expenses.component.html',
})
export class ExpensesComponent implements OnInit, AfterViewInit, OnChanges {
    @ViewChild('grid') grid: jqxGridComponent;
    @ViewChild('dateDti') dateDti: jqxDateTimeInputComponent;
    @ViewChild('activityMod') activityMod: ActivityModule;
    setting;
    theme = AppSetting.THEME
    constructor(
        private api: APIService,
        private router: Router,
        private route: ActivatedRoute,
        private scriptLoader: ScriptLoaderService,
    ) {
        
    }
    render(){
        console.log('render')
    }
    testOnChange(event){
        console.log(event)
    }
    ngOnChanges() {
        console.log('ngOnChanges')
    }
    ngOnInit() {
        let that = this;
        let user = that.api.getUserInfo();
        console.log('ngOnInit')
        let settings ={
            'my':{
                page:'my',
                title:'My Expenses',
                type: [1,2],
                status: [1,2,3],
                hideStatusColumn: false,
                selectionmode: 'singlerow',
                employee_id: [user.id],
                action:{
                    create: true,
                    edit: true,
                    approve: false
                },
            },
            'all':{
                page:'all',
                title:'All Expenses',
                type: [1,2],
                status: [1,2,3],
                hideStatusColumn: false,
                employee_id: null,
                selectionmode: 'singlerow',
                action:{
                    create: false,
                    edit: true,
                    approve: false
                },
            },
            'review':{
                page:'review',
                title:'Expenses Review',
                type: [1,2],
                status: [1],
                employee_id: null,
                action:{
                    create: false,
                    edit: false,
                    approve: true
                },
                hideStatusColumn: true,
                selectionmode: 'checkbox'
            }
        };
        this.route.params.subscribe(
            params => {
                let urls = that.route.snapshot.url;
                let uparams = that.route.snapshot.params;
                that.setting = settings[uparams.type];
                that.backToList();
                that.loadDataAdapter()
                that.initGrid();
            }
        );
        
    }
    dataAdapter;
    entrySource;
    loadDataAdapter(){
        let that = this;
        that.entrySource = Helpers.Source({
            datatype: "json",
            type: 'POST',
            url: AppSetting.EXPENSES_LIST_URL,
            data: {
                type: that.setting.type,
                filter_status: that.setting.status,
                employee_id: that.setting.employee_id,
                pageSize: 1000,
                "order_name" : "date",
                "order_type" : "desc",
            },
            datafields: [
                { name: 'id', type: 'int' },
                { name: 'city', type: 'string' },
                { name: 'date', type: 'string' },
                { name: 'purpose', type: 'string' },
                { name: 'amount', type: 'number' },
                { name: 'type', type: 'number' },
                { name: 'paid_by', type: 'number' },
                { name: 'status', type: 'number' },
            ],
            addrow: function (rowid, rowdata, position, commit) {
                // synchronize with the server - send insert command
                // call commit with parameter true if the synchronization with the server is successful 
                //and with parameter false if the synchronization failed.
                // you can pass additional argument to the commit callback which represents the new ID if it is generated from a DB.
                commit(true);
            },
            // filter: function() {
            //     // update the grid and send a request to the server.
            //     that.grid.updatebounddata('filter');
            // },
            // sort: function() {
            //     // update the grid and send a request to the server.
            //     that.grid.updatebounddata('sort');
            // },
            id: 'id',
            root: 'data',
            // localData: data
        })
        that.dataAdapter = new $.jqx.dataAdapter(that.entrySource, {
            beforeLoadComplete: function (records) {
                for (var i = 0; i < records.length; i++) {
                    var row = records[i];
                    records[i].date = new DatePipe("en-US").transform(row.date * 1000, 'yyyy-MM-dd');
                }
                return records;
            }
        });
    }
    gridSetting
    initGrid(){
        let that = this;

        
        let columns = [
            { 
                text: 'ID', dataField: 'id', width: 80 ,hidden: true
            },{ 
                text: Helpers.translate.get('Type'), dataField: 'type', width: 100,
                renderer: function (text, align, columnsheight) {
                    return '<div class="column-header-two-line">'+Helpers.translate.get('Type')+'</div>';
                },
                filtertype: 'list',
                createfilterwidget: function (column, columnElement, widget) {
                    widget.jqxDropDownList({ 
                        // source:[
                        //     {"label":"Expenses","value":1},
                        //     {"label":"Mileage","value":2},
                        // ] 
                        placeHolder: "Select:",
                        renderer: function (index, label, value) {
                            console.log(value,'value')
                            var classs = ['','fa fa-credit-card','fa fa-taxi'];
                            var name = Helpers.translate.get(['All','Expenses','Mileage']);
                            return '<span class="cell-ico '+classs[value]+'"></span> ' + (name[value]|| Helpers.translate.get('All')); 
                        }
                    });
                },
                cellsrenderer: function (row, datafield, value) {
                    var classs = ['','fa fa-credit-card','fa fa-taxi'];
                    var name = Helpers.translate.get(['All','Expenses','Mileage']);
                    return '<div style="padding: 8px;"><span class="cell-ico '+classs[value]+'"></span> ' + name[value] + '</div>'; 
                }
            },{ 
                text: Helpers.translate.get('Paid by'), dataField: 'paid_by', width: 100,
                renderer: function (text, align, columnsheight) {
                    return '<div class="column-header-two-line">'+Helpers.translate.get('Paid by')+'</div>';
                },
                filtertype: 'list',
                createfilterwidget: function (column, columnElement, widget) {
                    widget.jqxDropDownList({ 
                        // source:[
                        //     {"label":"Expenses","value":1},
                        //     {"label":"Mileage","value":2},
                        // ] 
                        placeHolder: "Select:",
                        renderer: function (index, label, value) {
                            var classs = ['','fa fa-building','fa fa-user'];
                            var name = Helpers.translate.get(['All','Company','Employee']);
                            return '<span class="cell-ico '+classs[value]+'"></span> ' + (name[value] || Helpers.translate.get('All')); 
                        }
                    });
                },
                cellsrenderer: function (row, datafield, value) {
                    var classs = ['','fa fa-building','fa fa-user'];
                    var name = Helpers.translate.get(['All','Company','Employee']);
                    return '<div style="padding: 8px;"><span class="cell-ico '+classs[value]+'"></span> ' + name[value] + '</div>'; 
                }
            // },{ 
            //     text: 'City', dataField: 'city'
            },{ 
                text: Helpers.translate.get('Date'), dataField: 'date', width: 100 ,
                renderer: function (text, align, columnsheight) {
                    return '<div class="column-header-two-line">'+Helpers.translate.get('Date')+'</div>';
                },
            },{  
                text: Helpers.translate.get('Purpose'), dataField: 'purpose' ,
                renderer: function (text, align, columnsheight) {
                    return '<div class="column-header-two-line">'+Helpers.translate.get('Purpose')+'</div>';
                },
            },{ 
                text: Helpers.translate.get('Amount'), dataField: 'amount', width: 120, 
                renderer: function (text, align, columnsheight) {
                    return '<div class="column-header-two-line">'+Helpers.translate.get('Amount')+'</div>';
                },
                cellsalign: 'right', 
                cellsformat: 'd'
            },{ 
                text: Helpers.translate.get('Status'), dataField: 'status', width: 100, 
                renderer: function (text, align, columnsheight) {
                    return '<div class="column-header-two-line">'+Helpers.translate.get('Status')+'</div>';
                },
                // hidden: that.setting.hideStatusColumn,
                filtertype: 'list',
                createfilterwidget: function (column, columnElement, widget) {
                    widget.jqxDropDownList({ 
                        // source:[
                        //     {"label":"Expenses","value":1},
                        //     {"label":"Mileage","value":2},
                        // ] 
                        placeHolder: "Select:",
                        renderer: function (index, label, value) {
                            var classs = ['','fa fa-hourglass-half','fa fa-check','fa fa-ban'];
                            var name = Helpers.translate.get(['All','Pending','Approved','Rejected']);
                            return '<span class="cell-ico '+classs[value]+'"></span> ' + (name[value]|| Helpers.translate.get('All')); 
                        }
                    });
                },
                cellsrenderer: function (row, datafield, value) {
                    var classs = ['','fa fa-hourglass-half','fa fa-check','fa fa-ban'];
                    var name = Helpers.translate.get(['All','Pending','Approved','Rejected']);
                    return '<div style="padding: 8px;"><span class="cell-ico '+classs[value]+'"></span> ' + name[value] + '</div>'; 
                }
            },
        ]
        let onCellClick = function (event) {
            // event arguments.
            var args = event.args;
            // row's bound index.
            var rowBoundIndex = args.rowindex;
            // row's visible index.
            var rowVisibleIndex = args.visibleindex;
            // right click.
            var rightclick = args.rightclick; 
            // original event.
            var ev = args.originalEvent;
            // column index.
            var columnindex = args.columnindex;
            // column data field.
            var dataField = args.datafield;
            // cell value
            var value = args.value;
            if(args && args.row){
                var dataRow = args.row.bounddata
                console.log(dataRow,'dataRow')
                if(dataRow) {
                    that.showDetail(dataRow.id)
                }
            }
        }
        if(that.gridSetting){
            that.grid.setOptions({
                source:that.dataAdapter,
                selectionmode: that.setting.selectionmode,
            });
            return;
        }
        that.gridSetting = {
            selectionmode: that.setting.selectionmode,
            columns: columns,
            source: that.dataAdapter,
            onCellClick: onCellClick,
            rendergridrows: (params: any): any[] => {
                return that.dataAdapter.records;
            },
        }

    }
    showDetail(id){
        let that = this;
        that.api.get({
            url: AppSetting.EXPENSES_DETAIL_URL + id
        }).done(function(res){
            if(res.data){
                that.setData(res.data)
                that.isCreateAction = res.data.type;
                that.initFormControl();
                that.initValidation();
            }
        })
    }
    itemDetail
    editable
    setData(data){
        let that = this;
        that.itemDetail = data;
        that.editable = false;
        if(
            that.itemDetail.status == 1 && that.setting.action.edit
            ){
            that.editable = true;
        }
        if(data.type==1){
            if(data.country) $('#cbx_country').val(data.country.id)

            $('#txt_city').val(data.city)
            if(data.cost_center) $('#cbx_cost').val(data.cost_center.id)
        }
        if(data.type==2){
            if(data.start_country)  $('#cbx_start_country').val(data.start_country.id||'')
            $('#txt_start_city').val(data.start_city)
            if(data.end_country)  $('#cbx_end_country').val(data.end_country.id||'')
            $('#txt_end_city').val(data.end_city)
            $('#txt_distance').val(data.distance)
        }
        $('#txt_amount').val(data.amount)
        $('#txt_purpose').val(data.purpose)
        $('[name="paid_by"][value="'+data.paid_by+'"]').prop('checked',true)
        if(data.activity_id && that.activityMod){
            that.activityMod.setValue(data.activity_id)
        } else if(that.activityMod) that.activityMod.setValue(null);
        
        if(that.dateDti){
            if(data.date){
                that.dateDti.setDate(new Date(new DatePipe("en-US").transform(data.date * 1000, 'yyyy-MM-dd')));
            }else{
                that.dateDti.setDate(new Date())
            }
        }
    }
    showDetailForm(id,type = 1){
        let that= this;
        that.initFormControl()
        
        if(id){
            that.api.get({
                url: AppSetting.EXPENSES_DETAIL_URL + id,
            }).done(function(res){
                if(res.data){
                    that.setData(res.data);
                    that.initValidation();
                }
            })
        }else{
            $('#addEntryFrm').trigger("reset");
            that.setData({
                type: type,
                status: 1
            });
            that.initValidation();
        }
    }
    // showCreateForm(type){
    //     // if(type == 2){
    //     //     alert ('function updating...');
    //     //     return;
    //     // }
    //     this.isCreateAction = type;
    //     this.itemDetail = undefined
    //     this.editable = true;
    //     this.initFormControl();
    //     this.resetCreateForm();
    // }
    initValidation(){
        let that = this;
        $('#addEntryFrm').jqxValidator('hide');
        var rules;
        if(that.itemDetail.type==1){
            rules = [
                    { input: '#txt_amount', message: Helpers.translate.get('Amount') + Helpers.translate.get(' should be not empty'), action: 'keyup, blur', rule: 'required' },
                    // { input: '#rad_paid_by', message: 'Paid by is required!', action: 'keyup, blur', rule: 'required' },
                    { input: '#txt_city', message: Helpers.translate.get('City') + Helpers.translate.get(' should be not empty'), action: 'keyup, blur', rule: 'required' },
                    { input: '#txt_purpose', message: Helpers.translate.get('Purpose') + Helpers.translate.get(' should be not empty'), action: 'keyup, blur', rule: 'required' },
                    { 
                        input: '#cbx_cost', message: Helpers.translate.get('Cost Center') + Helpers.translate.get(' should be not empty'), action: 'select, blur', 
                        rule: function () {
                            var value = $("#cbx_cost").val();
                            return value!="";
                        } 
                    },
                    { 
                        input: '#cbx_country', message: Helpers.translate.get('Country') + Helpers.translate.get(' should be not empty'), action: 'select, blur', 
                        rule: function () {
                            var value = $("#cbx_country").val();
                            return value!="";
                        } 
                    }
                    // { input: '#journal_entry_credit', message: 'Credit is required!', action: 'keyup, blur', rule: 'required' },
                    // { input: '#journal_entry_debit', message: 'Debit is required!', action: 'keyup, blur', rule: 'required' },
            ]
        }else{
            rules = [
                    { input: '#txt_amount', message: Helpers.translate.get('Amount') + Helpers.translate.get(' should be not empty'), action: 'keyup, blur', rule: 'required' },
                    // { input: '#rad_paid_by', message: 'Paid by is required!', action: 'keyup, blur', rule: 'required' },
                    { input: '#txt_start_city', message: Helpers.translate.get('City') + Helpers.translate.get(' should be not empty'), action: 'keyup, blur', rule: 'required' },
                    { input: '#txt_end_city', message: Helpers.translate.get('City') + Helpers.translate.get(' should be not empty'), action: 'keyup, blur', rule: 'required' },
                    { input: '#txt_distance', message: Helpers.translate.get('Distance') + Helpers.translate.get(' should be not empty'), action: 'keyup, blur', rule: 'required' },
                    { input: '#txt_purpose', message: Helpers.translate.get('Purpose') + Helpers.translate.get(' should be not empty'), action: 'keyup, blur', rule: 'required' },
                    
                    { 
                        input: '#cbx_start_country', message: Helpers.translate.get('Country') + Helpers.translate.get(' should be not empty'), action: 'select, blur', 
                        rule: function () {
                            var value = $("#cbx_start_country").val();
                            return value!="";
                        } 
                    },
                    { 
                        input: '#cbx_end_country', message: Helpers.translate.get('Country') + Helpers.translate.get(' should be not empty'), action: 'select, blur', 
                        rule: function () {
                            var value = $("#cbx_end_country").val();
                            return value!="";
                        } 
                    }
                    // { input: '#journal_entry_credit', message: 'Credit is required!', action: 'keyup, blur', rule: 'required' },
                    // { input: '#journal_entry_debit', message: 'Debit is required!', action: 'keyup, blur', rule: 'required' },
            ]
        }
        $('#addEntryFrm').jqxValidator({
            hintType: "label",
            rules: rules
        });
    }
    activitySetting
    activityDataAdapter;
    initActivityCbx(){
        let that = this;
        that.activitySetting = {
                
                onChange: function(event){
                    let item = that.activityMod.getSelectedItem();
                    console.log(item,'item')
                    if(item){
                        that.itemDetail.activity = item;
                        that.itemDetail.activity_id = item.id
                    }
                },
            }
    }
    cost_data;
    initCostCbx(){
        let that = this;

        if(!$('#cbx_cost').hasClass('jqx-widget')){
            this.api.send({
                url: AppSetting.COSTCENTER_LIST_URL,
                params: {
                    pageSize: 100
                }
            }).done(function(data){
                var selectedIndex = -1;
                that.cost_data = data.data.map(function(d,i){
                    if(that.itemDetail && that.itemDetail['cost_center']){
                        if(d.id == that.itemDetail['cost_center'].id){
                            selectedIndex = i;
                        }
                    }
                    d.label = d.name ;//+ ' ' + d.code
                    return d;
                });
                $("#cbx_cost").jqxDropDownList({
                    theme: AppSetting.THEME,
                    source: new $.jqx.dataAdapter(that.cost_data),    
                    width: '100%',
                    height: 30,
                    selectedIndex: selectedIndex,
                    // promptText: "Select cost...",
                    displayMember: 'label',
                    valueMember: 'id',
                    searchMode: 'contains',
                    // renderSelectedItem: function(index, item) {
                    //     console.log(item)
                    //     var d = item.originalItem;
                    //     if (d != null) {
                    //         var label = d.name + "(" + d.code + ')';
                    //         return label;
                    //     }
                    //     return "";   
                    // }
                }).bind('change', function(event){
                    // that.sync()
                })
                
            })
        }else{

        }
    }
    country_data
    initCountryCbx(){
        let that = this;

        if(!$('#cbx_country').hasClass('jqx-widget')){
            this.api.send({
                url: AppSetting.COUNTRY_LIST_URL,
                params: {
                    pageSize: 100
                }
            }).done(function(data){
                var selectedIndex = -1;
                that.country_data = data.data.map(function(d,i){
                    if(that.itemDetail && that.itemDetail['country']){
                        if(d.id == that.itemDetail['country'].id){
                            selectedIndex = i;
                        }
                    }
                    d.label = d.description ;//+ ' ' + d.code
                    return d;
                });
                $("#cbx_country").jqxComboBox({
                    selectedIndex: selectedIndex,
                    theme: AppSetting.THEME,
                    source: new $.jqx.dataAdapter(that.country_data),    
                    width: '100%',
                    height: 30,
                    // promptText: "Please",
                    displayMember: 'label',
                    valueMember: 'id',
                    searchMode: 'contains',
                    // renderSelectedItem: function(index, item) {
                    //     console.log(item)
                    //     var d = item.originalItem;
                    //     if (d != null) {
                    //         var label = d.name + "(" + d.code + ')';
                    //         return label;
                    //     }
                    //     return "";   
                    // }
                }).bind('change', function(event){
                    // that.sync()
                })
                $("#cbx_country input").on('click', function () {
                    $("#cbx_country").jqxComboBox('open');
                });

                var selectedIndex = -1;
                data.data.map(function(d,i){
                    if(that.itemDetail && that.itemDetail['start_country']){
                        if(d.id == that.itemDetail['start_country'].id){
                            selectedIndex = i;
                        }
                    }
                });
                $("#cbx_start_country").jqxComboBox({
                    selectedIndex: selectedIndex,
                    theme: AppSetting.THEME,
                    source: new $.jqx.dataAdapter(that.country_data),    
                    width: '100%',
                    height: 30,
                    // promptText: "Please",
                    displayMember: 'label',
                    valueMember: 'id',
                    searchMode: 'contains',
                    // renderSelectedItem: function(index, item) {
                    //     console.log(item)
                    //     var d = item.originalItem;
                    //     if (d != null) {
                    //         var label = d.name + "(" + d.code + ')';
                    //         return label;
                    //     }
                    //     return "";   
                    // }
                }).bind('change', function(event){
                    // that.sync()
                })
                $("#cbx_start_country input").on('click', function () {
                    $("#cbx_start_country").jqxComboBox('open');
                });

                var selectedIndex = -1;
                data.data.map(function(d,i){
                    if(that.itemDetail && that.itemDetail['end_country']){
                        if(d.id == that.itemDetail['end_country'].id){
                            selectedIndex = i;
                        }
                    }
                });
                $("#cbx_end_country").jqxComboBox({
                    selectedIndex: selectedIndex,
                    theme: AppSetting.THEME,
                    source: new $.jqx.dataAdapter(that.country_data),    
                    width: '100%',
                    height: 30,
                    // promptText: "Please",
                    displayMember: 'label',
                    valueMember: 'id',
                    searchMode: 'contains',
                    // renderSelectedItem: function(index, item) {
                    //     console.log(item)
                    //     var d = item.originalItem;
                    //     if (d != null) {
                    //         var label = d.name + "(" + d.code + ')';
                    //         return label;
                    //     }
                    //     return "";   
                    // }
                }).bind('change', function(event){
                    // that.sync()
                })
                $("#cbx_end_country input").on('click', function () {
                    $("#cbx_end_country").jqxComboBox('open');
                });
            })
        }
        // if(!$('#cbx_start_country').hasClass('jqx-widget')){
        //     this.api.send({
        //         url: AppSetting.COUNTRY_LIST_URL,
        //         params: {
        //             pageSize: 100
        //         }
        //     }).done(function(data){
                
        //     })
        // }
    }
    initUpload(){
        var imageTypes = ['.gif', '.jpg', '.png'];
            var videoTypes = ['.wmv', '.mov', '.avi', '.divx', '.mpeg', '.mpg', '.m4p'];
            $('#jqxFileUpload').jqxFileUpload({ 
                width: 240, 
                accept: 'image/*',
                theme: AppSetting.THEME,
                uploadUrl: '/', 
                fileInputName: 'fileToUpload', 
                // renderFiles: function (fileName) {
                //     // var reader = new FileReader();
                //     // reader.onload = function(){
                //     //   var output = document.getElementById('output');
                //     //   output.src = reader.result;
                //     // };
                //     // reader.readAsDataURL(event.target.files[0]);


                //     console.log(fileName,'fileName')
                //     var stopIndex = fileName.indexOf('.');
                //     var name = fileName.slice(0, stopIndex);
                //     var extension = fileName.slice(stopIndex);
                   
                //     return '<div><span>' + name + '<strong>' + extension + '</strong></span></div>';
                // }
            }).on('select', function (event) {
                var args = event.args;
                var fileName = args.file;
                var fileSize = args.size;
                console.log(args.owner)
                var files = $('#jqxFileUpload input[type="file"]').get().map(function(f){
                    return f.files[0]
                }).filter(function(f){
                    return f;
                });

                var i = files.length - 1;

                // var images = files.map(function(file,i){
                Helpers.getBase64(files[i]).then(
                  data => {
                      // console.log(data)
                       $('#jqxFileUpload .jqx-file-upload-file-row:eq('+i+') img').remove();
                       $('#jqxFileUpload .jqx-file-upload-file-row:eq('+i+')').prepend($('<img/>').attr('src',data))
                  }
                )
                // })
            });
    }
    initFormControl(){
        let that = this;
        that.initActivityCbx()
        that.initCostCbx()
        that.initCountryCbx()
        that.initUpload()
        
        
    }
    resetCreateForm(){
        $('#addEntryFrm').trigger("reset");
        $('#cbx_cost').jqxDropDownList('selectItem', null );
        $('#cbx_country').jqxComboBox('selectItem', null );
        // $('#txt_date ').jqxDateTimeInput('setDate', new Date());
        $('#jqxFileUpload').jqxFileUpload('cancelAll');
    }
    onAddItem(){
        let that = this;
        var valid = $('#addEntryFrm').jqxValidator('validate');
        if(!valid){
            toastr['warning']('Invalid.')
            return;
        }
        var formData = $('#addEntryFrm').serializeArray().reduce(function(obj, item) {
            obj[item.name] = item.value;
          return obj;
        }, {});
        formData.status =1;
        var files = $('#jqxFileUpload input[type="file"]').get().map(function(f){
            return f.files[0]
        }).filter(function(f){
            return f;
        });
        formData.date = that.dateDti.getDate().getTime()/1000;
        if(that.itemDetail.type==1){
            var cost = $("#cbx_cost").jqxDropDownList('getSelectedItem');
            var country = $("#cbx_country").jqxComboBox('getSelectedItem');
            if(cost && country){
                formData.country_id = country.originalItem.id;
                formData.cost_center_id = cost.originalItem.id;
            }else return;
        }
        if(that.itemDetail.type==2){
            var start_country = $("#cbx_start_country").jqxComboBox('getSelectedItem');
            var end_country = $("#cbx_end_country").jqxComboBox('getSelectedItem');
            if(start_country && end_country){
                formData.start_country_id = start_country.originalItem.id;
                formData.end_country_id = end_country.originalItem.id;
            }else return;
        }
        let url = AppSetting.EXPENSES_CREATE_URL;
        if(that.itemDetail.id){
            url = AppSetting.EXPENSES_UPDATE_URL + that.itemDetail.id;
        }
        formData.activity_id = that.activityMod.value;
        let activity:any = that.activityMod.getSelectedItem();
        console.log(activity,'activity')
        if(activity){
            if(
                !activity.credit_acct_code ||
                !activity.debit_acct_code
                ){
                toastr['warning']('Debit and Credit account undefined.')
                return;
            }
        }
        that.api.post({
            url: url,
            params: formData,
            files: {
                images: files
            }
        }).done(function(res){
            // that.showListEntry()
            
            that.dataAdapter.dataBind();
            that.resetCreateForm();
            that.backToList()
        })
    }
    isCreateAction = 0;
    backToList(){
        console.log('Backtolist')
        this.isCreateAction = 0;
        this.itemDetail = undefined;
    }
    ngAfterViewInit() {
        let that = this;
        console.log('ngAfterViewInit')
    }
    onApprove(){
        let that = this;
        Helpers.confirm('<h3>'+Helpers.translate.get('Are you sure you want to approve selected expense(s)?')+'</h3>',function(){
            that.onSetStatus(2)
        })
    }
    onReject(){
        let that = this;
        Helpers.confirm('<h3>'+Helpers.translate.get('Are you sure you want to reject selected expense(s)?')+'</h3>',function(){
            that.onSetStatus(3)
        })
    }
    onSetStatus(status){
        let that = this;
        let ids;
        if(that.itemDetail){
            ids = [that.itemDetail.id]
        }else{
            let selectedIndexs = that.grid.selectedrowindexes();
            if(selectedIndexs.length){
                ids = selectedIndexs.map(function(i){
                    return  that.grid.getrowdata(i).id
                });
            }else{
                toastr["warning"](Helpers.translate.get('No expenses are selected. Please select rows before authorisation'));
                return;
            }
        }
        that.api.post({
            url: AppSetting.EXPENSES_UPDATE_STATUS,
            params:{
                ids:ids,
                'status': status
            }
        }).done(function(res){
            if(that.itemDetail){
                that.itemDetail.status = status;
                that.dataAdapter.dataBind();
            }{
                that.dataAdapter.dataBind();
                that.grid.clearselection();
            }
        })
        //$('#myGrid').jqxGrid('getselectedrowindexes');
    }
    onActivityChange(){

    }
}
