import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { APIService } from '../../api.service';
import { AppSetting } from "../../settings";
declare var $:any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
})
export class LoginComponent implements OnInit, AfterViewInit, OnDestroy {
  loginForm;
  loginData
  constructor(
    private api: APIService
    ) { }

  ngOnInit() {
    $('body').addClass('empty-layout');
    // this.loginForm = new FormGroup({
    // 'username': new FormControl(this.loginData.username, [
    //   Validators.required,
    //   Validators.minLength(4),
    //   forbiddenNameValidator(/bob/i)
    // ]),
    // 'password': new FormControl(this.loginData.password, Validators.required)
    // });
    this.api_info = AppSetting.API_INFO
  }
  api_info
  ngAfterViewInit() {
    $('#login-form').validate({
        errorClass:"help-block",
        rules: {
            email: {required:true,email:true},
            password: {required:true}
        },
        highlight:function(e){$(e).closest(".form-group").addClass("has-error")},
        unhighlight:function(e){$(e).closest(".form-group").removeClass("has-error")},
    });
        switch(window.location.host){
            // Local
            case 'localhost:4200':
              this.api_info = {
                name: 'Development',
                url:'http://saaspiens.speranzainc.net:9002/'
              }
                break;
            // Dev
            case 'development.mysaaspiens.com:7000':
            // Demo
            case 'demo.mysaaspiens.com':
                break;
            // JP
            case 'jp.mysaaspiens.com':
                break;
            default:
        }
  }
  ngLogin(){
    if($('#login-form').valid()){
      var loginData = $('#login-form').serializeArray().reduce(function(obj, item) {
          obj[item.name] = item.value;
          return obj;
      }, {});
      this.api.login(loginData.email,loginData.password,function(data){
        console.log(data)
      })
      console.log(loginData);
    }
  }
  ngOnDestroy() {
    $('body').removeClass('empty-layout');
  }

}
