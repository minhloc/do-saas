import { Component, OnInit, AfterViewInit, OnChanges } from '@angular/core';
import { DatePipe, CurrencyPipe } from '@angular/common';
import { APIService } from '../../api.service';
import { Helpers } from "../../helpers";
import { AppSetting } from "../../settings";
import { AppMessage } from "../../message";
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { ScriptLoaderService } from '../../_services/script-loader.service';

declare var $:any;

@Component({
  selector: 'app-receipt-payment',
  templateUrl: './receipt-payment.component.html',
})
export class ReceiptPaymentComponent implements OnInit, AfterViewInit, OnChanges {
    types;
    item_detail;
    title;
    api_type = 1;
    dataTable
    constructor(
        private api: APIService,
        private router: Router,
        private route: ActivatedRoute,
        private scriptLoader: ScriptLoaderService,
    ) {
        router.events.subscribe((val) => {
            var snapshot = this.route.snapshot;
            if(val instanceof NavigationEnd) {
                if(
                    this.router.url.indexOf('/re-pay/')>=0 &&
                    this.router.url.indexOf(snapshot.params.type)>=0
                    ){

                    console.log(snapshot.params.type,'snapshot')
                    this.initPage()
                    console.log('INIT')
                }
            }
        });
    }
    render(){
        console.log('render')
    }
    ngOnChanges() {
        console.log('ngOnChanges')
    }
    ngOnInit() {
        
      
    }
    initPage(){
        let that = this;
        that.item_detail = undefined;
        var type_strs = ['receipt','payment']
        that.types =[{
                'type': 1,
                'title' : 'Receipt',
                'subtitle' : 'Receipt'
            },{
                'type': 2,
                'title' : 'Payment',
                'subtitle' : 'Payment'
            }];
        var sale_type = type_strs.indexOf(that.route.snapshot.params.type);
        that.api_type = that.types[sale_type].type;
        that.title = that.types[sale_type].title;
        var columns = [
                { 
                    "data": "business_partner_id", "orderable": false,
                    "render": function ( value, column, rowData, setting ) {
                        return setting.settings._iDisplayStart + setting.row+1;
                    }
                },
                { "data": "No" },
                { "data": "name" , "orderable": false},
                { "data": "phone" },
                { "data": "email" },
                { "data": "balance" },
                { 
                    "data":null, 
                    "className":"action-c", 
                    "defaultContent":'<span class="text-light mr-3 font-16"><i class="ti-pencil"></i></span>',
                    "orderable": false
                }
            ]
        if(that.dataTable){
            that.dataTable.ajax.reload();
        }else{
            that.dataTable = $('#repay-table').DataTable({
                destroy: true,
                "ajax": {
                    "url": AppSetting.RE_PAY_LIST_URL,
                    "contentType": "application/json",
                    "beforeSend": function(xhr){
                        xhr.setRequestHeader("Authorization", "bearer " + localStorage.getItem('token'));
                     },
                    "type": "POST",
                    "dataSrc": function ( data ) {
                        console.log(data,'data');
                        data.recordsFiltered = data.data.length;
                        data.recordsTotal = data.data.length;
                        // delete(data.links)
                        // delete(data.message)
                        // delete(data.meta)
                        // delete(data.status)
                        data.data.map(function(row){
                            row.balance = new CurrencyPipe("en-US").transform(row.balance)
                        //     row.date = new DatePipe("en-US").transform(row.date, 'dd/MM/yyyy');
                        //     row.due_date = new DatePipe("en-US").transform(row.due_date, 'dd/MM/yyyy');
                        })
                        return data.data;
                    },
                    "data": function ( d ) {
                        console.log(d,'d')
                        d.type = that.api_type;
                        d.page = d.start/d.length + 1
                        d.pageSize = d.length
                        d.order_name = columns[d.order[0].column].data
                        d.order_type = d.order[0].dir
                        return JSON.stringify( d );
                    },
                    "error": function(xhr, textStatus, errorThrown) {
                        Helpers.alert('Error');
                    },
                },
                "processing": true,
                "serverSide": true,
                // data: dataSet,
                "columns": columns,
                "order": [[ 1, "desc" ]],
                "pageLength": 20,
                "fixedHeader": true,
                "responsive": true,
                "sDom": 'rtip', 
            });
            $('#repay-table tbody').on( 'click', '.action-c span', function () {
                var dataRow = that.dataTable.row( $(this).parents('tr') ).data();
                console.log(dataRow);
                that.item_detail = dataRow;
                that.api.post({
                    url: AppSetting.RE_PAY_BYPARTNER_URL + dataRow.business_partner_id,
                    params: {
                        type: that.api_type
                    }
                }).done(function(data){
                    console.log(data,'PURCHASAE_DETAIL')
                    if(data.data){
                        data.data.map(function(row){
                            row.date = new DatePipe("en-US").transform(row.date * 1000, 'dd/MM/yyyy');
                            row.due_date = new DatePipe("en-US").transform(row.due_date * 1000, 'dd/MM/yyyy');
                            row.total_with_tax = new CurrencyPipe("en-US").transform(row.total_with_tax)

                        })
                        that.item_detail.items = data.data
                        that.types.map(function(d){
                            if(d.type == that.api_type){
                                that.item_detail.document = d.subtitle;
                            }
                        })
                        that.scriptLoader.load('./assets/js/scripts/form-plugins.js');
                    }
                })
            } );
        }

        $('#key-search').on('keyup', function() {
            that.dataTable.search(this.value).draw();
        });
        $('#type-filter').on('change', function() {
            that.dataTable.column(2).search($(this).val()).draw();
        });
    }
  ngAfterViewInit() {
    }

    backToList() {
        this.item_detail = undefined
    }

}
