import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesMigrationComponent } from './sales-migration.component';

describe('SalesMigrationComponent', () => {
  let component: SalesMigrationComponent;
  let fixture: ComponentFixture<SalesMigrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalesMigrationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesMigrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
