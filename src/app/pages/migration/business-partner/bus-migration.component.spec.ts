import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusMigrationComponent } from './bus-migration.component';

describe('BusMigrationComponent', () => {
  let component: BusMigrationComponent;
  let fixture: ComponentFixture<BusMigrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusMigrationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusMigrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
