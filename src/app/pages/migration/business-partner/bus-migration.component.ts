import { Component, OnInit, AfterViewInit, OnChanges,ViewChild, ViewEncapsulation  } from '@angular/core';
import { DatePipe, CurrencyPipe } from '@angular/common';
import { APIService } from '../../../api.service';
import { SystemService } from '../../../system.service';
import { Helpers} from "../../../helpers";
import { AppSetting } from "../../../settings";
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { AppMessage } from "../../../message";
import { FormsModule } from '@angular/forms';
import * as cloneDeep from 'lodash/cloneDeep';

import { jqxGridComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxgrid';
import { jqxComboBoxComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxcombobox';
import { jqxDropDownListComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxdropdownlist';
import { jqxDateTimeInputComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxdatetimeinput';
import { jqxValidatorComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxvalidator';
import { jqxFileUploadComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxfileupload';

import { jqxWindowComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxwindow';
import { jqxTabsComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxtabs';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { FormControl, FormGroup } from '@angular/forms';
import { HttpClient,HttpHeaders, HttpParams } from '@angular/common/http';
import * as XLSX from 'ts-xlsx';
declare var $:any;
declare var toastr:any;

@Component({
  selector: 'app-bus-migration',
  templateUrl: './bus-migration.component.html',
})
export class BusMigrationComponent implements OnInit, AfterViewInit {
    constructor(
        private api: APIService,
        private router: Router,
        private route: ActivatedRoute,
        private sys: SystemService,
        private httpClient:HttpClient
    ) { }
    @ViewChild('grid') grid: jqxGridComponent;
    @ViewChild('window') window: jqxWindowComponent;
    @ViewChild('validator') validator: jqxValidatorComponent;
    @ViewChild('fileUpload') fileUpload: jqxFileUploadComponent;

    theme = AppSetting.THEME;
    ngOnInit() {
        let that = this;
        this.route.params.subscribe(
            params => {
                
            }
        );
    }
    url
    ngAfterViewInit() {
        let that= this;
        that.url = AppSetting.MIGRATION_CUSTOMER_URL
        
    }
    preview_data
    onSelectFile(event){
        let that = this;
        var args = event.args;
        var fileName = args.file;
        var fileSize = args.size;
        var files = $(that.fileUpload.elementRef.nativeElement).find('input[type="file"]').get()
        .map(function(f){
            return f.files[0]
        }).filter(function(f){
            return f;
        });
        console.log(args,files,'-------')
        that.api.post({
            url: AppSetting.MIGRATION_CUSTOMER_URL,
            files: {
                business_partner: files[0]
            }
        }).done(function(res){
            // that.showListEntry()
            console.log(res,'res')
            that.preview_data = res.data;
            if(that.preview_data.invalid_count==0){
               that.migration_status = 1;
            }
        })
        let token = localStorage.getItem('token')
        
        that.fileUpload.cancelAll();
        
    }
    migration_status = 0;
    migration(){
        let that=this;
        that.api.post({
            url: AppSetting.MIGRATION_CUSTOMER_URL,
            params: {
                file_name: that.preview_data.file_name,
                migration: true
            },
        }).done(function(res){
            // that.showListEntry()
            console.log(res,'res')
            that.migration_status = 0;
            toastr.success(res.message)
            that.preview_data = res.data
        })
    }
}   
