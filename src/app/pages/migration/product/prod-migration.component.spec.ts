import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProdMigrationComponent } from './prod-migration.component';

describe('ProdMigrationComponent', () => {
  let component: ProdMigrationComponent;
  let fixture: ComponentFixture<ProdMigrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProdMigrationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProdMigrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
