import {Injectable} from "@angular/core";
import {HttpEvent, HttpHandler, HttpInterceptor,HttpRequest,HttpErrorResponse} from "@angular/common/http";
import {Observable} from "rxjs/Observable";

@Injectable()
export class AppHttpInterceptor implements HttpInterceptor {
    constructor() {
    }

    intercept(req: HttpRequest<any>,next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(req).catch((err: any) => {
            if (err instanceof HttpErrorResponse) {
                if (err.status === 401) {
                }
            }
            console.log('401')
            console.log(err,'HttpRequest::Error')
            return Observable.throw(err);
        });
    }
}
