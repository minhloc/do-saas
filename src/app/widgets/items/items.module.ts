import { Component, OnInit, Input,forwardRef,AfterViewInit,OnChanges, ViewChild, ViewEncapsulation, EventEmitter, Output } from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from "@angular/forms";
import * as cloneDeep from 'lodash/cloneDeep';
import { DatePipe, CurrencyPipe } from '@angular/common';
import { APIService } from '../../api.service';
import { Helpers} from "../../helpers";
import { AppSetting } from "../../settings";
import { AppMessage } from "../../message";

import { jqxWindowComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxwindow';
import { jqxGridComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxgrid';
import { jqxValidatorComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxvalidator';
import { jqxDropDownListComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxdropdownlist';
import { jqxFileUploadComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxfileupload';
import { jqxComboBoxComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxcombobox';

import { ItemGroupModule } from '../../widgets/item-group/item-group.module';
import { GeneralProductPtgModule } from '../../widgets/ptg/general/product/ptg-gen-product.module';
import { VatProductPtgModule } from '../../widgets/ptg/vat/product/ptg-vat-product.module';
import { InventoryPtgModule } from '../../widgets/ptg/inventory/ptg-inventory.module';
import { PartnerModule } from '../../widgets/partner/partner.module';
import { UomModule } from '../../widgets/uom/uom.module';

declare var $:any;
declare var toastr:any;

const noop = () => {
};

@Component({
    selector: 'module-items',
    templateUrl: './items.module.html',
    styleUrls: ['./items.module.css'],
    providers: [{
        provide: NG_VALUE_ACCESSOR,
        useExisting: forwardRef(() => ItemsModule),
        multi: true
    }]
})

export class ItemsModule implements OnInit,AfterViewInit, OnChanges, ControlValueAccessor {
    @Input() hidden: boolean;
    @Input() layout: string;
    @Input() key: string;
    @Output() onOpen = new EventEmitter();
    @Output() onClose = new EventEmitter();
    @Output() onChange = new EventEmitter();
    constructor(
        private api: APIService,
        ) {
        if(!!!this.key) this.key = 'code'
    }
    theme = AppSetting.THEME
    // The internal data model
    private innerValue: any = undefined;
    label = Helpers.translate.get("Please Choose");
    // Placeholders for the callbacks which are later provided
    // by the Control Value Accessor
    private onTouchedCallback: () => void = noop;
    private onChangeCallback: (_: any) => void = noop;

    @ViewChild('window') window: jqxWindowComponent;
    @ViewChild('detailWindow') detailWindow: jqxWindowComponent;
    @ViewChild('grid') grid: jqxGridComponent;
    @ViewChild('validator') validator: jqxValidatorComponent;
    @ViewChild('fileUpload') fileUpload: jqxFileUploadComponent;

    @ViewChild('itemGroupCbx') itemGroupCbx: ItemGroupModule;
    @ViewChild('genProdPtgCbx') genProdPtgCbx: GeneralProductPtgModule;
    @ViewChild('salesTaxGroupCbx') salesTaxGroupCbx: VatProductPtgModule;
    @ViewChild('purchaseTaxGroupCbx') purchaseTaxGroupCbx: VatProductPtgModule;
    @ViewChild('invtPtgCbx') invtPtgCbx: InventoryPtgModule;
    @ViewChild('supplierCbx') supplierCbx: PartnerModule;
    @ViewChild('baseUomCbx') baseUomCbx: UomModule;
    @ViewChild('itemUomCbx') itemUomCbx: UomModule;
    @ViewChild('dropDownGrid') dropDownGrid: jqxGridComponent;
    @ViewChild('dropDownButton') dropDownButton; 

    // get accessor
    get value(): any {
        return this.innerValue;
    };

    // set accessor including call the onchange callback
    set value(v: any) {
        if (v !== this.innerValue) {
            this.innerValue = v;
            this.onChangeCallback(v);
        }
        console.log('SetValue')
    }

    // Set touched on blur
    onBlur() {
        this.onTouchedCallback();
    }

    // From ControlValueAccessor interface
    writeValue(value: any) {
        if (value !== this.innerValue) {
            this.innerValue = value;
        }
    }

    // From ControlValueAccessor interface
    registerOnChange(fn: any) {
        this.onChangeCallback = fn;
    }

    // From ControlValueAccessor interface
    registerOnTouched(fn: any) {
        this.onTouchedCallback = fn;
    }



    ngOnInit() {
    }
    ngOnChanges(){

    }
    item_group_id;
    filterByGroup(id){
        let that = this;
        that.item_group_id = id
        if(that.dropDownGridSetting){
            that.dropDownGridSetting = undefined;
        }
    }

    dropDownDataAdapter
    dropDownGridSetting
    initDropDownGrid(){
        let that = this;
        
        let columns = [
            { text: '#', dataField: 'id', width: 120 ,hidden: true},
            { text: Helpers.translate.get('Code'), dataField: 'product_code', width: 120 },
            { text: Helpers.translate.get('Description'), dataField: 'description' },

        ]
        let conditions = null;
        if(that.item_group_id){
            conditions=[ 'item_group_id', that.item_group_id ];
        }
        console.log(conditions,'conditions')
        if(that.dropDownGridSetting){
            that.ensureDropDownGrid()
            that.dropDownGrid.focus()
            return;
        }

        Helpers.translate.do(()=>{
            if(that.dropDownGrid){
                that.dropDownGrid.setcolumnproperty('id','text', '#')
                that.dropDownGrid.setcolumnproperty('product_code','text', 'Code')
                that.dropDownGrid.setcolumnproperty('description','text', 'Description')
                that.dropDownGrid.setOptions({'localization': Helpers.getLocalizatio(Helpers.translate.activeLang)})
            }
        })

        let source = Helpers.Source({
            dataType: "json",
            type: 'POST',
            url: AppSetting.ITEMS_BINDING_URL,
            data:{
                conditions: conditions
            },
            dataFields: [
                { name: 'id', type: 'number' },
                { name: 'product_code', type: 'string' },
                { name: 'description', type: 'string' },
                { name: 'uom_id'},
                { name: 'item_uom'},
                { name: 'unit_price'},
                { name: 'sale_price'},
                { name: 'purchase_price'},
                { name: 'unit_cost'},
                { name: 'gen_product_ptg_code'},
                { name: 'vat_product_ptg_code'},
                { name: 'sale_tax_group_id'},
                { name: 'purchase_tax_group_id'},
                { name: 'invt_ptg_code'}
            
            ],
            filter: function() {
                // update the grid and send a request to the server.
                that.dropDownGrid.updatebounddata('filter');
            },
            sort: function() {
                // update the grid and send a request to the server.
                that.dropDownGrid.updatebounddata('sort');
            },
            id: 'id',
            root: 'data',
        });
        // that.gridPartner.setOptions({source:[]});
        that.dropDownDataAdapter = new jqx.dataAdapter(source, {
            beforeLoadComplete: function (records) {
                for (var i = 0; i < records.length; i++) {
                    var row = records[i];
                }
                return records;
            }
        });
        that.dropDownGridSetting = {
            columns: columns,
            source: that.dropDownDataAdapter,
            localization: Helpers.getLocalizatio(Helpers.translate.activeLang),
            ready: function() {
                that.ensureDropDownGrid()
                that.dropDownGrid.focus()
            },
            rendergridrows: (params: any): any[] => {
                return that.dropDownDataAdapter.records;
            },
            rendered(event: any): void {
            },
            onRowSelect: (event:any)=>{
                var args = event.args;
                let rowData = that.dropDownGrid.getrowdata(args.rowindex);
                if(rowData){
                    if(rowData){
                        that.setSelectedItem(rowData);
                        that.onChangeCallback(rowData[that.key])
                        if(
                            !rowData.gen_product_ptg_code ||
                            !rowData.vat_product_ptg_code ||
                            !rowData.invt_ptg_code 
                            ){
                            toastr.warning(Helpers.translate.get('This Item missing a posting group setting'))
                        }
                    }else{
                        that.setSelectedItem(undefined);
                        that.onChangeCallback(undefined)
                    }
                    that.onChange.emit()
                    if(that.dropDownButton) {
                        $(that.dropDownButton.nativeElement).dropdown('toggle');
                        $(that.dropDownButton.nativeElement).focus()
                    }
                }
            },
            onRowdoubleclick: (event:any)=>{
                var args = event.args;
                var rowBoundIndex = args.rowindex;
                var rowVisibleIndex = args.visibleindex;
                var rightclick = args.rightclick; 
                var columnindex = args.columnindex;
                var dataField = args.datafield;
                var value = args.value;
                if(args && args.row){
                    var dataRow = args.row.bounddata
                }
            },
            handlekeyboardnavigation: (event:any)=>{
                var key = event.charCode ? event.charCode : event.keyCode ? event.keyCode : 0;
                if (key == 13) {
                    let index = that.dropDownGrid.getselectedrowindex();
                    let rowData = that.dropDownGrid.getrowdata(index);
                    if(index>=0 && rowData){
                        that.setSelectedItem(rowData);
                        that.onChangeCallback(rowData[that.key])
                        that.onChange.emit();
                        if(that.dropDownButton) {
                            $(that.dropDownButton.nativeElement).dropdown('toggle');
                            $(that.dropDownButton.nativeElement).focus()
                        }
                    }
                    return true;
                };
            },
        }
        if(that.dropDownGrid){
            that.dropDownGrid.setOptions({source: that.dropDownDataAdapter})
        }
    }
    ensureDropDownGrid(){
        let that = this;
        if(that.dropDownGrid){
            let rowid;
            let index = that.dropDownGrid.getselectedrowindex();
            if(index == -1) index = 0;
            let rowData = that.dropDownGrid.getrowdata(index);
            if(rowData && rowData[that.key]==that.innerValue){
                console.log('NOTHING')
            }else{
                that.dropDownDataAdapter.records.map((row)=>{
                    if(row[that.key] == that.innerValue){
                        rowid = row.id
                    }
                })
                let rowIndex = that.dropDownGrid.getrowboundindexbyid(rowid)
                if(rowIndex == -1) rowIndex = 0;
                that.dropDownGrid.ensurerowvisible(rowIndex);
                that.dropDownGrid.selectedrowindex(rowIndex);
            }
        }
    }
    initDropdown($event){
        let that = this;
    }
    onDropdownSelectect(item){
        let that = this;
        that.setSelectedItem(item);
        that.onChangeCallback(item[that.key])
        that.onChange.emit()
    }
    switchToAdvance(){
        if(this.dropDownButton) $(this.dropDownButton.nativeElement).dropdown('toggle');
        
        this.open()
    }



    selectedItem;
    setSelectedItem(item){
        let that = this;
        that.selectedItem = item;
        if(item && item.id){
            that.label = item.product_code + ' ' + item.description;
            that.innerValue = item[that.key];
        }else{
            that.label = Helpers.translate.get("Please Choose");
            that.innerValue = undefined
        }
    }
    getSelectedItem(){
        return this.selectedItem;
    }
    isOnchangeEmit = false;
    setValue(v,isOnchangeEmit = false){
        this.innerValue = v
        this.isOnchangeEmit = isOnchangeEmit;
        this.initDisplayValue();
    }
    ensure(){
        let that = this;
        console.log('ensure',that.innerValue)
        if(that.grid && !!that.innerValue){
            let rowid;
            let index = that.grid.getselectedrowindex();
            let rowData = that.grid.getrowdata(index);
            if(rowData && rowData[that.key]==that.innerValue){

            }else{
                that.dataAdapter.records.map((row)=>{
                    if(row[that.key] == that.innerValue){
                        rowid = row.id
                    }
                })
                let rowIndex = that.grid.getrowboundindexbyid(rowid)
                that.grid.ensurerowvisible(rowIndex);
                that.grid.selectedrowindex(rowIndex);
            }
        }
    }
    initDisplayValue(){
        let that = this;
        setTimeout((any)=>{
            if(!!that.innerValue){
                let params = {};
                params[that.key] = that.innerValue;
                that.api.post({
                    url: AppSetting.GET_ITEMS_URL,
                    params:params
                }).done(function(res){
                    if(res.data){
                        that.setSelectedItem(res.data);
                    }else{
                        that.setSelectedItem(undefined);
                    }
                    if(that.isOnchangeEmit) that.onChange.emit()
                    that.isOnchangeEmit = false;
                })
            }else{
                that.setSelectedItem(undefined);
                if(that.isOnchangeEmit) that.onChange.emit()
                that.isOnchangeEmit = false;
            }
        }, 42)
    }
    taxgroups_data
    initEvents(){
        let that = this;
        if(!that.dropDownButton) return;
        $(that.dropDownButton.nativeElement).parent().on('show.bs.dropdown', function () {
            that.initDropDownGrid()
        });
        $(that.dropDownButton.nativeElement).keydown(function(e){
            if(e.keyCode == 13) {
                return false;
            }
        });
        that.initEvents = ()=>{};
    }
    ngAfterViewInit(){
        let that = this;
        that.loadAdapter();
        that.api.post({
            url: AppSetting.COMMON_BINDING_URL,
            params:{
                table:'tax_groups',
            }
        }).done(function(res){
            that.taxgroups_data = res.data;
            console.log(that.taxgroups_data,'that.taxgroups_data')
            if(that.layout == 'grid') 
                setTimeout(()=>{
                    that.initGrid();
                },200)
            that.initDisplayValue()
            setTimeout(()=>{
                
            },100)
        });
    }

    toggle(){
        if(this.window.isOpen()) this.close();
        else this.open()
    }
    open(){
        this.window.open();
        this.initGrid()
        // this.onOpen.emit()
        
    }
    eventWindowOpen(){
        this.ensure()
        this.onOpen.emit()
    }
    eventWindowClose(){
        this.onClose.emit()
    }
    close(){
        this.window.close();
        // this.onClose.emit()
    }
    clear(){
        let that = this;
        that.grid.clearselection()
        // that.label = "Please Choose";
    }
    done(){
        let that = this;
        let index = that.grid.getselectedrowindex();
        let rowData = that.grid.getrowdata(index);
        if(index>=0 && rowData){
            that.setSelectedItem(rowData);
            that.onChangeCallback(rowData[that.key])
            if(
                !rowData.gen_product_ptg_code ||
                !rowData.vat_product_ptg_code ||
                !rowData.invt_ptg_code 
                ){
                toastr.warning(Helpers.translate.get('This Item missing a posting group setting'))
            }
        }else{
            that.setSelectedItem(undefined);
            that.onChangeCallback(undefined)
        }
        that.onChange.emit()
        that.close();
    }
    initContent(){
        setTimeout(()=>{
            let that = this;
            console.log(this,self)
        },200)
    }

    source;
    dataAdapter;
    gridSetting;
    groupSource: any = Helpers.Source({
        dataType: "json",
        type: 'POST',
        datafields: [
            { name: 'id', type: 'number' },
            { name: 'code', type: 'string' },
            { name: 'description', type: 'string' },
            { name: 'uom_id'},
            { name: 'item_uom'},
            { name: 'unit_price'},
            { name: 'sale_price'},
            { name: 'purchase_price'},
            { name: 'unit_cost'},
            { name: 'gen_product_ptg_code'},
            { name: 'vat_product_ptg_code'},
            { name: 'sale_tax_group_id'},
            { name: 'purchase_tax_group_id'},
            { name: 'invt_ptg_code'}
        ],
        root: 'data',
        id: 'id',
        url: AppSetting.COMMON_BINDING_URL,
        data: {
            table:'item_group'
        },
        async: false,
        // addrow: function (rowid, rowdata, position, commit) {
        //     // synchronize with the server - send insert command
        //     // call commit with parameter true if the synchronization with the server is successful 
        //     //and with parameter false if the synchronization failed.
        //     // you can pass additional argument to the commit callback which represents the new ID if it is generated from a DB.
        //     commit(true);
        // },
    })
    groupAdapter: any = new jqx.dataAdapter(this.groupSource, {
        // beforeLoadComplete: (records: any[]): any[] => {
        //     // let data = new Array();
        //     // update the loaded records. Dynamically add EmployeeName and EmployeeID fields. 
        //     for (let i = 0; i < records.length; i++) {
                
        //     }
        //     return records;
        // }
    });

    loadAdapter(){
        let that = this;
        that.source = Helpers.Source({
            dataType: "json",
            type: 'POST',
            url: AppSetting.ITEMS_BINDING_URL,
            dataFields: [
                { name: 'id', type: 'number' },
                { name: 'product_code', type: 'string' },
                { name: 'description', type: 'string' },
                { name: 'current_stock', type: 'number'},
                { name: 'unit_price', type: 'number'},
                { name: 'sale_price', type: 'number'},
                { name: 'purchase_price', type: 'number'},
                { name: 'status', type: 'bool'},
                { name: 'item_group_id', type: 'number'},
                { name: 'item_group_description', type: 'string'},
                { name: 'gen_product_ptg_code', type: 'string'},
                { name: 'vat_product_ptg_code', type: 'string'},
                { name: 'invt_ptg_code', type: 'string'},
                { name: 'sale_tax_group_id', type: 'number'},
                { name: 'sale_tax_group_code', type: 'string'},
                { name: 'sale_price_exclude_tax', type: 'number'},
                { name: 'sale_price_include_tax', type: 'number'},
                { name: 'purchase_tax_group_id', type: 'number'},
                { name: 'purchase_tax_group_code', type: 'string'},
                { name: 'purchase_price_exclude_tax', type: 'number'},
                { name: 'purchase_price_include_tax', type: 'number'},
            
            ],
            filter: function() {
                // update the grid and send a request to the server.
                that.grid.updatebounddata('filter');
            },
            sort: function() {
                // update the grid and send a request to the server.
                that.grid.updatebounddata('sort');
            },
            id: 'id',
            root: 'data',
        });
        // that.gridPartner.setOptions({source:[]});
        that.dataAdapter = new jqx.dataAdapter(that.source, {
            beforeLoadComplete: function (records) {
                for (var i = 0; i < records.length; i++) {
                    var row = records[i];
                    if(row){
                        
                        if(that.taxgroups_data) for (var j = that.taxgroups_data.length - 1; j >= 0; j--) {
                            let tax = that.taxgroups_data[j];
                            if(tax.id == row.sale_tax_group_id){
                                row.sale_tax_group_code = tax.code;
                            }
                            if(tax.id == row.purchase_tax_group_id){
                                row.purchase_tax_group_code = tax.code;
                            }
                        }
                    }
                }
                return records;
            }
        });
    }
    pending = false;
    beforeEditData;
    currentEditData;
    cellEditor;
    salesTaxEditor
    purchaseTaxEditor

    initGrid(){
        let that = this;
        
        let columns = [
                { text: '#', dataField: 'id', width: 120 ,hidden: true},
                { text: '', dataField: 'sale_price_exclude_tax', hidden: true},
                { text: '', dataField: 'sale_price_include_tax', hidden: true},
                { text: '', dataField: 'purchase_price_exclude_tax', hidden: true},
                { text: '', dataField: 'purchase_price_include_tax', hidden: true},
                { 
                    text: Helpers.translate.get('Code'), dataField: 'product_code', width: 92 ,
                },{ 
                    text: Helpers.translate.get('Description'), dataField: 'description', minWidth:180,
                },{  
                    text: Helpers.translate.get('Group'), datafield: 'item_group_description', //displayfield: 'group', 
                    filtertype: 'list',
                    createfilterwidget: function (column, columnElement, widget) {
                        console.log('AAAA')
                        widget.jqxDropDownList({ 
                            source: that.groupAdapter,
                            displayMember: 'description',
                            valueMember: 'description',
                            
                        });
                    },
                    validation: (cell: any, value: any): any => {
                        if(that.pending){
                            return { result: false,message:'Choose ...'};
                        }else{
                            return true
                        }
                    },
                    createeditor: (row: number, cellvalue: any, editor: any, cellText: any, width: any, height: any): void => {
                        $('<span class="editor-dropdown-button"></span>')
                            .on('click',()=>{
                                console.log('dblclick',editor)
                                that.itemGroupCbx.open();
                                that.itemGroupCbx.setValue(that.currentEditData.item_group_id);
                                that.cellEditor = editor;
                            })
                            .insertAfter(editor)
                        editor.css({width:'100%'})
                    },
                    // cellsrenderer: function (row, datafield, value) {
                    //     console.log(arguments)
                    //     return value;
                    // },
                    width: 150 
                },{
                    text: Helpers.translate.get('Current Stock'), dataField: 'current_stock', width: 80 ,//hidden: true
                    cellsalign: 'right',
                    editable: false
                },{
                    text: Helpers.translate.get('Prod. Post.'), dataField: 'gen_product_ptg_code', width: 86,
                    validation: (cell: any, value: any): any => {
                        if(that.pending){
                            return { result: false,message:'Choose ...'};
                        }
                        if (value == '')
                            return { result: false, message: Helpers.translate.get('Product Posting Group') + Helpers.translate.get(' should be not empty') };
                        return true;
                    },
                    createeditor: (row: number, cellvalue: any, editor: any, cellText: any, width: any, height: any): void => {
                        $('<span class="editor-dropdown-button"></span>')
                            .on('click',()=>{
                                console.log('dblclick',editor)
                                that.genProdPtgCbx.open();
                                that.genProdPtgCbx.setValue(that.currentEditData.gen_product_ptg_code);
                                that.cellEditor = editor;
                            })
                            .insertAfter(editor)
                        editor.css({width:'100%'})
                    },
                },{ 
                    text: Helpers.translate.get('Sales Price'), dataField: 'sale_price', width: 100 ,//hidden: true
                    cellsalign: 'right', 
                    cellsformat: 'd',
                    columntype: 'numberinput',
                    createeditor: (row: number, cellvalue: any, editor: any, cellText: any, width: any, height: any): void => {
                        console.log(height,'height')
                        editor.jqxNumberInput({ 
                            decimalDigits: 2, 
                            digits: 9,
                            min: 0,
                        });
                    }
                },{ 
                    text: Helpers.translate.get('Purchase Price'), dataField: 'purchase_price', width: 100 ,//hidden: true
                    cellsalign: 'right', 
                    cellsformat: 'd',
                    columntype: 'numberinput',
                    createeditor: (row: number, cellvalue: any, editor: any, cellText: any, width: any, height: any): void => {
                        console.log(height,'height')
                        editor.jqxNumberInput({ 
                            decimalDigits: 2, 
                            digits: 9,
                            min: 0,
                        });
                    }
                },{ 
                    text: Helpers.translate.get('Sales TAX'), dataField: 'sale_tax_group_code', width: 86,
                    validation: (cell: any, value: any): any => {
                        if(that.pending){
                            return { result: false,message:'Choose ...'};
                        }
                        if (value == '')
                            return { result: false, message: Helpers.translate.get('Sales TAX') + Helpers.translate.get(' should be not empty') };
                        return true;
                    },
                    createeditor: (row: number, cellvalue: any, editor: any, cellText: any, width: any, height: any): void => {
                        $('<span class="editor-dropdown-button"></span>')
                            .on('click',()=>{
                                console.log('dblclick',editor)
                                that.salesTaxGroupCbx.open();
                                that.salesTaxGroupCbx.setValue(that.currentEditData.sale_tax_group_id);
                                that.cellEditor = editor;
                            })
                            .insertAfter(editor)
                        editor.css({width:'100%'})
                        that.salesTaxEditor = editor
                    },
                    initeditor: (row: number, cellvalue: any, editor: any, celltext: any, pressedkey: any): void => {
                        console.log('initeditor')
                    } 
                },{
                    text: Helpers.translate.get('Purchase TAX'), dataField: 'purchase_tax_group_code', width: 86,
                    validation: (cell: any, value: any): any => {
                        if(that.pending){
                            return { result: false,message:'Choose ...'};
                        }
                        if (value == '')
                            return { result: false, message: Helpers.translate.get('Purchase TAX') + Helpers.translate.get(' should be not empty') };
                        return true;
                    },
                    createeditor: (row: number, cellvalue: any, editor: any, cellText: any, width: any, height: any): void => {
                        $('<span class="editor-dropdown-button"></span>')
                            .on('click',()=>{
                                console.log('dblclick',editor)
                                that.purchaseTaxGroupCbx.open();
                                that.purchaseTaxGroupCbx.setValue(that.currentEditData.purchase_tax_group_id);
                                that.cellEditor = editor;
                            })
                            .insertAfter(editor)
                        editor.css({width:'100%'})
                        that.purchaseTaxEditor = editor
                    },
                    initeditor: (row: number, cellvalue: any, editor: any, celltext: any, pressedkey: any): void => {
                        console.log('initeditor')
                    }
                },{ 
                    text: Helpers.translate.get('Inventory Post.'), dataField: 'invt_ptg_code', width: 86,
                    validation: (cell: any, value: any): any => {
                        if(that.pending){
                            return { result: false,message:'Choose ...'};
                        }
                        if (value == '')
                            return { result: false, message: Helpers.translate.get('Inventory Posting Group') + Helpers.translate.get(' should be not empty') };
                        return true;
                    },
                    createeditor: (row: number, cellvalue: any, editor: any, cellText: any, width: any, height: any): void => {
                        $('<span class="editor-dropdown-button"></span>')
                            .on('click',()=>{
                                console.log('dblclick',editor)
                                that.invtPtgCbx.open();
                                that.invtPtgCbx.setValue(that.currentEditData.invt_ptg_code);
                                that.cellEditor = editor;
                            })
                            .insertAfter(editor)
                        editor.css({width:'100%'})
                    },
                },
                // { 
                //     text: 'Status', dataField: 'status', width: 60 , columntype: 'checkbox',
                //     filtertype: 'bool'
                // },
                
            ]
        
        if(that.gridSetting){
            return;
        }

        Helpers.translate.do(()=>{
            if(that.grid){
                that.grid.setcolumnproperty('product_code','text', Helpers.translate.get('Code'))
                that.grid.setOptions({'localization': Helpers.getLocalizatio(Helpers.translate.activeLang)})
            }
        })

        that.gridSetting = {
            columns: columns,
            source: that.dataAdapter,
            localization: Helpers.getLocalizatio(Helpers.translate.activeLang),
            ready: function() {
                that.initCbxs()
                that.ensure()
            },
            rendergridrows: (params: any): any[] => {
                return that.dataAdapter.records;
            },
            rendered(event: any): void {
            },
            onRowdoubleclick: (event:any)=>{
                // event arguments.
                var args = event.args;
                // row's bound index.
                var rowBoundIndex = args.rowindex;
                // row's visible index.
                var rowVisibleIndex = args.visibleindex;
                // right click.
                var rightclick = args.rightclick; 
                // original event.
                var ev = args.originalEvent;
                // column index.
                var columnindex = args.columnindex;
                // column data field.
                var dataField = args.datafield;
                // cell value
                var value = args.value;
                if(args && args.row){
                    var dataRow = args.row.bounddata
                    if(dataRow) that.showDetail(dataRow.id)
                }
            },
            onCellBeginEdit(event: any): void {
                let args = event.args;
                // if(!that.currentEditData){
                    that.beforeEditData = cloneDeep(args.row);
                    that.currentEditData = cloneDeep(args.row);
                // }
            },
            onCellEndEdit(event: any): void {
                let args = event.args;
                let row = args.row;
                that.currentEditData[args.datafield] = args.value;
                if (args.datafield === 'invt_ptg_code') {
                    if(
                        JSON.stringify(that.currentEditData) !=
                        JSON.stringify(that.beforeEditData)
                        ){
                        let params = cloneDeep(that.currentEditData);
                        params.product_code = params.product_code.toUpperCase();
                        let conditions
                        if(params.id<0){
                            delete params.id;
                        }
                        console.log(params,'params')
                        that.api.post({
                            url: AppSetting.ITEMS_COMMIT_URL,
                            params: {
                                id: params.id,
                                params: params
                            },
                            onDone:function(res){
                                if(!params.id){
                                    that.currentEditData.id = res.data.id
                                    toastr.success(Helpers.translate.get('Create successfuly'))
                                }else{
                                    toastr.success(Helpers.translate.get('Update successfuly'))
                                }
                                // let index = that.grid.getselectedrowindex();
                                // let id = that.grid.getrowid(index);
                                // that.grid.updaterow(id,that.currentEditData);
                                that.dataAdapter.dataBind();
                                //that.grid.updatebounddata('cells');
                                that.currentEditData = undefined;
                                that.beforeEditData = undefined;
                            },
                            onFail:function(){
                                toastr.warning(Helpers.translate.get('Fail to create/update'))
                            },
                        })
                    }
                }
            },
        }
    }
    reloadGrid(){
        this.dataAdapter.dataBind();
        this.grid.updatebounddata('cells');
    }
    editingItem:any = {
        id:0,
        gen_business_ptg_code: '',
        gen_product_ptg_code: '',
        sales_acct_code: '',
        sales_credit_memo_acct_code: '',
        purch_acct_code: '',
        purch_credit_memo_acct_code: '',
        purchase_tax_group_id:'',
        sale_tax_group_id:'',
        sale_price_exclude_tax:0,
        sale_price_include_tax:0,
        purchase_price_exclude_tax:0,
        purchase_price_include_tax:0,
    }
    clickAdd(){
        this.showDetail()
    }
    clickEdit(){
        let that = this;
        let index = that.grid.getselectedrowindex();
        let rowData = that.grid.getrowdata(index);
        if(rowData){
            that.showDetail(rowData.id)
        }
    }
    clickDelete(){
        let that = this;
        let index = that.grid.getselectedrowindex();
        let rowData = that.grid.getrowdata(index);
        if(rowData){
            Helpers.confirm('<h3>' + Helpers.translate.get('Are you sure you want to delete this item?') + '</h3>',function(){
                
                that.api.post({
                    url: AppSetting.DEL_SETUP_POST_URL,
                    params: {
                        id: rowData.id
                    }
                }).done(function(res){
                    if(res.code){
                        that.dataAdapter.dataBind();
                        that.grid.updatebounddata('cells');
                    }
                })
            })
        }
    }
    showDetail(id=0){
        let that = this;
        that.detailWindow.open();
        if(id){
            that.api.post({
                url: AppSetting.GET_ITEMS_URL,
                params: {
                    id: id
                }
            }).done(function(res){
                if(res.data){
                    that.setData(res.data)
                }
            })
        }else{
            that.setData({
                id:0,
                gen_business_ptg_code: '',
                gen_product_ptg_code: '',
                sales_acct_code: '',
                sales_credit_memo_acct_code: '',
                purch_acct_code: '',
                purch_credit_memo_acct_code: '',
                purchase_tax_group_id:'',
                sale_tax_group_id:'',
                sale_price_exclude_tax:0,
                sale_price_include_tax:0,
                purchase_price_exclude_tax:0,
                purchase_price_include_tax:0,
            })
        }
    }
    setData(data){
        let that = this
        that.editingItem = data
        if(data.gen_product_ptg_code && that.genProdPtgCbx){
            that.genProdPtgCbx.setValue(data.gen_product_ptg_code)
        } else if(that.genProdPtgCbx) that.genProdPtgCbx.setValue(null);
        
        if(data.sale_tax_group_id && that.salesTaxGroupCbx){
            that.salesTaxGroupCbx.setValue(data.sale_tax_group_id,true)
        } else if(that.salesTaxGroupCbx) that.salesTaxGroupCbx.setValue(null);

        if(data.invt_ptg_code && that.invtPtgCbx){
            that.invtPtgCbx.setValue(data.invt_ptg_code)
        } else if(that.invtPtgCbx) that.invtPtgCbx.setValue(null);

        if(data.item_group_id && that.itemGroupCbx){
            that.itemGroupCbx.setValue(data.item_group_id)
        } else if(that.itemGroupCbx) that.itemGroupCbx.setValue(null);

        if(data.supplier_id && that.supplierCbx){
            that.supplierCbx.setValue(data.supplier_id)
        } else if(that.supplierCbx) that.supplierCbx.setValue(null);

        if(data.purchase_tax_group_id && that.purchaseTaxGroupCbx){
            that.purchaseTaxGroupCbx.setValue(data.purchase_tax_group_id,true)
        } else if(that.purchaseTaxGroupCbx) that.purchaseTaxGroupCbx.setValue(null,true);

        if(data.uom_id && that.baseUomCbx){
            that.baseUomCbx.setValue(data.uom_id)
        } else if(that.baseUomCbx) that.baseUomCbx.setValue(null);
        if(data.item_uom && that.itemUomCbx){
            that.itemUomCbx.setValue(data.item_uom)
        } else if(that.itemUomCbx) that.itemUomCbx.setValue(null);
    }
    
    genProdPtgSetting
    initGenProdPtgCbx(){
        let that = this;
        if(that.genProdPtgSetting) return;
        that.genProdPtgSetting = {
            onOpen: ()=>{
                that.pending = true;
            },
            onClose: ()=>{
                that.pending = false;
                $('.jqx-grid-validation,.jqx-grid-validation-arrow-up').remove()
            },
            onChange: function(event){
                let item = that.genProdPtgCbx.getSelectedItem();
                if(that.detailWindow.isOpen()){
                    if(item){
                        // that.editingItem.gen_product_ptg = item;
                        // that.editingItem.gen_product_ptg_code = item.code
                        console.log(item,'item')
                        if(that.salesTaxGroupCbx){
                            that.salesTaxGroupCbx.setCode(item.def_vat_product_ptg_code,true);
                        }
                    }
                    let windowId = that.validator.host.attr('id')
                    $('#'+windowId+ ' .cbx_gen_product_ptg_code').blur()

                }else{
                    if(that.cellEditor) {
                        that.cellEditor.val(item.code)
                        that.salesTaxGroupCbx.setCode(item.def_vat_product_ptg_code,true);
                    }
                    if(that.salesTaxEditor){
                        if(item){
                            that.salesTaxEditor.val(item.def_vat_product_ptg_code)
                        }
                    }
                }
                
            },
        }
    }
    vatProdPtgSetting
    initVatProdPtgCbx(){
        let that = this;
        if(that.vatProdPtgSetting) return;
        that.vatProdPtgSetting = {
            onOpen: ()=>{
                that.pending = true;
            },
            onClose: ()=>{
                that.pending = false;
                $('.jqx-grid-validation,.jqx-grid-validation-arrow-up').remove()
            },
            onChange: function(event){
                let item = that.salesTaxGroupCbx.getSelectedItem();
                if(that.detailWindow.isOpen()){
                    if(item){
                        if(that.editingItem) {
                            that.editingItem.vat_product_ptg = item;
                            that.editingItem.sale_tax_group_id = item.id;
                            that.editingItem.vat_product_ptg_code = item.code;
                        }
                    }
                    let windowId = that.validator.host.attr('id')
                    $('#'+windowId+ ' .cbx_vat_product_ptg_code').blur()

                }else{
                    if(that.salesTaxEditor) {
                        that.salesTaxEditor.val(item.code)
                        that.currentEditData.sale_tax_group_id = item.id;
                        console.log(item,':item')
                    }
                }
                
            },
            onPurchaseTaxChange: function(event){
                let item = that.purchaseTaxGroupCbx.getSelectedItem();
                if(that.detailWindow.isOpen()){
                    if(item){
                        if(that.editingItem) {
                            that.editingItem.purchase_tax_group = item;
                            that.editingItem.purchase_tax_group_id = item.id;
                        }
                    }
                    let windowId = that.validator.host.attr('id')
                    $('#'+windowId+ ' .cbx_purchase_tax_group_id').blur()

                }else{
                    if(that.purchaseTaxEditor) {
                        that.purchaseTaxEditor.val(item.code)
                        that.currentEditData.purchase_tax_group_id = item.id;
                        console.log(item,':item')
                    }
                }
                
            },
        }
    }
    itemGroupSetting
    initItemGroupCbx(){
        let that = this;
        if(that.itemGroupSetting) return;
        that.itemGroupSetting = {
            onOpen: ()=>{
                that.pending = true;
            },
            onClose: ()=>{
                that.pending = false;
                $('.jqx-grid-validation,.jqx-grid-validation-arrow-up').remove()
            },
            onChange: function(event){
                console.log('ONCHANGE')
                let item = that.itemGroupCbx.getSelectedItem();
                if(that.detailWindow.isOpen()){
                    if(item){
                        // that.editingItem.gen_product_ptg = item;
                        // that.editingItem.item_group_id = item.id
                    }
                    let windowId = that.validator.host.attr('id')
                    $('#'+windowId+ ' .cbx_item_group_id').blur()

                }else{
                    if(that.cellEditor) that.cellEditor.val(item.description)
                    that.currentEditData.item_group_id = item.id
                }
                
            },
        }
    }
    invtPtgSetting
    initInvtPtgCbx(){
        let that = this;
        if(that.invtPtgSetting) return;
        that.invtPtgSetting = {
            onOpen: ()=>{
                that.pending = true;
            },
            onClose: ()=>{
                that.pending = false;
                $('.jqx-grid-validation,.jqx-grid-validation-arrow-up').remove()
            },
            onChange: function(event){
                let item = that.invtPtgCbx.getSelectedItem();
                if(that.detailWindow.isOpen()){
                    if(item){
                        // that.editingItem.gen_product_ptg = item;
                        // that.editingItem.vat_product_ptg_code = item.code
                    }
                    let windowId = that.validator.host.attr('id')
                    $('#'+windowId+ ' .cbx_invt_ptg_code').blur()

                }else{
                    if(that.cellEditor) {
                        that.cellEditor.val(item.code)

                    }
                }
                
            },
        }
    }
    supplierSetting;
    initSupplierCbx(){
        let that = this;
        if(that.supplierSetting) return;
        that.supplierSetting = {
            onOpen: ()=>{
                that.pending = true;
            },
            onClose: ()=>{
                that.pending = false;
                $('.jqx-grid-validation,.jqx-grid-validation-arrow-up').remove()
            },
            onChange: function(event){
                let item = that.supplierCbx.getSelectedItem();
                if(that.detailWindow.isOpen()){
                    if(item){
                        // that.editingItem.gen_product_ptg = item;
                        // that.editingItem.vat_product_ptg_code = item.code
                    }
                    let windowId = that.validator.host.attr('id')
                    $('#'+windowId+ ' .cbx_supplier_id').blur()

                }else{
                    if(that.cellEditor) {
                        // that.cellEditor.val(item.id)
                    }
                }
                
            },
        }
    }
    uomSetting;
    initUomCbx(){
        let that = this;
        if(that.uomSetting) return;
        that.uomSetting = {
            onOpen: ()=>{
                that.pending = true;
            },
            onClose: ()=>{
                that.pending = false;
                $('.jqx-grid-validation,.jqx-grid-validation-arrow-up').remove()
            },
            onBaseUomChange: function(event){
                let item = that.baseUomCbx.getSelectedItem();
                if(that.detailWindow.isOpen()){
                    if(item){
                        // that.editingItem.gen_product_ptg = item;
                        // that.editingItem.vat_product_ptg_code = item.code
                    }
                    let windowId = that.validator.host.attr('id')
                    $('#'+windowId+ ' .cbx_base_uom_id').blur()

                }else{
                    if(that.cellEditor) {
                        // that.cellEditor.val(item.id)
                    }
                }
                
            },
            onItemUomChange: function(event){
                let item = that.itemUomCbx.getSelectedItem();
                if(that.detailWindow.isOpen()){
                    if(item){
                        // that.editingItem.gen_product_ptg = item;
                        // that.editingItem.vat_product_ptg_code = item.code
                    }
                    let windowId = that.validator.host.attr('id')
                    $('#'+windowId+ ' .cbx_item_uom_id').blur()

                }else{
                    if(that.cellEditor) {
                        // that.cellEditor.val(item.id)
                    }
                }
                
            },
        }
    }
    initCbxs(){
        this.initGenProdPtgCbx()
        this.initVatProdPtgCbx()
        this.initInvtPtgCbx()
        this.initItemGroupCbx()
        this.initSupplierCbx()
        this.initUomCbx()
    }
    initDetailWindow(){
        let that = this;
        that.initCbxs()
        that.initValidation();
    }
    closeDetail(){
        this.detailWindow.close();
    }
    isSubmit = false;
    checkValidate(): void {
        console.log('AA')
        let that = this;
        setTimeout(() => {
            that.validator.validate();
        }, 200)
        this.isSubmit = true;
    }
    onValidationError(){
        this.isSubmit = false;
    }

    onValidationSuccess() : void {
        let that = this;
        if(that.isSubmit){
            var formData = cloneDeep(that.editingItem);
            formData.product_code = formData.product_code.toUpperCase();
            let newImages = formData.newImages;
            // delete formData.images;
            // delete formData.newImages;
            // formData.images = formData.newImages
            formData.sale_price = formData.sale_price_exclude_tax;
            formData.purchase_price = formData.purchase_price_exclude_tax;
            
            let vat = 0;
            if(formData.vat_product_ptg){
                vat = formData.vat_product_ptg.value;
            }
            formData.sale_price_include_tax = (((1+vat/100) * formData.sale_price) || 0)
            vat = 0;
            if(formData.purchase_tax_group){
                vat = formData.purchase_tax_group.value;
            }
            formData.purchase_price_include_tax = (((1+vat/100) * formData.purchase_price) || 0)
            console.log(formData,'params');
            that.api.post({
                url: AppSetting.ITEMS_COMMIT_URL,
                params: {
                    id:that.editingItem.id,
                    params: formData,
                    images: formData.newImages
                }
            }).done(function(res){
                if(res.code==1){
                    that.dataAdapter.dataBind();
                    // that.grid.updatebounddata('cells');
                    that.detailWindow.close()
                    toastr.success(Helpers.translate.get('Create successfuly'))
                }
            })
            
        }
    }
    rules
    windowId = '';
    initValidation(){
        let that = this;
        let windowId = that.validator.host.attr('id')
        that.windowId = windowId;
        if(that.rules){
            if(that.validator) that.validator.hide();
        }else
        that.rules =
        [
            { input: '#' + windowId+ ' .txt_product_code', message: Helpers.translate.get('Code') + Helpers.translate.get(' should be not empty'), action: 'keyup, blur', rule: 'required' },
            { input: '#' + windowId+ ' .txt_description', message: Helpers.translate.get('Name') + Helpers.translate.get(' should be not empty'), action: 'keyup, blur', rule: 'required' },
            { input: '#' + windowId+ ' .cbx_item_group_id', message: Helpers.translate.get('Item Group') + Helpers.translate.get(' should be not empty'), action: 'blur', rule: function () {
                    return !!that.itemGroupCbx.value;
                } 
            },{ input: '#' + windowId+ ' .cbx_supplier_id', message: Helpers.translate.get('Supplier') + Helpers.translate.get(' should be not empty'), action: 'blur', rule: function () {
                    return !!that.supplierCbx.value;
                } 
            },{ input: '#' + windowId+ ' .cbx_gen_product_ptg_code', message: Helpers.translate.get('General Product Posting Group') + Helpers.translate.get(' should be not empty'), action: 'blur', rule: function () {
                    return !!that.genProdPtgCbx.value;
                } 
            },{ input: '#' + windowId+ ' .cbx_vat_product_ptg_code', message: Helpers.translate.get('TAX Product Posting Group') + Helpers.translate.get(' should be not empty'), action: 'blur', rule: function () {
                    return !!that.salesTaxGroupCbx.value;
                } 
            },{ input: '#' + windowId+ ' .cbx_invt_ptg_code', message: Helpers.translate.get('Inventory Posting Group') + Helpers.translate.get(' should be not empty'), action: 'blur', rule: function () {
                    return !!that.invtPtgCbx.value;
                } 
            },

        ];

    }
    onSetDefaultPhoto(udid){
        this.editingItem.default_image = udid
    }
    onSelectPhoto(event){
        let that = this;
        var args = event.args;
        var fileName = args.file;
        var fileSize = args.size;
        console.log(args,that.fileUpload)
        var files = $(that.fileUpload.elementRef.nativeElement).find('input[type="file"]').get().map(function(f){
            return f.files[0]
        }).filter(function(f){
            return f;
        });

        // var images = files.map(function(file,i){
        Helpers.getBase64(files[files.length-1]).then(
          data => {
                that.fileUpload.cancelAll();
                if(!that.editingItem.newImages) that.editingItem.newImages = []

                let key = new Date().getTime()
                if(!that.editingItem.default_image) that.editingItem.default_image = key
                that.editingItem.newImages.push({
                    filename: fileName,
                    photo:data,
                    udid: key
                })
        //       // console.log(data)
        //        $('#jqxFileUpload .jqx-file-upload-file-row:eq('+i+') img').remove();
        //        $('#jqxFileUpload .jqx-file-upload-file-row:eq('+i+')').prepend($('<img/>').attr('src',data))
          }
        )
    }
}


/*
Copyright 2017-2018 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/