import { Component, OnInit, Input,forwardRef,AfterViewInit,OnChanges, ViewChild, ViewEncapsulation, EventEmitter, Output } from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from "@angular/forms";
import * as cloneDeep from 'lodash/cloneDeep';
import { DatePipe, CurrencyPipe } from '@angular/common';
import { APIService } from '../../../api.service';
import { Helpers} from "../../../helpers";
import { AppSetting } from "../../../settings";
import { AppMessage } from "../../../message";

import { jqxWindowComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxwindow';
import { jqxGridComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxgrid';
import { jqxValidatorComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxvalidator';
import { jqxDropDownListComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxdropdownlist';

import { AccountModule } from '../../../widgets/account/account.module';

declare var $:any;
declare var toastr:any;

const noop = () => {
};

@Component({
    selector: 'module-ptg-vender',
    templateUrl: './ptg-vender.module.html',
    styleUrls: ['./ptg-vender.module.css'],
    providers: [{
        provide: NG_VALUE_ACCESSOR,
        useExisting: forwardRef(() => VenderPtgModule),
        multi: true
    }]
})

export class VenderPtgModule implements OnInit,AfterViewInit, OnChanges, ControlValueAccessor {
    @Input() hidden: boolean;
    @Input() layout: string;
    @Input() key: string;
    @Output() onOpen = new EventEmitter();
    @Output() onClose = new EventEmitter();
    @Output() onChange = new EventEmitter();
    constructor(
        private api: APIService,
        ) {
        if(!!!this.key) this.key = 'code'
    }
    theme = AppSetting.THEME
    // The internal data model
    private innerValue: any = undefined;
    label = Helpers.translate.get("Please Choose");
    // Placeholders for the callbacks which are later provided
    // by the Control Value Accessor
    private onTouchedCallback: () => void = noop;
    private onChangeCallback: (_: any) => void = noop;

    @ViewChild('window') window: jqxWindowComponent;
    @ViewChild('detailWindow') detailWindow: jqxWindowComponent;
    @ViewChild('grid') grid: jqxGridComponent;
    @ViewChild('validator') validator: jqxValidatorComponent;
    @ViewChild('dropDownGrid') dropDownGrid: jqxGridComponent;
    @ViewChild('payAcctCbx') payAcctCbx: AccountModule;
    @ViewChild('dropDownButton') dropDownButton; 
    
    

    // get accessor
    get value(): any {
        return this.innerValue;
    };

    // set accessor including call the onchange callback
    set value(v: any) {
        if (v !== this.innerValue) {
            this.innerValue = v;
            this.onChangeCallback(v);
        }
        console.log('SetValue')
    }

    // Set touched on blur
    onBlur() {
        this.onTouchedCallback();
    }

    // From ControlValueAccessor interface
    writeValue(value: any) {
        if (value !== this.innerValue) {
            this.innerValue = value;
        }
    }

    // From ControlValueAccessor interface
    registerOnChange(fn: any) {
        this.onChangeCallback = fn;
    }

    // From ControlValueAccessor interface
    registerOnTouched(fn: any) {
        this.onTouchedCallback = fn;
    }



    ngOnInit() {
    }
    ngOnChanges(){

    }
    dropDownDataAdapter
    dropDownGridSetting
    initDropDownGrid(){
        let that = this;
        
        let columns = [
            { text: '#', dataField: 'id', width: 120 ,hidden: true},
            { text: Helpers.translate.get('Code'), dataField: 'code', width: 120 },
            { text: Helpers.translate.get('Paymentable Account'), dataField: 'pay_acct_code' },
        ]
        
        if(that.dropDownGridSetting){
            that.ensureDropDownGrid()
            that.dropDownGrid.focus()
            return;
        }
        Helpers.translate.do(()=>{
            if(that.grid){
                that.grid.setcolumnproperty('id','text', '#')
                that.grid.setcolumnproperty('code','text', Helpers.translate.get('Code'))
                that.grid.setcolumnproperty('pay_acct_code','text', Helpers.translate.get('Paymentable'))
                that.grid.setOptions({'localization': Helpers.getLocalizatio(Helpers.translate.activeLang)})
            }
        })
        let source = Helpers.Source({
            dataType: "json",
            type: 'POST',
            url: AppSetting.COMMON_BINDING_URL,
            data: {
                table:'ptg_vender'
            },
            dataFields: [
                { name: 'id', type: 'number' },
                { name: 'code', type: 'string' },
                { name: 'pay_acct_code', type: 'string' },
            
            ],
            filter: function() {
                // update the grid and send a request to the server.
                that.dropDownGrid.updatebounddata('filter');
            },
            sort: function() {
                // update the grid and send a request to the server.
                that.dropDownGrid.updatebounddata('sort');
            },
            id: 'id',
            root: 'data',
        });
        // that.gridPartner.setOptions({source:[]});
        that.dropDownDataAdapter = new jqx.dataAdapter(source, {
            beforeLoadComplete: function (records) {
                for (var i = 0; i < records.length; i++) {
                    var row = records[i];
                }
                return records;
            }
        });
        that.dropDownGridSetting = {
            columns: columns,
            localization: Helpers.getLocalizatio(Helpers.translate.activeLang),
            source: that.dropDownDataAdapter,
            ready: function() {
                that.ensureDropDownGrid()
                that.dropDownGrid.focus()
            },
            rendergridrows: (params: any): any[] => {
                return that.dropDownDataAdapter.records;
            },
            rendered(event: any): void {
            },
            onRowSelect: (event:any)=>{
                var args = event.args;
                let rowData = that.dropDownGrid.getrowdata(args.rowindex);
                if(rowData){
                    if(rowData){
                        that.setSelectedItem(rowData);
                        that.onChangeCallback(rowData[that.key])
                    }else{
                        that.setSelectedItem(undefined);
                        that.onChangeCallback(undefined)
                    }
                    that.onChange.emit()
                    if(that.dropDownButton) {
                        $(that.dropDownButton.nativeElement).dropdown('toggle');
                        $(that.dropDownButton.nativeElement).focus()
                    }
                }
            },
            onRowdoubleclick: (event:any)=>{
                var args = event.args;
                var rowBoundIndex = args.rowindex;
                var rowVisibleIndex = args.visibleindex;
                var rightclick = args.rightclick; 
                var columnindex = args.columnindex;
                var dataField = args.datafield;
                var value = args.value;
                if(args && args.row){
                    var dataRow = args.row.bounddata
                }
            },
            handlekeyboardnavigation: (event:any)=>{
                var key = event.charCode ? event.charCode : event.keyCode ? event.keyCode : 0;
                if (key == 13) {
                    let index = that.dropDownGrid.getselectedrowindex();
                    let rowData = that.dropDownGrid.getrowdata(index);
                    if(index>=0 && rowData){
                        that.setSelectedItem(rowData);
                        that.onChangeCallback(rowData[that.key])
                        that.onChange.emit();
                        if(that.dropDownButton) {
                            $(that.dropDownButton.nativeElement).dropdown('toggle');
                            $(that.dropDownButton.nativeElement).focus()
                        }
                    }
                    return true;
                };
            },
        }
    }
    ensureDropDownGrid(){
        let that = this;
        if(that.dropDownGrid){
            let rowid;
            let index = that.dropDownGrid.getselectedrowindex();
            if(index == -1) index = 0;
            let rowData = that.dropDownGrid.getrowdata(index);
            if(rowData && rowData[that.key]==that.innerValue){
                console.log('NOTHING')
            }else{
                that.dropDownDataAdapter.records.map((row)=>{
                    if(row[that.key] == that.innerValue){
                        rowid = row.id
                    }
                })
                let rowIndex = that.dropDownGrid.getrowboundindexbyid(rowid)
                if(rowIndex == -1) rowIndex = 0;
                that.dropDownGrid.ensurerowvisible(rowIndex);
                that.dropDownGrid.selectedrowindex(rowIndex);
            }
        }
    }
    initDropdown($event){
        let that = this;
    }
    onDropdownSelectect(item){
        let that = this;
        that.setSelectedItem(item);
        that.onChangeCallback(item[that.key])
        that.onChange.emit()
    }
    switchToAdvance(){
        if(this.dropDownButton) $(this.dropDownButton.nativeElement).dropdown('toggle');
        this.open()
    }


    selectedItem;
    setSelectedItem(item){
        let that = this;
        that.selectedItem = item;
        if(item  && item.id){
            that.label = item.code;
            that.innerValue = item[that.key];
        }else{
            that.label = Helpers.translate.get("Please Choose");
            that.innerValue = undefined
        }
    }
    getSelectedItem(){
        return this.selectedItem;
    }
    setValue(v){
        this.innerValue = v
        this.initDisplayValue();
        
    }
    ensure(){
        let that = this;
        console.log('ensure',that.innerValue)
        if(that.grid){
            let rowIndex = that.grid.getrowboundindexbyid(that.innerValue)
            that.grid.ensurerowvisible(rowIndex);
            that.grid.selectedrowindex(rowIndex);
        }
    }
    initDisplayValue(){
        let that = this;
        setTimeout((any)=>{
            if(!!that.innerValue){
                let params = {};
                params[that.key] = that.innerValue;
                that.api.post({
                    url: AppSetting.GET_VENDER_POST_URL,
                    params:params
                }).done(function(res){
                    if(res.data){
                        that.setSelectedItem(res.data);
                    }else{
                        that.setSelectedItem(undefined);
                    }
                })
            }else{
                that.setSelectedItem(undefined);
            }
        }, 42)
    }
    ngAfterViewInit(){
        let that = this;
        that.loadAdapter();
        if(that.layout == 'grid') 
            setTimeout(()=>{
                that.initGrid();
            },200)
        that.initDisplayValue()
        setTimeout(()=>{
            if(!that.dropDownButton) return;
            $(that.dropDownButton.nativeElement).parent().on('show.bs.dropdown', function () {
                that.initDropDownGrid()
            });
            $(that.dropDownButton.nativeElement).keydown(function(e){
                if(e.keyCode == 13) {
                    return false;
                }
            });
        },300)
    }

    toggle(){
        if(this.window.isOpen()) this.close();
        else this.open()
    }
    open(){
        this.window.open();
        this.initGrid()
        // this.onOpen.emit()
        
    }
    eventWindowOpen(){
        this.ensure()
        this.onOpen.emit()
    }
    eventWindowClose(){
        this.onClose.emit()
    }
    close(){
        this.window.close();
        // this.onClose.emit()
    }
    clear(){
        let that = this;
        that.grid.clearselection()
        // that.label = "Please Choose";
    }
    done(){
        let that = this;
        let index = that.grid.getselectedrowindex();
        let rowData = that.grid.getrowdata(index);
        if(index>=0 && rowData){
            that.setSelectedItem(rowData);
            that.onChangeCallback(rowData[that.key])
        }else{
            that.setSelectedItem(undefined);
            that.onChangeCallback(undefined)
        }
        that.onChange.emit()
        that.close();
    }
    initContent(){
        setTimeout(()=>{
            let that = this;
            console.log(this,self)
        },200)
    }

    source;
    dataAdapter;
    gridSetting;
    loadAdapter(){
        let that = this;
        that.source = Helpers.Source({
            dataType: "json",
            type: 'POST',
            url: AppSetting.COMMON_BINDING_URL,
            data: {
                table:'ptg_vender'
            },
            dataFields: [
                { name: 'id', type: 'number' },
                { name: 'code', type: 'string' },
                { name: 'pay_acct_code', type: 'string' },
            
            ],
            filter: function() {
                // update the grid and send a request to the server.
                that.grid.updatebounddata('filter');
            },
            sort: function() {
                // update the grid and send a request to the server.
                that.grid.updatebounddata('sort');
            },
            id: 'id',
            root: 'data',
        });
        // that.gridPartner.setOptions({source:[]});
        that.dataAdapter = new jqx.dataAdapter(that.source, {
            beforeLoadComplete: function (records) {
                for (var i = 0; i < records.length; i++) {
                    var row = records[i];
                }
                return records;
            }
        });
    }
    pending = false;
    beforeEditData;
    currentEditData;
    cellEditor;
    initGrid(){
        let that = this;
        
        let columns = [
                { text: '#', dataField: 'id', width: 120 ,hidden: true},
                { 
                    text: Helpers.translate.get('Code'), dataField: 'code', width: 120,
                    validation: (cell: any, value: any): any => {
                        if (value == '')
                            return { result: false, message: Helpers.translate.get('Code') + Helpers.translate.get(' should be not empty') };
                        return true;
                    },
                },
                { 
                    text: Helpers.translate.get('Paymentable Account'), dataField: 'pay_acct_code', width: 220,
                    validation: (cell: any, value: any): any => {
                        if(that.pending){
                            return { result: false,message:'Choose ...'};
                        }
                        if (value == '')
                            return { result: false, message: Helpers.translate.get('Paymentable Account') + Helpers.translate.get(' should be not empty') };
                        return true;
                    },
                    createeditor: (row: number, cellvalue: any, editor: any, cellText: any, width: any, height: any): void => {
                        $('<span class="editor-dropdown-button"></span>')
                            .on('click',()=>{
                                console.log('dblclick',editor)
                                that.payAcctCbx.open();
                                that.payAcctCbx.setValue(that.currentEditData.pay_acct_code);
                                that.cellEditor = editor;
                            })
                            .insertAfter(editor)
                        editor.css({width:'100%'})
                    },
                },
            ]
        
        if(that.gridSetting){
            return;
        }
        Helpers.translate.do(()=>{
            if(that.grid){
                that.grid.setcolumnproperty('id','text', '#')
                that.grid.setcolumnproperty('code','text', Helpers.translate.get('Code'))
                that.grid.setcolumnproperty('pay_acct_code','text', Helpers.translate.get('Paymentable Account'))
                that.grid.setOptions({'localization': Helpers.getLocalizatio(Helpers.translate.activeLang)})
            }
        })
        that.gridSetting = {
            columns: columns,
            localization: Helpers.getLocalizatio(Helpers.translate.activeLang),
            source: that.dataAdapter,
            ready: function() {
                that.initAcctCbx();
                that.ensure();
            },
            rendergridrows: (params: any): any[] => {
                return that.dataAdapter.records;
            },
            rendered(event: any): void {
                that.grid.expandallgroups();
            },
            onRowdoubleclick: (event:any)=>{
                // event arguments.
                var args = event.args;
                // row's bound index.
                var rowBoundIndex = args.rowindex;
                // row's visible index.
                var rowVisibleIndex = args.visibleindex;
                // right click.
                var rightclick = args.rightclick; 
                // original event.
                var ev = args.originalEvent;
                // column index.
                var columnindex = args.columnindex;
                // column data field.
                var dataField = args.datafield;
                // cell value
                var value = args.value;
                if(args && args.row){
                    var dataRow = args.row.bounddata
                    if(dataRow) that.showDetail(dataRow.id)
                }
            },
            onCellBeginEdit(event: any): void {
                let args = event.args;
                // if(!that.currentEditData){
                    that.beforeEditData = cloneDeep(args.row);
                    that.currentEditData = cloneDeep(args.row);
                // }
            },
            onCellEndEdit(event: any): void {
                let args = event.args;
                let row = args.row;
                that.currentEditData[args.datafield] = args.value;
                if (args.datafield === 'pay_acct_code') {
                    if(
                        JSON.stringify(that.currentEditData) !=
                        JSON.stringify(that.beforeEditData)
                        ){
                        let params = cloneDeep(that.currentEditData);
                        params.code = params.code.toUpperCase();
                        let conditions
                        if(params.id<0){
                            delete params.id;
                        }
                        that.api.post({
                            url: AppSetting.VENDER_POST_COMMIT_URL,
                            params: {
                                id: params.id,
                                params: params
                            },
                            onDone:function(res){
                                if(!params.id){
                                    that.currentEditData.id = res.data.id
                                    toastr.success(Helpers.translate.get('Create successfuly'))
                                }else{
                                    toastr.success(Helpers.translate.get('Update successfuly'))
                                }
                                // let index = that.grid.getselectedrowindex();
                                // let id = that.grid.getrowid(index);
                                // that.grid.updaterow(id,that.currentEditData);
                                // that.dataAdapter.dataBind();
                                //that.grid.updatebounddata('cells');
                                that.currentEditData = undefined;
                                that.beforeEditData = undefined;
                            },
                            onFail:function(){
                                toastr.warning(Helpers.translate.get('Fail to create/update'))
                            },
                        })
                    }
                }
            },
        }
    }
    reloadGrid(){
        this.dataAdapter.dataBind();
        this.grid.updatebounddata('cells');
    }
    editingItem = {
        id:0,
        code:'',
        pay_acct_code: ''
    }
    clickAdd(){
        this.showDetail()
    }
    clickEdit(){
        let that = this;
        let index = that.grid.getselectedrowindex();
        let rowData = that.grid.getrowdata(index);
        if(rowData){
            that.showDetail(rowData.id)
        }
    }
    clickDelete(){
        let that = this;
        let index = that.grid.getselectedrowindex();
        let rowData = that.grid.getrowdata(index);
        if(rowData){
            Helpers.confirm('<h3>' + Helpers.translate.get('Are you sure you want to delete this item?') + '</h3>',function(){
                
                that.api.post({
                    url: AppSetting.DEL_VENDER_POST_URL,
                    params: {
                        id: rowData.id
                    }
                }).done(function(res){
                    if(res.code){
                        that.dataAdapter.dataBind();
                        that.grid.updatebounddata('cells');
                    }
                })
            })
        }
    }
    showDetail(id=0){
        let that = this;
        that.detailWindow.open();
        if(id){
            that.api.post({
                url: AppSetting.GET_VENDER_POST_URL,
                params: {
                    id: id
                }
            }).done(function(res){
                if(res.data){
                    that.setData(res.data)
                }
            })
        }else{
            that.setData({
                id:0,
                code: '',
                pay_acct_code: '',
            })
        }
    }
    setData(data){
        let that = this
        that.editingItem = data
        
        if(data.pay_acct_code && that.payAcctCbx){
            that.payAcctCbx.setValue(data.pay_acct_code)
        } else if(that.payAcctCbx) that.payAcctCbx.setValue(null);

    }
    acctSetting
    initAcctCbx(){
        let that = this;
        that.acctSetting = {
                onOpen: ()=>{
                    that.pending = true;
                },
                onClose: ()=>{
                    that.pending = false;
                    $('.jqx-grid-validation,.jqx-grid-validation-arrow-up').remove()
                },
                onPayAcctChange: function(args){
                    let item = args.getSelectedItem();
                    if(that.detailWindow.isOpen()){
                        if(item){
                            // that.editingItem.pay_acct = item;
                            that.editingItem.pay_acct_code = item.acct_code
                        }
                        let windowId = that.validator.host.attr('id')
                        $('#'+windowId+ ' .cbx_payAcct').blur()
                    }else{
                        if(that.cellEditor) that.cellEditor.val(item.acct_code)
                    }

                },
                
            }
    }
    initDetailWindow(){
        console.log('initDetailWindow')
        let that = this;
        that.initAcctCbx()
        that.initValidation();
    }
    closeDetail(){
        this.detailWindow.close();
    }
    isSubmit = false;
    checkValidate(): void {
        console.log('AA')
        let that = this;
        setTimeout(() => {
            that.validator.validate();
        }, 200)
        this.isSubmit = true;
    }
    onValidationError(){
        this.isSubmit = false;
    }

    onValidationSuccess() : void {
        let that = this;
        if(that.isSubmit){
            let params = that.editingItem;
            params.code = params.code.toUpperCase();
            console.log(params,'params');
            that.api.post({
                url: AppSetting.VENDER_POST_COMMIT_URL,
                params: {
                    id:that.editingItem.id,
                    params: {
                        code: params.code,
                        pay_acct_code: params.pay_acct_code,
                    }
                }
            }).done(function(res){
                if(res.code==1){
                    that.dataAdapter.dataBind();
                    that.grid.updatebounddata('cells');
                    that.detailWindow.close()
                    toastr.success(Helpers.translate.get('Create successfuly'))
                }
            })
            
        }
    }
    rules
    initValidation(){
        let that = this;
        let windowId = that.validator.host.attr('id')
        if(that.rules){
            if(that.validator) that.validator.hide();
        }else
        that.rules =
        [
            { input: '#' + windowId+ ' .txt_code', message: Helpers.translate.get('Code') + Helpers.translate.get(' should be not empty'), action: 'keyup, blur', rule: 'required' },
            { input: '#' + windowId+ ' .cbx_payAcct', message: Helpers.translate.get('Paymentable Account') + Helpers.translate.get(' should be not empty'), action: 'blur', 
                rule: function () {
                    return !!that.payAcctCbx.value;
                } 
            }

        ];

    }
    addNewItem(){
        let that = this;
        var commit = that.grid.addrow(null, {
            id:-(new Date().getTime()),
            code: '',
            pay_acct_code: '',
        },'first');
        setTimeout(()=>{
            that.grid.beginrowedit(0)
        },200)
        
    }
}


/*
Copyright 2017-2018 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/