import { Component, OnInit, Input,forwardRef,AfterViewInit,OnChanges, ViewChild, ViewEncapsulation, EventEmitter, Output } from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from "@angular/forms";
import * as cloneDeep from 'lodash/cloneDeep';
import { DatePipe, CurrencyPipe } from '@angular/common';
import { APIService } from '../../../../api.service';
import { Helpers} from "../../../../helpers";
import { AppSetting } from "../../../../settings";
import { AppMessage } from "../../../../message";

import { jqxWindowComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxwindow';
import { jqxGridComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxgrid';
import { jqxValidatorComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxvalidator';
import { jqxDropDownListComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxdropdownlist';

import { GeneralBussinessPtgModule } from '../../../../widgets/ptg/general/business/ptg-gen-business.module';
import { GeneralProductPtgModule } from '../../../../widgets/ptg/general/product/ptg-gen-product.module';
import { AccountModule } from '../../../../widgets/account/account.module';

declare var $:any;
declare var toastr:any;

const noop = () => {
};

@Component({
    selector: 'module-ptg-gen-setup',
    templateUrl: './ptg-gen-setup.module.html',
    styleUrls: ['./ptg-gen-setup.module.css'],
    providers: [{
        provide: NG_VALUE_ACCESSOR,
        useExisting: forwardRef(() => GeneralSetupPtgModule),
        multi: true
    }]
})

export class GeneralSetupPtgModule implements OnInit,AfterViewInit, OnChanges, ControlValueAccessor {
    @Input() hidden: boolean;
    @Input() layout: string;
    @Input() key: string;
    @Output() onOpen = new EventEmitter();
    @Output() onClose = new EventEmitter();
    @Output() onChange = new EventEmitter();
    constructor(
        private api: APIService,
        ) {
        if(!!!this.key) this.key = 'code'
    }
    theme = AppSetting.THEME
    // The internal data model
    private innerValue: any = undefined;
    label = "Please Choose";
    // Placeholders for the callbacks which are later provided
    // by the Control Value Accessor
    private onTouchedCallback: () => void = noop;
    private onChangeCallback: (_: any) => void = noop;

    @ViewChild('window') window: jqxWindowComponent;
    @ViewChild('detailWindow') detailWindow: jqxWindowComponent;
    @ViewChild('grid') grid: jqxGridComponent;
    @ViewChild('validator') validator: jqxValidatorComponent;
    @ViewChild('vatCbx') vatCbx: jqxDropDownListComponent;
    @ViewChild('genBusPtgCbx') genBusPtgCbx: GeneralBussinessPtgModule;
    @ViewChild('genProdPtgCbx') genProdPtgCbx: GeneralProductPtgModule;
    @ViewChild('salesAcctCbx') salesAcctCbx: AccountModule;
    @ViewChild('purchAcctCbx') purchAcctCbx: AccountModule;
    @ViewChild('salesCreditMemoAcctCbx') salesCreditMemoAcctCbx: AccountModule;
    @ViewChild('purchCreditMemoAcctCbx') purchCreditMemoAcctCbx: AccountModule;
    @ViewChild('dropDownGrid') dropDownGrid: jqxGridComponent;
    @ViewChild('cogsAcctCbx') cogsAcctCbx: AccountModule;
    @ViewChild('invtAdjmtAcctCbx') invtAdjmtAcctCbx: AccountModule;
    @ViewChild('salesLineDiscAcctCbx') salesLineDiscAcctCbx: AccountModule;
    @ViewChild('salesInvtDiscAcctCbx') salesInvtDiscAcctCbx: AccountModule;
    @ViewChild('purchLineDiscAcctCbx') purchLineDiscAcctCbx: AccountModule;
    @ViewChild('purchInvtDiscAcctCbx') purchInvtDiscAcctCbx: AccountModule;
    

    // get accessor
    get value(): any {
        return this.innerValue;
    };

    // set accessor including call the onchange callback
    set value(v: any) {
        if (v !== this.innerValue) {
            this.innerValue = v;
            this.onChangeCallback(v);
        }
        console.log('SetValue')
    }

    // Set touched on blur
    onBlur() {
        this.onTouchedCallback();
    }

    // From ControlValueAccessor interface
    writeValue(value: any) {
        if (value !== this.innerValue) {
            this.innerValue = value;
        }
    }

    // From ControlValueAccessor interface
    registerOnChange(fn: any) {
        this.onChangeCallback = fn;
    }

    // From ControlValueAccessor interface
    registerOnTouched(fn: any) {
        this.onTouchedCallback = fn;
    }



    ngOnInit() {
    }
    ngOnChanges(){

    }

    dropDownDataAdapter
    dropDownGridSetting
    initDropDownGrid(){
        let that = this;
        
        let columns = [
            { text: '#', dataField: 'id', width: 120 ,hidden: true},
            { text: Helpers.translate.get('Gen. Bus. Post. Group'), dataField: 'gen_business_ptg_code', width: 120 },
            { text: Helpers.translate.get('Gen. Prod. Post. Group'), dataField: 'gen_product_ptg_code' },
        ]
        
        if(that.dropDownGridSetting){
            that.ensureDropDownGrid()
            return;
        }
        Helpers.translate.do(()=>{
            if(that.dropDownGrid){
                that.dropDownGrid.setcolumnproperty('id','text', '#')
                that.dropDownGrid.setOptions({'localization': Helpers.getLocalizatio(Helpers.translate.activeLang)})
            }
        })
        let source = Helpers.Source({
            dataType: "json",
            type: 'POST',
            url: AppSetting.COMMON_BINDING_URL,
            data: {
                table:'ptg_gen_setup'
            },
            dataFields: [
                { name: 'id', type: 'number' },
                { name: 'gen_business_ptg_code', type: 'string' },
                { name: 'gen_product_ptg_code', type: 'string' },
            
            ],
            filter: function() {
                // update the grid and send a request to the server.
                that.dropDownGrid.updatebounddata('filter');
            },
            sort: function() {
                // update the grid and send a request to the server.
                that.dropDownGrid.updatebounddata('sort');
            },
            id: 'id',
            root: 'data',
        });
        // that.gridPartner.setOptions({source:[]});
        that.dropDownDataAdapter = new jqx.dataAdapter(source, {
            beforeLoadComplete: function (records) {
                for (var i = 0; i < records.length; i++) {
                    var row = records[i];
                }
                return records;
            }
        });
        that.dropDownGridSetting = {
            columns: columns,
            localization: Helpers.getLocalizatio(Helpers.translate.activeLang),
            source: that.dropDownDataAdapter,
            ready: function() {
                that.ensureDropDownGrid()
            },
            rendergridrows: (params: any): any[] => {
                return that.dropDownDataAdapter.records;
            },
            rendered(event: any): void {
            },
            onRowSelect: (event:any)=>{
                var args = event.args;
                let rowData = that.dropDownGrid.getrowdata(args.rowindex);
                if(rowData){
                    if(rowData){
                        that.setSelectedItem(rowData);
                        that.onChangeCallback(rowData[that.key])
                    }else{
                        that.setSelectedItem(undefined);
                        that.onChangeCallback(undefined)
                    }
                    that.onChange.emit()
                    if(that.dropDownButton) $(that.dropDownButton).trigger('click')
                }
            },
            onRowdoubleclick: (event:any)=>{
                var args = event.args;
                var rowBoundIndex = args.rowindex;
                var rowVisibleIndex = args.visibleindex;
                var rightclick = args.rightclick; 
                var columnindex = args.columnindex;
                var dataField = args.datafield;
                var value = args.value;
                if(args && args.row){
                    var dataRow = args.row.bounddata
                }
            },
        }
    }
    ensureDropDownGrid(){
        let that = this;
        if(that.dropDownGrid){
            let rowid;
            let index = that.dropDownGrid.getselectedrowindex();
            let rowData = that.dropDownGrid.getrowdata(index);
            if(rowData && rowData[that.key]==that.innerValue){

            }else{
                that.dropDownDataAdapter.records.map((row)=>{
                    if(row[that.key] == that.innerValue){
                        rowid = row.id
                    }
                })
                let rowIndex = that.dropDownGrid.getrowboundindexbyid(rowid)
                that.dropDownGrid.ensurerowvisible(rowIndex);
                that.dropDownGrid.selectedrowindex(rowIndex);
            }
        }
    }
    dropDownButton
    initDropdown($event){
        this.dropDownButton = $event.target
        this.initDropDownGrid()
    }
    onDropdownSelectect(item){
        let that = this;
        that.setSelectedItem(item);
        that.onChangeCallback(item[that.key])
        that.onChange.emit()
    }
    switchToAdvance(){
        if(this.dropDownButton) $(this.dropDownButton).trigger('click');
        this.open()
    }

    
    selectedItem;
    setSelectedItem(item){
        let that = this;
        that.selectedItem = item;
        if(item  && item.id){
            that.label = item.code + ' ' + item.description;
            that.innerValue = item[that.key];
        }else{
            that.label = "Please Choose";
            that.innerValue = undefined
        }
    }
    getSelectedItem(){
        return this.selectedItem;
    }
    setValue(v){
        this.innerValue = v
        this.initDisplayValue();
        
    }
    ensure(){
        let that = this;
        console.log('ensure',that.innerValue)
        if(that.grid){
            let rowIndex = that.grid.getrowboundindexbyid(that.innerValue)
            that.grid.ensurerowvisible(rowIndex);
            that.grid.selectedrowindex(rowIndex);
        }
    }
    initDisplayValue(){
        let that = this;
        setTimeout((any)=>{
            if(that.innerValue!=undefined){
                // that.api.post({
                //     url: AppSetting.ACTIVITY_DETAIL_URL,
                //     params:{
                //         id: that.innerValue
                //     }
                // }).done(function(res){
                //     if(res.data){
                //         that.setSelectedItem(res.data);
                //         that.onChange.emit()
                //     }
                // })
            }else{
                //that.onChange.emit()
            }
        }, 42)
    }
    ngAfterViewInit(){
        let that = this;
        that.loadAdapter();
        if(that.layout == 'grid') 
            setTimeout(()=>{
                that.initGrid();
            },200)
        that.initDisplayValue()
    }

    toggle(){
        if(this.window.isOpen()) this.close();
        else this.open()
    }
    open(){
        this.window.open();
        this.initGrid()
        // this.onOpen.emit()
        
    }
    eventWindowOpen(){
        this.ensure()
        this.onOpen.emit()
    }
    eventWindowClose(){
        this.onClose.emit()
    }
    close(){
        this.window.close();
        // this.onClose.emit()
    }
    clear(){
        let that = this;
        that.grid.clearselection()
        // that.label = "Please Choose";
    }
    done(){
        let that = this;
        let index = that.grid.getselectedrowindex();
        let rowData = that.grid.getrowdata(index);
        if(index>=0 && rowData){
            that.setSelectedItem(rowData);
            that.onChangeCallback(rowData[that.key])
        }else{
            that.setSelectedItem(undefined);
            that.onChangeCallback(undefined)
        }
        that.onChange.emit()
        that.close();
    }
    initContent(){
        setTimeout(()=>{
            let that = this;
            console.log(this,self)
        },200)
    }

    source;
    dataAdapter;
    gridSetting;
    loadAdapter(){
        let that = this;
        that.source = Helpers.Source({
            dataType: "json",
            type: 'POST',
            url: AppSetting.COMMON_BINDING_URL,
            data: {
                table:'ptg_gen_setup'
            },
            dataFields: [
                { name: 'id', type: 'number' },
                { name: 'gen_business_ptg_code', type: 'string' },
                { name: 'gen_product_ptg_code', type: 'string' },
                { name: 'sales_acct_code', type: 'string' },
                { name: 'sales_credit_memo_acct_code', type: 'string' },
                { name: 'purch_acct_code', type: 'string' },
                { name: 'purch_credit_memo_acct_code', type: 'string' },
                { name: 'cogs_acct_code', type: 'string' },
                { name: 'invt_adjmt_acct_code', type: 'string' },
                { name: 'sales_disc_line_acct_code', type: 'string' },
                { name: 'sales_disc_invt_acct_code', type: 'string' },
                { name: 'purch_disc_line_acct_code', type: 'string' },
                { name: 'purch_disc_invt_acct_code', type: 'string' },
            
            ],
            filter: function() {
                // update the grid and send a request to the server.
                that.grid.updatebounddata('filter');
            },
            sort: function() {
                // update the grid and send a request to the server.
                that.grid.updatebounddata('sort');
            },
            id: 'id',
            root: 'data',
        });
        // that.gridPartner.setOptions({source:[]});
        that.dataAdapter = new jqx.dataAdapter(that.source, {
            beforeLoadComplete: function (records) {
                for (var i = 0; i < records.length; i++) {
                    var row = records[i];
                }
                return records;
            }
        });
    }
    pending = false;
    beforeEditData;
    currentEditData;
    cellEditor
    initGrid(){
        let that = this;
        
        let columns = [
                { text: '#', dataField: 'id', width: 120 ,hidden: true},
                { 
                    text: 'Gen. Bus. Posting Group', dataField: 'gen_business_ptg_code', width: 100,
                    renderer: function (text, align, columnsheight) {
                        return '<div class="column-header-two-line" title="'+Helpers.translate.get('Gen. Bus. Post. Group')+'">'+Helpers.translate.get('Gen. Bus. Post. Group')+'</div>';
                    },
                    filtertype: 'input',
                    validation: (cell: any, value: any): any => {
                        if(that.pending){
                            return { result: false,message:'Choose ...'};
                        }
                        // if (value == '')
                            // return { result: false, message: 'Business Posting Group should be not empty' };
                        return true;
                    },
                    createeditor: (row: number, cellvalue: any, editor: any, cellText: any, width: any, height: any): void => {
                        let span = $('<span class="editor-dropdown-button"></span>')
                            .on('click',()=>{
                                console.log('dblclick',editor)
                                that.genBusPtgCbx.open();
                                that.genBusPtgCbx.setValue(that.currentEditData.gen_business_ptg_code);
                                that.cellEditor = editor;
                            })

                        span.insertAfter(editor)
                        setInterval(()=>{
                            if(!span.parent().length){
                                span.insertAfter(editor)
                            }
                        }, 120)
                        editor.css({width:'100%'})
                    },
                },{ 
                    text: 'Gen. Prod. Posting Group', dataField: 'gen_product_ptg_code', width: 100,
                    renderer: function (text, align, columnsheight) {
                        return '<div class="column-header-two-line" title="'+Helpers.translate.get('Gen. Prod. Post. Group')+'">'+Helpers.translate.get('Gen. Prod. Post. Group')+'</div>';
                    },
                    filtertype: 'input',
                    createeditor: (row: number, cellvalue: any, editor: any, cellText: any, width: any, height: any): void => {
                        let span = $('<span class="editor-dropdown-button"></span>')
                            .on('click',()=>{
                                console.log('dblclick',editor)
                                that.genProdPtgCbx.open();
                                that.genProdPtgCbx.setValue(that.currentEditData.gen_product_ptg_code);
                                that.cellEditor = editor;
                            })
                            .insertAfter(editor)
                        setInterval(()=>{
                            if(!span.parent().length){
                                span.insertAfter(editor)
                            }
                        }, 120)
                        editor.css({width:'100%'})
                    },
                },{ 
                    text: 'Sales. Acct.', dataField: 'sales_acct_code', width: 90,
                    renderer: function (text, align, columnsheight) {
                        return '<div class="column-header-two-line" title="'+Helpers.translate.get('Sales Account')+'">'+Helpers.translate.get('Sales Account')+'</div>';
                    },
                    createeditor: (row: number, cellvalue: any, editor: any, cellText: any, width: any, height: any): void => {
                        let span = $('<span class="editor-dropdown-button"></span>')
                            .on('click',()=>{
                                console.log('dblclick',editor)
                                that.salesAcctCbx.open();
                                that.salesAcctCbx.setValue(that.currentEditData.sales_acct_code);
                                that.cellEditor = editor;
                            })
                            .insertAfter(editor)
                        setInterval(()=>{
                            if(!span.parent().length){
                                span.insertAfter(editor)
                            }
                        }, 120)
                        editor.css({width:'100%'})
                    },
                },{ 
                    text: 'Sales. Cred. Memo. Acct.', dataField: 'sales_credit_memo_acct_code', width: 90,
                    renderer: function (text, align, columnsheight) {
                        return '<div class="column-header-two-line" title="'+Helpers.translate.get('Sales Credit Memo Account')+'">'+Helpers.translate.get('Sales Credit Memo Account')+'</div>';
                    },
                    createeditor: (row: number, cellvalue: any, editor: any, cellText: any, width: any, height: any): void => {
                        let span = $('<span class="editor-dropdown-button"></span>')
                            .on('click',()=>{
                                console.log('dblclick',editor)
                                that.salesAcctCbx.open();
                                that.salesAcctCbx.setValue(that.currentEditData.sales_credit_memo_acct_code);
                                that.cellEditor = editor;
                            })
                            .insertAfter(editor)
                        setInterval(()=>{
                            if(!span.parent().length){
                                span.insertAfter(editor)
                            }
                        }, 120)
                        editor.css({width:'100%'})
                    },
                },{ 
                    text: 'Purch. Acct.', dataField: 'purch_acct_code', width: 90,
                    renderer: function (text, align, columnsheight) {
                        return '<div class="column-header-two-line" title="'+Helpers.translate.get('Purchase Account')+'">'+Helpers.translate.get('Purchase Account')+'</div>';
                    },
                    createeditor: (row: number, cellvalue: any, editor: any, cellText: any, width: any, height: any): void => {
                        let span = $('<span class="editor-dropdown-button"></span>')
                            .on('click',()=>{
                                console.log('dblclick',editor)
                                that.salesAcctCbx.open();
                                that.salesAcctCbx.setValue(that.currentEditData.purch_acct_code);
                                that.cellEditor = editor;
                            })
                            .insertAfter(editor)
                        setInterval(()=>{
                            if(!span.parent().length){
                                span.insertAfter(editor)
                            }
                        }, 120)
                        editor.css({width:'100%'})
                    },
                },{ 
                    text: 'Purch. Cred. Memo. Acct.', dataField: 'purch_credit_memo_acct_code', width: 90,
                    renderer: function (text, align, columnsheight) {
                        return '<div class="column-header-two-line" title="'+Helpers.translate.get('Purchase Credit Memo Account')+'">'+Helpers.translate.get('Purchase Credit Memo Account')+'</div>';
                    },
                    createeditor: (row: number, cellvalue: any, editor: any, cellText: any, width: any, height: any): void => {
                        let span = $('<span class="editor-dropdown-button"></span>')
                            .on('click',()=>{
                                console.log('dblclick',editor)
                                that.salesAcctCbx.open();
                                that.salesAcctCbx.setValue(that.currentEditData.purch_credit_memo_acct_code);
                                that.cellEditor = editor;
                            })
                            .insertAfter(editor)
                        setInterval(()=>{
                            if(!span.parent().length){
                                span.insertAfter(editor)
                            }
                        }, 120)
                        editor.css({width:'100%'})
                    },
                },{ 
                    text: 'COGS Acct.', dataField: 'cogs_acct_code', width: 90,
                    renderer: function (text, align, columnsheight) {
                        return '<div class="column-header-two-line" title="'+Helpers.translate.get('COGS Account')+'">'+Helpers.translate.get('COGS Account')+'</div>';
                    },
                    createeditor: (row: number, cellvalue: any, editor: any, cellText: any, width: any, height: any): void => {
                        let span = $('<span class="editor-dropdown-button"></span>')
                            .on('click',()=>{
                                console.log('dblclick',editor)
                                that.salesAcctCbx.open();
                                that.salesAcctCbx.setValue(that.currentEditData.cogs_acct_code);
                                that.cellEditor = editor;
                            })
                            .insertAfter(editor)
                        setInterval(()=>{
                            if(!span.parent().length){
                                span.insertAfter(editor)
                            }
                        }, 120)
                        editor.css({width:'100%'})
                    },
                },{ 
                    text: 'Invt. Adjmt. Acct.', dataField: 'invt_adjmt_acct_code', width: 90,
                    renderer: function (text, align, columnsheight) {
                        return '<div class="column-header-two-line" title="'+Helpers.translate.get('Inventory Adjustment Account')+'">'+Helpers.translate.get('Inventory Adjustment Account')+'</div>';
                    },
                    createeditor: (row: number, cellvalue: any, editor: any, cellText: any, width: any, height: any): void => {
                        let span = $('<span class="editor-dropdown-button"></span>')
                            .on('click',()=>{
                                console.log('dblclick',editor)
                                that.salesAcctCbx.open();
                                that.salesAcctCbx.setValue(that.currentEditData.invt_adjmt_acct_code);
                                that.cellEditor = editor;
                            })
                            .insertAfter(editor)
                        setInterval(()=>{
                            if(!span.parent().length){
                                span.insertAfter(editor)
                            }
                        }, 120)
                        editor.css({width:'100%'})
                    },
                },{ 
                    text: 'Sales Line Disc. Acct.', dataField: 'sales_disc_line_acct_code', width: 90,
                    renderer: function (text, align, columnsheight) {
                        return '<div class="column-header-two-line" title="'+Helpers.translate.get('Sales Line Discount Account')+'">'+Helpers.translate.get('Sales Line Discount Account')+'</div>';
                    },
                    createeditor: (row: number, cellvalue: any, editor: any, cellText: any, width: any, height: any): void => {
                        let span = $('<span class="editor-dropdown-button"></span>')
                            .on('click',()=>{
                                console.log('dblclick',editor)
                                that.salesAcctCbx.open();
                                that.salesAcctCbx.setValue(that.currentEditData.sales_disc_line_acct_code);
                                that.cellEditor = editor;
                            })
                            .insertAfter(editor)
                        setInterval(()=>{
                            if(!span.parent().length){
                                span.insertAfter(editor)
                            }
                        }, 120)
                        editor.css({width:'100%'})
                    },
                },{ 
                    text: 'Sales Inv. Disc. Acct.', dataField: 'sales_disc_invt_acct_code', width: 90,
                    renderer: function (text, align, columnsheight) {
                        return '<div class="column-header-two-line" title="'+Helpers.translate.get('Sales Inventory Discount Account')+'">'+Helpers.translate.get('Sales Inventory Discount Account')+'</div>';
                    },
                    createeditor: (row: number, cellvalue: any, editor: any, cellText: any, width: any, height: any): void => {
                        let span = $('<span class="editor-dropdown-button"></span>')
                            .on('click',()=>{
                                console.log('dblclick',editor)
                                that.salesAcctCbx.open();
                                that.salesAcctCbx.setValue(that.currentEditData.sales_disc_invt_acct_code);
                                that.cellEditor = editor;
                            })
                            .insertAfter(editor)
                        setInterval(()=>{
                            if(!span.parent().length){
                                span.insertAfter(editor)
                            }
                        }, 120)
                        editor.css({width:'100%'})
                    },
                },{ 
                    text: 'Purchase Line Disc. Acct.', dataField: 'purch_disc_line_acct_code', width: 90,
                    renderer: function (text, align, columnsheight) {
                        return '<div class="column-header-two-line" title="'+Helpers.translate.get('Purchase Line Discount Account')+'">'+Helpers.translate.get('Purchase Line Discount Account')+'</div>';
                    },
                    createeditor: (row: number, cellvalue: any, editor: any, cellText: any, width: any, height: any): void => {
                        let span = $('<span class="editor-dropdown-button"></span>')
                            .on('click',()=>{
                                console.log('dblclick',editor)
                                that.salesAcctCbx.open();
                                that.salesAcctCbx.setValue(that.currentEditData.purch_disc_line_acct_code);
                                that.cellEditor = editor;
                            })
                            .insertAfter(editor)
                        setInterval(()=>{
                            if(!span.parent().length){
                                span.insertAfter(editor)
                            }
                        }, 120)
                        editor.css({width:'100%'})
                    },
                },{ 
                    text: 'Purchase Inv. Disc. Acct.', dataField: 'purch_disc_invt_acct_code', width: 90,
                    renderer: function (text, align, columnsheight) {
                        return '<div class="column-header-two-line" title="'+Helpers.translate.get('Purchase Inventory Discount Account')+'">'+Helpers.translate.get('Purchase Inventory Discount Account')+'</div>';
                    },
                    createeditor: (row: number, cellvalue: any, editor: any, cellText: any, width: any, height: any): void => {
                        let span = $('<span class="editor-dropdown-button"></span>')
                            .on('click',()=>{
                                console.log('dblclick',editor)
                                that.salesAcctCbx.open();
                                that.salesAcctCbx.setValue(that.currentEditData.purch_disc_invt_acct_code);
                                that.cellEditor = editor;
                            })
                            .insertAfter(editor)
                        setInterval(()=>{
                            if(!span.parent().length){
                                span.insertAfter(editor)
                            }
                        }, 120)
                        editor.css({width:'100%'})
                    },
                },
            ]
        
        if(that.gridSetting){
            return;
        }
        Helpers.translate.do(()=>{
            if(that.grid){
                that.grid.setcolumnproperty('id','text', '#')
                that.grid.setOptions({'localization': Helpers.getLocalizatio(Helpers.translate.activeLang)})
            }
        })
        that.gridSetting = {
            columns: columns,
            localization: Helpers.getLocalizatio(Helpers.translate.activeLang),
            source: that.dataAdapter,
            ready: function() {
                that.initGenBusPtgCbx()
                that.initGenProdPtgCbx()
                that.initAcctCbx()
                that.ensure()
            },
            rendergridrows: (params: any): any[] => {
                return that.dataAdapter.records;
            },
            rendered(event: any): void {
                that.grid.expandallgroups();
            },
            onRowdoubleclick: (event:any)=>{
                // event arguments.
                var args = event.args;
                // row's bound index.
                var rowBoundIndex = args.rowindex;
                // row's visible index.
                var rowVisibleIndex = args.visibleindex;
                // right click.
                var rightclick = args.rightclick; 
                // original event.
                var ev = args.originalEvent;
                // column index.
                var columnindex = args.columnindex;
                // column data field.
                var dataField = args.datafield;
                // cell value
                var value = args.value;
                if(args && args.row){
                    var dataRow = args.row.bounddata
                    if(dataRow) that.showDetail(dataRow.id)
                }
            },
            onCellBeginEdit(event: any): void {
                let args = event.args;
                // if(!that.currentEditData){
                    that.beforeEditData = cloneDeep(args.row);
                    that.currentEditData = cloneDeep(args.row);
                // }
            },
            onCellEndEdit(event: any): void {
                let args = event.args;
                let row = args.row;
                that.currentEditData[args.datafield] = args.value;
                if (args.datafield === 'purch_disc_invt_acct_code') {
                    if(
                        JSON.stringify(that.currentEditData) !=
                        JSON.stringify(that.beforeEditData)
                        ){
                        let params = cloneDeep(that.currentEditData);
                        let conditions
                        if(params.id<0){
                            delete params.id;
                        }
                        that.api.post({
                            url: AppSetting.SETUP_POST_COMMIT_URL,
                            params: {
                                id: params.id,
                                params: params
                            },
                            onDone:function(res){
                                if(!params.id){
                                    that.currentEditData.id = res.data.id
                                    toastr.success(Helpers.translate.get('Create successfuly'))
                                }else{
                                    toastr.success(Helpers.translate.get('Update successfuly'))
                                }
                                // let index = that.grid.getselectedrowindex();
                                // let id = that.grid.getrowid(index);
                                // that.grid.updaterow(id,that.currentEditData);
                                // that.dataAdapter.dataBind();
                                //that.grid.updatebounddata('cells');
                                that.currentEditData = undefined;
                                that.beforeEditData = undefined;
                            },
                            onFail:function(){
                                toastr.warning(Helpers.translate.get('Fail to create/update'))
                            },
                        })
                    }
                }
            },
        }
    }
    reloadGrid(){
        this.dataAdapter.dataBind();
        this.grid.updatebounddata('cells');
    }
    editingItem = {
        id:0,
        gen_business_ptg_code: '',
        gen_product_ptg_code: '',
        sales_acct_code: '',
        sales_credit_memo_acct_code: '',
        purch_acct_code: '',
        purch_credit_memo_acct_code: '',
        cogs_acct_code: '',
        invt_adjmt_acct_code: '',
        sales_disc_line_acct_code: '',
        sales_disc_invt_acct_code: '',
        purch_disc_line_acct_code: '',
        purch_disc_invt_acct_code: '',
    }
    clickAdd(){
        this.showDetail()
    }
    clickEdit(){
        let that = this;
        let index = that.grid.getselectedrowindex();
        let rowData = that.grid.getrowdata(index);
        if(rowData){
            that.showDetail(rowData.id)
        }
    }
    clickDelete(){
        let that = this;
        let index = that.grid.getselectedrowindex();
        let rowData = that.grid.getrowdata(index);
        if(rowData){
            Helpers.confirm('<h3>' + Helpers.translate.get('Are you sure you want to delete this item?') + '</h3>',function(){
                
                that.api.post({
                    url: AppSetting.DEL_SETUP_POST_URL,
                    params: {
                        id: rowData.id
                    }
                }).done(function(res){
                    if(res.code){
                        that.dataAdapter.dataBind();
                        that.grid.updatebounddata('cells');
                    }
                })
            })
        }
    }
    showDetail(id=0){
        let that = this;
        that.detailWindow.open();
        if(id){
            that.api.post({
                url: AppSetting.GET_SETUP_POST_URL,
                params: {
                    id: id
                }
            }).done(function(res){
                if(res.data){
                    that.setData(res.data)
                }
            })
        }else{
            that.setData({
                id:0,
                gen_business_ptg_code: '',
                gen_product_ptg_code: '',
                sales_acct_code: '',
                sales_credit_memo_acct_code: '',
                purch_acct_code: '',
                purch_credit_memo_acct_code: '',
                cogs_acct_code: '',
                invt_adjmt_acct_code: '',
                sales_disc_line_acct_code: '',
                sales_disc_invt_acct_code: '',
                purch_disc_line_acct_code: '',
                purch_disc_invt_acct_code: '',
            })
        }
    }
    setData(data){
        let that = this
        that.editingItem = data
        if(data.gen_business_ptg_code && that.genBusPtgCbx){
            that.genBusPtgCbx.setValue(data.gen_business_ptg_code)
        } else if(that.genBusPtgCbx) that.genBusPtgCbx.setValue(null);
        if(data.gen_product_ptg_code && that.genProdPtgCbx){
            that.genProdPtgCbx.setValue(data.gen_product_ptg_code)
        } else if(that.genProdPtgCbx) that.genProdPtgCbx.setValue(null);

        if(data.sales_acct_code && that.salesAcctCbx){
            that.salesAcctCbx.setValue(data.sales_acct_code)
        } else if(that.salesAcctCbx) that.salesAcctCbx.setValue(null);
        if(data.purch_acct_code && that.purchAcctCbx){
            that.purchAcctCbx.setValue(data.purch_acct_code)
        } else if(that.purchAcctCbx) that.purchAcctCbx.setValue(null);
        
        if(data.sales_credit_memo_acct_code && that.salesCreditMemoAcctCbx){
            that.salesCreditMemoAcctCbx.setValue(data.sales_credit_memo_acct_code)
        } else if(that.salesCreditMemoAcctCbx) that.salesCreditMemoAcctCbx.setValue(null);

        if(data.purch_credit_memo_acct_code && that.purchCreditMemoAcctCbx){
            that.purchCreditMemoAcctCbx.setValue(data.purch_credit_memo_acct_code)
        } else if(that.purchCreditMemoAcctCbx) that.purchCreditMemoAcctCbx.setValue(null);
        
        if(data.cogs_acct_code && that.cogsAcctCbx){
            that.cogsAcctCbx.setValue(data.cogs_acct_code)
        } else if(that.cogsAcctCbx) that.cogsAcctCbx.setValue(null);
        
        if(data.invt_adjmt_acct_code && that.invtAdjmtAcctCbx){
            that.invtAdjmtAcctCbx.setValue(data.invt_adjmt_acct_code)
        } else if(that.invtAdjmtAcctCbx) that.invtAdjmtAcctCbx.setValue(null);
        
        if(data.sales_disc_line_acct_code && that.salesLineDiscAcctCbx){
            that.salesLineDiscAcctCbx.setValue(data.sales_disc_line_acct_code)
        } else if(that.salesLineDiscAcctCbx) that.salesLineDiscAcctCbx.setValue(null);

        if(data.sales_disc_invt_acct_code && that.salesInvtDiscAcctCbx){
            that.salesInvtDiscAcctCbx.setValue(data.sales_disc_invt_acct_code)
        } else if(that.salesInvtDiscAcctCbx) that.salesInvtDiscAcctCbx.setValue(null);

        if(data.purch_disc_line_acct_code && that.purchLineDiscAcctCbx){
            that.purchLineDiscAcctCbx.setValue(data.purch_disc_line_acct_code)
        } else if(that.purchLineDiscAcctCbx) that.purchLineDiscAcctCbx.setValue(null);

        if(data.purch_disc_invt_acct_code && that.purchInvtDiscAcctCbx){
            that.purchInvtDiscAcctCbx.setValue(data.purch_disc_invt_acct_code)
        } else if(that.purchInvtDiscAcctCbx) that.purchInvtDiscAcctCbx.setValue(null);

    }
    genBusPtgSetting
    initGenBusPtgCbx(){
        let that = this;
        if(that.genBusPtgSetting) return;
        that.genBusPtgSetting = {
                onOpen: ()=>{
                    that.pending = true;
                },
                onClose: ()=>{
                    that.pending = false;
                    $('.jqx-grid-validation,.jqx-grid-validation-arrow-up').remove()
                },
                onChange: (event)=>{
                    let item = that.genBusPtgCbx.getSelectedItem();
                    if(that.detailWindow.isOpen()){
                        if(item && that.editingItem){
                            // that.editingItem.gen_business_ptg = item;
                            that.editingItem.gen_business_ptg_code = item.code
                        }
                        let windowId = that.validator.host.attr('id')
                        $('#'+windowId+ ' .cbx_genBusPtg').blur()

                    }else{
                        if(that.cellEditor) that.cellEditor.val(item.code)
                    }
                },
            }
    }
    genProdPtgSetting
    initGenProdPtgCbx(){
        let that = this;
        that.genProdPtgSetting = {
                onOpen: ()=>{
                    that.pending = true;
                },
                onClose: ()=>{
                    that.pending = false;
                    $('.jqx-grid-validation,.jqx-grid-validation-arrow-up').remove()
                },
                onChange: function(event){
                    let item = that.genProdPtgCbx.getSelectedItem();
                    if(that.detailWindow.isOpen()){
                        if(item){
                            // that.editingItem.gen_product_ptg = item;
                            that.editingItem.gen_product_ptg_code = item.code
                        }
                        let windowId = that.validator.host.attr('id')
                        $('#'+windowId+ ' .cbx_genProdPtg').blur()

                    }else{
                        if(that.cellEditor) that.cellEditor.val(item.code)
                    }
                    
                },
            }
    }
    acctSetting
    initAcctCbx(){
        let that = this;
        that.acctSetting = {
                onOpen: ()=>{
                    that.pending = true;
                },
                onClose: ()=>{
                    that.pending = false;
                    $('.jqx-grid-validation,.jqx-grid-validation-arrow-up').remove()
                },
                onSalesAcctChange: function(args){
                    let item = args.getSelectedItem();
                    if(that.detailWindow.isOpen()){
                        if(item){
                            // that.editingItem.sales_acct = item;
                            that.editingItem.sales_acct_code = item.acct_code
                        }
                        let windowId = that.validator.host.attr('id')
                        $('#'+windowId+ ' .cbx_salesAcct').blur()
                    }else if(that.cellEditor) that.cellEditor.val(item.acct_code)
                },
                onSalesCreditMemoAcctChange: function(args){
                    let item = args.getSelectedItem();
                    if(that.detailWindow.isOpen()){
                        if(item){
                            // that.editingItem.sales_credit_memo_acct = item;
                            that.editingItem.sales_credit_memo_acct_code = item.acct_code
                        }
                        let windowId = that.validator.host.attr('id')
                        $('#'+windowId+ ' .cbx_salesCreditMemoAcct').blur()
                    }else if(that.cellEditor) that.cellEditor.val(item.acct_code)
                },
                onPurchAcctChange: function(args){
                    let item = args.getSelectedItem();
                    if(that.detailWindow.isOpen()){
                        if(item){
                            // that.editingItem.purch_acct = item;
                            that.editingItem.purch_acct_code = item.acct_code
                        }
                        let windowId = that.validator.host.attr('id')
                        $('#'+windowId+ ' .cbx_purchAcct').blur()
                    }else if(that.cellEditor) that.cellEditor.val(item.acct_code)
                },
                onPurchCreditMemoAcctChange: function(args){
                    let item = args.getSelectedItem();
                    if(that.detailWindow.isOpen()){
                        if(item){
                            // that.editingItem.purch_credit_memo_acct = item;
                            that.editingItem.purch_credit_memo_acct_code = item.acct_code
                        }
                        let windowId = that.validator.host.attr('id')
                        $('#'+windowId+ ' .cbx_purchCreditMemoAcct').blur()
                    }else if(that.cellEditor) that.cellEditor.val(item.acct_code)
                },
                onCOGSAcctChange: function(args){
                    let item = args.getSelectedItem();
                    if(that.detailWindow.isOpen()){
                        if(item){
                            // that.editingItem.cogs_acct = item;
                            that.editingItem.cogs_acct_code = item.acct_code
                        }
                        let windowId = that.validator.host.attr('id')
                        // $('#'+windowId+ ' .cbx_purchCreditMemoAcct').blur()
                    }else if(that.cellEditor) that.cellEditor.val(item.acct_code)
                },
                onInvtAdjmtAcctChange: function(args){
                    let item = args.getSelectedItem();
                    if(that.detailWindow.isOpen()){
                        if(item){
                            // that.editingItem.invt_adjmt_acct = item;
                            that.editingItem.invt_adjmt_acct_code = item.acct_code
                        }
                        let windowId = that.validator.host.attr('id')
                        // $('#'+windowId+ ' .cbx_purchCreditMemoAcct').blur()
                    }else if(that.cellEditor) that.cellEditor.val(item.acct_code)
                },
                onSalesLineDiscountAcctChange: function(args){
                    let item = args.getSelectedItem();
                    if(that.detailWindow.isOpen()){
                        if(item){
                            // that.editingItem.invt_adjmt_acct = item;
                            that.editingItem.sales_disc_line_acct_code = item.acct_code
                        }
                        let windowId = that.validator.host.attr('id')
                        // $('#'+windowId+ ' .cbx_purchCreditMemoAcct').blur()
                    }else if(that.cellEditor) that.cellEditor.val(item.acct_code)
                },
                onSalesInvtDiscountAcctChange: function(args){
                    let item = args.getSelectedItem();
                    if(that.detailWindow.isOpen()){
                        if(item){
                            // that.editingItem.invt_adjmt_acct = item;
                            that.editingItem.sales_disc_invt_acct_code = item.acct_code
                        }
                        let windowId = that.validator.host.attr('id')
                        // $('#'+windowId+ ' .cbx_purchCreditMemoAcct').blur()
                    }else if(that.cellEditor) that.cellEditor.val(item.acct_code)
                },
                onPurchLineDiscountAcctChange: function(args){
                    let item = args.getSelectedItem();
                    if(that.detailWindow.isOpen()){
                        if(item){
                            // that.editingItem.invt_adjmt_acct = item;
                            that.editingItem.purch_disc_line_acct_code = item.acct_code
                        }
                        let windowId = that.validator.host.attr('id')
                        // $('#'+windowId+ ' .cbx_purchCreditMemoAcct').blur()
                    }else if(that.cellEditor) that.cellEditor.val(item.acct_code)
                },
                onPurchInvtDiscountAcctChange: function(args){
                    let item = args.getSelectedItem();
                    if(that.detailWindow.isOpen()){
                        if(item){
                            // that.editingItem.invt_adjmt_acct = item;
                            that.editingItem.purch_disc_invt_acct_code = item.acct_code
                        }
                        let windowId = that.validator.host.attr('id')
                        // $('#'+windowId+ ' .cbx_purchCreditMemoAcct').blur()
                    }else if(that.cellEditor) that.cellEditor.val(item.acct_code)
                },
            }
    }
    initDetailWindow(){
        let that = this;
        that.initGenBusPtgCbx()
        that.initGenProdPtgCbx()
        that.initAcctCbx()
        that.initValidation();
    }
    closeDetail(){
        this.detailWindow.close();
    }
    isSubmit = false;
    checkValidate(): void {
        console.log('AA')
        let that = this;
        setTimeout(() => {
            that.validator.validate();
        }, 200)
        this.isSubmit = true;
    }
    onValidationError(){
        this.isSubmit = false;
    }

    onValidationSuccess() : void {
        let that = this;
        if(that.isSubmit){
            let params = that.editingItem;
            console.log(params,'params');
            that.api.post({
                url: AppSetting.SETUP_POST_COMMIT_URL,
                params: {
                    id:that.editingItem.id,
                    params: {
                        gen_business_ptg_code: params.gen_business_ptg_code,
                        gen_product_ptg_code: params.gen_product_ptg_code,
                        sales_acct_code: params.sales_acct_code,
                        sales_credit_memo_acct_code: params.sales_credit_memo_acct_code,
                        purch_acct_code: params.purch_acct_code,
                        purch_credit_memo_acct_code: params.purch_credit_memo_acct_code,
                        cogs_acct_code: params.cogs_acct_code,
                        invt_adjmt_acct_code: params.invt_adjmt_acct_code,
                        sales_disc_line_acct_code: params.sales_disc_line_acct_code,
                        sales_disc_invt_acct_code: params.sales_disc_invt_acct_code,
                        purch_disc_line_acct_code: params.purch_disc_line_acct_code,
                        purch_disc_invt_acct_code: params.purch_disc_invt_acct_code,
                    }
                }
            }).done(function(res){
                if(res.code==1){
                    that.dataAdapter.dataBind();
                    that.grid.updatebounddata('cells');
                    that.detailWindow.close()
                    toastr.success(Helpers.translate.get('Create successfuly'))
                }
            })
            
        }
    }
    rules
    initValidation(){
        let that = this;
        let windowId = that.validator.host.attr('id')
        if(that.rules){
            if(that.validator) that.validator.hide();
        }else
        that.rules =
        [
            // { input: '#' + windowId+ ' .cbx_genBusPtg', message: 'Bus. Post. is required!', action: 'blur', rule: function () {
            //         return !!that.genBusPtgCbx.value;
            //     } 
            // },
            // { input: '#' + windowId+ ' .cbx_genProdPtg', message: ' Prod. Post. is required!', action: 'blur', rule: function () {
            //         return !!that.genProdPtgCbx.value;
            //     } 
            // },{ input: '#' + windowId+ ' .cbx_salesAcct', message: 'Sales Account is required!', action: 'blur', rule: function () {
            //         return !!that.salesAcctCbx.value;
            //     } 
            // },{ input: '#' + windowId+ ' .cbx_purchAcct', message: 'Purchase Account is required!', action: 'blur', rule: function () {
            //         return !!that.purchAcctCbx.value;
            //     } 
            // },

        ];

    }
}


/*
Copyright 2017-2018 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/