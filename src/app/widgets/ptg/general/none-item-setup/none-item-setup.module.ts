import { Component, OnInit, Input,forwardRef,AfterViewInit,OnChanges, ViewChild, ViewEncapsulation, EventEmitter, Output } from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from "@angular/forms";
import * as cloneDeep from 'lodash/cloneDeep';
import { DatePipe, CurrencyPipe } from '@angular/common';
import { APIService } from '../../../../api.service';
import { Helpers} from "../../../../helpers";
import { AppSetting } from "../../../../settings";
import { AppMessage } from "../../../../message";

import { jqxWindowComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxwindow';
import { jqxGridComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxgrid';
import { jqxValidatorComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxvalidator';
import { jqxDropDownListComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxdropdownlist';

import { VatProductPtgModule } from '../../../../widgets/ptg/vat/product/ptg-vat-product.module';
import { GeneralProductPtgModule } from '../../../../widgets/ptg/general/product/ptg-gen-product.module';

declare var $:any;
declare var toastr:any;

const noop = () => {
};

@Component({
    selector: 'module-none-item-setup',
    templateUrl: './none-item-setup.module.html',
    styleUrls: ['./none-item-setup.module.css'],
    providers: [{
        provide: NG_VALUE_ACCESSOR,
        useExisting: forwardRef(() => NoneItemSetupModule),
        multi: true
    }]
})

export class NoneItemSetupModule implements OnInit,AfterViewInit, OnChanges, ControlValueAccessor {
    @Input() gen_posting_type: number;
    @Input() hidden: boolean;
    @Input() layout: string;
    @Input() key: string;
    @Output() onOpen = new EventEmitter();
    @Output() onClose = new EventEmitter();
    @Output() onChange = new EventEmitter();
    constructor(
        private api: APIService,
        ) {
        if(!!!this.key) this.key = 'code'
    }
    theme = AppSetting.THEME
    // The internal data model
    private innerValue: any = undefined;
    label = Helpers.translate.get("Please Choose");
    // Placeholders for the callbacks which are later provided
    // by the Control Value Accessor
    private onTouchedCallback: () => void = noop;
    private onChangeCallback: (_: any) => void = noop;

    @ViewChild('window') window: jqxWindowComponent;
    @ViewChild('detailWindow') detailWindow: jqxWindowComponent;
    @ViewChild('grid') grid: jqxGridComponent;
    @ViewChild('validator') validator: jqxValidatorComponent;
    @ViewChild('vatProdPtgCbx') vatProdPtgCbx: VatProductPtgModule;
    @ViewChild('genProdPtgCbx') genProdPtgCbx: GeneralProductPtgModule;
    @ViewChild('dropDownGrid') dropDownGrid: jqxGridComponent;
    

    // get accessor
    get value(): any {
        return this.innerValue;
    };

    // set accessor including call the onchange callback
    set value(v: any) {
        if (v !== this.innerValue) {
            this.innerValue = v;
            this.onChangeCallback(v);
        }
        console.log('SetValue')
    }

    // Set touched on blur
    onBlur() {
        this.onTouchedCallback();
    }

    // From ControlValueAccessor interface
    writeValue(value: any) {
        if (value !== this.innerValue) {
            this.innerValue = value;
        }
    }

    // From ControlValueAccessor interface
    registerOnChange(fn: any) {
        this.onChangeCallback = fn;
    }

    // From ControlValueAccessor interface
    registerOnTouched(fn: any) {
        this.onTouchedCallback = fn;
    }



    ngOnInit() {
    }
    ngOnChanges(){

    }

    dropDownDataAdapter
    dropDownGridSetting
    initDropDownGrid(){
        let that = this;
        
        let columns = [
            { text: '#', dataField: 'id', width: 120 ,hidden: true},
            { text: Helpers.translate.get('Code'), dataField: 'code', width: 120 },
            { text: Helpers.translate.get('Description'), dataField: 'description' },
        ]
        
        if(that.dropDownGridSetting){
            that.dropDownDataAdapter.dataBind();
            that.ensureDropDownGrid()
            return;
        }
        Helpers.translate.do(()=>{
            if(that.dropDownGrid){
                that.dropDownGrid.setcolumnproperty('id','text', '#')
                that.dropDownGrid.setcolumnproperty('code','text', 'Code')
                that.dropDownGrid.setcolumnproperty('description','text', 'Description')
                that.dropDownGrid.setOptions({'localization': Helpers.getLocalizatio(Helpers.translate.activeLang)})
            }
        })
        let conditions;
        if(that.gen_posting_type){
            conditions = ['gen_posting_type',that.gen_posting_type]
        }
        let source = Helpers.Source({
            dataType: "json",
            type: 'POST',
            url: AppSetting.COMMON_BINDING_URL,
            data: {
                table:'none_item_setup',
                conditions: conditions,
                all: true
            },
            dataFields: [
                { name: 'id', type: 'number' },
                { name: 'gen_posting_type', type: 'string' },
                { name: 'code', type: 'string' },
                { name: 'description', type: 'string' },
                { name: 'gen_product_ptg_code', type: 'string' },
                { name: 'def_vat_product_ptg_code', type: 'string' },
            
            ],
            filter: function() {
                // update the grid and send a request to the server.
                that.dropDownGrid.updatebounddata('filter');
            },
            sort: function() {
                // update the grid and send a request to the server.
                that.dropDownGrid.updatebounddata('sort');
            },
            id: 'id',
            root: 'data',
        });
        // that.gridPartner.setOptions({source:[]});
        that.dropDownDataAdapter = new jqx.dataAdapter(source, {
            beforeLoadComplete: function (records) {
                for (var i = 0; i < records.length; i++) {
                    var row = records[i];
                }
                return records;
            }
        });
        that.dropDownGridSetting = {
            columns: columns,
            localization: Helpers.getLocalizatio(Helpers.translate.activeLang),
            source: that.dropDownDataAdapter,
            ready: function() {
                that.ensureDropDownGrid()
            },
            rendergridrows: (params: any): any[] => {
                return that.dropDownDataAdapter.records;
            },
            rendered(event: any): void {
            },
            onRowSelect: (event:any)=>{
                var args = event.args;
                let rowData = that.dropDownGrid.getrowdata(args.rowindex);
                if(rowData){
                    if(rowData){
                        that.setSelectedItem(rowData);
                        that.onChangeCallback(rowData[that.key])
                    }else{
                        that.setSelectedItem(undefined);
                        that.onChangeCallback(undefined)
                    }
                    that.onChange.emit()
                    if(that.dropDownButton) $(that.dropDownButton).trigger('click')
                }
            },
            onRowdoubleclick: (event:any)=>{
                var args = event.args;
                var rowBoundIndex = args.rowindex;
                var rowVisibleIndex = args.visibleindex;
                var rightclick = args.rightclick; 
                var columnindex = args.columnindex;
                var dataField = args.datafield;
                var value = args.value;
                if(args && args.row){
                    var dataRow = args.row.bounddata
                }
            },
        }
    }
    ensureDropDownGrid(){
        let that = this;
        if(that.dropDownGrid){
            let rowid;
            let index = that.dropDownGrid.getselectedrowindex();
            let rowData = that.dropDownGrid.getrowdata(index);
            if(rowData && rowData[that.key]==that.innerValue){

            }else{
                that.dropDownDataAdapter.records.map((row)=>{
                    if(row[that.key] == that.innerValue){
                        rowid = row.id
                    }
                })
                let rowIndex = that.dropDownGrid.getrowboundindexbyid(rowid)
                that.dropDownGrid.ensurerowvisible(rowIndex);
                that.dropDownGrid.selectedrowindex(rowIndex);
            }
        }
    }
    dropDownButton
    initDropdown($event){
        this.dropDownButton = $event.target
        this.initDropDownGrid()
    }
    onDropdownSelectect(item){
        let that = this;
        that.setSelectedItem(item);
        that.onChangeCallback(item[that.key])
        that.onChange.emit()
    }
    switchToAdvance(){
        if(this.dropDownButton) $(this.dropDownButton).trigger('click');
        this.open()
    }


    selectedItem;
    setSelectedItem(item){
        let that = this;
        that.selectedItem = item;
        if(item  && item.id){
            that.label = item.code;// + ' ' + item.description;
            that.innerValue = item[that.key];
        }else{
            that.label = Helpers.translate.get("Please Choose");
            that.innerValue = undefined
        }
    }
    getSelectedItem(){
        return this.selectedItem;
    }
    setValue(v){
        this.innerValue = v
        this.initDisplayValue();
        
    }
    ensure(){
        let that = this;
        console.log('ensure',that.innerValue)
        if(that.grid){
            let rowIndex = that.grid.getrowboundindexbyid(that.innerValue)
            that.grid.ensurerowvisible(rowIndex);
            that.grid.selectedrowindex(rowIndex);
        }
    }
    initDisplayValue(){
        let that = this;
        setTimeout((any)=>{
            if(that.innerValue!=undefined){
                that.api.post({
                    url: AppSetting.GET_NONE_ITEM_SETUP_URL,
                    params:{
                        id: that.innerValue
                    }
                }).done(function(res){
                    if(res.data){
                        that.setSelectedItem(res.data);
                        that.onChange.emit()
                    }
                })
            }else{
                //that.onChange.emit()
            }
        }, 42)
    }
    ngAfterViewInit(){
        let that = this;
        that.loadAdapter();
        if(that.layout == 'grid') 
            setTimeout(()=>{
                that.initGrid();
            },200)
        that.initDisplayValue()
    }

    toggle(){
        if(this.window.isOpen()) this.close();
        else this.open()
    }
    open(){
        this.window.open();
        this.initGrid()
        // this.onOpen.emit()
        
    }
    eventWindowOpen(){
        this.ensure()
        this.onOpen.emit()
    }
    eventWindowClose(){
        this.onClose.emit()
    }
    close(){
        this.window.close();
        // this.onClose.emit()
    }
    clear(){
        let that = this;
        that.grid.clearselection()
        // that.label = "Please Choose";
    }
    done(){
        let that = this;
        let index = that.grid.getselectedrowindex();
        let rowData = that.grid.getrowdata(index);
        if(index>=0 && rowData){
            that.setSelectedItem(rowData);
            that.onChangeCallback(rowData[that.key])
        }else{
            that.setSelectedItem(undefined);
            that.onChangeCallback(undefined)
        }
        that.onChange.emit()
        that.close();
    }
    initContent(){
        setTimeout(()=>{
            let that = this;
            console.log(this,self)
        },200)
    }

    source;
    dataAdapter;
    gridSetting;
    loadAdapter(){
        let that = this;
        let conditions;
        if(that.gen_posting_type){
            conditions = ['gen_posting_type',that.gen_posting_type]
        }
        that.source = Helpers.Source({
            dataType: "json",
            type: 'POST',
            url: AppSetting.COMMON_BINDING_URL,
            data: {
                table:'none_item_setup',
                conditions: conditions
            },
            dataFields: [
                { name: 'id', type: 'number' },
                { name: 'gen_posting_type', type: 'string' },
                { name: 'code', type: 'string' },
                { name: 'description', type: 'string' },
                { name: 'gen_product_ptg_code', type: 'string' },
                { name: 'def_vat_product_ptg_code', type: 'string' },
            
            ],
            filter: function() {
                // update the grid and send a request to the server.
                that.grid.updatebounddata('filter');
            },
            sort: function() {
                // update the grid and send a request to the server.
                that.grid.updatebounddata('sort');
            },
            id: 'id',
            root: 'data',
        });
        // that.gridPartner.setOptions({source:[]});
        that.dataAdapter = new jqx.dataAdapter(that.source, {
            beforeLoadComplete: function (records) {
                for (var i = 0; i < records.length; i++) {
                    var row = records[i];
                }
                return records;
            }
        });
    }
    pending = false;
    beforeEditData;
    currentEditData;
    cellEditor
    initGrid(){
        let that = this;
        
        let columns = [
                { text: '#', dataField: 'id', width: 120 ,hidden: true},
                
                // { 
                //     text: 'Type', dataField: 'type',width: 100,
                //     validation: (cell: any, value: any): any => {
                //         if (value == '')
                //             return { result: false, message: 'Type should be not empty' };
                //         return true;
                //     }
                // },
                {
                    text: 'General Posting Type', 
                    renderer: function (text, align, columnsheight) {
                        return '<div class="column-header-two-line">'+Helpers.translate.get('General Posting Type')+'</div>';
                    },
                    dataField: 'gen_posting_type', width: 92 ,
                    columntype: 'dropdownlist',
                    cellsrenderer: function (row, column, value,html,setting,data) {
                        let item_types = {
                            '1': Helpers.translate.get('Sales'),
                            '2': Helpers.translate.get('Purchases'),
                        }
                        return [
                        '<div style="padding: 6px;line-height: 20px;">',
                            item_types[value],
                        '</div>',
                        ].join('')
                    },
                    createeditor: (row: number, column: any, editor: any): void => {
                        // assign a new data source to the dropdownlist.
                        editor.jqxDropDownList({ 
                            selectedIndex:-1,
                            // autoOpen: true,
                            dropDownWidth : 360,
                            // dropDownHeight : 360,
                            autoDropDownHeight: true, 
                            source: new $.jqx.dataAdapter({
                                datatype: "array",
                                localdata: 
                                [
                                    {value:1,label: Helpers.translate.get('Sales')},
                                    {value:2,label: Helpers.translate.get('Purchases')}
                                ],
                                datafields: [
                                    { name: 'value', type: 'string'},
                                    { name: 'label', type: 'string'}
                                ]
                            }),
                            displayMember: 'label', 
                            valueMember: 'value' 
                        }).on('change', function (event) {
                            var args = event.args;
                            if (args && args.item) {
                                var item = args.item.originalItem;
                                console.log(item)
                                // that.lineGrid.setcellvalue(row,'name',item.originalItem.name)
                                that.vatProdPtgCbx.setType(item.value);
                            }
                        })
                    },
                    initeditor: (row: number, cellvalue: any, editor: any, celltext: any, pressedkey: any): void => {
                        if(!cellvalue){
                            console.log('Clear')
                        }
                            editor.jqxDropDownList('selectItem',cellvalue)
                        
                        console.log('initeditor',cellvalue)
                        // editor.jqxDropDownList("open");
                    },
                    // update the editor's value before saving it.
                    cellvaluechanging: (row: number, column: any, columntype: any, oldvalue: any, newvalue: any): any => {
                        // return the old value, if the new value is empty.
                        console.log(row,'row')
                        if (newvalue == '') return oldvalue;
                    },
                    geteditorvalue: function (row, cellvalue, editor) {
                        // return the editor's value.
                        let item = editor.jqxDropDownList('getSelectedItem'); 
                        if(item && item.originalItem){
                            
                            return item.originalItem.value
                        }
                        return '';
                    }
                },{ 
                    text: 'Code', dataField: 'code', width: 200,
                    renderer: function (text, align, columnsheight) {
                        return '<div class="column-header-two-line">'+Helpers.translate.get('Code')+'</div>';
                    },
                    validation: (cell: any, value: any): any => {
                        let row = cell.row;
                        let rowData = that.grid.getrowdata(row);
                        if (value == '')
                            return { result: false, message: Helpers.translate.get('Code') + Helpers.translate.get(' should be not empty') };
                        let c = that.dataAdapter.records
                            .filter(function(row){
                                return row.code == value && rowData.id !=row.id;
                            }).length;
                        if(c){
                            return { result: false, message: Helpers.translate.get('Code') + Helpers.translate.get(' invalid') };
                        }
                        return true;
                    }
                },{ 
                    text: 'Description', dataField: 'description',
                    renderer: function (text, align, columnsheight) {
                        return '<div class="column-header-two-line">' + Helpers.translate.get('Description') + '</div>';
                    },
                    validation: (cell: any, value: any): any => {
                        if (value == '')
                            return { result: false, message: Helpers.translate.get('Description') + Helpers.translate.get(' should be not empty') };
                        return true;
                    }
                },{ 
                    text: 'General Product Posting Group', dataField: 'gen_product_ptg_code',maxWidth:220,
                    renderer: function (text, align, columnsheight) {
                        return '<div class="column-header-two-line">'+Helpers.translate.get('General Product Posting Group')+'</div>';
                    },
                    validation: (cell: any, value: any): any => {
                        if(that.pending){
                            return { result: false,message:'Choose ...'};
                        }
                        // if (value == '')
                            // return { result: false, message: 'Default TAX Business Posting Group should be not empty' };
                        return true;
                    },
                    createeditor: (row: number, cellvalue: any, editor: any, cellText: any, width: any, height: any): void => {
                        $('<span class="editor-dropdown-button"></span>')
                            .on('click',()=>{
                                console.log('dblclick',editor)
                                that.genProdPtgCbx.open();
                                that.genProdPtgCbx.setValue(that.currentEditData.gen_product_ptg_code);
                                that.cellEditor = editor;
                            })
                            .insertAfter(editor)
                        editor.css({width:'100%'})
                    },
                },{ 
                    text: 'Def. TAX Group', dataField: 'def_vat_product_ptg_code',maxWidth:220,
                    renderer: function (text, align, columnsheight) {
                        return '<div class="column-header-two-line">'+Helpers.translate.get('Default TAX Group')+'</div>';
                    },
                    validation: (cell: any, value: any): any => {
                        if(that.pending){
                            return { result: false,message:'Choose ...'};
                        }
                        // if (value == '')
                            // return { result: false, message: 'Default TAX Business Posting Group should be not empty' };
                        return true;
                    },
                    createeditor: (row: number, cellvalue: any, editor: any, cellText: any, width: any, height: any): void => {
                        $('<span class="editor-dropdown-button"></span>')
                            .on('click',()=>{
                                console.log('dblclick',editor)
                                that.vatProdPtgCbx.open();
                                
                                that.vatProdPtgCbx.setValue(that.currentEditData.def_vat_product_ptg_code);
                                that.cellEditor = editor;
                            })
                            .insertAfter(editor)
                        editor.css({width:'100%'})
                    },
                },
                
            ]
        
        if(that.gridSetting){
            return;
        }
        that.gridSetting = {
            columns: columns,
            localization: Helpers.getLocalizatio(Helpers.translate.activeLang),
            source: that.dataAdapter,
            ready: function() {
                that.initVatProdPtgCbx()
                that.initGenProdPtgCbx()
                that.ensure()
            },
            rendergridrows: (params: any): any[] => {
                return that.dataAdapter.records;
            },
            rendered(event: any): void {
                that.grid.expandallgroups();
            },
            onRowdoubleclick: (event:any)=>{
                // event arguments.
                var args = event.args;
                // row's bound index.
                var rowBoundIndex = args.rowindex;
                // row's visible index.
                var rowVisibleIndex = args.visibleindex;
                // right click.
                var rightclick = args.rightclick; 
                // original event.
                var ev = args.originalEvent;
                // column index.
                var columnindex = args.columnindex;
                // column data field.
                var dataField = args.datafield;
                // cell value
                var value = args.value;
                if(args && args.row){
                    var dataRow = args.row.bounddata
                    if(dataRow) that.showDetail(dataRow.id)
                }
            },
            onCellBeginEdit(event: any): void {
                let args = event.args;
                // if(!that.currentEditData){
                    that.beforeEditData = cloneDeep(args.row);
                    that.currentEditData = cloneDeep(args.row);
                // }
            },
            onCellEndEdit(event: any): void {
                let args = event.args;
                let row = args.row;
                that.currentEditData[args.datafield] = args.value;
                if (args.datafield === 'def_vat_product_ptg_code') {
                    if(
                        JSON.stringify(that.currentEditData) !=
                        JSON.stringify(that.beforeEditData)
                        ){
                        let params = cloneDeep(that.currentEditData);
                        let conditions
                        if(row.id<0){
                            delete params.id;
                        }
                        params.code = params.code.toUpperCase();
                        that.api.post({
                            url: AppSetting.NONE_ITEM_SETUP_COMMIT_URL,
                            params: {
                                id: row.id,
                                params: params
                            },
                            onDone:function(res){
                                if(!row.id){
                                    that.currentEditData.id = res.data.id
                                    toastr.success(Helpers.translate.get('Create successfuly'))
                                }else{
                                    toastr.success(Helpers.translate.get('Update successfuly'))
                                }
                                // let index = that.grid.getselectedrowindex();
                                // let id = that.grid.getrowid(index);
                                // that.grid.updaterow(id,that.currentEditData);
                                that.dataAdapter.dataBind();
                                //that.grid.updatebounddata('cells');
                                that.currentEditData = undefined;
                                that.beforeEditData = undefined;
                            },
                            onFail:function(){
                                toastr.warning(Helpers.translate.get('Fail to create/update'))
                            },
                        })
                    }
                }
            },
        }
    }
    reloadGrid(){
        this.dataAdapter.dataBind();
        this.grid.updatebounddata('cells');
    }
    editingItem = {
        id:0,
        description:'',
        code:'',
        gen_posting_type: '',
        gen_product_ptg_code: '',
        def_vat_product_ptg_code: '',
    }
    clickAdd(){
        this.showDetail()
    }
    clickEdit(){
        let that = this;
        let index = that.grid.getselectedrowindex();
        let rowData = that.grid.getrowdata(index);
        if(rowData){
            that.showDetail(rowData.id)
        }
    }
    clickDelete(){
        let that = this;
        let index = that.grid.getselectedrowindex();
        let rowData = that.grid.getrowdata(index);
        if(rowData){
            Helpers.confirm('<h3>' + Helpers.translate.get('Are you sure you want to delete this item?') + '</h3>',function(){
                
                that.api.post({
                    url: AppSetting.DEL_NONE_ITEM_SETUP_URL,
                    params: {
                        id: rowData.id
                    }
                }).done(function(res){
                    if(res.code){
                        that.dataAdapter.dataBind();
                        that.grid.updatebounddata('cells');
                    }
                })
            })
        }
    }
    showDetail(id=0){
        let that = this;
        that.detailWindow.open();
        if(id){
            that.api.post({
                url: AppSetting.GET_NONE_ITEM_SETUP_URL,
                params: {
                    id: id
                }
            }).done(function(res){
                if(res.data){
                    that.setData(res.data)
                }
            })
        }else{
            that.setData({
                id:0,
                description:'',
                code:'',
                gen_posting_type: '',
                gen_product_ptg_code: '',
                def_vat_product_ptg_code: '',
            })
        }
    }
    setData(data){
        let that = this
        that.editingItem = data
        if(data.def_vat_product_ptg_code && that.vatProdPtgCbx){
            that.vatProdPtgCbx.setValue(data.def_vat_product_ptg_code)
        } else if(that.vatProdPtgCbx) that.vatProdPtgCbx.setValue(null);

        if(data.gen_product_ptg_code && that.genProdPtgCbx){
            that.genProdPtgCbx.setValue(data.gen_product_ptg_code)
        } else if(that.genProdPtgCbx) that.genProdPtgCbx.setValue(null);

        
    }
    
    vatProdPtgSetting
    initVatProdPtgCbx(){
        let that = this;
        if(that.vatProdPtgSetting) return;
        that.vatProdPtgSetting = {
                onOpen: ()=>{
                    that.pending = true;
                },
                onClose: ()=>{
                    that.pending = false;
                    $('.jqx-grid-validation,.jqx-grid-validation-arrow-up').remove()
                },
                onChange: function(event){
                    let item = that.vatProdPtgCbx.getSelectedItem();
                    if(that.detailWindow.isOpen()){
                        if(item){
                            // that.editingItem.gen_product_ptg = item;
                            that.editingItem.def_vat_product_ptg_code = item.code
                        }
                        let windowId = that.validator.host.attr('id')
                        $('#'+windowId+ ' .vat_product_ptg_code').blur()

                    }else{
                        if(that.cellEditor) that.cellEditor.val(item.code)
                    }
                    
                },
            }
    }
    genProdPtgSetting
    initGenProdPtgCbx(){
        let that = this;
        if(that.genProdPtgSetting) return;
        that.genProdPtgSetting = {
                onOpen: ()=>{
                    that.pending = true;
                },
                onClose: ()=>{
                    that.pending = false;
                    $('.jqx-grid-validation,.jqx-grid-validation-arrow-up').remove()
                },
                onChange: function(event){
                    let item = that.genProdPtgCbx.getSelectedItem();
                    if(that.detailWindow.isOpen()){
                        if(item){
                            // that.editingItem.gen_product_ptg = item;
                            that.editingItem.gen_product_ptg_code = item.code
                        }
                        let windowId = that.validator.host.attr('id')
                        $('#'+windowId+ ' .vat_product_ptg_code').blur()

                    }else{
                        if(that.cellEditor) that.cellEditor.val(item.code)
                    }
                    
                },
            }
    }
    
    initDetailWindow(){
        let that = this;
        that.initVatProdPtgCbx()
        that.initGenProdPtgCbx()
        that.initValidation();
    }
    closeDetail(){
        this.detailWindow.close();
    }
    isSubmit = false;
    checkValidate(): void {
        console.log('AA')
        let that = this;
        setTimeout(() => {
            that.validator.validate();
        }, 200)
        this.isSubmit = true;
    }
    onValidationError(){
        this.isSubmit = false;
    }

    onValidationSuccess() : void {
        let that = this;
        if(that.isSubmit){
            let params = that.editingItem;
            console.log(params,'params');
            // params.vat = +params.vat;
            params.code = params.code.toUpperCase();
            that.api.post({
                url: AppSetting.NONE_ITEM_SETUP_COMMIT_URL,
                params: {
                    id:that.editingItem.id,
                    params: params
                    // {
                    //     description: params.description,
                    //     vat_business_ptg_code: params.vat_business_ptg_code,
                    //     vat_product_ptg_code: params.vat_product_ptg_code,
                    //     sales_acct_code: params.sales_acct_code,
                    //     purch_acct_code: params.purch_acct_code,
                    // }
                }
            }).done(function(res){
                if(res.code==1){
                    that.dataAdapter.dataBind();
                    // that.grid.updatebounddata('cells');
                    that.detailWindow.close()
                    toastr.success(Helpers.translate.get('Create successfuly'))
                }
            })
            
        }
    }
    rules
    initValidation(){
        let that = this;
        let windowId = that.validator.host.attr('id')
        if(that.rules){
            if(that.validator) that.validator.hide();
        }else
        that.rules =
        [
            { input: '#' + windowId+ ' .txt_description', message: Helpers.translate.get('Description') + Helpers.translate.get(' should be not empty'), action: 'keyup, blur', rule: 'required' },
            // { input: '#' + windowId+ ' .txt_type', message: 'Type is required!', action: 'keyup, blur', rule: 'required' },
            // { input: '#' + windowId+ ' .txt_vat', message: 'TAX is required!', action: 'keyup, blur', rule: 'required' },
            // { 
            //     input: '#' + windowId+ ' .vat_business_ptg_code', message: 'TAX Bus. Posting Groups is required!', action: 'blur', rule: function () {
            //         return !!that.vatBusPtgCbx.value;
            //     } 
            // },
            // { input: '#' + windowId+ ' .vat_product_ptg_code', message: ' TAX Prod. Posting Groups is required!', action: 'blur', rule: function () {
            //         return !!that.vatProdPtgCbx.value;
            //     } 
            // },{ input: '#' + windowId+ ' .sales_acct_code', message: 'Sales Account is required!', action: 'blur', rule: function () {
            //         return !!that.salesAcctCbx.value;
            //     } 
            // },{ input: '#' + windowId+ ' .purch_acct_code', message: 'Purchase Account is required!', action: 'blur', rule: function () {
            //         return !!that.purchAcctCbx.value;
            //     } 
            // },

        ];

    }
    onGenPostingTypeChange(){
        let that = this;
        if(that.vatProdPtgCbx){
            that.vatProdPtgCbx.setType(that.editingItem.gen_posting_type);
        }
    }
}


/*
Copyright 2017-2018 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/