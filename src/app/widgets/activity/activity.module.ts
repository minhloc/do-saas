import { Component, OnInit, Input,forwardRef,AfterViewInit,OnChanges, ViewChild, ViewEncapsulation, EventEmitter, Output } from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from "@angular/forms";
import * as cloneDeep from 'lodash/cloneDeep';
import { DatePipe, CurrencyPipe } from '@angular/common';
import { APIService } from '../../api.service';
import { Helpers} from "../../helpers";
import { AppSetting } from "../../settings";
import { AppMessage } from "../../message";

import { jqxWindowComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxwindow';
import { jqxGridComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxgrid';
import { jqxValidatorComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxvalidator';

import { AccountModule } from '../account/account.module';

declare var $:any;
declare var toastr:any;

const noop = () => {
};

@Component({
    selector: 'module-activity',
    templateUrl: './activity.module.html',
    styleUrls: ['./activity.module.css'],
    providers: [{
        provide: NG_VALUE_ACCESSOR,
        useExisting: forwardRef(() => ActivityModule),
        multi: true
    }]
})

export class ActivityModule implements OnInit,AfterViewInit, OnChanges, ControlValueAccessor {
    @Input() hidden: boolean;
    @Input() layout: string;
    @Input() key: string;
    @Output() onOpen = new EventEmitter();
    @Output() onClose = new EventEmitter();
    @Output() onChange = new EventEmitter();
    constructor(
        private api: APIService,
        ) {
        if(!!!this.key) this.key = 'id'
    }
    theme = AppSetting.THEME
    // The internal data model
    private innerValue: any = undefined;
    label = Helpers.translate.get("Please Choose");
    // Placeholders for the callbacks which are later provided
    // by the Control Value Accessor
    private onTouchedCallback: () => void = noop;
    private onChangeCallback: (_: any) => void = noop;

    @ViewChild('window') window: jqxWindowComponent;
    @ViewChild('detailWindow') detailWindow: jqxWindowComponent;
    @ViewChild('grid') grid: jqxGridComponent;
    @ViewChild('validator') validator: jqxValidatorComponent;
    @ViewChild('dropDownGrid') dropDownGrid: jqxGridComponent;
    @ViewChild('debitAcctCbx') debitAcctCbx: AccountModule;
    @ViewChild('creditAcctCbx') creditAcctCbx: AccountModule;
    
    acct_id
    // get accessor
    get value(): any {
        return this.innerValue;
    };

    // set accessor including call the onchange callback
    set value(v: any) {
        if (v !== this.innerValue) {
            this.innerValue = v;
            this.onChangeCallback(v);
        }
        console.log('SetValue')
    }

    // Set touched on blur
    onBlur() {
        this.onTouchedCallback();
    }

    // From ControlValueAccessor interface
    writeValue(value: any) {
        if (value !== this.innerValue) {
            this.innerValue = value;
        }
    }

    // From ControlValueAccessor interface
    registerOnChange(fn: any) {
        this.onChangeCallback = fn;
    }

    // From ControlValueAccessor interface
    registerOnTouched(fn: any) {
        this.onTouchedCallback = fn;
    }

    ngOnInit() {
    }
    ngOnChanges(){

    }
    dropDownDataAdapter
    dropDownGridSetting
    initDropDownGrid(){
        let that = this;
        
        let columns = [
            { text: '#', dataField: 'id', width: 120 ,hidden: true},
            { text: Helpers.translate.get("Code"), dataField: 'code', width: 120 },
            { text: Helpers.translate.get("Title"), dataField: 'title' },
        ]
        
        if(that.dropDownGridSetting){
            // that.dropDownDataAdapter.dataBind();
            // setTimeout(()=>{
                that.ensureDropDownGrid()
            // }, 800)
            return;
        }
        Helpers.translate.do(()=>{
            if(that.dropDownGrid){
                that.dropDownGrid.setcolumnproperty('code','text', 'Code')
                that.dropDownGrid.setcolumnproperty('title','text', 'Title')
                that.dropDownGrid.setOptions({'localization': Helpers.getLocalizatio(Helpers.translate.activeLang)})
            }
        })
        let source = Helpers.Source({
            dataType: "json",
            type: 'POST',
            url: AppSetting.COMMON_BINDING_URL,
            data: {
                table:'acct_activity'
            },
            dataFields: [
                { name: 'id', type: 'number' },
                { name: 'code', type: 'string' },
                { name: 'title', type: 'string' },
                { name: 'credit_acct_code', type: 'string' },
                { name: 'debit_acct_code', type: 'string' },
            
            ],
            filter: function() {
                // update the grid and send a request to the server.
                that.dropDownGrid.updatebounddata('filter');
            },
            sort: function() {
                // update the grid and send a request to the server.
                that.dropDownGrid.updatebounddata('sort');
            },
            id: 'id',
            root: 'data',
        });
        // that.gridPartner.setOptions({source:[]});
        that.dropDownDataAdapter = new jqx.dataAdapter(source, {
            beforeLoadComplete: function (records) {
                for (var i = 0; i < records.length; i++) {
                    var row = records[i];
                }
                return records;
            },
        });
        that.dropDownGridSetting = {
            columns: columns,
            localization: Helpers.getLocalizatio(Helpers.translate.activeLang),
            source: that.dropDownDataAdapter,
            ready: function() {
                that.ensureDropDownGrid()
            },
            rendergridrows: (params: any): any[] => {
                return that.dropDownDataAdapter.records;
            },
            rendered(event: any): void {
            },
            onBindingcomplete(event:any):void{
                // setTimeout(that.ensureDropDownGrid,800)
            },
            onRowSelect: (event:any)=>{
                var args = event.args;
                let rowData = that.dropDownGrid.getrowdata(args.rowindex);
                if(rowData){
                    if(rowData){
                        that.setSelectedItem(rowData);
                        that.onChangeCallback(rowData[that.key])
                        if(
                            !rowData.credit_acct_code ||
                            !rowData.debit_acct_code
                            ){
                            toastr.warning(Helpers.translate.get('This Item missing the debit,credit setting'))
                        }
                    }else{
                        that.setSelectedItem(undefined);
                        that.onChangeCallback(undefined)
                    }
                    that.onChange.emit()
                    if(that.dropDownButton) $(that.dropDownButton).trigger('click')
                }
            },
            onRowdoubleclick: (event:any)=>{
                var args = event.args;
                var rowBoundIndex = args.rowindex;
                var rowVisibleIndex = args.visibleindex;
                var rightclick = args.rightclick; 
                var columnindex = args.columnindex;
                var dataField = args.datafield;
                var value = args.value;
                if(args && args.row){
                    var dataRow = args.row.bounddata
                }
            },
        }
    }
    ensureDropDownGrid(){
        let that = this;
        if(that.dropDownGrid){
            let rowid;
            let index = that.dropDownGrid.getselectedrowindex();
            let rowData = that.dropDownGrid.getrowdata(index);
            if(rowData && rowData[that.key]==that.innerValue){

            }else{
                that.dropDownDataAdapter.records.map((row)=>{
                    if(row[that.key] == that.innerValue){
                        rowid = row.id
                    }
                })
                let rowIndex = that.dropDownGrid.getrowboundindexbyid(rowid)
                if(rowIndex>=0){
                    that.dropDownGrid.ensurerowvisible(rowIndex);
                    that.dropDownGrid.selectedrowindex(rowIndex);
                }
            }
        }
    }
    dropDownButton
    initDropdown($event){
        this.dropDownButton = $event.target
        this.initDropDownGrid()
    }
    onDropdownSelectect(item){
        let that = this;
        that.setSelectedItem(item);
        that.onChangeCallback(item[that.key])
        that.onChange.emit()
    }
    switchToAdvance(){
        if(this.dropDownButton) $(this.dropDownButton).trigger('click');
        this.open()
    }
    selectedItem;
    setSelectedItem(item){
        let that = this;
        that.selectedItem = item;
        if(item  && item.id){
            that.label = item.code + ' ' + item.title;
            that.innerValue = item[that.key];
        }else{
            that.label = Helpers.translate.get("Please Choose");
            that.innerValue = undefined
        }
    }
    getSelectedItem(){
        return this.selectedItem;
    }
    setValue(v){
        this.innerValue = v
        this.initDisplayValue();
        
    }
    ensure(){
        let that = this;
        console.log('ensure',that.innerValue)
        if(that.grid){
            let rowIndex = that.grid.getrowboundindexbyid(that.innerValue)
            if(rowIndex){
                that.grid.ensurerowvisible(rowIndex);
                that.grid.selectedrowindex(rowIndex);
            }
        }
    }
    initDisplayValue(){
        let that = this;
        setTimeout((any)=>{
            if(that.innerValue>0){
                that.api.post({
                    url: AppSetting.ACTIVITY_DETAIL_URL,
                    params:{
                        id: that.innerValue
                    }
                }).done(function(res){
                    if(res.data){
                        that.setSelectedItem(res.data);
                    }else{
                        that.setSelectedItem(undefined);
                    }
                })
            }else{
                that.setSelectedItem(undefined);
            }
        }, 42)
    }
    ngAfterViewInit(){
        let that = this;
        that.loadAdapter();
        if(that.layout == 'grid') 
            setTimeout(()=>{
                that.initGrid();
            },200)
        that.initDisplayValue()
    }

    toggle(){
        if(this.window.isOpen()) this.close();
        else this.open()
    }
    open(){
        this.window.open();
        this.initGrid()
        // this.onOpen.emit()
        
    }
    eventWindowOpen(){
        this.ensure()
        this.onOpen.emit()
    }
    eventWindowClose(){
        this.onClose.emit()
    }
    close(){
        this.window.close();
        // this.onClose.emit()
    }
    clear(){
        let that = this;
        that.grid.clearselection()
        // that.label = "Please Choose";
    }
    done(){
        let that = this;
        let index = that.grid.getselectedrowindex();
        let rowData = that.grid.getrowdata(index);
        if(rowData && rowData.id < 0){
            toastr.warning('Activity invalid !')
            return;
        }
        if(index>=0 && rowData){
            that.setSelectedItem(rowData);
            that.onChangeCallback(rowData.id)
            if(
                !rowData.credit_acct_code ||
                !rowData.debit_acct_code
                ){
                toastr.warning(Helpers.translate.get('This Item missing the debit,credit setting'))
            }
        }else{
            that.setSelectedItem(undefined);
            that.onChangeCallback(undefined)
        }
        that.onChange.emit()
        that.close();
    }
    initContent(){
        setTimeout(()=>{
            let that = this;
            console.log(this,self)
        },200)
    }

    source;
    dataAdapter;
    gridSetting;
    loadAdapter(){
        let that = this;

        
        that.source = Helpers.Source({
            dataType: "json",
            type: 'POST',
            url: AppSetting.COMMON_BINDING_URL,
            data: {
                table:'acct_activity'
            },
            dataFields: [
                { name: 'id', type: 'number' },
                { name: 'code', type: 'string' },
                { name: 'title', type: 'string' },
                { name: 'credit_acct_code', type: 'string' },
                { name: 'debit_acct_code', type: 'string' },
            
            ],
            filter: function() {
                // update the grid and send a request to the server.
                that.grid.updatebounddata('filter');
            },
            sort: function() {
                // update the grid and send a request to the server.
                that.grid.updatebounddata('sort');
            },
            id: 'id',
            root: 'data',
        });
        // that.gridPartner.setOptions({source:[]});
        that.dataAdapter = new jqx.dataAdapter(that.source, {
            beforeLoadComplete: function (records) {
                for (var i = 0; i < records.length; i++) {
                    var row = records[i];
                }
                return records;
            }
        });
    }
    pending = false;
    beforeEditData
    currentEditData
    initGrid(){
        let that = this;
        
        let columns = [
                { 
                    text: 'ID', dataField: 'id', width: 80 , hidden: true
                },{
                    text: Helpers.translate.get("Code"), dataField: 'code', width: 80 ,
                    validation: (cell: any, value: any): any => {
                        let row = cell.row;
                        let rowData = that.grid.getrowdata(row);
                        if (value == '')
                            return { result: false, message: Helpers.translate.get('Code') + Helpers.translate.get(' should be not empty') };
                        let c = that.dataAdapter.records
                            .filter(function(row){
                                return row.code == value && rowData.id !=row.id;
                            }).length;
                        if(c){
                            return { result: false, message: Helpers.translate.get('Code') + Helpers.translate.get(' invalid') };
                        }
                        return true;
                    }
                },{ 
                    text: Helpers.translate.get("Title"), dataField: 'title',
                    validation: (cell: any, value: any): any => {
                        if (value == '')
                            return { result: false, message: Helpers.translate.get('Title') + Helpers.translate.get(' should be not empty') };
                        return true;
                    }
                },{ 
                    text: Helpers.translate.get('Debit Account'), dataField: 'debit_acct_code',width: 180 ,
                    filtertype: 'input',
                    validation: (cell: any, value: any): any => {
                        if(that.pending){
                            return { result: false,message:'Choose ...'};
                        }else{
                            return true
                        }
                    },
                    createeditor: (row: number, cellvalue: any, editor: any, cellText: any, width: any, height: any): void => {
                        $('<span class="editor-dropdown-button"></span>')
                            .on('click',()=>{
                                console.log('dblclick',editor)
                                that.debitAcctCbx.open();
                                that.debitAcctCbx.setValue(that.currentEditData.debit_acct_code);
                                that.cellEditor = editor;
                            })
                            .insertAfter(editor)
                        editor.css({width:'100%'})
                    },
                },{ 
                    text: Helpers.translate.get('Credit Account'), dataField: 'credit_acct_code',width: 180 ,
                    filtertype: 'input',
                    validation: (cell: any, value: any): any => {
                        if(that.pending){
                            return { result: false,message:'Choose ...'};
                        }else{
                            return true
                        }
                    },
                    createeditor: (row: number, cellvalue: any, editor: any, cellText: any, width: any, height: any): void => {
                        $('<span class="editor-dropdown-button"></span>')
                            .on('click',()=>{
                                console.log('dblclick',editor)
                                that.creditAcctCbx.open();
                                that.creditAcctCbx.setValue(that.currentEditData.credit_acct_code);
                                that.cellEditor = editor;
                            })
                            .insertAfter(editor)
                        editor.css({width:'100%'})
                    },
                }
            ]
        
        if(that.gridSetting){
            return;
        }
        Helpers.translate.do(()=>{
            if(that.grid){
                that.grid.setcolumnproperty('code','text', 'Code')
                that.grid.setcolumnproperty('title','text', 'Title')
                that.grid.setcolumnproperty('debit_acct_code','text', 'Debit Account')
                that.grid.setcolumnproperty('credit_acct_code','text', 'Credit Account')
                that.grid.setOptions({'localization': Helpers.getLocalizatio(Helpers.translate.activeLang)})
            }
        })
        that.gridSetting = {
            columns: columns,
            localization: Helpers.getLocalizatio(Helpers.translate.activeLang),
            source: that.dataAdapter,
            ready: function() {
                that.initAcctCbx()
                that.ensure()
            },
            rendergridrows: (params: any): any[] => {
                return that.dataAdapter.records;
            },
            rendered(event: any): void {

            },
            onCellBeginEdit(event: any): void {
                let args = event.args;
                that.beforeEditData = cloneDeep(args.row);
                that.currentEditData = cloneDeep(args.row);
            },
            onCellEndEdit(event: any): void {
                let args = event.args;
                let row = args.row;
                that.currentEditData[args.datafield] = args.value;
                if (args.datafield === 'credit_acct_code') {
                    if(
                        JSON.stringify(that.currentEditData) !=
                        JSON.stringify(that.beforeEditData)
                        ){
                        let params = cloneDeep(that.currentEditData);
                        params.code = params.code.toUpperCase();
                        if(params.id<0){
                            delete params.id;
                        }
                        that.api.post({
                            url: AppSetting.ACTIVITY_COMMIT_URL,
                            params: params,
                            onDone:function(res){
                                toastr.success(Helpers.translate.get('Update successfuly'))
                                that.dataAdapter.dataBind();
                                if(that.dropDownDataAdapter) that.dropDownDataAdapter.dataBind();
                                that.currentEditData = undefined;
                                that.beforeEditData = undefined;
                            },
                            onFail:function(){
                                toastr.warning(Helpers.translate.get('Fail to update'))
                            },
                        })
                    }
                }
            },
            editSettings: { 
                saveOnPageChange: true, 
                saveOnBlur: true, 
                saveOnSelectionChange: false, 
                cancelOnEsc: true, 
                saveOnEnter: true, 
                editOnDoubleClick: false, 
                editOnF2: false 
            },
        }
    }
    reloadGrid(){
        this.dataAdapter.dataBind();
        this.grid.updatebounddata('cells');
    }
    editingItem :any = {
        id:0,
        code: '',
        title: '',
        credit_acct_code: '',
        debit_acct_code: '',
    }
    clickAdd(){
        this.showDetail()
    }
    clickEdit(){
        let that = this;
        let index = that.grid.getselectedrowindex();
        let rowData = that.grid.getrowdata(index);
        if(rowData){
            that.showDetail(rowData.id)
        }
    }
    clickDelete(){
        let that = this;
        let index = that.grid.getselectedrowindex();
        let rowData = that.grid.getrowdata(index);
        if(rowData){
            Helpers.confirm('<h3>' + Helpers.translate.get('Are you sure you want to delete this item?') + '</h3>',function(){
                
                that.api.post({
                    url: AppSetting.ACTIVITY_DELETE_URL,
                    params: {
                        id: rowData.id
                    }
                }).done(function(res){
                    if(res.code){
                        that.dataAdapter.dataBind();
                        // that.grid.updatebounddata('cells');
                    }
                })
            })
        }
    }
    showDetail(id=0){
        let that = this;
        that.detailWindow.open();
        if(id){
            that.api.post({
                url: AppSetting.ACTIVITY_DETAIL_URL,
                params: {
                    id: id
                }
            }).done(function(res){
                if(res.data){
                    that.setData(res.data)
                }
            })
        }else{
            that.setData({
                id:0,
                code: '',
                title: '',
                credit_acct_code: '',
                debit_acct_code: '',
            })
        }
    }
    setData(data){
        let that = this
        that.editingItem = data;
        if(data.credit_acct_code && that.creditAcctCbx){
            that.creditAcctCbx.setValue(data.credit_acct_code)
        } else if(that.creditAcctCbx) that.creditAcctCbx.setValue(null);
        if(data.debit_acct_code && that.debitAcctCbx){
            that.debitAcctCbx.setValue(data.debit_acct_code)
        } else if(that.debitAcctCbx) that.debitAcctCbx.setValue(null);
    }
    acctSetting
    initAcctCbx(){
        let that = this;
        that.acctSetting = {
                onOpen: ()=>{
                    that.pending = true;
                },
                onClose: ()=>{
                    that.pending = false;
                    $('.jqx-grid-validation,.jqx-grid-validation-arrow-up').remove()
                },
                onDebitAcctChange: function(args){
                    let item = args.getSelectedItem();
                    if(that.detailWindow.isOpen()){
                        if(item){
                            // that.editingItem.debit_acct = item;
                            // that.editingItem.debit_acct_id = item.id
                        }
                        let windowId = that.validator.host.attr('id')
                        $('#'+windowId+ ' .cbx_debit_acct_id').blur()
                    }else if(that.cellEditor) that.cellEditor.val(item.acct_code)
                },
                onCreditAcctChange: function(args){
                    let item = args.getSelectedItem();
                    if(that.detailWindow.isOpen()){
                        if(item){
                            // that.editingItem.credit_acct = item;
                            // that.editingItem.credit_acct_id = item.id
                        }
                        let windowId = that.validator.host.attr('id')
                        $('#'+windowId+ ' .cbx_credit_acct_id').blur()
                    }else if(that.cellEditor) that.cellEditor.val(item.acct_code)
                }
            }
    }
    initDetailWindow(){
        console.log('initDetailWindow')
        let that = this;
        
        that.initAcctCbx();
        that.initValidation();
    }
    cellEditor
    closeDetail(){
        this.detailWindow.close();
    }
    isSubmit = false;
    checkValidate(): void {
        let that = this;
        setTimeout(() => {
            that.validator.validate();
        }, 200)
        this.isSubmit = true;
    }
    onValidationError(){
        this.isSubmit = false;
    }

    onValidationSuccess() : void {
        let that = this;
        if(that.isSubmit){
            let params = that.editingItem;
            params.code = params.code.toUpperCase();
            console.log(params,'params');
            that.api.post({
                url: AppSetting.ACTIVITY_COMMIT_URL,
                params: params
            }).done(function(res){
                // if(res.code==1){
                    that.dataAdapter.dataBind();
                    if(that.dropDownDataAdapter) that.dropDownDataAdapter.dataBind();
                    //that.grid.updatebounddata('cells');
                    that.detailWindow.close()
                    toastr.success(Helpers.translate.get('Create successfuly'))
                // }
            })
            
        }
    }
    rules
    initValidation(){
        let that = this;

        let windowId = that.validator.host.attr('id')
        if(that.rules){
            if(that.validator) that.validator.hide();
        }else
        that.rules =
        [
            // { input: '#' + windowId+ ' .txt_code', message: 'Code is required!', action: 'keyup, blur', rule: 'required' },
            // { input: '#' + windowId+ ' .txt_description', message: 'Description is required!', action: 'keyup, blur', rule: 'required' },
            // { input: '#cbx_category_id', message: 'Category is required!', action: 'change, select', rule: function () {
            //         var value = that.categoryCbx.val();
            //         return value!="";
            //     } 
            // },
            // { input: '#cbx_parent_id', message: 'Parent is required!', action: 'change, select', rule: function () {
            //         var value = that.parentCbx.val();
            //         return value!="";
            //     } 
            // }

        ];
    }
}


/*
Copyright 2017-2018 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/