import { Component, OnInit, Input,forwardRef,AfterViewInit,OnChanges, ViewChild, ViewEncapsulation, EventEmitter, Output } from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from "@angular/forms";
import * as cloneDeep from 'lodash/cloneDeep';
import { DatePipe, CurrencyPipe } from '@angular/common';
import { APIService } from '../../api.service';
import { Helpers} from "../../helpers";
import { AppSetting } from "../../settings";
import { AppMessage } from "../../message";

import { jqxWindowComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxwindow';
import { jqxGridComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxgrid';
import { jqxValidatorComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxvalidator';
import { jqxDropDownListComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxdropdownlist';
import { jqxFileUploadComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxfileupload';
import { jqxComboBoxComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxcombobox';


declare var $:any;
declare var toastr:any;

const noop = () => {
};

@Component({
    selector: 'module-company',
    templateUrl: './company.module.html',
    styleUrls: ['./company.module.css'],
    providers: [{
        provide: NG_VALUE_ACCESSOR,
        useExisting: forwardRef(() => CompanyModule),
        multi: true
    }]
})

export class CompanyModule implements OnInit,AfterViewInit, OnChanges, ControlValueAccessor {
    @Input() hidden: boolean;
    @Input() layout: string;
    @Input() type: string;
    @Input() key: string;
    @Output() onOpen = new EventEmitter();
    @Output() onClose = new EventEmitter();
    @Output() onChange = new EventEmitter();
    constructor(
        private api: APIService,
        ) {
        if(!!!this.key) this.key = 'id'
    }
    theme = AppSetting.THEME
    // The internal data model
    private innerValue: any = undefined;
    label = "Please Choose";
    // Placeholders for the callbacks which are later provided
    // by the Control Value Accessor
    private onTouchedCallback: () => void = noop;
    private onChangeCallback: (_: any) => void = noop;

    @ViewChild('window') window: jqxWindowComponent;
    @ViewChild('detailWindow') detailWindow: jqxWindowComponent;
    @ViewChild('grid') grid: jqxGridComponent;
    @ViewChild('validator') validator: jqxValidatorComponent;
    @ViewChild('fileUpload') fileUpload: jqxFileUploadComponent;


    @ViewChild('dropDownGrid') dropDownGrid: jqxGridComponent;
    

    // get accessor
    get value(): any {
        return this.innerValue;
    };

    // set accessor including call the onchange callback
    set value(v: any) {
        if (v !== this.innerValue) {
            this.innerValue = v;
            this.onChangeCallback(v);
        }
        console.log('SetValue')
    }

    // Set touched on blur
    onBlur() {
        this.onTouchedCallback();
    }

    // From ControlValueAccessor interface
    writeValue(value: any) {
        if (value !== this.innerValue) {
            this.innerValue = value;
        }
    }

    // From ControlValueAccessor interface
    registerOnChange(fn: any) {
        this.onChangeCallback = fn;
    }

    // From ControlValueAccessor interface
    registerOnTouched(fn: any) {
        this.onTouchedCallback = fn;
    }



    ngOnInit() {
    }
    ngOnChanges(){

    }

    dropDownDataAdapter
    dropDownGridSetting
    initDropDownGrid(){
        let that = this;
        let columns = [
            { text: '#', dataField: 'id', width: 120 ,hidden: true},
            { text: 'Name', dataField: 'name', width: 120 },
            { text: 'City', dataField: 'city' },
        ]
        
        if(that.dropDownGridSetting){
            that.ensureDropDownGrid()
            return;
        }
        let source = Helpers.Source({
            dataType: "json",
            type: 'POST',
            url: AppSetting.COMMON_BINDING_URL,
            data: {
                table: 'company_info',
            },
            dataFields: [
                { name: 'id', type: 'number' },
                { name: 'name', type: 'string' },
                { name: 'city', type: 'string' },
            
            ],
            filter: function() {
                // update the grid and send a request to the server.
                that.dropDownGrid.updatebounddata('filter');
            },
            sort: function() {
                // update the grid and send a request to the server.
                that.dropDownGrid.updatebounddata('sort');
            },
            id: 'id',
            root: 'data',
        });
        // that.gridPartner.setOptions({source:[]});
        that.dropDownDataAdapter = new jqx.dataAdapter(source, {
            beforeLoadComplete: function (records) {
                for (var i = 0; i < records.length; i++) {
                    var row = records[i];
                }
                return records;
            }
        });
        that.dropDownGridSetting = {
            columns: columns,
            source: that.dropDownDataAdapter,
            ready: function() {
                that.ensureDropDownGrid()
            },
            rendergridrows: (params: any): any[] => {
                return that.dropDownDataAdapter.records;
            },
            rendered(event: any): void {
            },
            onRowSelect: (event:any)=>{
                var args = event.args;
                let rowData = that.dropDownGrid.getrowdata(args.rowindex);
                if(rowData){
                    if(rowData){
                        that.setSelectedItem(rowData);
                        that.onChangeCallback(rowData[that.key])
                    }else{
                        that.setSelectedItem(undefined);
                        that.onChangeCallback(undefined)
                    }
                    that.onChange.emit()
                    if(that.dropDownButton) $(that.dropDownButton).trigger('click')
                }
            },
            onRowdoubleclick: (event:any)=>{
                var args = event.args;
                var rowBoundIndex = args.rowindex;
                var rowVisibleIndex = args.visibleindex;
                var rightclick = args.rightclick; 
                var columnindex = args.columnindex;
                var dataField = args.datafield;
                var value = args.value;
                if(args && args.row){
                    var dataRow = args.row.bounddata
                }
            },
        }
    }
    ensureDropDownGrid(){
        let that = this;
        if(that.dropDownGrid){
            let rowid;
            let index = that.dropDownGrid.getselectedrowindex();
            let rowData = that.dropDownGrid.getrowdata(index);
            if(rowData && rowData[that.key]==that.innerValue){

            }else{
                that.dropDownDataAdapter.records.map((row)=>{
                    if(row[that.key] == that.innerValue){
                        rowid = row.id
                    }
                })
                if(rowid>=0){
                    let rowIndex = that.dropDownGrid.getrowboundindexbyid(rowid)
                    that.dropDownGrid.ensurerowvisible(rowIndex);
                    that.dropDownGrid.selectedrowindex(rowIndex);
                }
            }
        }
    }
    dropDownButton
    initDropdown($event){
        this.dropDownButton = $event.target
        this.initDropDownGrid()
    }
    onDropdownSelectect(item){
        let that = this;
        that.setSelectedItem(item);
        that.onChangeCallback(item[that.key])
        that.onChange.emit()
    }
    switchToAdvance(){
        if(this.dropDownButton) $(this.dropDownButton).trigger('click');
        this.open()
    }


    selectedItem;
    setSelectedItem(item){
        let that = this;
        that.selectedItem = item;
        if(item  && item.id){
            that.label = item.name;
            that.innerValue = item[that.key];
            
        }else{
            that.label = "Please Choose";
            that.innerValue = undefined
        }
    }
    getSelectedItem(){
        return this.selectedItem;
    }
    setValue(v){
        this.innerValue = v
        this.initDisplayValue();
        
    }
    ensure(){
        let that = this;
        console.log('ensure',that.innerValue)
        if(that.grid && !!that.innerValue){
            let rowid;
            let index = that.grid.getselectedrowindex();
            let rowData = that.grid.getrowdata(index);
            if(rowData && rowData[that.key]==that.innerValue){

            }else{
                that.dataAdapter.records.map((row)=>{
                    if(row[that.key] == that.innerValue){
                        rowid = row.id
                    }
                })
                let rowIndex = that.grid.getrowboundindexbyid(rowid)
                that.grid.ensurerowvisible(rowIndex);
                that.grid.selectedrowindex(rowIndex);
            }
        }
    }
    initDisplayValue(){
        let that = this;
        setTimeout((any)=>{
            if(!!that.innerValue){
                that.api.get({
                    url: AppSetting.GET_COMPANY + that.innerValue,
                }).done(function(res){
                    if(res.data){
                        that.setSelectedItem(res.data);
                    }
                })
            }else{
                that.setSelectedItem(null);
            }
        }, 42)
    }
    ngAfterViewInit(){
        let that = this;
        that.loadAdapter();
        if(that.layout == 'grid') 
            setTimeout(()=>{
                that.initGrid();
            },200)
        that.initDisplayValue()
    }

    toggle(){
        if(this.window.isOpen()) this.close();
        else this.open()
    }
    open(){
        this.window.open();
        this.initGrid()
        // this.onOpen.emit()
        
    }
    eventWindowOpen(){
        this.ensure()
        this.onOpen.emit()
    }
    eventWindowClose(){
        this.onClose.emit()
    }
    close(){
        this.window.close();
        // this.onClose.emit()
    }
    clear(){
        let that = this;
        that.grid.clearselection()
        // that.label = "Please Choose";
    }
    done(){
        let that = this;
        let index = that.grid.getselectedrowindex();
        let rowData = that.grid.getrowdata(index);
        if(index>=0 && rowData){
            that.setSelectedItem(rowData);
            that.onChangeCallback(rowData[that.key]);
            
        }else{
            that.setSelectedItem(undefined);
            that.onChangeCallback(undefined)
        }
        that.onChange.emit()
        that.close();
    }
    initContent(){
        setTimeout(()=>{
            let that = this;
            console.log(this,self)
        },200)
    }

    source;
    dataAdapter;
    gridSetting;
    
    loadAdapter(){
        let that = this;
        that.source = Helpers.Source({
            dataType: "json",
            type: 'POST',
            url: AppSetting.COMMON_BINDING_URL,
            data: {
                table: 'company_info',
            },
            dataFields: [
                { name: 'id', type: 'number' },
                { name: 'name', type: 'string' },
                { name: 'city', type: 'string' },
            
            ],
            filter: function() {
                // update the grid and send a request to the server.
                that.grid.updatebounddata('filter');
            },
            sort: function() {
                // update the grid and send a request to the server.
                that.grid.updatebounddata('sort');
            },
            id: 'id',
            root: 'data',
        });
        // that.gridPartner.setOptions({source:[]});
        that.dataAdapter = new jqx.dataAdapter(that.source, {
            beforeLoadComplete: function (records) {
                for (var i = 0; i < records.length; i++) {
                    var row = records[i];
                }
                return records;
            }
        });
    }
    pending = false;
    beforeEditData;
    currentEditData;
    cellEditor
    initGrid(){
        let that = this;
        
        let columns = [
                { text: '#', dataField: 'id', width: 120 ,hidden: true},
                { 
                    text: 'Name', dataField: 'name', width: 92 ,
                },{ 
                    text: 'City', dataField: 'city', minWidth:180,
                },{ 
                    text: 'Status', dataField: 'is_active', width: 60 , columntype: 'checkbox',
                    filtertype: 'bool'
                },
                
            ]
        
        if(that.gridSetting){
            return;
        }
        that.gridSetting = {
            columns: columns,
            source: that.dataAdapter,
            ready: function() {
                that.initCbxs()
                that.ensure()
            },
            rendergridrows: (params: any): any[] => {
                return that.dataAdapter.records;
            },
            rendered(event: any): void {
                that.grid.expandallgroups();
            },
            onRowdoubleclick: (event:any)=>{
                // event arguments.
                var args = event.args;
                // row's bound index.
                var rowBoundIndex = args.rowindex;
                // row's visible index.
                var rowVisibleIndex = args.visibleindex;
                // right click.
                var rightclick = args.rightclick; 
                // original event.
                var ev = args.originalEvent;
                // column index.
                var columnindex = args.columnindex;
                // column data field.
                var dataField = args.datafield;
                // cell value
                var value = args.value;
                if(args && args.row){
                    var dataRow = args.row.bounddata
                    if(dataRow) that.showDetail(dataRow.id)
                }
            },
            onCellBeginEdit(event: any): void {
                let args = event.args;
                // if(!that.currentEditData){
                    that.beforeEditData = cloneDeep(args.row);
                    that.currentEditData = cloneDeep(args.row);
                // }
            },
            onCellEndEdit(event: any): void {
                let args = event.args;
                let row = args.row;
                that.currentEditData[args.datafield] = args.value;
                if (args.datafield === 'invt_ptg_code') {
                    if(
                        JSON.stringify(that.currentEditData) !=
                        JSON.stringify(that.beforeEditData)
                        ){
                        let params = cloneDeep(that.currentEditData);
                        let conditions
                        if(params.id<0){
                            delete params.id;
                        }
                        console.log(params,'params')
                        that.api.post({
                            url: AppSetting.ITEMS_COMMIT_URL,
                            params: {
                                id: params.id,
                                params: params
                            },
                            onDone:function(res){
                                if(!params.id){
                                    that.currentEditData.id = res.data.id
                                    toastr.success('Create Items successfuly')
                                }else{
                                    toastr.success('Update Items successfuly')
                                }
                                // let index = that.grid.getselectedrowindex();
                                // let id = that.grid.getrowid(index);
                                // that.grid.updaterow(id,that.currentEditData);
                                that.dataAdapter.dataBind();
                                //that.grid.updatebounddata('cells');
                                that.currentEditData = undefined;
                                that.beforeEditData = undefined;
                            },
                            onFail:function(){
                                toastr.warning('Fail to create/update Items')
                            },
                        })
                    }
                }
            },
        }
    }
    reloadGrid(){
        this.dataAdapter.dataBind();
        this.grid.updatebounddata('cells');
    }
    editingItem:any = {
        id:0,
        username: '',
        email: ''
    }
    clickAdd(){
        this.showDetail()
    }
    clickEdit(){
        let that = this;
        let index = that.grid.getselectedrowindex();
        let rowData = that.grid.getrowdata(index);
        if(rowData){
            that.showDetail(rowData.id)
        }
    }
    clickDelete(){
        // let that = this;
        // let index = that.grid.getselectedrowindex();
        // let rowData = that.grid.getrowdata(index);
        // if(rowData){
        //     Helpers.confirm('<h3>Are you sure you want to delete bussiness posting group?</h3>',function(){
                
        //         that.api.post({
        //             url: AppSetting.DEL_SETUP_POST_URL,
        //             params: {
        //                 id: rowData.id
        //             }
        //         }).done(function(res){
        //             if(res.code){
        //                 that.dataAdapter.dataBind();
        //                 that.grid.updatebounddata('cells');
        //             }
        //         })
        //     })
        // }
    }
    showDetail(id=0){
        let that = this;
        that.detailWindow.open();
        if(id){
            that.api.post({
                url: AppSetting.GET_ITEMS_URL,
                params: {
                    id: id
                }
            }).done(function(res){
                if(res.data){
                    that.setData(res.data)
                }
            })
        }else{
            that.setData({
                id:0,
                // gen_business_ptg_code: '',
                // gen_product_ptg_code: '',
                // sales_acct_code: '',
                // sales_credit_memo_acct_code: '',
                // purch_acct_code: '',
                // purch_credit_memo_acct_code: '',
            })
        }
    }
    setData(data){
        let that = this
        that.editingItem = data
        // if(data.gen_product_ptg_code && that.genProdPtgCbx){
        //     that.genProdPtgCbx.setValue(data.gen_product_ptg_code)
        // } else if(that.genProdPtgCbx) that.genProdPtgCbx.setValue(null);
        // if(data.vat_product_ptg_code && that.vatProdPtgCbx){
        //     that.vatProdPtgCbx.setValue(data.vat_product_ptg_code)
        // } else if(that.vatProdPtgCbx) that.vatProdPtgCbx.setValue(null);
        // if(data.invt_ptg_code && that.invtPtgCbx){
        //     that.invtPtgCbx.setValue(data.invt_ptg_code)
        // } else if(that.invtPtgCbx) that.invtPtgCbx.setValue(null);

       

    }
    
    // genProdPtgSetting
    // initGenProdPtgCbx(){
    //     let that = this;
    //     if(that.genProdPtgSetting) return;
    //     that.genProdPtgSetting = {
    //         onOpen: ()=>{
    //             that.pending = true;
    //         },
    //         onClose: ()=>{
    //             that.pending = false;
    //             $('.jqx-grid-validation,.jqx-grid-validation-arrow-up').remove()
    //         },
    //         onChange: function(event){
    //             let item = that.genProdPtgCbx.getSelectedItem();
    //             if(that.detailWindow.isOpen()){
    //                 if(item){
    //                     // that.editingItem.gen_product_ptg = item;
    //                     // that.editingItem.gen_product_ptg_code = item.code
    //                 }
    //                 let windowId = that.validator.host.attr('id')
    //                 $('#'+windowId+ ' .cbx_gen_product_ptg_code').change()

    //             }else{
    //                 if(that.cellEditor) {
    //                     that.cellEditor.val(item.code)

    //                 }
    //             }
                
    //         },
    //     }
    // }
    
    initCbxs(){
        // this.initGenProdPtgCbx()
    }
    initDetailWindow(){
        let that = this;
        that.initCbxs()
        that.initValidation();
    }
    closeDetail(){
        this.detailWindow.close();
    }
    isSubmit = false;
    checkValidate(): void {
        console.log('AA')
        let that = this;
        setTimeout(() => {
            that.validator.validate();
        }, 200)
        this.isSubmit = true;
    }
    onValidationError(){
        this.isSubmit = false;
    }

    onValidationSuccess() : void {
        let that = this;
        if(that.isSubmit){
            var formData = cloneDeep(that.editingItem);
            let newImages = formData.newImages;
            // delete formData.images;
            // delete formData.newImages;
            // formData.images = formData.newImages

            console.log(formData,'params');
            that.api.post({
                url: AppSetting.ITEMS_COMMIT_URL,
                params: {
                    id:that.editingItem.id,
                    params: formData,
                    images: formData.newImages
                }
            }).done(function(res){
                if(res.code==1){
                    that.dataAdapter.dataBind();
                    // that.grid.updatebounddata('cells');
                    that.detailWindow.close()
                    toastr.success('Create successfuly')
                }
            })
            
        }
    }
    rules
    initValidation(){
        let that = this;
        let windowId = that.validator.host.attr('id')
        if(that.rules){
            if(that.validator) that.validator.hide();
        }else
        that.rules =
        [
            { input: '#' + windowId+ ' .txt_name', message: 'Name is required!', action: 'keyup, blur', rule: 'required' },
            { input: '#' + windowId+ ' .txt_city', message: 'City is required!', action: 'keyup, blur', rule: 'required' },
            // { input: '#' + windowId+ ' .cbx_gen_product_ptg_code', message: 'General Product Posting Group is required!', action: 'change', rule: function () {
            //         return !!that.genProdPtgCbx.value;
            //     } 
            // },{ input: '#' + windowId+ ' .cbx_vat_product_ptg_code', message: 'TAX Product Posting Group is required!', action: 'change', rule: function () {
            //         return !!that.vatProdPtgCbx.value;
            //     } 
            // },{ input: '#' + windowId+ ' .cbx_invt_ptg_code', message: 'Inventory Posting Group is required!', action: 'change', rule: function () {
            //         return !!that.invtPtgCbx.value;
            //     } 
            // },

        ];

    }
    onSetDefaultPhoto(udid){
        this.editingItem.default_image = udid
    }
    onSelectPhoto(event){
        let that = this;
        var args = event.args;
        var fileName = args.file;
        var fileSize = args.size;
        console.log(args,that.fileUpload)
        var files = $(that.fileUpload.elementRef.nativeElement).find('input[type="file"]').get().map(function(f){
            return f.files[0]
        }).filter(function(f){
            return f;
        });

        // var images = files.map(function(file,i){
        Helpers.getBase64(files[files.length-1]).then(
          data => {
                that.fileUpload.cancelAll();
                if(!that.editingItem.newImages) that.editingItem.newImages = []

                let key = new Date().getTime()
                if(!that.editingItem.default_image) that.editingItem.default_image = key
                that.editingItem.newImages.push({
                    filename: fileName,
                    photo:data,
                    udid: key
                })
        //       // console.log(data)
        //        $('#jqxFileUpload .jqx-file-upload-file-row:eq('+i+') img').remove();
        //        $('#jqxFileUpload .jqx-file-upload-file-row:eq('+i+')').prepend($('<img/>').attr('src',data))
          }
        )
    }
}


/*
Copyright 2017-2018 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/