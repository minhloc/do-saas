import { Component, OnInit, Input,forwardRef,AfterViewInit,OnChanges, ViewChild, ViewEncapsulation, EventEmitter, Output } from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from "@angular/forms";

import { DatePipe, CurrencyPipe,DecimalPipe } from '@angular/common';
import { APIService } from '../../api.service';
import { Helpers} from "../../helpers";
import { AppSetting } from "../../settings";
import { AppMessage } from "../../message";

import { jqxWindowComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxwindow';
import { jqxGridComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxgrid';
import { jqxValidatorComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxvalidator';
import { jqxDropDownListComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxdropdownlist';
import { FormulasModule } from '../../widgets/formulas/formulas.module';

declare var $:any;
declare var toastr:any;
declare var moment:any;

const noop = () => {
};

@Component({
    selector: 'module-account',
    templateUrl: './account.module.html',
    styleUrls: ['./account.module.css'],
    providers: [{
        provide: NG_VALUE_ACCESSOR,
        useExisting: forwardRef(() => AccountModule),
        multi: true
    }]
})

export class AccountModule implements OnInit,AfterViewInit, OnChanges, ControlValueAccessor {
    @Input() hidden: boolean;
    @Input() key: string;
    @Input() none_item: boolean;
    @Input() none_control_acct: boolean;
    @Input() gen_posting_type: number;
    @Input() layout: string;
    @Output() onOpen = new EventEmitter();
    @Output() onClose = new EventEmitter();
    @Output() onChange = new EventEmitter();
    constructor(
        private api: APIService,
        ) {
        if(!!!this.key) this.key = 'id'
    }
    theme = AppSetting.THEME
    // The internal data model
    @ViewChild('journalDetailGrid') journalDetailGrid: jqxGridComponent;
    @ViewChild('validator') validator: jqxValidatorComponent;
    @ViewChild('categoryCbx') categoryCbx: jqxDropDownListComponent;
    @ViewChild('parentCbx') parentCbx: jqxDropDownListComponent;
    @ViewChild('gen_posting_typeCbx') gen_posting_typeCbx: jqxDropDownListComponent;
    @ViewChild('dropDownGrid') dropDownGrid: jqxGridComponent;
    @ViewChild('formulas') formulas: FormulasModule;
    @ViewChild('dropDownButton') dropDownButton; 

    // get accessor
    get value(): any {
        return this.innerValue;
    };
    private innerValue: any = undefined;
    label = Helpers.translate.get("Please Choose");
    // Placeholders for the callbacks which are later provided
    // by the Control Value Accessor
    private onTouchedCallback: () => void = noop;
    private onChangeCallback: (_: any) => void = noop;

    @ViewChild('window') window: jqxWindowComponent;
    @ViewChild('detailWindow') detailWindow: jqxWindowComponent;
    @ViewChild('journalLineWindow') journalLineWindow: jqxWindowComponent;
    @ViewChild('grid') grid: jqxGridComponent;

    // set accessor including call the onchange callback
    set value(v: any) {
        if (v !== this.innerValue) {
            this.innerValue = v;
            this.onChangeCallback(v);
        }
        console.log('SetValue')
    }

    // Set touched on blur
    onBlur() {
        this.onTouchedCallback();
    }

    // From ControlValueAccessor interface
    writeValue(value: any) {
        if (value !== this.innerValue) {
            this.innerValue = value;
        }
    }

    // From ControlValueAccessor interface
    registerOnChange(fn: any) {
        this.onChangeCallback = fn;
    }

    // From ControlValueAccessor interface
    registerOnTouched(fn: any) {
        this.onTouchedCallback = fn;
    }



    ngOnInit() {
    }
    ngOnChanges(){

    }

    dropDownGridSetting
    initDropDownGrid(){
        let that = this;
        
        let columns = [
            { text: '#', dataField: 'id', width: 120 ,hidden: true},
            { 
                text: Helpers.translate.get('Account No.'), dataField: 'acct_code', width: 80 ,
                renderer: function (text, align, columnsheight) {
                    return '<div class="column-header-two-line">'+Helpers.translate.get('Account No.')+'</div>';
                },
            },
            { text: Helpers.translate.get('Account Name'), dataField: 'acct_name' ,
                renderer: function (text, align, columnsheight) {
                    return '<div class="column-header-two-line">'+Helpers.translate.get('Account Name')+'</div>';
                },
            },
            { text: 'Parent', dataField: 'parent_id' ,hidden: true},
            { text: 'Category', dataField: 'category_id' ,hidden: true},
        ]
        
        if(that.dropDownGridSetting){
            that.ensureDropDownGrid()
            that.dropDownGrid.focus()
            return;
        }
        Helpers.translate.do(()=>{
            if(that.dropDownGrid){
                that.dropDownGrid.setcolumnproperty('id','text', '#')
                that.dropDownGrid.setcolumnproperty('acct_code','text', Helpers.translate.get('Account No.'))
                that.dropDownGrid.setcolumnproperty('acct_name','text', Helpers.translate.get('Account Name'))
                that.dropDownGrid.setOptions({'localization': Helpers.getLocalizatio(Helpers.translate.activeLang)})
            }
        })
        let conditions:any = [
            ['type',3]
        ];
        if(that.gen_posting_type){
            conditions.push([
                ['gen_posting_type',null],
                'or',
                ['gen_posting_type',that.gen_posting_type]
                ])
        }
        if(that.none_item){
            conditions.push(['none_item',+that.none_item])
        }
        if(that.none_control_acct){
            conditions.push(['control_acct',0])
        }
        let source = Helpers.Source({
            dataType: "json",
            type: 'POST',
            // url: AppSetting.ACCT_LIST_URL,
            // data: {
            //     type: 3
            // },
            url: AppSetting.COMMON_BINDING_URL,
            data: {
                table:'acct',
                conditions: conditions,
                all:true
            },
            dataFields: [
                { name: 'id', type: 'number' },
                { name: 'parent_id', type: 'number' },
                { name: 'acct_code', type: 'string' },
                { name: 'acct_name', type: 'string' },
                { name: 'category_id', type: 'number' },
            
            ],
            // filter: function() {
            //     // update the grid and send a request to the server.
            //     that.dropDownGrid.updatebounddata('filter');
            // },
            // sort: function() {
            //     // update the grid and send a request to the server.
            //     that.dropDownGrid.updatebounddata('sort');
            // },
            id: 'id',
            root: 'data',
        });
        // that.gridPartner.setOptions({source:[]});
        let dataAdapter = new jqx.dataAdapter(source, {
            beforeLoadComplete: function (records) {
                for (var i = 0; i < records.length; i++) {
                    var row = records[i];
                }
                return records;
            }
        });
        that.dropDownDataAdapter = dataAdapter;
        let groupsrenderer = function (text, group, expanded, data) {
            if (
                data.groupcolumn.datafield == 'category_id' ||
                data.groupcolumn.datafield == 'parent_id'
                ) {
                // var number = dataAdapter.formatNumber(group, data.groupcolumn.cellsformat);
                var stext = data.groupcolumn.text + ': ' + group;
                if (data.subItems.length > 0) {
                    var findFather = function(id){
                        for (var i = 0; i < that.cateDataAdapter.originaldata.length; i++) {
                            var item = that.cateDataAdapter.originaldata[i];
                            if(item.id == id) return item.acct_name;
                        };
                        return '';
                    }
                    stext = findFather(data.subItems[0].parent_id)
                } else {
                    var rows = new Array();
                    var findDrawer = function(id){
                        for (var i = 0; i < that.cateDataAdapter.records.length; i++) {
                            var item = that.cateDataAdapter.records[i];
                            if(item.id == id) return item.acct_name;
                        }
                        return '';
                    }
                    var getRows = function (group, rows) {
                        if (group.subGroups && group.subGroups.length > 0) {
                            for (var i = 0; i < group.subGroups.length; i++) {
                                getRows(group.subGroups[i], rows);
                            }
                        }
                        else if(group.subItems){
                            stext = findDrawer(group.subItems[0].category_id);
                            for (var i = 0; i < group.subItems.length; i++) {
                                rows.push(group.subItems[i]);
                            }
                        }
                    }
                    getRows(data, rows);
                }
                
                return [
                    '<div class="jqx-grid-groups-row jqx-grid-groups-row-office" style="position: absolute;">',
                        '<span>' + stext + ' </span>',
                    '</div>'
                ].join('');
            }
            else {
                return '<div ><span>' + text + '</span>';
            }
        }
        that.dropDownGridSetting = {
            columns: columns,
            localization: Helpers.getLocalizatio(Helpers.translate.activeLang),
            source: dataAdapter,
            groupsrenderer: groupsrenderer,
             groups:['category_id','parent_id'],
            ready: function() {
                that.ensureDropDownGrid()
                that.dropDownGrid.focus()
            },
            rendergridrows: (params: any): any[] => {
                return dataAdapter.records;
            },
            rendered(event: any): void {
                that.grid.expandallgroups();
            },
            onRowSelect: (event:any)=>{
                var args = event.args;
                let rowData = that.dropDownGrid.getrowdata(args.rowindex);
                if(rowData){
                    if(rowData){
                        that.setSelectedItem(rowData);
                        that.onChangeCallback(rowData[that.key])
                    }else{
                        that.setSelectedItem(undefined);
                        that.onChangeCallback(undefined)
                    }
                    that.onChange.emit(that)
                    if(that.dropDownButton) {
                        $(that.dropDownButton.nativeElement).dropdown('toggle');
                        $(that.dropDownButton.nativeElement).focus()
                    }
                }
            },
            onRowdoubleclick: (event:any)=>{
                var args = event.args;
                var rowBoundIndex = args.rowindex;
                var rowVisibleIndex = args.visibleindex;
                var rightclick = args.rightclick; 
                var columnindex = args.columnindex;
                var dataField = args.datafield;
                var value = args.value;
                if(args && args.row){
                    var dataRow = args.row.bounddata
                }
            },
            handlekeyboardnavigation: (event:any)=>{
                var key = event.charCode ? event.charCode : event.keyCode ? event.keyCode : 0;
                if (key == 13) {
                    let index = that.dropDownGrid.getselectedrowindex();
                    let rowData = that.dropDownGrid.getrowdata(index);
                    if(index>=0 && rowData){
                        that.setSelectedItem(rowData);
                        that.onChangeCallback(rowData[that.key])
                        that.onChange.emit(that);
                        if(that.dropDownButton) {
                            $(that.dropDownButton.nativeElement).dropdown('toggle');
                            $(that.dropDownButton.nativeElement).focus()
                        }
                    }
                    return true;
                };
            },
        }
    }
    dropDownDataAdapter
    ensureDropDownGrid(){
        let that = this;
        if(that.dropDownGrid){
            that.dropDownGrid.selectedrowindex(-1);
            let rowid;
            let index = that.dropDownGrid.getselectedrowindex();
            if(index == -1) index = 0;
            let rowData = that.dropDownGrid.getrowdata(index);
            if(rowData && rowData[that.key]==that.innerValue){
                console.log('NOTHING')
            }else{
                that.dropDownDataAdapter.records.map((row)=>{
                    if(row[that.key] == that.innerValue){
                        rowid = row.id
                    }
                })
                let rowIndex = that.dropDownGrid.getrowboundindexbyid(rowid)
                if(rowIndex == -1) rowIndex = 0;
                that.dropDownGrid.ensurerowvisible(rowIndex);
                that.dropDownGrid.selectedrowindex(rowIndex);
            }
        }
    }
    initDropdown($event){
        let that = this;
        
        
    }
    switchToAdvance(){
        if(this.dropDownButton) $(this.dropDownButton.nativeElement).dropdown('toggle');
        this.open()
    }


    selectedItem;
    setSelectedItem(item){
        let that = this;
        that.selectedItem = item;
        if(item  && item.id){
            that.label = item.acct_code
            that.innerValue = item[that.key];
        }else{
            that.label = Helpers.translate.get("Please Choose");
            that.innerValue = undefined
        }
    }
    getSelectedItem(){
        return this.selectedItem;
    }
    setValue(v){
        this.innerValue = v
        this.initDisplayValue();
        
    }
    ensure(){
        let that = this;
        if(that.grid){
            let rowid;
            let index = that.grid.getselectedrowindex();
            let rowData = that.grid.getrowdata(index);
            if(rowData && rowData[that.key]==that.innerValue){

            }else{
                that.dataAdapter.records.map((row)=>{
                    if(row[that.key] == that.innerValue){
                        rowid = row.id
                    }
                })
                let rowIndex = that.grid.getrowboundindexbyid(rowid)
                that.grid.ensurerowvisible(rowIndex);
                that.grid.selectedrowindex(rowIndex);
            }
        }
    }
    initDisplayValue(){
        let that = this;
        setTimeout((any)=>{
            if(!!that.innerValue){
                let params = {};
                params[that.key] = that.innerValue;
                that.api.post({
                    url: AppSetting.GET_ACCT_URL,
                    params: params
                }).done(function(res){
                    if(res.data){
                        that.setSelectedItem(res.data);
                    }
                })
            }else{
                that.setSelectedItem(undefined);
            }
        }, 42)
    }
    initEvents(){
        let that = this;
        this.getCateAndParentData(()=>{
            var source = Helpers.Source({
                datafields: [
                    { name: 'id', type: 'number' },
                    { name: 'parent_id', type: 'number' },
                    { name: 'acct_code', type: 'string' },
                    { name: 'acct_name', type: 'string' },
                    { name: 'category_id', type: 'string' },
                    // { name: 'DrawerName', type: 'string'},
                ],
                localData: that.acct_data
            });
            that.cateDataAdapter = new jqx.dataAdapter(source, {
                beforeLoadComplete: function (records) {
                    var filteredRecords = new Array();
                    for (var i = 0; i < records.length; i++) {
                        records[i].label = records[i].acct_code + ' ' + records[i].acct_name
                        if (records[i].parent_id == null)
                            filteredRecords.push(records[i]);
                    }
                    return filteredRecords;
                },
                loadComplete: function (records) {
                    if(that.dropDownButton){
                        $(that.dropDownButton.nativeElement).parent().on('show.bs.dropdown', function () {
                            that.initDropDownGrid()
                        });
                        $(that.dropDownButton.nativeElement).keydown(function(e){
                            if(e.keyCode == 13) {
                                return false;
                            }
                        });
                    }
                },
                autoBind: true
            });
        })
        that.initEvents = ()=>{}
    }

    filterCtrlAcct = 0
    toggleControlAcct(event: any){
        let that = this;
        this.filterCtrlAcct = +event.target.checked;
        var datafield = 'control_acct';
        if(this.filterCtrlAcct){
            that.grid.removefilter(datafield, true);
            var filtertype = 'numericfilter';
            var filtergroup = new $.jqx.filter();
            var filtervalue = this.filterCtrlAcct;
            var filtercondition = 'EQUAL';
            var filter_or_operator = 0;
            var filter = filtergroup.createfilter(filtertype, filtervalue, filtercondition);
            filtergroup.addfilter(filter_or_operator, filter);
            console.log(filter,'filter')
            that.grid.addfilter(datafield, filtergroup);
            // apply the filters.
            that.grid.applyfilters();

        }else{
            that.grid.removefilter(datafield, true);
        }
    }

    ngAfterViewInit(){
        let that = this;
        if(that.layout == 'grid') that.loadAdapter();
            // setTimeout(()=>{
            //     that.initGrid();
            // },200)
        that.initDisplayValue()
            
        
    }

    toggle(){
        if(this.window.isOpen()) this.close();
        else this.open()
    }
    open(){
        this.window.open();
        this.loadAdapter();
        
        // this.onOpen.emit()
        
    }
    eventWindowOpen(){
        this.ensure()
        this.onOpen.emit()
    }
    eventWindowClose(){
        this.onClose.emit()
    }
    close(){
        this.window.close();
        // this.onClose.emit()
    }
    clear(){
        let that = this;
        that.grid.clearselection()
        // that.label = "Please Choose";
    }
    done(){
        let that = this;
        let index = that.grid.getselectedrowindex();
        let rowData = that.grid.getrowdata(index);
        if(index>=0 && rowData){
            that.setSelectedItem(rowData);
            that.onChangeCallback(rowData[that.key])
        }else{
            that.setSelectedItem(undefined);
            that.onChangeCallback(undefined)
        }
        that.onChange.emit(that)
        that.close();
    }
    initContent(){
        setTimeout(()=>{
            let that = this;
            console.log(this,self)
        },200)
    }

    source;
    dataAdapter;
    gridSetting;
    cateDataAdapter
    acct_data
    getCateAndParentData(callback){
        let that = this;
        if(that.acct_data){
            callback()
        }else{
            
            that.api.post({
                url: AppSetting.ACCT_LIST_URL,
                params:{
                    type: [1,2]
                }
            }).done(function(res){
                that.acct_data = res.data.map((item)=>{
                    item.label = item.acct_code + ' ' + item.acct_name
                    return item;
                })
                callback()
            })
        }
    }
    loadAdapter(){
        let that = this;
        this.getCateAndParentData(()=>{
            var source = Helpers.Source({
                datafields: [
                    { name: 'id', type: 'number' },
                    { name: 'parent_id', type: 'number' },
                    { name: 'acct_code', type: 'string' },
                    { name: 'acct_name', type: 'string' },
                    { name: 'category_id', type: 'string' },
                    // { name: 'DrawerName', type: 'string'},
                ],
                localData: that.acct_data
            });
            that.cateDataAdapter = new jqx.dataAdapter(source, {
                beforeLoadComplete: function (records) {
                    var filteredRecords = new Array();
                    for (var i = 0; i < records.length; i++) {
                        records[i].label = records[i].acct_code + ' ' + records[i].acct_name
                        if (records[i].parent_id == null)
                            filteredRecords.push(records[i]);
                    }
                    return filteredRecords;
                },
                loadComplete: function (records) {
                    that.loadAcctAdapter();
                    that.initGrid()
                },
                autoBind: true
            });
        })
    }
    loadAcctAdapter(){
        let that = this;

        let conditions:any = [
            ['type',3]
        ];
        if(that.gen_posting_type){
            conditions.push([
                ['gen_posting_type',null],
                'or',
                ['gen_posting_type',that.gen_posting_type]
                ])
        }
        if(that.none_item){
            conditions.push(['none_item',+that.none_item])
        }
        if(that.none_control_acct){
            conditions.push(['control_acct',0])
        }
        that.source = Helpers.Source({
            dataType: "json",
            type: 'POST',
            // url: AppSetting.ACCT_LIST_URL,
            // data: {
            //     type: 3
            // },
            url: AppSetting.COMMON_BINDING_URL,
            data: {
                table:'acct',
                conditions: conditions,
                all:true
            },
            dataFields: [
                { name: 'id', type: 'number' },
                { name: 'parent_id', type: 'number' },
                { name: 'acct_code', type: 'string' },
                { name: 'acct_name', type: 'string' },
                { name: 'category_id', type: 'number' },
                { name: 'balance', type: 'number'},
                { name: 'control_acct', type: 'number'},
            
            ],
            
            id: 'id',
            root: 'data',
        });
        // that.gridPartner.setOptions({source:[]});
        that.dataAdapter = new jqx.dataAdapter(that.source, {
            beforeLoadComplete: function (records) {
                for (var i = 0; i < records.length; i++) {
                    var row = records[i];
                    row.balance = +(row.balance.toFixed(2)) 
                }
                return records;
            }
        });
    }
    pending = false;
    initGrid(){
        let that = this;
        let groupsrenderer = function (text, group, expanded, data) {
            if (
                data.groupcolumn.datafield == 'category_id' ||
                data.groupcolumn.datafield == 'parent_id'
                ) {
                // var number = dataAdapter.formatNumber(group, data.groupcolumn.cellsformat);
                var stext = data.groupcolumn.text + ': ' + group;
                var aggregate
                if (data.subItems.length > 0) {
                    var findFather = function(id){
                        for (var i = 0; i < that.cateDataAdapter.originaldata.length; i++) {
                            var item = that.cateDataAdapter.originaldata[i];
                            if(item.id == id) return item.acct_name;
                        };
                        return '';
                    }
                    stext = findFather(data.subItems[0].parent_id)
                    aggregate = this.getcolumnaggregateddata('balance', ['sum'], false, data.subItems);
                } else {
                    var rows = new Array();
                    var findDrawer = function(id){
                        for (var i = 0; i < that.cateDataAdapter.records.length; i++) {
                            var item = that.cateDataAdapter.records[i];
                            if(item.id == id) return item.acct_name;
                        }
                        return '';
                    }
                    var getRows = function (group, rows) {
                        if (group.subGroups && group.subGroups.length > 0) {
                            for (var i = 0; i < group.subGroups.length; i++) {
                                getRows(group.subGroups[i], rows);
                            }
                        }
                        else if(group.subItems){
                            stext = findDrawer(group.subItems[0].category_id);
                            for (var i = 0; i < group.subItems.length; i++) {
                                rows.push(group.subItems[i]);
                            }
                        }
                    }
                    getRows(data, rows);
                    aggregate = this.getcolumnaggregateddata('balance', ['sum'], false, rows);
                }
                
                return [
                    '<div class="jqx-grid-groups-row jqx-grid-groups-row-office" style="position: absolute;">',
                        '<span>' + stext + ' </span>',
                        '<span class="jqx-grid-groups-row-details jqx-grid-groups-row-details-office">',
                        "Total" + ' (' + new DecimalPipe("en-US").transform(aggregate.sum.toFixed(2)) + ')',
                        '</span>',
                    '</div>'
                ].join('');
            }
            else {
                return '<div ><span>' + text + '</span>';
            }
        }
        let columns = [
                { text: 'Category', dataField: 'category_id', width: 200 ,hidden: true},
                { text: 'FatherNum', dataField: 'parent_id', width: 200,hidden: true},
                { text: 'Control Account', dataField: 'control_acct', width: 200 ,hidden: true},
                { 
                    text: Helpers.translate.get('Account No.'), dataField: 'acct_code', width: 200 , groupable: false,
                    // filtertype: 'list'
                },
                { 
                    text: Helpers.translate.get('Account Name'), dataField: 'acct_name',  groupable: false,
                    // filtertype: 'checkedlist'
                },

                { text: Helpers.translate.get('Balance'), dataField: 'balance', width: 160, groupable: false,
                    // aggregates: ["sum"], 
                    cellsalign: 'right', 
                    cellsformat: 'd'
                }
            ]
        
        if(that.gridSetting){
            return;
        }
        Helpers.translate.do(()=>{
            if(that.grid){
                that.grid.setcolumnproperty('acct_code','text', Helpers.translate.get('Account No.'))
                that.grid.setcolumnproperty('acct_name','text', Helpers.translate.get('Account Name'))
                that.grid.setOptions({'localization': Helpers.getLocalizatio(Helpers.translate.activeLang)})
            }
        })
        that.gridSetting = {
            columns: columns,
            localization: Helpers.getLocalizatio(Helpers.translate.activeLang),
            source: that.dataAdapter,
            groupsrenderer:groupsrenderer,
            groups:['category_id','parent_id'],
            ready: function() {
                
                that.ensure()
            },
            rendergridrows: (params: any): any[] => {
                return that.dataAdapter.records;
            },
            rendered(event: any): void {
                that.grid.expandallgroups();
            },
            editSettings: { 
                saveOnPageChange: true, 
                saveOnBlur: true, 
                saveOnSelectionChange: false, 
                cancelOnEsc: true, 
                saveOnEnter: true, 
                editOnDoubleClick: false, 
                editOnF2: false 
            },
            onRowdoubleclick: (event:any)=>{
                // event arguments.
                var args = event.args;
                // row's bound index.
                var rowBoundIndex = args.rowindex;
                // row's visible index.
                var rowVisibleIndex = args.visibleindex;
                // right click.
                var rightclick = args.rightclick; 
                // original event.
                var ev = args.originalEvent;
                // column index.
                var columnindex = args.columnindex;
                // column data field.
                var dataField = args.datafield;
                // cell value
                var value = args.value;
                if(args && args.row){
                    var dataRow = args.row.bounddata
                    if(dataRow) that.showDetail(dataRow.id)
                }
            }
        }
    }
    reloadGrid(){
        this.dataAdapter.dataBind();
        this.grid.updatebounddata('cells');
    }
    editingItem
    clickAdd(){
        this.showDetail()
    }
    clickEdit(){
        let that = this;
        let index = that.grid.getselectedrowindex();
        let rowData = that.grid.getrowdata(index);
        if(rowData){
            that.showDetail(rowData.id)
        }
    }
    showDetail(id=0){
        let that = this;
        that.detailWindow.open();
        if(id){
            that.api.post({
                url: AppSetting.GET_ACCT_URL,
                params:{
                    id:id
                }
            }).done(function(res){
                if(res.data){
                    that.setData(res.data)
                }
            })
        }else{
            that.setData({
                id:0,
                acct_code:'',
                acct_name:'',
                category_id:null,
                parent_id:null,
                type:3,
                level:3
            })
        }
    }
    setData(data){
        let that = this
        that.editingItem = data;
        if(data.category_id && that.categoryCbx){
            that.categoryCbx.val(data.category_id)
        } else if(that.categoryCbx) that.categoryCbx.selectItem(null);
        if(data.parent_id && that.parentCbx){
            that.parentCbx.val(data.parent_id)
        } else if(that.parentCbx) that.parentCbx.selectItem(null);
        if(data.gen_posting_type && that.gen_posting_typeCbx){
            that.gen_posting_typeCbx.val(data.gen_posting_type)
        } else if(that.gen_posting_typeCbx) that.gen_posting_typeCbx.selectItem(null);
    }
    cateSetting;
    parentSetting;
    parentAdapter
    initDetailWindow(){
        console.log('initDetailWindow')
        let that = this;
        if(!that.cateSetting){
            that.cateSetting = {
                source: that.cateDataAdapter,
                width: '100%',
                height: 30,
                promptText: "Select Item...",
                displayMember: 'label',
                valueMember: 'id',
                onSelect: function(event){

                },
                onChange: function(event){
                    var args = event.args;
                    if(args && args.item && args.item.originalItem){
                        var item = args.item.originalItem;
                        if(that.editingItem){
                            that.editingItem.category_id = item.id;
                            that.parentAdapter = new jqx.dataAdapter(that.cateDataAdapter.originaldata, {
                                beforeLoadComplete: function (records) {
                                    var filteredRecords = new Array();
                                    for (var i = 0; i < records.length; i++) {
                                        if (records[i].parent_id == item.id)
                                            filteredRecords.push(records[i]);
                                    }
                                    return filteredRecords;
                                }
                            });
                            that.parentCbx.setOptions({
                                source: that.parentAdapter
                            })
                        }
                        let windowId = that.validator.host.attr('id')
                        $('#' + windowId+ ' .cbx_category_id').blur()
                    }
                },
                bindingComplete: (event)=>{
                    console.log('bindingComplete',event)
                    if(that.editingItem){
                        event.args.owner.val(that.editingItem.category_id)
                    }
                },
            }
        }
        if(!that.parentSetting){
            console.log('HHH')
            that.parentAdapter = new jqx.dataAdapter(that.cateDataAdapter.originaldata, {
                beforeLoadComplete: function (records) {
                    var filteredRecords = new Array();
                    for (var i = 0; i < records.length; i++) {
                        if (records[i].parent_id)
                            filteredRecords.push(records[i]);
                    }
                    return filteredRecords;
                }
            });
            that.parentSetting = {
                source: that.parentAdapter,
                width: '100%',
                height: 30,
                promptText: "Select Item...",
                displayMember: 'label',
                valueMember: 'id',
                onSelect: function(event){

                },
                onChange: function(event){
                    var args = event.args;
                    if(args && args.item && args.item.originalItem){
                        var item = args.item.originalItem;
                        if(that.editingItem){
                            that.editingItem.parent_id = item.id;
                        }
                    }
                    let windowId = that.validator.host.attr('id')
                    $('#' + windowId+ ' .cbx_parent_id').blur()
                },
                bindingComplete: (event)=>{
                    console.log('bindingComplete',event)
                    if(that.editingItem){
                        event.args.owner.val(that.editingItem.parent_id)
                    }
                },
            }
        }
        that.genPostTypeSetting = {
            source: [
                {id:'',label:''},
                {id:1,label:'Sales'},
                {id:2,label:'Purchase'},
            ],
            width: '100%',
            height: 30,
            promptText: "Select Item...",
            displayMember: 'label',
            valueMember: 'id',
            onChange: function(event){
                var args = event.args;
                if(args && args.item && args.item.originalItem){
                    var item = args.item.originalItem;
                    if(that.editingItem){
                        that.editingItem.gen_posting_type = item.id;
                    }
                }
            },
        }
        that.initValidation();
    }
    genPostTypeSetting
    closeDetail(){
        this.detailWindow.close();
    }
    isSubmit = false;
    checkValidate(): void {
        let that = this;
        setTimeout(() => {
            that.validator.validate();
        }, 200)
        this.isSubmit = true;
    }
    onValidationError(){
        this.isSubmit = false;
    }

    onValidationSuccess() : void {
        let that = this;
        if(that.isSubmit){
            let params = that.editingItem;
            console.log(params,'params');
            that.api.post({
                url: AppSetting.ACCOUNT_COMMIT_URL,
                params: {
                    id: that.editingItem.id,
                    params: params
                }
            }).done(function(res){
                if(res.code==1){
                    that.dataAdapter.dataBind();
                    that.grid.updatebounddata('cells');
                    that.detailWindow.close()
                    toastr.success(Helpers.translate.get('Create successfuly'))
                }
            })
            
        }
    }
    rules
    initValidation(){
        let that = this;
        let windowId = that.validator.host.attr('id')
        if(that.rules){
            if(this.validator) this.validator.hide();
        }else
        this.rules =
        [
            { input: '#' + windowId+ ' .txt_acct_code', message: Helpers.translate.get('Account No.') + Helpers.translate.get(' should be not empty'), action: 'keyup, blur', rule: 'required' },
            { input: '#' + windowId+ ' .txt_acct_name', message: Helpers.translate.get('Account Name') + Helpers.translate.get(' should be not empty'), action: 'keyup, blur', rule: 'required' },
            // { input: '#cbx_parent_id', message: 'Parent is required!', action: 'keyup, blur', rule: 'required' },
            { input: '#' + windowId+ ' .cbx_category_id', message: Helpers.translate.get('Account Category') + Helpers.translate.get(' should be not empty'), action: 'change, select', rule: function () {
                    var cat = that.categoryCbx.val();
                    var parent = that.parentCbx.val();
                    if(parent!="" && cat == '') return false;;
                    return true;
                } 
            },
            // { input: '#' + windowId+ ' .cbx_parent_id', message: 'Parent is required!', action: 'change, select', rule: function () {
            //         var value = that.parentCbx.val();
            //         return value!="";
            //     } 
            // }

        ];
    }

    clickDelete(){
        let that = this;
        let index = that.grid.getselectedrowindex();
        let rowData = that.grid.getrowdata(index);
        if(rowData){
            Helpers.confirm('<h3>' + Helpers.translate.get('Are you sure you want to delete this item?') + '</h3>',function(){
                
                that.api.post({
                    url: AppSetting.ACCT_DELETE_URL,
                    params: {
                        id: rowData.id
                    }
                }).done(function(res){
                    if(res.code){
                        that.dataAdapter.dataBind();
                        that.grid.updatebounddata('cells');
                    }
                })
            })
        }
    }
    acct_id
    openJournalLineWindow(){
        let that = this;
        let index = that.grid.getselectedrowindex();
        let rowData = that.grid.getrowdata(index);
        if(rowData){
            this.journalLineWindow.open();
            that.acct_id = rowData.id;
            
        }
    }
    initJournalWindow(){
        let that = this;
        let windowId = that.journalLineWindow.host.attr('id')
        // var start = moment().subtract(29, 'days');
        var start = moment().startOf('month');
        // var end = moment();
        var end = moment().endOf('month');
        function cb(start, end) {
            $('#'+ windowId+ ' .filterrange span').html(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
        }
        $('#'+ windowId+ ' .filterrange').daterangepicker({
            startDate: start,
            endDate: end,
            "alwaysShowCalendars": true,
            locale: {
              format: 'YYYY-MM-DD'
            },
            ranges: {
               'Today': [moment(), moment()],
               'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
               'Last 7 Days': [moment().subtract(6, 'days'), moment()],
               'Last 30 Days': [moment().subtract(29, 'days'), moment()],
               'This Month': [moment().startOf('month'), moment().endOf('month')],
               'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cb).on('apply.daterangepicker', function(ev, picker) {
          console.log(picker.startDate.format('YYYY-MM-DD'));
          console.log(picker.endDate.format('YYYY-MM-DD'));
          if(that.journalDetailGrid) that.addfilter()
        });
        cb(start, end);
        // that.loadJournalLineDataAdapter();
        // that.initJournalLineGrid();
        that.addfilter()
    }
    addfilter(){
        let that = this;
        let windowId = that.journalLineWindow.host.attr('id')
        console.log('ADD FILTER')
        var dr = $('#'+ windowId+ ' .filterrange').data('daterangepicker');
        var startDate = dr.startDate.format('YYYY-MM-DD 00:00:00');
        var endDate = dr.endDate.format('YYYY-MM-DD 23:59:59');
        that.loadJournalLineDataAdapter(startDate,endDate);
        that.initJournalLineGrid();

        // return;
        // that.journalDetailGrid.clearfilters();
        // var filtergroup = new jqx.filter();
        // var filter_or_operator = 0;

        // var filtervalue = startDate;
        // var filtercondition = 'GREATER_THAN_OR_EQUAL';
        // var filter = filtergroup.createfilter('datefilter', filtervalue, filtercondition);
        // filtergroup.addfilter(filter_or_operator, filter);

        // var filtervalue = endDate;
        // var filtercondition = 'LESS_THAN_OR_EQUAL';
        // var filter = filtergroup.createfilter('datefilter', filtervalue, filtercondition);
        // filtergroup.addfilter(filter_or_operator, filter);
        // // that.journalDetailGrid.removefilter('created_at',false);
        // that.journalDetailGrid.addfilter('created_at', filtergroup);

        // var filtergroup = new jqx.filter();
        // var filtervalue = that.acct_id;
        // var filtercondition = 'EQUAL';
        // var filter = filtergroup.createfilter('stringfilter', filtervalue, filtercondition);
        // filtergroup.addfilter(filter_or_operator, filter);
        // // that.journalDetailGrid.removefilter('acct_id',false);
        // that.journalDetailGrid.addfilter('acct_id', filtergroup);

        // // // apply the filters.
        // that.journalDetailGrid.applyfilters();
            
    }
    journalLineDataAdapter
    loadJournalLineDataAdapter(start,end){
        let that = this;
        var source = Helpers.Source({
            datatype: "json",
            type: 'POST',
            url: AppSetting.JOURNAL_LINE_BINDING_URL,
            data: {
                conditions: [
                ['acct_id',that.acct_id],
                ['created_at','>=',start],
                ['created_at','<=',end]
                ],
            },
            dataFields: [
                { name: 'id', type: 'number' },
                { name: 'acct_id', type: 'number' },
                { name: 'document_no', type: 'number' },
                { name: 'header_id', type: 'number' },
                { name: 'trans_id', type: 'number' },
                { name: 'acct_code', type: 'number' },
                { name: 'acct_name', type: 'string' },
                { name: 'line_number', type: 'number' },
                { name: 'line_memo', type: 'string' },
                { name: 'credit', type: 'number'},
                { name: 'debit', type: 'number'},
                { name: 'curr_balance', type: 'number'},
                { name: 'posting_at', type: 'date'},
                { name: 'created_at', type: 'date'},
            ],
            filter: function() {
                // if(that.journalDetailGrid) 
                    that.journalDetailGrid.updatebounddata('filter');
            },
            sort: function() {
                // if(that.journalDetailGrid) 
                    that.journalDetailGrid.updatebounddata('sort');
            },
            id: 'id',
            root: 'data',
        });
        that.journalLineDataAdapter = new jqx.dataAdapter(source)
    }
    journalGridSetting
    initJournalLineGrid(){
        let that = this;
        let columns = [
            { text: 'acct_id', dataField: 'acct_id', width: 80, hidden: true},
            { 
                text: Helpers.translate.get('Trans ID'), dataField: 'trans_id', width: 80,
                filtercondition :'CONTAIN'
            },
            { 
                text:Helpers.translate.get('Document No.'), dataField: 'document_no', width: 100,
                filtercondition :'CONTAIN'
            },
            { 
                text: 'Date', dataField: 'created_at', width: 100,
                cellsformat: 'yyyy-MM-dd HH:mm:ss',hidden: true,
                filtertype:'range'
            },
            { 
                text: Helpers.translate.get('Date'), dataField: 'posting_at', width: 100,
                cellsformat: 'yyyy-MM-dd',
                filtertype:'date'
            },
            { 
                text: Helpers.translate.get('Line Memo'), dataField: 'line_memo',
                filtercondition :'CONTAIN'
            },
            

            { 
                text: Helpers.translate.get('Credit Amount'), dataField: 'credit', width: 100, groupable: false,
                // aggregates: ["sum"], 
                cellsalign: 'right', 
                cellsformat: 'd',
                filtercondition :'CONTAIN'
            },
            { 
                text: Helpers.translate.get('Debit Amount'), dataField: 'debit', width: 100, groupable: false,
                // aggregates: ["sum"], 
                cellsalign: 'right', 
                cellsformat: 'd',
                filtercondition :'CONTAIN'
            },
            { 
                text: Helpers.translate.get('Cumulative Balance'), dataField: 'curr_balance', width: 100, groupable: false,
                // aggregates: ["sum"], 
                cellsalign: 'right', 
                cellsformat: 'd',
                filtercondition :'CONTAIN'
            }
        ]
        if(that.journalGridSetting){
            that.journalDetailGrid.setOptions({
                source: that.journalLineDataAdapter
            })
            // that.addfilter()
            return;
        }else{
            that.journalGridSetting = {
                columns: columns,
                source: that.journalLineDataAdapter,
                ready: function() {
                    //that.addfilter()
                    console.log('ready')
                },
                rendergridrows: (params: any): any[] => {
                    return that.journalLineDataAdapter.records;
                },
                rendered(event: any): void {
                    console.log('rendered')
                },
            }
        }
    }
    showunused
    expandAll = true
    toggleUnused(event: any){
        let that = this;
        console.log(event.target.checked)
        this.showunused = event.target.checked;
        // $("#treegrid").jqxGrid('clearfilters');
        var datafield = 'balance';
        if(this.showunused){
            that.grid.removefilter(datafield, true);
            var filtertype = 'numericfilter';
            var filtergroup = new $.jqx.filter();
            var filtervalue = 0;
            var filtercondition = 'NOT_EQUAL';
            var filter_or_operator = 0;
            var filter = filtergroup.createfilter(filtertype, filtervalue, filtercondition);
                filtergroup.addfilter(filter_or_operator, filter);
            console.log(filter,'filter')
            that.grid.addfilter(datafield, filtergroup);
            // apply the filters.
            that.grid.applyfilters();

        }else{
            that.grid.removefilter(datafield, true);
        }
    }

    toggleExpandAll(event: any){
        console.log(event.target.checked)
        this.expandAll = event.target.checked;
        if(this.expandAll)
            this.grid.expandallgroups();
        else
            this.grid.collapseallgroups();
    }
    openFormulas(){
        this.formulas.open()
    }
}


/*
Copyright 2017-2018 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/