import { Component, OnInit, Input,forwardRef,AfterViewInit,OnChanges, ViewChild, ViewEncapsulation, EventEmitter, Output } from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from "@angular/forms";
import * as cloneDeep from 'lodash/cloneDeep';
import { DatePipe, CurrencyPipe,DecimalPipe } from '@angular/common';
import { APIService } from '../../api.service';
import { Helpers} from "../../helpers";
import { AppSetting } from "../../settings";
import { AppMessage } from "../../message";

import { jqxWindowComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxwindow';
import { jqxGridComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxgrid';
import { jqxValidatorComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxvalidator';
import { jqxTextAreaComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxtextarea';

declare var $:any;
declare var toastr:any;

const noop = () => {
};

@Component({
    selector: 'module-formulas',
    templateUrl: './formulas.module.html',
    styleUrls: ['./formulas.module.css'],
    providers: [{
        provide: NG_VALUE_ACCESSOR,
        useExisting: forwardRef(() => FormulasModule),
        multi: true
    }]
})

export class FormulasModule implements OnInit,AfterViewInit, OnChanges, ControlValueAccessor {
    @Input() hidden: boolean;
    @Input() layout: string;
    @Input() key: string;
    @Output() onOpen = new EventEmitter();
    @Output() onClose = new EventEmitter();
    @Output() onChange = new EventEmitter();
    constructor(
        private api: APIService,
        ) {
        if(!!!this.key) this.key = 'code'
    }
    theme = AppSetting.THEME
    // The internal data model
    private innerValue: any = undefined;
    label = "Please Choose";
    // Placeholders for the callbacks which are later provided
    // by the Control Value Accessor
    private onTouchedCallback: () => void = noop;
    private onChangeCallback: (_: any) => void = noop;

    @ViewChild('window') window: jqxWindowComponent;
    @ViewChild('detailWindow') detailWindow: jqxWindowComponent;
    @ViewChild('grid') grid: jqxGridComponent;
    @ViewChild('validator') validator: jqxValidatorComponent;
    @ViewChild('dropDownGrid') dropDownGrid: jqxGridComponent;
    @ViewChild('formulasTxt') formulasTxt: jqxTextAreaComponent;
    

    // get accessor
    get value(): any {
        return this.innerValue;
    };

    // set accessor including call the onchange callback
    set value(v: any) {
        if (v !== this.innerValue) {
            this.innerValue = v;
            this.onChangeCallback(v);
        }
        console.log('SetValue')
    }

    // Set touched on blur
    onBlur() {
        this.onTouchedCallback();
    }

    // From ControlValueAccessor interface
    writeValue(value: any) {
        if (value !== this.innerValue) {
            this.innerValue = value;
        }
    }

    // From ControlValueAccessor interface
    registerOnChange(fn: any) {
        this.onChangeCallback = fn;
    }

    // From ControlValueAccessor interface
    registerOnTouched(fn: any) {
        this.onTouchedCallback = fn;
    }



    ngOnInit() {
    }
    ngOnChanges(){

    }

    dropDownDataAdapter
    dropDownGridSetting
    initDropDownGrid(){
        let that = this;
        
        let columns = [
            { text: '#', dataField: 'id', width: 120 ,hidden: true},
            { text: 'Title', dataField: 'title', width: 120 },
            { text: 'Formulas', dataField: 'formulas' },
        ]
        
        if(that.dropDownGridSetting){
            that.ensureDropDownGrid()
            return;
        }
        let source = Helpers.Source({
            dataType: "json",
            type: 'POST',
            url: AppSetting.COMMON_BINDING_URL,
            data: {
                table:'acct_formulas'
            },
            dataFields: [
                { name: 'id', type: 'number' },
                { name: 'title', type: 'string' },
                { name: 'formulas', type: 'string' },
            
            ],
            filter: function() {
                // update the grid and send a request to the server.
                that.dropDownGrid.updatebounddata('filter');
            },
            sort: function() {
                // update the grid and send a request to the server.
                that.dropDownGrid.updatebounddata('sort');
            },
            id: 'id',
            root: 'data',
        });
        // that.gridPartner.setOptions({source:[]});
        that.dropDownDataAdapter = new jqx.dataAdapter(source, {
            beforeLoadComplete: function (records) {
                for (var i = 0; i < records.length; i++) {
                    var row = records[i];
                }
                return records;
            }
        });
        that.dropDownGridSetting = {
            columns: columns,
            source: that.dropDownDataAdapter,
            ready: function() {
                that.ensureDropDownGrid()
            },
            rendergridrows: (params: any): any[] => {
                return that.dropDownDataAdapter.records;
            },
            rendered(event: any): void {
            },
            onRowSelect: (event:any)=>{
                var args = event.args;
                let rowData = that.dropDownGrid.getrowdata(args.rowindex);
                if(rowData){
                    if(rowData){
                        that.setSelectedItem(rowData);
                        that.onChangeCallback(rowData[that.key])
                    }else{
                        that.setSelectedItem(undefined);
                        that.onChangeCallback(undefined)
                    }
                    that.onChange.emit(that)
                    if(that.dropDownButton) $(that.dropDownButton).trigger('click')
                }
            },
            onRowdoubleclick: (event:any)=>{
                var args = event.args;
                var rowBoundIndex = args.rowindex;
                var rowVisibleIndex = args.visibleindex;
                var rightclick = args.rightclick; 
                var columnindex = args.columnindex;
                var dataField = args.datafield;
                var value = args.value;
                if(args && args.row){
                    var dataRow = args.row.bounddata
                }
            },
        }
    }
    ensureDropDownGrid(){
        let that = this;
        if(that.dropDownGrid){
            let rowid;
            let index = that.dropDownGrid.getselectedrowindex();
            let rowData = that.dropDownGrid.getrowdata(index);
            if(rowData && rowData[that.key]==that.innerValue){

            }else{
                that.dropDownDataAdapter.records.map((row)=>{
                    if(row[that.key] == that.innerValue){
                        rowid = row.id
                    }
                })
                let rowIndex = that.dropDownGrid.getrowboundindexbyid(rowid)
                that.dropDownGrid.ensurerowvisible(rowIndex);
                that.dropDownGrid.selectedrowindex(rowIndex);
            }
        }
    }
    dropDownButton
    initDropdown($event){
        this.dropDownButton = $event.target
        this.initDropDownGrid()
    }
    onDropdownSelectect(item){
        let that = this;
        that.setSelectedItem(item);
        that.onChangeCallback(item[that.key])
        that.onChange.emit()
    }
    switchToAdvance(){
        if(this.dropDownButton) $(this.dropDownButton).trigger('click');
        this.open()
    }

    
    selectedItem;
    setSelectedItem(item){
        let that = this;
        that.selectedItem = item;
        if(item  && item.id){
            that.label = item.code + ' ' + item.description;
            that.innerValue = item[that.key];
        }else{
            that.label = "Please Choose";
            that.innerValue = undefined
        }
    }
    getSelectedItem(){
        return this.selectedItem;
    }
    setValue(v){
        this.innerValue = v
        this.initDisplayValue();
        
    }
    ensure(){
        let that = this;
        console.log('ensure',that.innerValue)
        if(that.grid){
            let rowid;
            let index = that.grid.getselectedrowindex();
            let rowData = that.grid.getrowdata(index);
            if(rowData && rowData[that.key]==that.innerValue){

            }else{
                that.dataAdapter.records.map((row)=>{
                    if(row[that.key] == that.innerValue){
                        rowid = row.id
                    }
                })
                let rowIndex = that.grid.getrowboundindexbyid(rowid)
                that.grid.ensurerowvisible(rowIndex);
                that.grid.selectedrowindex(rowIndex);
            }
        }
    }
    initDisplayValue(){
        let that = this;
        setTimeout((any)=>{
            if(!!that.innerValue){
                let params = {};
                params[that.key] = that.innerValue;
                that.api.post({
                    url: AppSetting.GET_FORMULAS_URL,
                    params:params
                }).done(function(res){
                    if(res.data){
                        that.setSelectedItem(res.data);
                        // that.onChange.emit()
                    }
                })
            }else{
                that.setSelectedItem(undefined);
            }
        }, 42)
    }
    ngAfterViewInit(){
        let that = this;
        // that.loadAdapter();
        if(that.layout == 'grid') 
            setTimeout(()=>{
                that.loadAcctAdapter();
            },200)
        that.initDisplayValue()
    }

    toggle(){
        if(this.window.isOpen()) this.close();
        else this.open()
    }
    open(){
        this.window.open();
        this.loadAcctAdapter()
        // this.onOpen.emit()
        
    }
    eventWindowOpen(){
        this.ensure()
        this.onOpen.emit()
    }
    eventWindowClose(){
        this.onClose.emit()
    }
    close(){
        this.window.close();
        // this.onClose.emit()
    }
    clear(){
        let that = this;
        that.grid.clearselection()
        // that.label = "Please Choose";
    }
    done(){
        let that = this;
        let index = that.grid.getselectedrowindex();
        let rowData = that.grid.getrowdata(index);
        if(index>=0 && rowData){
            that.setSelectedItem(rowData);
            that.onChangeCallback(rowData[that.key])
        }else{
            that.setSelectedItem(undefined);
            that.onChangeCallback(undefined)
        }
        that.onChange.emit(that)
        that.close();
    }
    initContent(){
        setTimeout(()=>{
            let that = this;
            console.log(this,self)
        },200)
    }

    source;
    dataAdapter;
    gridSetting;
    loadAdapter(){
        let that = this;
        that.source = Helpers.Source({
            dataType: "json",
            type: 'POST',
            url: AppSetting.COMMON_BINDING_URL,
            data: {
                table:'acct_formulas'
            },
            dataFields: [
                { name: 'id', type: 'number' },
                { name: 'title', type: 'string' },
                { name: 'formulas', type: 'string' },
                { name: 'balance', type: 'string' },
            
            ],
            filter: function() {
                // update the grid and send a request to the server.
                that.grid.updatebounddata('filter');
            },
            sort: function() {
                // update the grid and send a request to the server.
                that.grid.updatebounddata('sort');
            },
            id: 'id',
            root: 'data',
        });
        // that.gridPartner.setOptions({source:[]});
        that.dataAdapter = new jqx.dataAdapter(that.source, {
            beforeLoadComplete: function (records) {
                console.log(records,'records')
                for (let rowIndex = 0; rowIndex < records.length; rowIndex++) {
                    let row = records[rowIndex];
                    if(row){
                        let evalFormulas = row.formulas.toString();
                        let sum_formulas = row.formulas.match(new RegExp(/[0-9]+[.]{2}[0-9a-zA-Z]+/g));
                        let acct_codes
                        if(sum_formulas) for (let i = sum_formulas.length - 1; i >= 0; i--) {
                            let sum_formula = sum_formulas[i];
                            let from_to_acct_code = sum_formula.match(new RegExp(/[0-9]+/g));
                            let from_acct_code = Math.min.apply(null,from_to_acct_code);
                            let to_acct_code = Math.max.apply(null,from_to_acct_code);
                            acct_codes = [];
                            for (let i = that.acct_source.length - 1; i >= 0; i--) {
                                let acct = that.acct_source[i];
                                if(+acct.acct_code >= +from_acct_code && +acct.acct_code <= +to_acct_code){
                                    acct_codes.push('@'+acct.acct_code)
                                }
                            }
                            if(acct_codes.length>0){
                                evalFormulas = evalFormulas.replace(new RegExp(sum_formula,'g'),acct_codes.join('+'))
                            }else{
                                evalFormulas = evalFormulas.replace(new RegExp(sum_formula,'g'),'0')
                            }
                        }
                        console.log(evalFormulas,'evalFormulas')
                        acct_codes = evalFormulas.match(new RegExp(/[(?!@)0-9]+/g)).sort(function(a,b){
                            return b.length - a.length
                        })

                        if(acct_codes) acct_codes.map((acct_code)=>{
                            for (let i = that.acct_source.length - 1; i >= 0; i--) {
                                let acct = that.acct_source[i];
                                if(
                                    acct.acct_code == acct_code ||
                                    '@'+acct.acct_code == acct_code
                                    ){
                                    evalFormulas = evalFormulas.replace(new RegExp(acct_code,'g'),'('+acct.balance+')')
                                    break;
                                }
                            }
                        })
                        console.log(evalFormulas,'evalFormulas')
                        try{
                            row.balance = new DecimalPipe("en-US").transform(eval(evalFormulas));
                        }catch(e){
                            row.balance = 'NaN';
                        }
                    }
                }
                return records;
            }
        });
        
    }
    loadAcctAdapter(){
        let that = this;
        if(that.acct_source){
            that.loadAdapter();
            that.initGrid();
            return
        }
        that.api.post({
            url: AppSetting.ACCT_LIST_URL,
            params: {
                type: 3
            },
        }).done(function(res){
            that.acct_source = res.data;
            that.loadAdapter();
            that.initGrid();
        })
    }
    pending = false;
    beforeEditData;
    currentEditData;
    initGrid(){
        let that = this;
        
        let columns = [
                { text: '#', dataField: 'id', width: 120 ,hidden: true},
                { 
                    text: 'Title', dataField: 'title', width: 200,
                    validation: (cell: any, value: any): any => {
                        let row = cell.row;
                        let rowData = that.grid.getrowdata(row);
                        if (value == '')
                            return { result: false, message: 'Title should be not empty' };
                        return true;
                    }
                },
                { 
                    text: 'Formulas', dataField: 'formulas',
                    validation: (cell: any, value: any): any => {
                        if (value == '')
                            return { result: false, message: 'Formulas should be not empty' };
                        try{
                            let $ev = value.replace(new RegExp(/[0-9]+|[.]{2}/g),'1');
                            eval($ev)
                        }catch(e){
                            return { result: false, message: 'Formulas is not valid' };
                        }
                        return true;
                    }
                },
                { 
                    text: 'Balance', dataField: 'balance',width:120,editable:false
                },
            ]
        
        if(that.gridSetting){
            return;
        }
        that.gridSetting = {
            columns: columns,
            source: that.dataAdapter,
            ready: function() {
                
                that.ensure()
            },
            rendergridrows: (params: any): any[] => {
                return that.dataAdapter.records;
            },
            rendered(event: any): void {
                that.grid.expandallgroups();
            },
            onRowdoubleclick: (event:any)=>{
                // event arguments.
                var args = event.args;
                // row's bound index.
                var rowBoundIndex = args.rowindex;
                // row's visible index.
                var rowVisibleIndex = args.visibleindex;
                // right click.
                var rightclick = args.rightclick; 
                // original event.
                var ev = args.originalEvent;
                // column index.
                var columnindex = args.columnindex;
                // column data field.
                var dataField = args.datafield;
                // cell value
                var value = args.value;
                if(args && args.row){
                    var dataRow = args.row.bounddata
                    if(dataRow) that.showDetail(dataRow.id)
                }
            },
            onCellBeginEdit(event: any): void {
                let args = event.args;
                that.beforeEditData = cloneDeep(args.row);
                that.currentEditData = cloneDeep(args.row);
            },
            onCellEndEdit(event: any): void {
                let args = event.args;
                let row = args.row;
                that.currentEditData[args.datafield] = args.value;
                if (args.datafield === 'formulas') {
                    if(
                        JSON.stringify(that.currentEditData) !=
                        JSON.stringify(that.beforeEditData)
                        ){
                        let params = cloneDeep(that.currentEditData);
                        let conditions
                        if(params.id<0){
                            delete params.id;
                        }
                        that.api.post({
                            url: AppSetting.FORMULAS_COMMIT_URL,
                            params: {
                                id: params.id,
                                params: params
                            },
                            onDone:function(res){
                                if(!params.id){
                                    that.currentEditData.id = res.data.id
                                    toastr.success('Create Formulas successfuly')
                                }else{
                                    toastr.success('Update Formulas successfuly')
                                }
                                let index = that.grid.getselectedrowindex();
                                let id = that.grid.getrowid(index);
                                // that.grid.updaterow(id,that.currentEditData);
                            },
                            onFail:function(){
                                toastr.warning('Fail to create/update Formulas')
                            },
                        })
                    }
                }
            },
        }
    }
    reloadGrid(){
        this.dataAdapter.dataBind();
        this.grid.updatebounddata('cells');
    }
    editingItem
    clickAdd(){
        this.showDetail()
    }
    clickEdit(){
        let that = this;
        let index = that.grid.getselectedrowindex();
        let rowData = that.grid.getrowdata(index);
        if(rowData){
            that.showDetail(rowData.id)
        }
    }
    clickDelete(){
        let that = this;
        let index = that.grid.getselectedrowindex();
        let rowData = that.grid.getrowdata(index);
        if(rowData){
            Helpers.confirm('<h3>Are you sure you want to delete Formulas?</h3>',function(){
                
                that.api.post({
                    url: AppSetting.DEL_FORMULAS_URL,
                    params: {
                        id: rowData.id
                    }
                }).done(function(res){
                    if(res.code){
                        that.dataAdapter.dataBind();
                        that.grid.updatebounddata('cells');
                    }
                })
            })
        }
    }
    showDetail(id=0){
        let that = this;
        that.detailWindow.open();
        if(id){
            that.api.post({
                url: AppSetting.GET_FORMULAS_URL,
                params: {
                    id: id
                }
            }).done(function(res){
                if(res.data){
                    that.setData(res.data)
                }
            })
        }else{
            that.setData({
                id:0,
                title:'',
                formulas:'',
            })
        }
    }
    setData(data){
        let that = this
        that.editingItem = data
    }
    formulasSetting
    acct_source
    initFormulas(){
        let that = this;
        if(!that.formulasSetting){
            let source = that.acct_source.map(
                (row:any)=>{
                    return row.acct_code+ ' ' + row.acct_name
                    // return '@'+row.acct_code
                }
            )
            that.formulasSetting = {
                source:(query:any, response:any) => {
                    // var item = query.split(/ \s*/).pop();
                    var item = query.match(new RegExp(/[0-9]+|[-+\*\/() ]|[.]{2}/g)).pop()
                    // update the search query.
                    if(item.match(new RegExp(/[0-9]+/g))){
                        that.formulasTxt.setOptions({
                            query: item
                        })
                        response(source);
                    }
                },
                renderer: (itemValue:any, inputValue:any) => {
                    console.log(itemValue,'itemValue')
                    console.log(inputValue,'inputValue')
                    let row = itemValue.match(new RegExp(/[0-9]+/g))
                    var terms = inputValue.match(new RegExp(/[0-9]+|[-+\*\/() ]|[.]{2}/g));
                    if(row[0]){

                        // remove the current input
                        terms.pop();
                        // add the selected item
                        terms.push(row[0]);
                        // add placeholder to get the comma-and-space at the end
                        // terms.push("");
                    }
                    var value = terms.join("");
                    return value;
                }
            }
        }
    }
    cateSetting;
    parentSetting;
    parentAdapter
    initDetailWindow(){
        console.log('initDetailWindow')
        let that = this;
        that.initFormulas()
        that.initValidation();
    }
    closeDetail(){
        this.detailWindow.close();
    }
    isSubmit = false;
    checkValidate(): void {
        let that = this;
        setTimeout(() => {
            that.validator.validate();
        }, 200)
        this.isSubmit = true;
    }
    onValidationError(){
        this.isSubmit = false;
    }

    onValidationSuccess() : void {
        let that = this;
        if(that.isSubmit){
            let params = that.editingItem;
            console.log(params,'params');
            that.api.post({
                url: AppSetting.FORMULAS_COMMIT_URL,
                params: {
                    id:that.editingItem.id,
                    params: params
                }
            }).done(function(res){
                if(res.code==1){
                    that.dataAdapter.dataBind();
                    that.grid.updatebounddata('cells');
                    that.detailWindow.close()
                    toastr.success('Create successfuly')
                }
            })
            
        }
    }
    rules
    initValidation(){
        let that = this;

        let windowId = that.validator.host.attr('id')
        if(that.rules){
            if(that.validator) that.validator.hide();
        }else
        that.rules =
        [
            { input: '#' + windowId+ ' .txt_title', message: 'Title is required!', action: 'keyup, blur', rule: 'required' },
            { 
                input: '#' + windowId+ ' .txt_formulas', message: 'Formulas is not valid!', action: 'blur',
                rule: function () {
                    var value = that.formulasTxt.val();
                    try{
                        let $ev = value.replace(new RegExp(/[0-9]+|[..]/g),'1');
                        eval($ev)
                    }catch(e){
                        return false
                    }
                    return true;
                } 
            },

        ];
    }
    countries = new Array("Afghanistan", "Albania", "Algeria", "Andorra", "Angola", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegovina", "Botswana", "Brazil", "Brunei", "Bulgaria", "Burkina Faso", "Burma", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Central African Republic", "Chad", "Chile", "China", "Colombia", "Comoros", "Congo, Democratic Republic", "Congo, Republic of the", "Costa Rica", "Cote d'Ivoire", "Croatia", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Fiji", "Finland", "France", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Greece", "Greenland", "Grenada", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, North", "Korea, South", "Kuwait", "Kyrgyzstan", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macedonia", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Mauritania", "Mauritius", "Mexico", "Micronesia", "Moldova", "Mongolia", "Morocco", "Monaco", "Mozambique", "Namibia", "Nauru", "Nepal", "Netherlands", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Norway", "Oman", "Pakistan", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Poland", "Portugal", "Qatar", "Romania", "Russia", "Rwanda", "Samoa", "San Marino", " Sao Tome", "Saudi Arabia", "Senegal", "Serbia and Montenegro", "Seychelles", "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "Spain", "Sri Lanka", "Sudan", "Suriname", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Togo", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Yemen", "Zambia", "Zimbabwe");

}


/*
Copyright 2017-2018 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/