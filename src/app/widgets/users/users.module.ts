import { Component, OnInit, Input,forwardRef,AfterViewInit,OnChanges, ViewChild, ViewEncapsulation, EventEmitter, Output } from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from "@angular/forms";
import * as cloneDeep from 'lodash/cloneDeep';
import { DatePipe, CurrencyPipe } from '@angular/common';
import { APIService } from '../../api.service';
import { Helpers} from "../../helpers";
import { AppSetting } from "../../settings";
import { AppMessage } from "../../message";

import { jqxWindowComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxwindow';
import { jqxGridComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxgrid';
import { jqxValidatorComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxvalidator';
import { jqxDropDownListComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxdropdownlist';
import { jqxFileUploadComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxfileupload';
import { jqxComboBoxComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxcombobox';

import { CompanyModule } from '../../widgets/company/company.module';

declare var $:any;
declare var toastr:any;

const noop = () => {
};

@Component({
    selector: 'module-users',
    templateUrl: './users.module.html',
    styleUrls: ['./users.module.css'],
    providers: [{
        provide: NG_VALUE_ACCESSOR,
        useExisting: forwardRef(() => UsersModule),
        multi: true
    }]
})

export class UsersModule implements OnInit,AfterViewInit, OnChanges, ControlValueAccessor {
    @Input() hidden: boolean;
    @Input() layout: string;
    @Input() type: string;
    @Input() key: string;
    @Output() onOpen = new EventEmitter();
    @Output() onClose = new EventEmitter();
    @Output() onChange = new EventEmitter();
    constructor(
        private api: APIService,
        ) {
        if(!!!this.key) this.key = 'id'
    }
    theme = AppSetting.THEME
    // The internal data model
    private innerValue: any = undefined;
    label = "Please Choose";
    // Placeholders for the callbacks which are later provided
    // by the Control Value Accessor
    private onTouchedCallback: () => void = noop;
    private onChangeCallback: (_: any) => void = noop;

    @ViewChild('window') window: jqxWindowComponent;
    @ViewChild('detailWindow') detailWindow: jqxWindowComponent;
    @ViewChild('grid') grid: jqxGridComponent;
    @ViewChild('validator') validator: jqxValidatorComponent;
    @ViewChild('fileUpload') fileUpload: jqxFileUploadComponent;

    @ViewChild('companyCbx') companyCbx: CompanyModule;

    @ViewChild('dropDownGrid') dropDownGrid: jqxGridComponent;
    

    // get accessor
    get value(): any {
        return this.innerValue;
    };

    // set accessor including call the onchange callback
    set value(v: any) {
        if (v !== this.innerValue) {
            this.innerValue = v;
            this.onChangeCallback(v);
        }
        console.log('SetValue')
    }

    // Set touched on blur
    onBlur() {
        this.onTouchedCallback();
    }

    // From ControlValueAccessor interface
    writeValue(value: any) {
        if (value !== this.innerValue) {
            this.innerValue = value;
        }
    }

    // From ControlValueAccessor interface
    registerOnChange(fn: any) {
        this.onChangeCallback = fn;
    }

    // From ControlValueAccessor interface
    registerOnTouched(fn: any) {
        this.onTouchedCallback = fn;
    }



    ngOnInit() {
    }
    ngOnChanges(){

    }

    dropDownDataAdapter
    dropDownGridSetting
    initDropDownGrid(){
        let that = this;
        let user = this.api.getUserInfo();
        let columns = [
            { text: '#', dataField: 'id', width: 120 ,hidden: true},
            { text: Helpers.translate.get('Username'), dataField: 'username', width: 120 },
            { text: Helpers.translate.get('Email'), dataField: 'email' },
        ]
        
        if(that.dropDownGridSetting){
            that.ensureDropDownGrid()
            return;
        }
        Helpers.translate.do(()=>{
            if(that.dropDownGrid){
                that.dropDownGrid.setcolumnproperty('id','text', '#')
                that.dropDownGrid.setcolumnproperty('username','text', 'Username')
                that.dropDownGrid.setcolumnproperty('email','text', 'Email')
                that.dropDownGrid.setcolumnproperty('is_active','text', 'Activated')
                that.dropDownGrid.setOptions({'localization': Helpers.getLocalizatio(Helpers.translate.activeLang)})
            }
        })
        let source = Helpers.Source({
            dataType: "json",
            type: 'POST',
            url: AppSetting.COMMON_BINDING_URL,
            data: {
                table: 'users',
                conditions: ['id','<>', user.id]
            },
            dataFields: [
                { name: 'id', type: 'number' },
                { name: 'username', type: 'string' },
                { name: 'email', type: 'string' },
                { name: 'company_info_id', type: 'string' },
                { name: 'is_active', type: 'bool' },
            
            ],
            filter: function() {
                // update the grid and send a request to the server.
                that.dropDownGrid.updatebounddata('filter');
            },
            sort: function() {
                // update the grid and send a request to the server.
                that.dropDownGrid.updatebounddata('sort');
            },
            id: 'id',
            root: 'data',
        });
        // that.gridPartner.setOptions({source:[]});
        that.dropDownDataAdapter = new jqx.dataAdapter(source, {
            beforeLoadComplete: function (records) {
                for (var i = 0; i < records.length; i++) {
                    var row = records[i];
                }
                return records;
            }
        });
        that.dropDownGridSetting = {
            columns: columns,
            localization: Helpers.getLocalizatio(Helpers.translate.activeLang),
            source: that.dropDownDataAdapter,
            ready: function() {
                that.ensureDropDownGrid()
            },
            rendergridrows: (params: any): any[] => {
                return that.dropDownDataAdapter.records;
            },
            rendered(event: any): void {
            },
            onRowSelect: (event:any)=>{
                var args = event.args;
                let rowData = that.dropDownGrid.getrowdata(args.rowindex);
                if(rowData){
                    if(rowData){
                        that.setSelectedItem(rowData);
                        that.onChangeCallback(rowData[that.key])
                    }else{
                        that.setSelectedItem(undefined);
                        that.onChangeCallback(undefined)
                    }
                    that.onChange.emit()
                    if(that.dropDownButton) $(that.dropDownButton).trigger('click')
                }
            },
            onRowdoubleclick: (event:any)=>{
                var args = event.args;
                var rowBoundIndex = args.rowindex;
                var rowVisibleIndex = args.visibleindex;
                var rightclick = args.rightclick; 
                var columnindex = args.columnindex;
                var dataField = args.datafield;
                var value = args.value;
                if(args && args.row){
                    var dataRow = args.row.bounddata
                }
            },
        }
    }
    ensureDropDownGrid(){
        let that = this;
        if(that.dropDownGrid){
            let rowid;
            let index = that.dropDownGrid.getselectedrowindex();
            let rowData = that.dropDownGrid.getrowdata(index);
            if(rowData && rowData[that.key]==that.innerValue){

            }else{
                that.dropDownDataAdapter.records.map((row)=>{
                    if(row[that.key] == that.innerValue){
                        rowid = row.id
                    }
                })
                if(rowid>=0){
                    let rowIndex = that.dropDownGrid.getrowboundindexbyid(rowid)
                    that.dropDownGrid.ensurerowvisible(rowIndex);
                    that.dropDownGrid.selectedrowindex(rowIndex);
                }
            }
        }
    }
    dropDownButton
    initDropdown($event){
        this.dropDownButton = $event.target
        this.initDropDownGrid()
    }
    onDropdownSelectect(item){
        let that = this;
        that.setSelectedItem(item);
        that.onChangeCallback(item[that.key])
        that.onChange.emit()
    }
    switchToAdvance(){
        if(this.dropDownButton) $(this.dropDownButton).trigger('click');
        this.open()
    }


    selectedItem;
    setSelectedItem(item){
        let that = this;
        that.selectedItem = item;
        if(item  && item.id){
            that.label = item.username;
            that.innerValue = item[that.key];
            
        }else{
            that.label = "Please Choose";
            that.innerValue = undefined
        }
    }
    getSelectedItem(){
        return this.selectedItem;
    }
    setValue(v){
        this.innerValue = v
        this.initDisplayValue();
        
    }
    ensure(){
        let that = this;
        console.log('ensure',that.innerValue)
        if(that.grid && !!that.innerValue){
            let rowid;
            let index = that.grid.getselectedrowindex();
            let rowData = that.grid.getrowdata(index);
            if(rowData && rowData[that.key]==that.innerValue){

            }else{
                that.dataAdapter.records.map((row)=>{
                    if(row[that.key] == that.innerValue){
                        rowid = row.id
                    }
                })
                let rowIndex = that.grid.getrowboundindexbyid(rowid)
                that.grid.ensurerowvisible(rowIndex);
                that.grid.selectedrowindex(rowIndex);
            }
        }
    }
    initDisplayValue(){
        let that = this;
        setTimeout((any)=>{
            if(!!that.innerValue){
                that.api.get({
                    url: AppSetting.PARTNER_DETAIL_URL + that.innerValue,
                }).done(function(res){
                    if(res.data){
                        that.setSelectedItem(res.data);
                    }
                })
            }
        }, 42)
    }
    ngAfterViewInit(){
        let that = this;
        that.loadAdapter();
        if(that.layout == 'grid') 
            setTimeout(()=>{
                that.initGrid();
            },200)
        that.initDisplayValue()
    }

    toggle(){
        if(this.window.isOpen()) this.close();
        else this.open()
    }
    open(){
        this.window.open();
        this.initGrid()
        // this.onOpen.emit()
        
    }
    eventWindowOpen(){
        this.ensure()
        this.onOpen.emit()
    }
    eventWindowClose(){
        this.onClose.emit()
    }
    close(){
        this.window.close();
        // this.onClose.emit()
    }
    clear(){
        let that = this;
        that.grid.clearselection()
        // that.label = "Please Choose";
    }
    done(){
        let that = this;
        let index = that.grid.getselectedrowindex();
        let rowData = that.grid.getrowdata(index);
        if(index>=0 && rowData){
            that.setSelectedItem(rowData);
            that.onChangeCallback(rowData[that.key]);
            
        }else{
            that.setSelectedItem(undefined);
            that.onChangeCallback(undefined)
        }
        that.onChange.emit()
        that.close();
    }
    initContent(){
        setTimeout(()=>{
            let that = this;
            console.log(this,self)
        },200)
    }

    source;
    dataAdapter;
    gridSetting;
    
    loadAdapter(){
        let that = this;
        let user = that.api.getUserInfo();
        that.source = Helpers.Source({
            dataType: "json",
            type: 'POST',
            url: AppSetting.COMMON_BINDING_URL,
            data: {
                table: 'users',
                conditions: ['id','<>', user.id]
            },
            dataFields: [
                { name: 'id', type: 'number' },
                { name: 'username', type: 'string' },
                { name: 'email', type: 'string' },
                { name: 'company_info_id', type: 'string' },
                { name: 'is_active', type: 'bool' },
            
            ],
            filter: function() {
                // update the grid and send a request to the server.
                that.grid.updatebounddata('filter');
            },
            sort: function() {
                // update the grid and send a request to the server.
                that.grid.updatebounddata('sort');
            },
            id: 'id',
            root: 'data',
        });
        // that.gridPartner.setOptions({source:[]});
        that.dataAdapter = new jqx.dataAdapter(that.source, {
            beforeLoadComplete: function (records) {
                for (var i = 0; i < records.length; i++) {
                    var row = records[i];
                }
                return records;
            }
        });
    }
    pending = false;
    beforeEditData;
    currentEditData;
    cellEditor
    initGrid(){
        let that = this;
        
        let columns = [
                { text: '#', dataField: 'id', width: 120 ,hidden: true},
                { 
                    text: Helpers.translate.get('Username'), dataField: 'username', width: 160 ,
                },{ 
                    text: Helpers.translate.get('Email'), dataField: 'email', minWidth:180,editable: false
                },{ 
                    text: Helpers.translate.get('Activated'), dataField: 'is_active', width: 80 , columntype: 'checkbox',
                    filtertype: 'bool'
                },
                
            ]
        
        if(that.gridSetting){
            return;
        }

        Helpers.translate.do(()=>{
            if(that.grid){
                that.grid.setcolumnproperty('id','text', '#')
                that.grid.setOptions({'localization': Helpers.getLocalizatio(Helpers.translate.activeLang)})
            }
        })

        that.gridSetting = {
            columns: columns,
            source: that.dataAdapter,
            localization: Helpers.getLocalizatio(Helpers.translate.activeLang),
            ready: function() {
                that.initCbxs()
                that.ensure()
            },
            rendergridrows: (params: any): any[] => {
                return that.dataAdapter.records;
            },
            rendered(event: any): void {
                that.grid.expandallgroups();
            },
            onRowdoubleclick: (event:any)=>{
                // event arguments.
                var args = event.args;
                // row's bound index.
                var rowBoundIndex = args.rowindex;
                // row's visible index.
                var rowVisibleIndex = args.visibleindex;
                // right click.
                var rightclick = args.rightclick; 
                // original event.
                var ev = args.originalEvent;
                // column index.
                var columnindex = args.columnindex;
                // column data field.
                var dataField = args.datafield;
                // cell value
                var value = args.value;
                if(args && args.row){
                    var dataRow = args.row.bounddata
                    if(dataRow) that.showDetail(dataRow.id)
                }
            },
            onCellBeginEdit(event: any): void {
                let args = event.args;
                // if(!that.currentEditData){
                    that.beforeEditData = cloneDeep(args.row);
                    that.currentEditData = cloneDeep(args.row);
                // }
            },
            onCellEndEdit(event: any): void {
                let args = event.args;
                let row = args.row;
                let params;
                that.currentEditData[args.datafield] = args.value;
                if (args.datafield === 'is_active') {
                    if(
                        JSON.stringify(that.currentEditData) !=
                        JSON.stringify(that.beforeEditData)
                        ){

                        params = {
                            // email:that.currentEditData.email,
                            username:that.currentEditData.username,
                            is_active:+that.currentEditData.is_active,
                        }
                        that.api.put({
                            url: AppSetting.UPDATE_USER + row.id,
                            params: params,
                            onDone:function(res){
                                toastr.success('Update User successfuly')
                                // let index = that.grid.getselectedrowindex();
                                // let id = that.grid.getrowid(index);
                                // that.grid.updaterow(id,that.currentEditData);
                                that.dataAdapter.dataBind();
                                //that.grid.updatebounddata('cells');
                                that.currentEditData = undefined;
                                that.beforeEditData = undefined;
                            },
                            onFail:function(){
                                toastr.warning('Fail to update User')
                            },
                        })
                    }else{
                        that.currentEditData = undefined;
                        that.beforeEditData = undefined;
                    }
                }
            },
        }
    }
    reloadGrid(){
        this.dataAdapter.dataBind();
        this.grid.updatebounddata('cells');
    }
    editingItem:any = {
        id:0,
        username: '',
        email: '',
        type: 0,
        roles:[]
    }
    clickAdd(){
        this.showDetail()
    }
    clickEdit(){
        let that = this;
        let index = that.grid.getselectedrowindex();
        let rowData = that.grid.getrowdata(index);
        if(rowData){
            that.showDetail(rowData.id)
        }
    }
    clickDelete(){
        // let that = this;
        // let index = that.grid.getselectedrowindex();
        // let rowData = that.grid.getrowdata(index);
        // if(rowData){
        //     Helpers.confirm('<h3>Are you sure you want to delete bussiness posting group?</h3>',function(){
                
        //         that.api.post({
        //             url: AppSetting.DEL_SETUP_POST_URL,
        //             params: {
        //                 id: rowData.id
        //             }
        //         }).done(function(res){
        //             if(res.code){
        //                 that.dataAdapter.dataBind();
        //                 that.grid.updatebounddata('cells');
        //             }
        //         })
        //     })
        // }
    }
    showDetail(id=0){
        let that = this;
        that.detailWindow.open();
        if(id){
            that.api.get({
                url: AppSetting.GET_USER + id,
                // params: {
                //     id: id
                // }
            }).done(function(res){
                if(res.data){
                    that.setData(res.data)
                }
            })
        }else{
            that.setData({
                id:0,
                roles:[],
                "company_info_id": 1,
                "username": "",
                "email": "",
                "password": "",
                "type": 0,
                "role": [],
                "is_active": 1
            })
        }
    }
    setData(data){
        let that = this
        that.editingItem = data
        console.log(data.company_info_id,'data.company_info_id')
        // if(data.company_info_id && that.companyCbx){
        //     that.companyCbx.setValue(data.company_info_id)
        // } else if(that.companyCbx) that.companyCbx.setValue(null);
        that.setRoles();
    }
    setRoles(){
        let that = this;
        let data = that.editingItem
        if(!that.roles) return;
        
            console.log(data.roles,'data.roles')
            let checkeds = data.roles.map(function(role){
                return role.role_id;
            })
            that.roles.map(function(role){
                role.checked = checkeds.indexOf(role.id)>=0;
            })
    }
    trackByIdx(index: number, obj: any): any {
    return index;
  }
    companySetting
    initCompanyCbx(){
        let that = this;
        if(that.companySetting) return;
        that.companySetting = {
            onOpen: ()=>{
                that.pending = true;
            },
            onClose: ()=>{
                that.pending = false;
                $('.jqx-grid-validation,.jqx-grid-validation-arrow-up').remove()
            },
            onChange: function(event){
                let item = that.companyCbx.getSelectedItem();
                if(that.detailWindow.isOpen()){
                    if(item){
                        // that.editingItem.gen_product_ptg = item;
                        // that.editingItem.company_info_id = item.id
                    }
                    let windowId = that.validator.host.attr('id')
                    $('#'+windowId+ ' .cbx_company_info_id').blur()

                }else{
                    if(that.cellEditor) {
                        that.cellEditor.val(item.id)

                    }
                }
                
            },
        }
    }
    
    initCbxs(){
        // this.initCompanyCbx()
    }
    roles
    initRoles(){
        let that = this;
        if(that.roles) return;
        this.api.get({
            url: AppSetting.LIST_ROLE,
        }).done(function(res){
            that.roles = res.data,
            that.setRoles();
        })
    }
    initDetailWindow(){
        let that = this;
        that.initCbxs()
        that.initRoles()
        that.initValidation();
    }
    closeDetail(){
        this.detailWindow.close();
    }
    onTypeChange(event){
        let that = this;
        console.log('TYPE CHANGE',that.editingItem.type)
        that.roles.map(function(role){
            role.checked = !that.editingItem.type;
        })
    }
    onRoleChange(event){
        console.log('ROLE CHANGE')
        let that = this;
        that.editingItem.type = false
    }

    isSubmit = false;
    checkValidate(): void {
        let that = this;
        setTimeout(() => {
            that.validator.validate();
        }, 200)
        this.isSubmit = true;
    }
    onValidationError(){
        this.isSubmit = false;
    }

    onValidationSuccess() : void {
        let that = this;
        if(that.isSubmit){
            var formData = cloneDeep(that.editingItem);
            formData.type = +formData.type
            formData.is_active = 1;
            formData.role = that.roles.map(function(v,i){
                if(v.checked) return v.id;
            }).filter(function(v){
                return v;
            })
            console.log(formData,'formData')
                delete formData.id;
                delete formData.roles;
            if(that.editingItem.id){
                delete formData.email;
                that.api.put({
                    url: AppSetting.UPDATE_USER + that.editingItem.id,
                    params: formData
                }).done(function(res){
                    // if(res.code==1){
                        that.dataAdapter.dataBind();
                        // that.grid.updatebounddata('cells');
                        that.detailWindow.close()
                        toastr.success('Create successfuly')
                    // }
                })
            }else{
                that.api.post({
                    url: AppSetting.CTEATE_USER,
                    params: formData
                }).done(function(res){
                    // if(res.code==1){
                        that.dataAdapter.dataBind();
                        // that.grid.updatebounddata('cells');
                        that.detailWindow.close()
                        toastr.success('Create successfuly')
                    // }
                })
            }
            
        }
    }
    rules
    initValidation(){
        let that = this;
        let windowId = that.validator.host.attr('id')
        if(that.rules){
            if(that.validator) that.validator.hide();
        }else
        that.rules =
        [
            { input: '#' + windowId+ ' .txt_username', message: Helpers.translate.get('Username') + Helpers.translate.get(' should be not empty'), action: 'keyup, blur', rule: 'required' },
            { input: '#' + windowId+ ' .txt_email', message: Helpers.translate.get('Email') + Helpers.translate.get(' should be not empty'), action: 'keyup, blur', rule: 'email' },
            { input: '#' + windowId+ ' .txt_password', message: Helpers.translate.get('Password') + Helpers.translate.get(' should be not empty'), action: 'keyup, blur', rule: 'required' },
            // { input: '#' + windowId+ ' .cbx_company_info_id', message: 'Company is required!', action: 'blur', rule: function () {
            //         return !!that.companyCbx.value;
            //     } 
            // // },{ input: '#' + windowId+ ' .cbx_vat_product_ptg_code', message: 'VAT Product Posting Group is required!', action: 'change', rule: function () {
            // //         return !!that.vatProdPtgCbx.value;
            // //     } 
            // // },{ input: '#' + windowId+ ' .cbx_invt_ptg_code', message: 'Inventory Posting Group is required!', action: 'change', rule: function () {
            // //         return !!that.invtPtgCbx.value;
            // //     } 
            // },

        ];

    }
    // onSetDefaultPhoto(udid){
    //     this.editingItem.default_image = udid
    // }
    // onSelectPhoto(event){
    //     let that = this;
    //     var args = event.args;
    //     var fileName = args.file;
    //     var fileSize = args.size;
    //     console.log(args,that.fileUpload)
    //     var files = $(that.fileUpload.elementRef.nativeElement).find('input[type="file"]').get().map(function(f){
    //         return f.files[0]
    //     }).filter(function(f){
    //         return f;
    //     });

    //     // var images = files.map(function(file,i){
    //     Helpers.getBase64(files[files.length-1]).then(
    //       data => {
    //             that.fileUpload.cancelAll();
    //             if(!that.editingItem.newImages) that.editingItem.newImages = []

    //             let key = new Date().getTime()
    //             if(!that.editingItem.default_image) that.editingItem.default_image = key
    //             that.editingItem.newImages.push({
    //                 filename: fileName,
    //                 photo:data,
    //                 udid: key
    //             })
    //     //       // console.log(data)
    //     //        $('#jqxFileUpload .jqx-file-upload-file-row:eq('+i+') img').remove();
    //     //        $('#jqxFileUpload .jqx-file-upload-file-row:eq('+i+')').prepend($('<img/>').attr('src',data))
    //       }
    //     )
    // }
}


/*
Copyright 2017-2018 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/