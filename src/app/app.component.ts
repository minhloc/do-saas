import { Component, OnInit, AfterViewInit, ViewEncapsulation, ViewChild} from '@angular/core';
import {Router, NavigationStart, NavigationEnd} from '@angular/router';
import {Helpers} from "./helpers";
import { APIService } from './api.service';
import { AppSetting } from "./settings";
import { AppMessage } from "./message";

import { VatProductPtgModule } from './widgets/ptg/vat/product/ptg-vat-product.module';
import { UomModule } from './widgets/uom/uom.module';

import { TranslateService } from './translate.service';

@Component({
  selector: 'body',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  encapsulation: ViewEncapsulation.None,

})

export class AppComponent implements OnInit, AfterViewInit {
  @ViewChild('vatProdPtgCbx') vatProdPtgCbx: VatProductPtgModule;
  @ViewChild('uomCbx') uomCbx: UomModule;
  title = 'app';
  customMessage = '1';
  
  constructor(
    private _router: Router, 
    private api: APIService, 
    private translate: TranslateService,
    ) {
    Helpers.translate = translate;
    this.language = translate.activeLang
  }
  ngOnInit() {
    let that = this;
		this._router.events.subscribe((route) => {
			if (route instanceof NavigationStart) {
				Helpers.setLoading(true);
        // Helpers.bodyClass('fixed-navbar');
				Helpers.bodyClass('fixed-layout sidebar-mini');
			}
			if (route instanceof NavigationEnd) {
				window.scrollTo(0, 0);
				Helpers.setLoading(false);

				// Initialize page: handlers ...
				Helpers.initPage();
        if(!that.GENERAL_SETUP){
          setTimeout((any)=>{
            let user = that.api.getUserInfo();
            if(user.id){
              let api = that.api.get({
                  url: AppSetting.GET_GENERAL_SETUP_URL + user.company_info_id
              })
              if(api) api.done(function(data){
                that.GENERAL_SETUP = data.data
                  console.log(data,'GET_GENERAL_SETUP_URL',user)
              })
            }
          },500)
        }
      }

    });
  }
  sap_avaiable(event){
    console.log(event,this)
    let that = this;
    let user = that.api.getUserInfo();
    that.api.post({
        url: AppSetting.UPDATE_GENERAL_SETUP_URL,
        params: {
          id: user.company_info_id,
          sap_avaiable: +event
        }
    }).done(function(data){
      that.GENERAL_SETUP.sap_avaiable = +event;
    })
  }
  GENERAL_SETUP
  ngAfterViewInit() {
      // this.customMessage = 'message';
      let that = this;
      
  }
  showVatProdPtg(){
    this.vatProdPtgCbx.open();
  }
  showUOM(){
    this.uomCbx.open();
  }
  language
  languageChange(event){
    console.log(this.language,event)
    this.translate.use(this.language)
  }

}
