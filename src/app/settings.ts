
export class AppSetting {
    public static THEME = 'office';
    // public static BASE_URL='http://local.saaspiens.speranzainc.net/api/v1/';
    // public static WEB_API_URL='http://local.saaspiens.speranzainc.net/api/web/';
    // public static BASE_URL='http://saaspiens.speranzainc.net:9002/api/v1/';
    // public static WEB_API_URL='http://saaspiens.speranzainc.net:9002/api/web/';
    public static API_INFO = (function(){
        let api_url = '';
        let api_name = '';
        switch(window.location.host){
            // Local
            case 'localhost:4200':
                api_url = 'http://saaspiens.speranzainc.net:9002/';
                // api_url = 'http://local.saaspiens.speranzainc.net/';
                // api_url = 'http://jp.mysaaspiens.com:7100/';
                api_name = 'Development';
                break;
            // Dev
            case 'development.mysaaspiens.com:7000':
                api_url = 'http://saaspiens.speranzainc.net:9002/';
                api_name = 'Development';
                break;
            case '54.255.190.113:7000':
                api_url = 'http://saaspiens.speranzainc.net:9002/';
                api_name = 'Development';
                break;
            case '54.255.190.113:7200':
                api_url = 'http://54.255.190.113:7300/';
                api_name = 'lnlvietnam';
                break;
            case 'demo.mysaaspiens.com':
                api_url = 'http://demo.mysaaspiens.com:8200/';
                api_name = 'Demo';
                break;
            // JP
            case 'jp.mysaaspiens.com':
                api_name = 'Japan';
                api_url = 'http://jp.mysaaspiens.com:7100/';
                break;
            default:
                api_url = '/';
                api_name = 'Unknown';
        }
        return {
            url: api_url,
            name: api_name
        }
    }());
    public static API_DOMAIN  = AppSetting.API_INFO.url;
    public static BASE_URL=AppSetting.API_DOMAIN + 'api/v1/';
    public static WEB_API_URL=AppSetting.API_DOMAIN + 'api/web/';
    public static LOGIN_URL= AppSetting.BASE_URL+ 'Login';
    public static GET_GENERAL_SETUP_URL= AppSetting.BASE_URL+ 'findSerial/';
    public static UPDATE_GENERAL_SETUP_URL= AppSetting.BASE_URL+ 'SerialUpdate';
    public static PRODUCT_LIST_URL= AppSetting.BASE_URL+ 'GetProducts';
    public static PRODUCT_DETAIL_URL= AppSetting.BASE_URL+ 'GetProduct/';
    public static UOM_LIST_URL= AppSetting.BASE_URL+ 'GetUoms';
    public static GROUP_LIST_URL= AppSetting.BASE_URL+ 'ItemGroups';
    public static TAX_LIST_URL= AppSetting.BASE_URL+ 'GetTaxGroups';
    public static PARTNER_LIST_URL= AppSetting.BASE_URL+ 'GetPartners';
    public static PARTNER_CREATE_URL= AppSetting.BASE_URL+ 'PartnerCreate';
    public static PARTNER_UPDATE_URL= AppSetting.BASE_URL+ 'PartnerUpdate/';
    public static RE_PAY_LIST_URL= AppSetting.BASE_URL+ 'GetReceipts';
    public static RE_PAY_BYPARTNER_URL= AppSetting.BASE_URL+ 'GetReceipt/';
    public static RE_PAY_DETAIL_URL= AppSetting.BASE_URL+ 'GetDetailReceipt/';
    public static PARTNER_DETAIL_URL= AppSetting.BASE_URL+ 'GetPartner/';
    public static UPDATE_PROFILE_URL= AppSetting.BASE_URL+ 'UserProfile';
    public static CHANGE_PASSWORD_URL= AppSetting.BASE_URL+ 'UserChangePass';

    public static EXPENSES_CREATE_URL= AppSetting.BASE_URL+ 'ExpenseCreate';
    public static EXPENSES_LIST_URL= AppSetting.BASE_URL+ 'GetExpenses';
    public static EXPENSES_DETAIL_URL= AppSetting.BASE_URL+ 'GetExpense/';
    public static EXPENSES_UPDATE_URL= AppSetting.BASE_URL+ 'ExpenseUpdate/';


    public static GET_LIST_NONE_ITEM_ACCT_URL= AppSetting.WEB_API_URL+ 'Acct/ListNoneItem';
    public static GET_ACCT_URL= AppSetting.WEB_API_URL+ 'Acct/Get';
    public static ACCT_DELETE_URL= AppSetting.WEB_API_URL+ 'Acct/Del';
    public static ACCT_LIST_URL= AppSetting.WEB_API_URL+ 'Acct/List';
    public static ACCOUNT_CREATE_URL= AppSetting.WEB_API_URL+ 'Acct/Create';
    public static ACCOUNT_COMMIT_URL= AppSetting.WEB_API_URL+ 'Acct/Commit';
    
    public static JOURNAL_BINDING_URL= AppSetting.WEB_API_URL+ 'Journal/Binding';
    public static JOURNAL_LINE_BINDING_URL= AppSetting.WEB_API_URL+ 'Journal/BindingLine';
    public static JOURNAL_DETAIL_URL= AppSetting.WEB_API_URL+ 'Journal/Get/';
    public static JOURNAL_CREATE_URL= AppSetting.WEB_API_URL+ 'Journal/Create';

    public static COUNTRY_LIST_URL= AppSetting.BASE_URL+ 'GetCountries';
    public static COSTCENTER_LIST_URL= AppSetting.BASE_URL+ 'GetCostCenters';

    public static SALES_LIST_URL= AppSetting.BASE_URL+ 'GetSales';
    public static SALE_CREATE_URL= AppSetting.BASE_URL+ 'SaleCreate';
    public static SALE_INVOICE_COMMIT_URL= AppSetting.WEB_API_URL+ 'Sales/Commit';
    public static SALE_DETAIL_URL= AppSetting.BASE_URL+ 'GetSale/';
    public static SALE_CLONE_URL= AppSetting.BASE_URL+ 'CloneSaleToInvoice/';
    public static SALE_CONVERT_URL= AppSetting.WEB_API_URL+ 'Sales/Convert';

    public static CREDITNOTE_CREATE_URL= AppSetting.BASE_URL+ 'CreditNoteCreate';

    public static CREDITNOTE_DETAIL_URL= AppSetting.BASE_URL+ 'GetCreditNote/';

    public static PURCHASES_LIST_URL= AppSetting.BASE_URL+ 'GetPurchases';
    public static PURCHASE_CREATE_URL= AppSetting.BASE_URL+ 'PurchaseCreate';
    public static PURCHASE_COMMIT_URL= AppSetting.WEB_API_URL+ 'Purchases/Commit';
    public static PURCHASE_INVOICE_CREATE_URL= AppSetting.WEB_API_URL+ 'Purchases/Create';
    public static PURCHASE_DETAIL_URL= AppSetting.BASE_URL+ 'GetPurchase/';
    public static PURCHASE_CONVERT_URL= AppSetting.WEB_API_URL+ 'Purchases/Convert';

    public static GROUP_CREATE_URL= AppSetting.BASE_URL+ 'ItemGroupCreate';
    public static GROUP_UPDATE_URL= AppSetting.BASE_URL+ 'ItemGroupUpdate/';
    public static GROUP_DETAIL_URL= AppSetting.BASE_URL+ 'GetItemGroup/';
    public static GROUP_DELETE_URL= AppSetting.BASE_URL+ 'ItemGroupDelete/';

    public static PRODUCT_CREATE_URL= AppSetting.BASE_URL+ 'ProductCreate';
    public static PRODUCT_UPDATE_URL= AppSetting.BASE_URL+ 'ProductUpdate/';

    public static COMMON_BINDING_URL= AppSetting.WEB_API_URL+ 'Common/Binding';
    public static COMMON_UPDATE_URL= AppSetting.WEB_API_URL+ 'Common/Update';
    public static COMMON_CREATE_URL= AppSetting.WEB_API_URL+ 'Common/Create';
    public static COMMON_COMMIT_URL= AppSetting.WEB_API_URL+ 'Common/Commit';

    public static GET_BUS_POST_URL = AppSetting.WEB_API_URL+ 'Ptg/GetBusPost';
    public static DEL_BUS_POST_URL = AppSetting.WEB_API_URL+ 'Ptg/DelBusPost';
    public static BUS_POST_COMMIT_URL = AppSetting.WEB_API_URL+ 'Ptg/CommitBusPost';

    public static GET_PROD_POST_URL = AppSetting.WEB_API_URL+ 'Ptg/GetProdPost';
    public static DEL_PROD_POST_URL = AppSetting.WEB_API_URL+ 'Ptg/DelProdPost';
    public static PROD_POST_COMMIT_URL = AppSetting.WEB_API_URL+ 'Ptg/CommitProdPost';

    public static GET_SETUP_POST_URL = AppSetting.WEB_API_URL+ 'Ptg/GetSetupPost';
    public static DEL_SETUP_POST_URL = AppSetting.WEB_API_URL+ 'Ptg/DelSetupPost';
    public static SETUP_POST_COMMIT_URL = AppSetting.WEB_API_URL+ 'Ptg/CommitSetupPost';

    public static GET_CUSTOMER_POST_URL = AppSetting.WEB_API_URL+ 'Ptg/GetCustomerPost';
    public static DEL_CUSTOMER_POST_URL = AppSetting.WEB_API_URL+ 'Ptg/DelCustomerPost';
    public static CUSTOMER_POST_COMMIT_URL = AppSetting.WEB_API_URL+ 'Ptg/CommitCustomerPost';

    public static GET_VENDER_POST_URL = AppSetting.WEB_API_URL+ 'Ptg/GetVenderPost';
    public static DEL_VENDER_POST_URL = AppSetting.WEB_API_URL+ 'Ptg/DelVenderPost';
    public static VENDER_POST_COMMIT_URL = AppSetting.WEB_API_URL+ 'Ptg/CommitVenderPost';

    public static GET_BANK_POST_URL = AppSetting.WEB_API_URL+ 'Ptg/GetBankPost';
    public static DEL_BANK_POST_URL = AppSetting.WEB_API_URL+ 'Ptg/DelBankPost';
    public static BANK_POST_COMMIT_URL = AppSetting.WEB_API_URL+ 'Ptg/CommitBankPost';

    public static GET_INVENTORY_POST_URL = AppSetting.WEB_API_URL+ 'Ptg/GetInventoryPost';
    public static DEL_INVENTORY_POST_URL = AppSetting.WEB_API_URL+ 'Ptg/DelInventoryPost';
    public static INVENTORY_POST_COMMIT_URL = AppSetting.WEB_API_URL+ 'Ptg/CommitInventoryPost';

    public static GET_INVT_SETUP_POST_URL = AppSetting.WEB_API_URL+ 'Ptg/GetInvtSetupPost';
    public static DEL_INVT_SETUP_POST_URL = AppSetting.WEB_API_URL+ 'Ptg/DelInvtSetupPost';
    public static INVT_SETUP_POST_COMMIT_URL = AppSetting.WEB_API_URL+ 'Ptg/CommitInvtSetupPost';

    public static GET_VAT_BUS_POST_URL = AppSetting.WEB_API_URL+ 'Ptg/GetVatBusPost';
    public static DEL_VAT_BUS_POST_URL = AppSetting.WEB_API_URL+ 'Ptg/DelVatBusPost';
    public static VAT_BUS_POST_COMMIT_URL = AppSetting.WEB_API_URL+ 'Ptg/CommitVatBusPost';

    public static GET_VAT_PROD_POST_URL = AppSetting.WEB_API_URL+ 'Ptg/GetVatProdPost';
    public static DEL_VAT_PROD_POST_URL = AppSetting.WEB_API_URL+ 'Ptg/DelVatProdPost';
    public static VAT_PROD_POST_COMMIT_URL = AppSetting.WEB_API_URL+ 'Ptg/CommitVatProdPost';

    public static GET_VAT_SETUP_POST_URL = AppSetting.WEB_API_URL+ 'Ptg/GetVatSetupPost';
    public static DEL_VAT_SETUP_POST_URL = AppSetting.WEB_API_URL+ 'Ptg/DelVatSetupPost';
    public static VAT_SETUP_POST_COMMIT_URL = AppSetting.WEB_API_URL+ 'Ptg/CommitVatSetupPost';

    public static BINDING_NONE_ITEM_SETUP_URL = AppSetting.WEB_API_URL+ 'Ptg/BindingNoneItemSetup';
    public static GET_NONE_ITEM_SETUP_URL = AppSetting.WEB_API_URL+ 'Ptg/GetNoneItemSetup';
    public static DEL_NONE_ITEM_SETUP_URL = AppSetting.WEB_API_URL+ 'Ptg/DelNoneItemSetup';
    public static NONE_ITEM_SETUP_COMMIT_URL = AppSetting.WEB_API_URL+ 'Ptg/CommitNoneItemSetup';

    public static EXPENSES_UPDATE_STATUS= AppSetting.WEB_API_URL+ 'Common/ExpensesUpdateStatus';
    
    public static SALES_INFO_URL= AppSetting.WEB_API_URL+ 'Analytics/SalesInfo';
    public static PURCHASES_INFO_URL= AppSetting.WEB_API_URL+ 'Analytics/PurchasesInfo';
    public static FINANCE_INFO_URL= AppSetting.WEB_API_URL+ 'Analytics/FinanceInfo';
    public static PARTNER_INFO_URL= AppSetting.WEB_API_URL+ 'Analytics/PartnerInfo';

    public static BANK_DETAIL_URL= AppSetting.BASE_URL+ 'GetBank/';
    public static BANK_CREATE_URL= AppSetting.BASE_URL+ 'BankCreate';
    public static BANK_UPDATE_URL= AppSetting.BASE_URL+ 'BankUpdate/';

    public static GET_LOCATION_URL = AppSetting.WEB_API_URL+ 'Location/Get';
    public static DEL_LOCATION_URL = AppSetting.WEB_API_URL+ 'Location/Del';
    public static LOCATION_COMMIT_URL = AppSetting.WEB_API_URL+ 'Location/Commit';

    public static ACTIVITY_BINDING_URL= AppSetting.WEB_API_URL+ 'Activity/Binding';
    public static ACTIVITY_COMMIT_URL= AppSetting.WEB_API_URL+ 'Activity/Commit';
    public static ACTIVITY_DETAIL_URL= AppSetting.WEB_API_URL+ 'Activity/Get';
    public static ACTIVITY_DELETE_URL= AppSetting.WEB_API_URL+ 'Activity/Del';

    
    public static SUMMARY_URL= AppSetting.WEB_API_URL+ 'Analytics/Summary';
    public static ANALYTICS_SALES_7WEEK_URL= AppSetting.WEB_API_URL+ 'Analytics/Sales/Last7Week';
    public static ANALYTICS_SALES_7DAY_URL= AppSetting.WEB_API_URL+ 'Analytics/Sales/Last7Day';
    public static ANALYTICS_SALES_12MONTH_URL= AppSetting.WEB_API_URL+ 'Analytics/Sales/Last12Month';
    public static ANALYTICS_PURCHASES_7WEEK_URL= AppSetting.WEB_API_URL+ 'Analytics/Purchases/Last7Week';
    public static ANALYTICS_PURCHASES_7DAY_URL= AppSetting.WEB_API_URL+ 'Analytics/Purchases/Last7Day';
    public static ANALYTICS_PURCHASES_12MONTH_URL= AppSetting.WEB_API_URL+ 'Analytics/Purchases/Last12Month';
    public static ANALYTICS_DASHBOARDV1_URL= AppSetting.BASE_URL+ 'Dashboard';
    public static ANALYTICS_DASHBOARD_URL= AppSetting.BASE_URL+ 'DashboardForPhase2';

    public static ITEMS_BINDING_URL= AppSetting.WEB_API_URL+ 'Items/Binding';
    public static GET_ITEMS_URL = AppSetting.WEB_API_URL+ 'Items/Get';
    public static DEL_ITEMS_URL = AppSetting.WEB_API_URL+ 'Items/Del';
    public static ITEMS_COMMIT_URL = AppSetting.WEB_API_URL+ 'Items/Commit';

    public static GET_UOM_URL = AppSetting.WEB_API_URL+ 'Uom/Get';
    public static DEL_UOM_URL = AppSetting.WEB_API_URL+ 'Uom/Del';
    public static UOM_COMMIT_URL = AppSetting.WEB_API_URL+ 'Uom/Commit';

    public static ITEM_JOURNAL_CREATE_URL= AppSetting.WEB_API_URL+ 'ItemJournal/Create';
    public static GET_ITEM_JOURNAL_URL= AppSetting.WEB_API_URL+ 'ItemJournal/Get';

    public static LIST_ROLE = AppSetting.BASE_URL + 'GetRoles';
    public static CTEATE_USER = AppSetting.BASE_URL + 'UserCreate';
    public static UPDATE_USER = AppSetting.BASE_URL + 'UserUpdate/';
    public static GET_USER = AppSetting.BASE_URL + 'GetUser/';
    public static UPDATE_PROFILE_USER = AppSetting.BASE_URL + 'UserProfile/';

    public static GET_COMPANY = AppSetting.BASE_URL + 'company/';

    public static GET_FORMULAS_URL = AppSetting.WEB_API_URL+ 'Formulas/Get';
    public static DEL_FORMULAS_URL = AppSetting.WEB_API_URL+ 'Formulas/Del';
    public static FORMULAS_COMMIT_URL = AppSetting.WEB_API_URL+ 'Formulas/Commit';

    public static MIGRATION_CUSTOMER_URL = AppSetting.WEB_API_URL+ 'Migration/Import/Customer';
    public static MIGRATION_SALES_URL = AppSetting.WEB_API_URL+ 'Migration/Import/Sales';
    public static MIGRATION_PRODUCT_URL = AppSetting.WEB_API_URL+ 'Migration/Import/Product';
}