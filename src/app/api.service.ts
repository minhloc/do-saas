import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Router } from '@angular/router';
import { AppSetting } from "./settings";
import { Helpers } from ".//helpers";
import { Response } from "@angular/http"
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/Rx';
declare var toastr:any;
declare var $:any;
@Injectable()
export class APIService {
    constructor(

        private http: HttpClient,
        private router: Router
        ) {
    }
    objs: object[] = [];
    token ;//= "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vc2Fhc3BpZW5zLnNwZXJhbnphaW5jLm5ldDo5MDAyL2FwaS92MS9Mb2dpbiIsImlhdCI6MTUzNjIwNjYyNSwiZXhwIjoyMTYwMDE1MzYyMDY2MjUsIm5iZiI6MTUzNjIwNjYyNSwianRpIjoiTHNxZFFCRmxSZkc4T3BKYiIsInN1YiI6OSwicHJ2IjoiODdlMGFmMWVmOWZkMTU4MTJmZGVjOTcxNTNhMTRlMGIwNDc1NDZhYSJ9.xW1_7A8UQsXTc6aKFxolqKs1RBQNFldYe8K6M5Cul2U"
    set (opt,value) {
        return this;
    }
    private handleError(error,callback) {
        Helpers.setLoading(false);
        if(callback) callback();
      if (error.error instanceof ErrorEvent) {
        // A client-side or network error occurred. Handle it accordingly.
        console.error('An error occurred:', error.error.message);
      } else {
        // The backend returned an unsuccessful response code.
        // The response body may contain clues as to what went wrong,
        console.error(
          'Backend returned code ${error.status}, ' +
          'body was: ${error.error}',error);
      }
      // return an observable with a user-facing error message
      console.log(error)
      toastr.error('Connection Failed. Please check your network connection and try again')
    };
    private handleDone(res, callback) {
        Helpers.setLoading(false);
        if(res){
            let status = res.status
            let messages = res.message
            let errors = res.errors
            let warning_message = res.warning_message
            if(warning_message){
                toastr.warning(res.warning_message)
            }
            if(status!=undefined){
                if(status == 402){
                    if(errors){
                        toastr.error(errors.join('<br/>'))
                    }else if(typeof messages == 'object'){
                        messages = Object.keys(messages).map(function(key) {
                          return messages[key]
                        });
                        toastr.error(messages.join('<br/>'))
                    } else if(typeof messages == 'string'){
                        toastr.error(messages)
                    }
                } else if(status == 200){
                    if(callback) callback(res)
                } else if(status == 0 || status == 1){
                    let code = res.code;
                    let errors = res.errors;
                    console.log(res,'res')
                    if(code!=1){
                        if(errors){
                            let msg = Object.keys(errors).map((key)=>{
                                return errors[key];
                            }).join('<br/>')
                            toastr.error(msg)
                        }else if(res.message){
                            toastr.error(res.message)
                        }
                    }else if(callback) callback(res)
                } else {
                    toastr.error(messages)
                }
            }else{
                if(callback) callback(res)
            }
        }else{
            toastr.error('Loi Roi')
        }
    }
    get (opt) {
        let callback = opt.onDone || undefined;
        let failCallback = opt.onFail || undefined;
        this.token = localStorage.getItem('token');
        if(!this.token){
            this.router.navigateByUrl('/login');
            return;
        }
        this.objs.push(opt)
        Helpers.setLoading(true);
        this.http
            .get(
                opt.url || '', 
                // new HttpParams(
                {
                    params: opt.params || {},
                    headers: new HttpHeaders()
                        .append('Authorization', 'bearer ' + this.token)
                        .append('Content-Type','application/json')
                }
                // )
            )
            .subscribe(
                (res:any) => this.handleDone(res, callback),
                error => this.handleError(error,failCallback)
            )
        let funs = {
            done: function(fun){
                callback = fun
                return funs;
            },
            fail: function(fun){
                failCallback = fun;
                return funs;
            }
        }
        return funs
    };
    post (opt) {
        opt.method = 'post';
        return this.send(opt)
    };
    put(opt){
        opt.method = 'put';
        return this.send(opt)
    }
    delete(opt){
        opt.method = 'delete';
        return this.send(opt)
    }
    send (opt) {
        let that = this;
        let callback = opt.onDone || undefined;
        let failCallback = opt.onFail || undefined;
        this.token = localStorage.getItem('token');
        if(!this.token){
            this.router.navigateByUrl('/login');
            return;
        }
        this.objs.push(opt)
        var headers = new HttpHeaders()
            .append('Authorization', 'bearer ' + this.token)
            // .append('Accept', 'application/json')
                // .append("Access-Control-Allow-Methods", "GET, HEAD, POST, PUT, PATCH, DELETE, OPTIONS")
                // .append("Access-Control-Allow-Headers", "Content-Type, Authorization, Accept")
                // .append("Access-Control-Allow-Credentials", "true")
                // .append("Access-Control-Allow-Origin", "*")
        var objectToFormData = function(obj, form = undefined, namespace = undefined) {
    
          var fd = form || new FormData();
          var formKey;
          
          for(var property in obj) {
            if(obj.hasOwnProperty(property)) {
              
              if(namespace) {
                formKey = namespace + '[' + property + ']';
              } else {
                formKey = property;
              }
             
              // if the property is an object, but not a File,
              // use recursivity.
              if(typeof obj[property] === 'object' && !(obj[property] instanceof File)) {
                
                objectToFormData(obj[property], fd, property);
                
              } else {
                
                // if it's a string or a File object
                fd.append(formKey, obj[property]);
              }
              
            }
          }
          
          return fd;
            
        };
        let formData = objectToFormData(opt.params);
        if(opt.files){
            // headers
                // .append('Content-Type','multipart/form-data')
                // .append('Content-Type',undefined )
                // .append('Content-Type','application/x-www-form-urlencoded')
            for ( var key in opt.files || {} ) {
                if(opt.files[key].length){
                    // opt.files[key].map(function(f){
                    for ( var i in opt.files[key] || {} ) {
                        formData.append(key+'[]', opt.files[key][i],opt.files[key][i].name);
                    }
                }else{
                    formData.append(key, opt.files[key]);
                }
            }

            
        }else{
            formData = opt.params;
            headers.append('Content-Type','application/json');
        }
        Helpers.setLoading(true);
        let method = opt.method || 'post';
        this.http[method](
                opt.url || '', 
                formData,
                {    
                    headers: headers
                }
            )
            .subscribe(
                (res:any) => this.handleDone(res, callback),
                error => this.handleError(error,failCallback)
            )
        let funs = {
            done: function(fun){
                callback = fun
                return funs;
            },
            fail: function(fun){
                failCallback = fun;
                return funs;
            }
        }
        return funs
    }

    login(email,password,callback){
        // let _this = this;
        this.http
            .post(
                AppSetting.BASE_URL + 'Login', 
                {
                    email: email,
                    password: password
                }, {
                    headers: new HttpHeaders()
                        .append('Content-Type','application/json')
                }
            )
            .subscribe(
                    (data:any) => {
                        console.log(data)
                        if(data){
                            let status = data.status
                            let messages = data.message.error
                            if(status == 402){
                                Helpers.alert(messages.join(''))
                            } else if(status == 200){
                                let token = data.access_token;
                                let user = data.user;
                                let role = data.role;
                                // if(user && user.type == 0){
                                //     //
                                //     this.router.navigateByUrl('/error_403');
                                // }else{

                                    localStorage.setItem('token',token);
                                    localStorage.setItem('user', JSON.stringify(user));
                                    this.router.navigateByUrl('/');
                                // }
                            } else {

                            }
                        }else{

                        }
                    },
                    error => this.handleError(error,null)
            )
    }

    logout(){
        let that = this
        let dialog = Helpers.confirm('Are you sure you want to log out?',function(){
            localStorage.removeItem('token');
            that.router.navigateByUrl('/login');
        })
        dialog.set({
            'oktext':'Log out'
        })
    }
    getUserInfo(){
        if(!localStorage.getItem('user')) this.router.navigateByUrl('/login');
        return JSON.parse(localStorage.getItem('user')) || {};
    }


}