import { Component, AfterViewInit,OnChanges, OnInit } from '@angular/core';
import { APIService } from '../../api.service';
import { Helpers } from "../../helpers";
import { TranslateService } from './../../translate.service';

@Component({
  selector: '[app-header]',
  templateUrl: './app-header.component.html',
})
export class AppHeader implements OnInit,AfterViewInit ,OnChanges{

  constructor(
      private api: APIService,
      private translate: TranslateService,
  ) {
    console.log(translate.data,'translate.data')
  }
  ngOnInit(){
    // this.route.params.subscribe(
    //         params => {
    //         })
    this.user = this.api.getUserInfo();
    console.log('HH')
  }
  ngOnChanges() {
    console.log('Onchange')
  }
  user
  translate_data: any;
  ngAfterViewInit()  {
    let that = this;
    setTimeout(() => {
      that.translate_data = Helpers.translate.data;
    });
    Helpers.translate.do(()=>{
      that.translate_data = Helpers.translate.data;
      console.log(that.translate_data.Expenses)
    })
	}
  onLogout(){
    this.api.logout();
  }

}
