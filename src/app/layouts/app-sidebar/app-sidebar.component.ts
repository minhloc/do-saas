import { Component, AfterViewInit,OnChanges, OnInit } from '@angular/core';
import { Helpers } from "../../helpers";

@Component({
  selector: '[app-sidebar]',
  templateUrl: './app-sidebar.component.html'
})
export class AppSidebar implements OnInit,AfterViewInit {
  ngOnInit() {

  }
  translate_data: any;
  ngAfterViewInit()  {
    let that = this;
    setTimeout(() => {
      that.translate_data = Helpers.translate.data;
    });
    Helpers.translate.do(()=>{
      that.translate_data = Helpers.translate.data;
      console.log(that.translate_data.Expenses)
    })
    }
}
