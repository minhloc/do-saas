import { NgModule,ApplicationRef } from '@angular/core';
import { HttpClientModule,HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { LayoutComponent } from './/layouts/layout.component';

import { HomeComponent } from './pages/home/home.component';
import { FinanceComponent } from './pages/finance/finance.component';
import { LoginComponent } from './pages/login/login.component';
import { Error404Component } from './pages/error-404/error-404.component';
import { Error4042Component } from './pages/error-404-2/error-404-2.component';
import { Error403Component } from './pages/error-403/error-403.component';
import { Error500Component } from './pages/error-500/error-500.component';
import { MaintenanceComponent } from './pages/maintenance/maintenance.component';

// Truong Khuong add [
import { ProfileComponent } from './pages/profile/profile.component';
import { ProductsComponent } from './pages/ecommerce/products/products.component';
import { ProductDetailsComponent } from './pages/ecommerce/products/product-details.component';
import { CustomersComponent } from './pages/ecommerce/customers/customers.component';
import { SaleComponent } from './pages/finance/sale.component';
import { PurchaseComponent } from './pages/finance/purchase.component';
import { ReceiptPaymentComponent } from './pages/receipt-payment/receipt-payment.component';
import { ExpensesComponent } from './pages/expenses/expenses.component';
import { CrmComponent } from './pages/crm/crm.component';
import { CrmDetailComponent } from './pages/crm/crm-detail.component';
import { AccountsComponent } from './pages/finance/accounts/accounts.component';
import { TaxReportsComponent } from './pages/finance/tax-reports/tax-reports.component';
import { WithHoldingComponent } from './pages/finance/with-holding/with-holding.component';
import { SalesComponent } from './pages/finance/sales/sales.component';
import { SalesInfoComponent } from './pages/sales/sales.component';
import { PurchasesComponent } from './pages/finance/purchases/purchases.component';
import { PurchasesInfoComponent } from './pages/purchases/purchases.component';
import { JournalComponent } from './pages/finance/journal/journal.component';
import { StockComponent } from './pages/stock/stock.component';
import { BankComponent } from './pages/finance/bank/bank.component';
import { ActivityComponent } from './pages/finance/activity/activity.component';
import { BusPostComponent } from './pages/posting-group/general/business/bus-post.component';
import { ProdPostComponent } from './pages/posting-group/general/product/prod-post.component';
import { SetupPostComponent } from './pages/posting-group/general/setup/setup-post.component';
import { VatSetupPostComponent } from './pages/posting-group/vat/setup/vat-setup-post.component';
import { CustPostComponent } from './pages/posting-group/customer/cust-post.component';
import { VenderPostComponent } from './pages/posting-group/vender/vender-post.component';
import { BankPostComponent } from './pages/posting-group/bank/bank-post.component';
import { InventoryPostComponent } from './pages/posting-group/inventory/inventory-post.component';
import { InvtSetupPostComponent } from './pages/posting-group/invt-setup/invt-setup-post.component';
import { VatBusPostComponent } from './pages/posting-group/vat/business/vat-bus-post.component';
import { VatProdPostComponent } from './pages/posting-group/vat/product/vat-prod-post.component';
import { ItemJournalsComponent } from './pages/inventory-costing/item-journals/item-journals.component';
import { Activity360Component } from './pages/360/360.component';
import { UsersComponent } from './pages/users/users.component';
import { FormulasComponent } from './pages/formulas/formulas.component';
import { CRMSalesComponent } from './pages/crm-sales/crm-sales.component';
import { BusMigrationComponent } from './pages/migration/business-partner/bus-migration.component';
import { ProdMigrationComponent } from './pages/migration/product/prod-migration.component';
import { SalesMigrationComponent } from './pages/migration/sales/sales-migration.component';
import { NoneItemSetupComponent } from './pages/posting-group/general/none-item-setup/none-item-setup.component';

import { AgmCoreModule } from '@agm/core';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';

import { ActivityModule } from './widgets/activity/activity.module';
import { AccountModule } from './widgets/account/account.module';
import { GeneralBussinessPtgModule } from './widgets/ptg/general/business/ptg-gen-business.module';
import { GeneralProductPtgModule } from './widgets/ptg/general/product/ptg-gen-product.module';
import { GeneralSetupPtgModule } from './widgets/ptg/general/setup/ptg-gen-setup.module';
import { VatSetupPtgModule } from './widgets/ptg/vat/setup/ptg-vat-setup.module';
import { NoneItemSetupModule } from './widgets/ptg/general/none-item-setup/none-item-setup.module';
import { VenderPtgModule } from './widgets/ptg/vender/ptg-vender.module';
import { BankPtgModule } from './widgets/ptg/bank/ptg-bank.module';
import { InventoryPtgModule } from './widgets/ptg/inventory/ptg-inventory.module';
import { InvtSetupPtgModule } from './widgets/ptg/invt-setup/ptg-invt-setup.module';
import { VatBussinessPtgModule } from './widgets/ptg/vat/business/ptg-vat-business.module';
import { VatProductPtgModule } from './widgets/ptg/vat/product/ptg-vat-product.module';
import { ItemGroupModule } from './widgets/item-group/item-group.module';
import { ItemsModule } from './widgets/items/items.module';
import { PartnerModule } from './widgets/partner/partner.module';
import { UomModule } from './widgets/uom/uom.module';
import { LocationModule } from './widgets/location/location.module';
import { UsersModule } from './widgets/users/users.module';
import { CompanyModule } from './widgets/company/company.module';
import { CustomerPtgModule } from './widgets/ptg/customer/ptg-customer.module';
import { FormulasModule } from './widgets/formulas/formulas.module';
// ]
import { TranslatePipe } from './translate.pipe';
const routes: Routes = [
    {path: '', redirectTo: 'index', pathMatch: 'full'},
    {
        "path": "",
        "component": LayoutComponent,
        "children": [
            {
                path: "index",
                pathMatch: 'full',
                redirectTo: '360/customers/my'
            },{
                path: "360",
                pathMatch: 'full',
                redirectTo: '360/customers/my'
            },{
                path: "360/my-activity",
                pathMatch: 'full',
                redirectTo: '360/customers/my'
            },{
                path: "360/all-activity",
                pathMatch: 'full',
                redirectTo: '360/customers'
            },
            // ===[ CUSTOMER
            {
                path: "360/customers/my",
                component: CrmComponent,
                data: {
                    breadcrumbs: [{
                        link:'/360',
                        text:'360'
                    },{
                        text:'My Customers'
                    }],
                    is360: true,
                    partner_type:'customers',
                    my: true,
                    favourites: false,
                    type: 'sales',
                }
            },{
                path: "360/customers",
                component: CrmComponent,
                data: {
                    breadcrumbs: [{
                        link:'/360',
                        text:'360'
                    },{
                        text:'Customers'
                    }],
                    is360: true,
                    partner_type:'customers',
                    my: false,
                    favourites: false,
                    type: 'sales',
                }
            },{
                path: "360/customers/favourites/my",
                component: CrmComponent,
                data: {
                    breadcrumbs: [{
                        link:'/360',
                        text:'360'
                    },{
                        link:'/360/customers/my',
                        text:'My Customers'
                    },{
                        text:'Favourites'
                    }],
                    is360: true,
                    partner_type:'customers',
                    my: true,
                    favourites: true,
                    type: 'sales',
                }
            },{
                path: "360/customers/favourites",
                component: CrmComponent,
                data: {
                    breadcrumbs: [{
                        link:'/360',
                        text:'360'
                    },{
                        link:'/360/customers/my',
                        text:'Customers'
                    },{
                        text:'Favourites'
                    }],
                    is360: true,
                    partner_type:'customers',
                    my: false,
                    favourites: true,
                    type: 'sales',
                }
            },{
                path: "360/customers/add",
                component: CrmDetailComponent,
                data: {
                    breadcrumbs: [{
                        link:'/360',
                        text:'360'
                    },{
                        link:'/360/customers',
                        text:'Customers'
                    },{
                        text:'Create a new Customer'
                    }],
                    is360: true,
                    my: false,
                    favourites: false,
                    partner_type:'customers',
                    type: 'sales',
                }
            },{
                path: "360/customers/:id",
                component: CrmDetailComponent,
                data: {
                    breadcrumbs: [{
                        link:'/360',
                        text:'360'
                    },{
                        link:'/360/customers',
                        text:'Customers'
                    },{
                        text:'Customer Card'
                    }],
                    is360:true,
                    type: 'sales',
                    partner_type:'customers',
                }
            },
            // CUSTOMER ]===
            // ===[ SUPPLIERS
            {
                path: "360/suppliers/my",
                component: CrmComponent,
                data: {
                    breadcrumbs: [{
                        link:'/360',
                        text:'360'
                    },{
                        text:'My Suppliers'
                    }],
                    is360: true,
                    partner_type:'suppliers',
                    my: true,
                    favourites: false,
                    type: 'sales',
                }
            },{
                path: "360/suppliers",
                component: CrmComponent,
                data: {
                    breadcrumbs: [{
                        link:'/360',
                        text:'360'
                    },{
                        text:'Suppliers'
                    }],
                    is360: true,
                    partner_type:'suppliers',
                    my: false,
                    favourites: false,
                    type: 'sales',
                }
            },{
                path: "360/suppliers/favourites/my",
                component: CrmComponent,
                data: {
                    breadcrumbs: [{
                        link:'/360',
                        text:'360'
                    },{
                        link:'/360/suppliers/my',
                        text:'My Suppliers'
                    },{
                        text:'Favourites'
                    }],
                    is360: true,
                    partner_type:'suppliers',
                    my: true,
                    favourites: true,
                    type: 'sales',
                }
            },{
                path: "360/suppliers/favourites",
                component: CrmComponent,
                data: {
                    breadcrumbs: [{
                        link:'/360',
                        text:'360'
                    },{
                        link:'/360/suppliers/my',
                        text:'Suppliers'
                    },{
                        text:'Favourites'
                    }],
                    is360: true,
                    partner_type:'suppliers',
                    my: false,
                    favourites: true,
                    type: 'sales',
                }
            },{
                path: "360/suppliers/add",
                component: CrmDetailComponent,
                data: {
                    breadcrumbs: [{
                        link:'/360',
                        text:'360'
                    },{
                        link:'/360/suppliers',
                        text:'Customers'
                    },{
                        text:'Create a new Supplier'
                    }],
                    is360: true,
                    my: false,
                    favourites: false,
                    partner_type:'suppliers',
                    type: 'sales',
                }
            },{
                path: "360/suppliers/:id",
                component: CrmDetailComponent,
                data: {
                    breadcrumbs: [{
                        link:'/360',
                        text:'360'
                    },{
                        link:'/360/suppliers',
                        text:'Suppliers'
                    },{
                        text:'Supplier Card'
                    }],
                    is360:true,
                    type: 'sales',
                    partner_type:'suppliers',
                }
            },
            // SUPPLIERS ]===
            {
                path: "360/sales/:type",
                component: SalesComponent,
                data: {
                    breadcrumbs: [{
                        link:'/360',
                        text:'360'
                    },{
                        link:'/360/sales',
                        text:'Sales'
                    }],
                    is360: true,
                    finance: true,
                    items: ['invoices','credit-notes']
                }
            },
            {
                path: "360/sales/:type/my",
                component: SalesComponent,
                data: {
                    breadcrumbs: [{
                        link:'/360',
                        text:'360'
                    },{
                        link:'/360/sales',
                        text:'Sales'
                    }],
                    is360: true,
                    my: true,
                    finance: true,
                    items: ['invoices','credit-notes']
                }
            },
            {
                path: "360/purchases/:type",
                component: PurchasesComponent,
                data: {
                    breadcrumbs: [{
                        link:'/360',
                        text:'360'
                    },{
                        link:'/360/purchases',
                        text:'Purchase'
                    }],
                    is360: true,
                    items: ['supplier','orders']
                }
            },
            {
                path: "360/purchases/:type/:my",
                component: PurchasesComponent,
                data: {
                    breadcrumbs: [{
                        link:'/360',
                        text:'360'
                    },{
                        link:'/360/purchases',
                        text:'Purchase'
                    }],
                    is360: true,
                    items: ['invoices','credit-notes'],
                    my:true

                }
            },














            
            {
                path: "profile",
                component: ProfileComponent
            },
            

            {
                path: "sales/customers",
                component: CrmComponent,
                data: {
                    breadcrumbs: [{
                        link:'/sales',
                        text:'Sales'
                    },{
                        text:'Customers'
                    }],
                    type: 'sales',
                    partner_type:'customers'
                }
            },
            {
                path: "sales/favourites/customers",
                component: CrmComponent,
                data: {
                    breadcrumbs: [{
                        link:'/sales',
                        text:'Sales'
                    },{
                        link:'/sales/customers',
                        text:'Customers'
                    },{
                        text:'Favourites'
                    }],
                    type: 'sales',
                    partner_type:'customers',
                    favourites:true,
                }
            },
          
            
            {
                path: "purchases/suppliers",
                component: CrmComponent,
                data: {
                    breadcrumbs: [{
                        link:'/purchases',
                        text:'Purchase'
                    },{
                        text:'Suppliers'
                    }],
                    type: 'purchases',
                    partner_type:'suppliers'
                }
            },
            {
                path: "purchases/favourites/suppliers",
                component: CrmComponent,
                data: {
                    breadcrumbs: [{
                        link:'/purchases',
                        text:'Purchase'
                    },{
                        link:'/purchases/suppliers',
                        text:'Suppliers'
                    },{
                        text:'Favourites'
                    }],
                    type: 'purchases',
                    partner_type:'suppliers',
                    favourites:true,
                }
            },
            
            {
                path: "sales/customers/add",
                component: CrmDetailComponent,
                data: {
                    breadcrumbs: [{
                        link:'/sales',
                        text:'Sales'
                    },{
                        link:'/sales/customers',
                        text:'Customers'
                    },{
                        text:'Create a new Customer'
                    }],
                    type: 'sales',
                    partner_type:'customers',
                }
            },
            {
                path: "sales/customers/:id",
                component: CrmDetailComponent,
                data: {
                    breadcrumbs: [{
                        link:'/sales',
                        text:'Sales'
                    },{
                        link:'/sales/customers',
                        text:'Customers'
                    },{
                        text:'Customer Card'
                    }],
                    type: 'sales',
                    partner_type:'customers',
                }
            },
            {
                path: "sales/customers/:id/:type",
                component: SalesComponent,
                data: {
                    breadcrumbs: [{
                        link:'/sales',
                        text:'Sales'
                    },{
                        link:'/sales/customers',
                        text:'Customers'
                    }],
                    type: 'sales',
                    partner_type:'customers',
                    items: ['invoices','credit-notes']
                }
            },
            
            
            {
                path: "purchases/suppliers/add",
                component: CrmDetailComponent,
                data: {
                    breadcrumbs: [{
                        link:'/purchases',
                        text:'Purchase'
                    },{
                        link:'/purchases/suppliers',
                        text:'Suppliers'
                    },{
                        text:'Create a new Supplier'
                    }],
                    type: 'purchases',
                    partner_type:'suppliers',
                }
            },
            {
                path: "purchases/suppliers/:id",
                component: CrmDetailComponent,
                data: {
                    breadcrumbs: [{
                        link:'/purchases',
                        text:'Purchase'
                    },{
                        link:'/purchases/suppliers',
                        text:'Suppliers'
                    },{
                        text:'Supplier Card'
                    }],
                    type: 'purchases',
                    partner_type:'suppliers',
                }
            },
            
            {
                path: "crm",
                // pathMatch: 'full',
                component: CRMSalesComponent
            },
            {
                path: "expenses",
                pathMatch: 'full',
                redirectTo: 'expenses/my'
                // component: CrmComponent
            },
            {
                path: "expenses/:type",
                component: ExpensesComponent
            },


            {
                path: "finance",
                component: FinanceComponent
            },
            {
                path: "finance/accounts",
                component: AccountsComponent
            },
            {
                path: "finance/tax-reports",
                component: TaxReportsComponent
            },
            {
                path: "finance/with-holding-tax",
                component: WithHoldingComponent
            },

            {
                path: "finance/journal-entry",
                component: JournalComponent
            },
            {
                path: "finance/bank",
                component: BankComponent,
            },
            {
                path: "finance/activity",
                component: ActivityComponent,
                data: {
                    breadcrumbs: [{
                        link:'/finance',
                        text:'Finance'
                    },{
                        link:'',
                        text:'Expense Activity Setting'
                    }],
                }
            },
            {
                path: "activity",
                component: ActivityComponent,
                data: {
                    breadcrumbs: [{
                        link:'',
                        text:'Expense Activity Setting'
                    }],
                }
            },

            {
                path: "finance/sales",
                component: SaleComponent
            },
            {
                path: "sales",
                // component: SalesInfoComponent,
                pathMatch: 'full',
                redirectTo: 'sales/quotes'
            },
            {
                path: "360/sales",
                // component: SalesInfoComponent,
                pathMatch: 'full',
                redirectTo: '360/sales/quotes'
            },
            {
                path: "360/purchases",
                // component: SalesInfoComponent,
                pathMatch: 'full',
                redirectTo: '360/purchases/orders'
            },
           
            {
                path: "finance/sales/:type",
                component: SalesComponent,
                data: {
                    breadcrumbs: [{
                        link:'/finance',
                        text:'Finance'
                    // },{
                    //     link:'/finance/sales',
                    //     text:'Sales'
                    }],
                    finance: true,
                    items: ['invoices','credit-notes']
                }
            },
            {
                path: "sales/:type",
                component: SalesComponent,
                data: {
                    breadcrumbs: [{
                        link:'/sales',
                        text:'Sales'
                    }],
                    items: ['customer','quotes','orders']
                }
            },
            {
                path: "finance/purchases",
                component: PurchaseComponent,
                data:{
                    breadcrumbs: [{
                        link:'/finance',
                        text:'Finance'
                    }],
                    finance: true,
                }
            },
            {
                path: "purchases",
                // component: PurchasesInfoComponent
                pathMatch: 'full',
                redirectTo: 'purchases/orders'
            },
            {
                path: "finance/purchases/:type",
                component: PurchasesComponent,
                data: {
                    breadcrumbs: [{
                        link:'/finance',
                        text:'Finance'
                    // },{
                    //     link:'/finance/purchases',
                    //     text:'Purchases'
                    }],
                    items: ['invoices','credit-notes'],
                    finance: true,
                }
            },
            
            {
                path: "purchases/:type",
                component: PurchasesComponent,
                data: {
                    breadcrumbs: [{
                        link:'/purchases',
                        text:'Purchases'
                    }],
                    finance: false,
                    items: ['supplier','orders']
                }
            },
            

            // {
            //     path: "360",
            //     pathMatch: 'full',
            //     redirectTo: '360/sales/mycustomers'
            //     // component: CrmComponent
            // },
            // {
            //     path: "360/:type",
            //     component: Activity360Component
            // },

            {
                path: "stock",
                component: StockComponent
            },

            {
                path: "posting-group/general/business",
                component: BusPostComponent
            },
            {
                path: "posting-group/general/product",
                component: ProdPostComponent
            },
            {
                path: "posting-group/general/setup",
                component: SetupPostComponent
            },
            {
                path: "posting-group/general/none-item-setup",
                component: NoneItemSetupComponent
            },
            {
                path: "posting-group/customer",
                component: CustPostComponent
            },
            {
                path: "posting-group/vender",
                component: VenderPostComponent
            },
            {
                path: "posting-group/bank",
                component: BankPostComponent
            },
            {
                path: "posting-group/inventory",
                component: InventoryPostComponent
            },
            {
                path: "posting-group/inventory-setup",
                component: InvtSetupPostComponent
            },
            {
                path: "posting-group/vat/business",
                component: VatBusPostComponent
            },
            {
                path: "posting-group/vat/product",
                component: VatProdPostComponent
            },
            {
                path: "posting-group/vat/setup",
                component: VatSetupPostComponent
            },
            {
                path: "inventory-costing/item-journals",
                component: ItemJournalsComponent
            },{
                path: "users",
                component: UsersComponent
            },{
                path: "formulas",
                component: FormulasComponent
            },{
                path: "migration/business-partner",
                component: BusMigrationComponent
            },{
                path: "migration/sales",
                component: SalesMigrationComponent
            },{
                path: "migration/product",
                component: ProdMigrationComponent
            }
            // ]
        ]
    },
    {
        "path": "login",
        "component": LoginComponent
    },
    {
        "path": "error_404",
        "component": Error404Component
    },
    {
        "path": "error_404-2",
        "component": Error4042Component
    },
    {
        "path": "error_403",
        "component": Error403Component
    },
    {
        "path": "error_500",
        "component": Error500Component
    },
    {
        "path": "maintenance",
        "component": MaintenanceComponent
    },
    {
        "path": "**",
        "redirectTo": "error_404",
        "pathMatch": "full"
    },
];
import { jqxDropDownButtonComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxdropdownbutton';
import { jqxGridComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxgrid';
import { jqxComboBoxComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxcombobox';
import { jqxDropDownListComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxdropdownlist';
import { jqxDateTimeInputComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxdatetimeinput';
import { jqxValidatorComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxvalidator';
import { jqxFileUploadComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxfileupload';
import { jqxWindowComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxwindow';
import { jqxTabsComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxtabs';
import { jqxNumberInputComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxnumberinput';
import { jqxPanelComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxpanel';
import { jqxChartComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxchart';
import { jqxTextAreaComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxtextarea';
@NgModule({
  declarations: [
      TranslatePipe,
    HomeComponent,
    LoginComponent,
    Error404Component,
    Error4042Component,
    Error403Component,
    Error500Component,
    MaintenanceComponent,
    // Truong Khuong add [
    CustomersComponent,
    ProductsComponent,
    ProductDetailsComponent,
    ReceiptPaymentComponent,
    ExpensesComponent,

    FinanceComponent,
    AccountsComponent,
    TaxReportsComponent,
    WithHoldingComponent,
    JournalComponent,
    SaleComponent,
    SalesComponent,
    SalesInfoComponent,
    PurchaseComponent,
    PurchasesComponent,
    PurchasesInfoComponent,
    CrmComponent,
    CrmDetailComponent,
    StockComponent,
    BankComponent,
    ActivityComponent,
    BusPostComponent,
    ProdPostComponent,
    SetupPostComponent,
    CustPostComponent,
    VenderPostComponent,
    BankPostComponent,
    InventoryPostComponent,
    InvtSetupPostComponent,
    VatBusPostComponent,
    VatProdPostComponent,
    VatSetupPostComponent,
    ItemJournalsComponent,
    Activity360Component,
    UsersComponent,
    FormulasComponent,
    CRMSalesComponent,
    BusMigrationComponent,
    ProdMigrationComponent,
    SalesMigrationComponent,
    NoneItemSetupComponent,
    ProfileComponent,

    jqxGridComponent,
    jqxComboBoxComponent,
    jqxDropDownListComponent,
    jqxDateTimeInputComponent,
    jqxValidatorComponent,
    jqxFileUploadComponent,
    jqxWindowComponent,
    jqxTabsComponent,
    jqxDropDownButtonComponent,
    jqxNumberInputComponent,
    jqxPanelComponent,
    jqxChartComponent,
    jqxTextAreaComponent,

    ActivityModule,
    AccountModule,
    GeneralBussinessPtgModule,
    GeneralProductPtgModule,
    GeneralSetupPtgModule,
    CustomerPtgModule,
    VenderPtgModule,
    BankPtgModule,
    InventoryPtgModule,
    InvtSetupPtgModule,
    VatBussinessPtgModule,
    VatProductPtgModule,
    VatSetupPtgModule,
    ItemGroupModule,
    ItemsModule,
    PartnerModule,
    UomModule,
    LocationModule,
    UsersModule,
    CompanyModule,
    FormulasModule,
    NoneItemSetupModule
    // ]
  ],
    imports: [ 
        RouterModule.forRoot(routes),CommonModule,FormsModule,
        ReactiveFormsModule,
        AgmCoreModule.forRoot({
          apiKey: 'AIzaSyAZMjE-rC-H-PrtnwrGurrwJfdvTQrbuMk'
        }),
    ],
    exports: [ 
        // TranslateModule,
        TranslatePipe,
        RouterModule,
        ActivityModule,
        AccountModule,
        GeneralBussinessPtgModule,
        GeneralProductPtgModule,
        GeneralSetupPtgModule,
        CustomerPtgModule,
        VenderPtgModule,
        BankPtgModule,
        InventoryPtgModule,
        InvtSetupPtgModule,
        VatBussinessPtgModule,
        VatProductPtgModule,
        VatSetupPtgModule,
        ItemGroupModule,
        ItemsModule,
        PartnerModule,
        UomModule,
        LocationModule,
        UsersModule,
        CompanyModule,
        FormulasModule,
        NoneItemSetupModule
    ]
})

export class AppRoutingModule { }
