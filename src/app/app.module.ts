import { BrowserModule } from '@angular/platform-browser';
import { NgModule,APP_INITIALIZER } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule,HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormControl, FormGroup } from '@angular/forms';
// import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './/app.component';

import { AppRoutingModule } from './/app-routing.module';
import { LayoutModule } from './/layouts/layout.module';
import { ScriptLoaderService } from './_services/script-loader.service';
import { APIService } from './/api.service';
import { AppSetting } from ".//settings";
import { SystemService } from ".//system.service";
import { AppMessage } from ".//message";
import { AppHttpInterceptor } from './AppHttpInterceptor';
import { TranslateService } from './translate.service';
// import { TranslatePipe } from './translate.pipe';
import { FormsModule } from '@angular/forms';
export function setupTranslateFactory(
    service: TranslateService): Function {
    let lang = localStorage.getItem('lang') || 'en';
    return () => service.use(lang);
}
@NgModule({
    declarations: [
      AppComponent
    ],
    imports: [
        BrowserModule,
	    CommonModule,
        AppRoutingModule,
        LayoutModule,
	    HttpClientModule,
        FormsModule,
        // TranslatePipe
    ],
    providers: [
        // TranslatePipe,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AppHttpInterceptor,
            multi: true
        },
        ScriptLoaderService, APIService, AppSetting, AppMessage,SystemService,
        TranslateService,
        {
            provide: APP_INITIALIZER,
            useFactory: setupTranslateFactory,
            deps: [ TranslateService ],
            multi: true
        }
    ],
    bootstrap: [AppComponent]
})
export class AppModule { } 
