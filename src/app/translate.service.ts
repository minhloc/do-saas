import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable()
export class TranslateService {
    data: any = {};
    activeLang;
    constructor(private http: HttpClient) {

    }
    use(lang: string): Promise<{}> {
        localStorage.setItem('lang',lang);
        return new Promise<{}>((resolve, reject) => {
            const langPath = `assets/i18n/${lang || 'en'}.json`;
            this.activeLang = lang;
            this.http.get<{}>(langPath).subscribe(
                translation => {
                    this.data = Object.assign(this.data, translation || {});
                    resolve(this.data);
                    this.funs.map((fun:any,i:any)=>{
                        console.log('call fun')
                        fun();
                    })
                },
                error => {
                    this.data = {};
                    console.log('ERROR')
                    resolve(this.data);
                }
            );
        });
    }
    get(key: any){
        if(typeof key == 'string')
            return this.data[key] || key;
        else if(typeof key == 'object')
            return key.map((k)=>{return this.data[k] || k})
    }
    funs = []
    do(fun:any){
        this.funs.push(fun)
    }
}