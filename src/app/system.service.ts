import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { APIService } from './api.service';
import { AppSetting } from "./settings";
@Injectable()
export class SystemService {
    constructor(
        private api: APIService
        ) {
    }
    Summary(){
        let that= this;
        var promise = new Promise((resolve) => {
            that.api.post({
                url: AppSetting.SUMMARY_URL,
            }).done(function(res){
                resolve(res);
            })
        });
        return promise;
    }
    SaleInfo(my = false) {
        let that= this;
        var promise = new Promise((resolve) => {
            that.api.post({
                url: AppSetting.SALES_INFO_URL,
                params: {my:my}
            }).done(function(res){
                resolve(res);
            })
        });
        return promise;
    }
    PurchaseInfo(my = false) {
        let that= this;
        var promise = new Promise((resolve) => {
            that.api.post({
                url: AppSetting.PURCHASES_INFO_URL,
                params: {my:my}
            }).done(function(res){
                resolve(res);
            })
        });
        return promise;
    }
    FinanceInfo() {
        let that= this;
        var promise = new Promise((resolve) => {
            that.api.post({
                url: AppSetting.FINANCE_INFO_URL
            }).done(function(res){
                resolve(res);
            })
        });
        return promise;
    }
    PartnerInfo(id,my = false) {
        let that= this;
        var promise = new Promise((resolve) => {
            that.api.post({
                url: AppSetting.PARTNER_INFO_URL,
                params:{
                    partner_id:id,
                    my:my
                }
            }).done(function(res){
                resolve(res);
            })
        });
        return promise;
    }
    Dashboard() {
        let that= this;
        var promise = new Promise((resolve) => {
            that.api.post({
                url: AppSetting.ANALYTICS_DASHBOARD_URL,
                params:{
                }
            }).done(function(res){
                resolve(res);
            })
        });
        return promise;
    }
    DashboardV1() {
        let that= this;
        var promise = new Promise((resolve) => {
            that.api.post({
                url: AppSetting.ANALYTICS_DASHBOARDV1_URL,
                params:{
                }
            }).done(function(res){
                resolve(res);
            })
        });
        return promise;
    }
}