import * as $ from "jquery";

declare var toastr: any;
declare var jQuery:any;
declare var $:any;
declare var Clipboard:any;
declare var window:any;

export class Helpers {
	static setLoading(loading) {
		let body = $('body');
		if (loading) {
			$('.preloader-backdrop').fadeIn(200);
		} else {
			$('.preloader-backdrop').fadeOut(200);
		}
	}

	static bodyClass(Class) {
		$('body').attr('class', Class);
	}

	static initLayout() {
	    // SIDEBAR ACTIVATE METISMENU
		$(".metismenu").metisMenu();

		// SIDEBAR TOGGLE ACTION
	    $('.js-sidebar-toggler').click(function() {
	        if( $('body').hasClass('drawer-sidebar') ) {
	            $('#sidebar').backdrop();
	        } else {
	            $('body').toggleClass('sidebar-mini');
	            if(! $('body').hasClass('sidebar-mini') ) {
	                $('#sidebar-collapse').hide();
	                setTimeout(function () {
	                    $('#sidebar-collapse').fadeIn(300,function(){
	                    });
	                }, 300);
	            }
	            setTimeout(function () {
		            $(window).trigger('resize')
		        }, 320)
	        }
	    });

	    // QUICK SIDEBAR TOGGLE ACTION
	    $('.quick-sidebar-toggler').click(function(){
	        $('.quick-sidebar').backdrop();
	    });
	    $('.sidenav-backdrop.backdrop').click(function(){
	    	$('.quick-sidebar').removeClass('shined');
        	$('body').removeClass('has-backdrop');
       
	    })

	    // SEARCH BAR ACTION
	    $('.js-search-toggler').click(function() {
	        $('.search-top-bar').backdrop().find('.search-input').focus();
	    });

	    // Session timeout
	    
	    var idle_timer;
	    (function(){
	        $('#timeout-activate').click(function(){
	            if(+$('#timeout-count').val()) {
	                activate(+$('#timeout-count').val());
	            }
	        });

	        $('#timeout-reset').click(function(){
	            reset();
	        });

	        function reset(){
	            $( document ).idleTimer("destroy");
	            if(idle_timer) clearTimeout(idle_timer);
	            $('#session-dialog').modal('hide');
	            $('.timeout-toggler').removeClass('active');
	            $('#timeout-reset-box').hide();
	            $('#timeout-activate-box').show();
	        }

	        function activate(count){
	            $('#session-dialog').modal('hide');
	            $('#timeout-reset-box').show();
	            $('#timeout-activate-box').hide();
	            $( document ).idleTimer( count * 60000 );
	            
	            setTimeout(function(){
	                $('.timeout-toggler').addClass('active');
	            },(count-1) * 60000);

	            $( document ).on( "idle.idleTimer", function(event, elem, obj){
	                // function you want to fire when the user goes idle
	                toastr.warning('Your session is about to expire. The page will redirect after 15 seconds with no activity.','Session Timeout Notification',{
	                    "progressBar": true,
	                    "timeOut": 5000,
	                });
	                idle_timer = setTimeout(timeOutHandler,5000);
	            });

	            $( document ).on( "active.idleTimer", function(event, elem, obj, triggerevent){
	                // function you want to fire when the user becomes active again
	                clearTimeout(idle_timer);
	                $( document ).idleTimer("reset");
	                toastr.clear();
	                toastr.success('You returned to the active mode.','You are back.');
	            });

	            function timeOutHandler() {
	                reset();
	                alert('Your session has expired. You can redirect this page or logout.');
	            }
	        }
	    })();
	}

	static initPage() {
		$('.jqx-validator-hint').remove()
	    // Activate bootstrap select
	    if($(".selectpicker").length>0) {
	        $('.selectpicker').selectpicker();
	    }

	    // Activate Tooltips
	    $('[data-toggle="tooltip"]').tooltip();

	    // Activate Popovers
	    $('[data-toggle="popover"]').popover();

	    // Activate slimscroll
	    $('.scroller').each(function(){
	        $(this).slimScroll({
	            height: $(this).attr('data-height') || '100%',
	            color: $(this).attr('data-color') || '#71808f',
	            railOpacity: '0.9',
	            size: '4px',
	        });
	    });

	    $('.slimScrollBar').hide();

	    
	    // Pre Copy to clipboard

	    if($(".clipboard-copy").length>0) {
	        new Clipboard('.clipboard-copy', {
	            target: function (t) {
	                return t.nextElementSibling;
	            }
	        }).on('success', function (e) {
	            e.clearSelection();
	            e.trigger.textContent = 'Copied';
	            window.setTimeout(function () {
	                e.trigger.textContent = 'Copy';
	            }, 2000);
	        });
	    }

		// PANEL ACTIONS
	    // ======================

	    $('.ibox-collapse').click(function(){
	    	var ibox = $(this).closest('div.ibox');
	    	ibox.toggleClass('collapsed-mode').children('.ibox-body').slideToggle(200);
	    });
	    $('.ibox-remove').click(function(){
	    	$(this).closest('div.ibox').remove();
	    });
	    $('.fullscreen-link').click(function(){
	        if($('body').hasClass('fullscreen-mode')) {
	            $('body').removeClass('fullscreen-mode');
	            $(this).closest('div.ibox').removeClass('ibox-fullscreen');
	            $(window).off('keydown',toggleFullscreen);
	        } else {
	            $('body').addClass('fullscreen-mode');
	            $(this).closest('div.ibox').addClass('ibox-fullscreen');
	            $(window).on('keydown', toggleFullscreen);
	        }
	    });
	    function toggleFullscreen(e) {
	        // pressing the ESC key - KEY_ESC = 27 
	        if(e.which == 27) {
	            $('body').removeClass('fullscreen-mode');
	            $('.ibox-fullscreen').removeClass('ibox-fullscreen');
	            $(window).off('keydown',toggleFullscreen);
	        }
	    }

	}
	static translate: any = {
		get: function(key){return key},
		// activeLang: '',
	};
	static alert(message,callback = null) {
		var $elm = $([
		      	'<div class="modal modal-big modal-alert fade show" id="alert-dialog">',
		        '<div class="modal-dialog" style="width:480px;" role="document">',
		          '<div class="modal-content ">',
		            '<div class="modal-body">',
		            	'<button class="close" data-dismiss="modal" aria-label="Close"></button>',
		              	'<p class="text-center mb-4">...</p>',
		              	'<div class="text-center">',
		              		'<button class="btn btn-outline-secondary">',
		              			this.translate.get('Ok'),
		              		'</button>',
		            	'</div>',
		            '</div>',
		          '</div>',
		        '</div>',
		      '</div>'
		      ].join(''))
		$('body').append($elm);

      	$elm.find('.modal-body .btn').click(function(){
      		$elm.modal('hide');
      		if(typeof callback == 'function') callback();
      	})
      	$elm.find('.modal-body>p').html(message)
      	$elm.modal('show');
      	$elm.on("hidden.bs.modal", function(){
		    $elm.remove()
		});
		return {
			set:function(opt,value){

			}
		}
	}
	static confirm(message,callback = null) {
		var $elm = $([
		      '<div class="modal modal-big modal-confirm fade show" id="alert-dialog">',
		        '<div class="modal-dialog" style="width:480px;" role="document">',
		          '<div class="modal-content ">',
		            '<div class="modal-body">',
		            	'<button class="close" data-dismiss="modal" aria-label="Close"></button>',
		              	'<p class="text-center mb-4">...</p>',
		              	'<div class="text-center">',
		              		'<button class="btn mr-1 btn-primary ok-btn">',
		              			this.translate.get('Ok'),
		              		'</button>',
		              		'<button class="btn btn-danger cancel-btn" data-dismiss="modal">',
		              			this.translate.get('Cancel'),
		              		'</button>',
		            	'</div>',
		            '</div>',
		          '</div>',
		        '</div>',
		      '</div>'
		      ].join(''))
		$('body').append($elm);

      	$elm.find('.modal-body .ok-btn').click(function(){
      		$elm.modal('hide');
      		if(typeof callback == 'function') callback();
      	})
      	$elm.find('.modal-body>p').html(message)
      	$elm.modal('show');
      	$elm.on("hidden.bs.modal", function(){
		    $elm.remove()
		});
		return {
			set:function(opt,value = null){
				if(opt.oktext){
					$elm.find('.ok-btn').text(opt.oktext)
				}
				if(opt.canceltext){
					$elm.find('.cancel-btn').text(opt.canceltext)
				}
			}
		}
	}
	static Source(source){
		var token = localStorage.getItem('token');
		source.beforeSend=function (xhr, settings){
			xhr.setRequestHeader ('Authorization', 'bearer ' + token);
			
        }
        return source;
	}
	static getBase64(file) {
	  return new Promise((resolve, reject) => {
	    const reader = new FileReader();
	    reader.readAsDataURL(file);
	    reader.onload = () => resolve(reader.result);
	    reader.onerror = error => reject(error);
	  });
	}
	static getLocalizatio(culture) {
	    var localization = null;
	    switch (culture) {
	        case "vi":
	            localization =
	             {
	                 // separator of parts of a date (e.g. '/' in 11/05/1955)
	                 '/': "/",
	                 // separator of parts of a time (e.g. ':' in 05:44 PM)
	                 ':': ":",
	                 // the first day of the week (0 = Sunday, 1 = Monday, etc)
	                 firstDay: 1,
	                 days: {
	                     // full day names
	                     names: ["Chủ nhật", "Thứ 2", "Thứ 3", "Thứ 4", "Thứ 5", "Thứ 6", "Thứ 7"],
	                     // abbreviated day names
	                     namesAbbr: ["CN", "T2", "T3", "T4", "T5", "T6", "T7"],
	                     // shortest day names
	                     namesShort: ["CN", "T2", "T3", "T4", "T5", "T6", "T7"]
	                 },

	                 months: {
	                     // full month names (13 months for lunar calendards -- 13th month should be "" if not lunar)
	                     names: ["Tháng 1", "Tháng 2", "Tháng 3", "Tháng 4", "Tháng 5", "Tháng 6", "Tháng 7", "Tháng 8", "Tháng 9", "Tháng 10", "Tháng 11", "Tháng 12", ""],
	                     // abbreviated month names
	                     namesAbbr: ["Th1", "Th2", "Th3", "Th4", "Th5", "Th6", "Th7", "Th8", "Th9", "Th10", "Th11", "Th12", ""]
	                 },
	                 // AM and PM designators in one of these forms:
	                 // The usual view, and the upper and lower case versions
	                 //      [standard,lowercase,uppercase]
	                 // The culture does not use AM or PM (likely all standard date formats use 24 hour time)
	                 //      null
	                 AM: ["Sáng", "Sáng", "Sáng"],
	                 PM: ["Chiều", "Chiều", "Chiều"],
	                 eras: [
	                 // eras in reverse chronological order.
	                 // name: the name of the era in this culture (e.g. A.D., C.E.)
	                 // start: when the era starts in ticks (gregorian, gmt), null if it is the earliest supported era.
	                 // offset: offset in years from gregorian calendar
	                 { "name": "A.D.", "start": null, "offset": 0 }
	                 ],
	                 twoDigitYearMax: 2029,
	                 patterns:
	                  {
	                      d: "dd.MM.yyyy",
	                      D: "dddd, d. MMMM yyyy",
	                      t: "HH:mm",
	                      T: "HH:mm:ss",
	                      f: "dddd, d. MMMM yyyy HH:mm",
	                      F: "dddd, d. MMMM yyyy HH:mm:ss",
	                      M: "dd MMMM",
	                      Y: "MMMM yyyy"

	                  },
	                 percentsymbol: "%",
	                 currencysymbol: "€",
	                 currencysymbolposition: "after",
	                 decimalseparator: '.',
	                 thousandsseparator: ',',
	                 pagergotopagestring: "Đến trang",
	                 pagershowrowsstring: "Số lượng:",
	                 pagerrangestring: " của ",
	                 pagerpreviousbuttonstring: "Trang trước",
	                 pagernextbuttonstring: "Trang tiếp",
	                 pagerfirstbuttonstring: "Trang đầu",
	                 pagerlastbuttonstring: "Trang cuối",
	                 groupsheaderstring: "Kéo và thả cột vô đây để gom nhóm theo column đó",
	                 sortascendingstring: "Sắp xếp tăng dần",
	                 sortdescendingstring: "Sắp xếp giảm dần",
	                 sortremovestring: "Bỏ sắp xếp",
	                 groupbystring: "Nhóm theo cột này",
	                 groupremovestring: "Bỏ gom nhóm",
	                 filterclearstring: "Bỏ lọc",
	                 filterstring: "Lọc",
	                 filtershowrowstring: "Hiển thị theo điều kiện:",
	                 filterorconditionstring: "hoặc",
	                 filterandconditionstring: "và",
	                 filterselectallstring: "(Chọn tất cả)",
	                 filterchoosestring: "Vui lòng chọn:",
	                 filterstringcomparisonoperators: ['rỗng', 'khác rỗng', 'chứa', 'chứa(match case)',
	                   'không chứa', 'không chứa(phù hợp)', 'bắt đầu với', 'bắt đầu với(phù hợp)',
	                   'kết thúc với', 'kết thúc với(phù hợp)', 'bằng', 'bằng(phù hợp)', 'rỗng', 'khác rỗng'],
	                filternumericcomparisonoperators: ['Bằng', 'Khác', 'nhỏ hơn', 'nhỏ hơn hoặc bằng', 'lớn hơn', 'lớn hơn hoặc bằng', 'rỗng', 'khác rỗng'],
	                filterdatecomparisonoperators: ['bằng', 'khác', 'nhỏ hơn', 'nhỏ hơn hoặc bằng', 'lớn hơn', 'lớn hơn hoặc bằng', 'rỗng', 'khác rỗng'],
	                filterbooleancomparisonoperators: ['bằng', 'khác'],
	                validationstring: "Giá trị nhập vào không hợp lệ",
	                emptydatastring: "Không có dữ liệu để hiển thị",
	                filterselectstring: "Chọn bộ lọc",
	                loadtext: "Chờ tý...",
	                clearstring: "Hủy",
	                todaystring: "Hôm nay"
	             }
	            break;
	        case "en":
	        default:
	            localization =
	            {
	                // separator of parts of a date (e.g. '/' in 11/05/1955)
	                '/': "/",
	                // separator of parts of a time (e.g. ':' in 05:44 PM)
	                ':': ":",
	                // the first day of the week (0 = Sunday, 1 = Monday, etc)
	                firstDay: 0,
	                days: {
	                    // full day names
	                    names: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
	                    // abbreviated day names
	                    namesAbbr: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
	                    // shortest day names
	                    namesShort: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"]
	                },
	                months: {
	                    // full month names (13 months for lunar calendards -- 13th month should be "" if not lunar)
	                    names: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December", ""],
	                    // abbreviated month names
	                    namesAbbr: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec", ""]
	                },
	                // AM and PM designators in one of these forms:
	                // The usual view, and the upper and lower case versions
	                //      [standard,lowercase,uppercase]
	                // The culture does not use AM or PM (likely all standard date formats use 24 hour time)
	                //      null
	                AM: ["AM", "am", "AM"],
	                PM: ["PM", "pm", "PM"],
	                eras: [
	                // eras in reverse chronological order.
	                // name: the name of the era in this culture (e.g. A.D., C.E.)
	                // start: when the era starts in ticks (gregorian, gmt), null if it is the earliest supported era.
	                // offset: offset in years from gregorian calendar
	                { "name": "A.D.", "start": null, "offset": 0 }
	                ],
	                twoDigitYearMax: 2029,
	                patterns: {
	                    // short date pattern
	                    d: "M/d/yyyy",
	                    // long date pattern
	                    D: "dddd, MMMM dd, yyyy",
	                    // short time pattern
	                    t: "h:mm tt",
	                    // long time pattern
	                    T: "h:mm:ss tt",
	                    // long date, short time pattern
	                    f: "dddd, MMMM dd, yyyy h:mm tt",
	                    // long date, long time pattern
	                    F: "dddd, MMMM dd, yyyy h:mm:ss tt",
	                    // month/day pattern
	                    M: "MMMM dd",
	                    // month/year pattern
	                    Y: "yyyy MMMM",
	                    // S is a sortable format that does not vary by culture
	                    S: "yyyy\u0027-\u0027MM\u0027-\u0027dd\u0027T\u0027HH\u0027:\u0027mm\u0027:\u0027ss",
	                    // formatting of dates in MySQL DataBases
	                    ISO: "yyyy-MM-dd hh:mm:ss",
	                    ISO2: "yyyy-MM-dd HH:mm:ss",
	                    d1: "dd.MM.yyyy",
	                    d2: "dd-MM-yyyy",
	                    d3: "dd-MMMM-yyyy",
	                    d4: "dd-MM-yy",
	                    d5: "H:mm",
	                    d6: "HH:mm",
	                    d7: "HH:mm tt",
	                    d8: "dd/MMMM/yyyy",
	                    d9: "MMMM-dd",
	                    d10: "MM-dd",
	                    d11: "MM-dd-yyyy"
	                },
	                percentsymbol: "%",
	                currencysymbol: "$",
	                currencysymbolposition: "before",
	                decimalseparator: '.',
	                thousandsseparator: ',',
	                pagergotopagestring: "Go to page:",
	                pagershowrowsstring: "Show rows:",
	                pagerrangestring: " of ",
	                pagerpreviousbuttonstring: "previous",
	                pagernextbuttonstring: "next",
	                pagerfirstbuttonstring: "first",
	                pagerlastbuttonstring: "last",
	                groupsheaderstring: "Drag a column and drop it here to group by that column",
	                sortascendingstring: "Sort Ascending",
	                sortdescendingstring: "Sort Descending",
	                sortremovestring: "Remove Sort",
	                groupbystring: "Group By this column",
	                groupremovestring: "Remove from groups",
	                filterclearstring: "Clear",
	                filterstring: "Filter",
	                filtershowrowstring: "Show rows where:",
	                filterorconditionstring: "Or",
	                filterandconditionstring: "And",
	                filterselectallstring: "(Select All)",
	                filterchoosestring: "Please Choose:",
	                filterstringcomparisonoperators: ['empty', 'not empty', 'enthalten', 'enthalten(match case)',
	                   'does not contain', 'does not contain(match case)', 'starts with', 'starts with(match case)',
	                   'ends with', 'ends with(match case)', 'equal', 'equal(match case)', 'null', 'not null'],
	                filternumericcomparisonoperators: ['equal', 'not equal', 'less than', 'less than or equal', 'greater than', 'greater than or equal', 'null', 'not null'],
	                filterdatecomparisonoperators: ['equal', 'not equal', 'less than', 'less than or equal', 'greater than', 'greater than or equal', 'null', 'not null'],
	                filterbooleancomparisonoperators: ['equal', 'not equal'],
	                validationstring: "Entered value is not valid",
	                emptydatastring: "No data to display",
	                filterselectstring: "Select Filter",
	                loadtext: "Loading...",
	                clearstring: "Clear",
	                todaystring: "Today"
	            }
	            break;
	    }
	    return localization;
	}
}